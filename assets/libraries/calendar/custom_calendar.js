const getfechassalidas = async () => {
    const response = await fetch(`${PATH_API}api/agenda-salida-unidades`);
    const response_parsed = await response.json();
    let data_return = [];
    response_parsed.map(({ titulo, datetime_fin, datetime_inicio }) => {
        return data_return.push({
            title: titulo,
            start: datetime_inicio,
            end: datetime_fin
        })
    })

    return data_return;
}

document.addEventListener('DOMContentLoaded', async function () {
    var calendarEl = document.getElementById('calendar');
    const fechas_from_api = await getfechassalidas();
    var fecha_inicial = moment().format('YYYY-MM-DD');


    var calendar = new FullCalendar.Calendar(calendarEl, {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        initialDate: fecha_inicial,
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectMirror: true,
        select: function (arg) {
            $("#modal_agregar_salida").modal();
            $("#title_modal").text('Agendar salida');
            var selected_date = moment(arg.start).format('YYYY-MM-DD');
            document.getElementById("date_inicio").value = selected_date;
            $("#msg_vidation").removeClass('alert alert-danger').text('');
            $("#btn-modal-agregar").attr('disabled', false);


            if (
                moment(selected_date).subtract({ days: 2 }).format('YYYY-MM-DD') > moment().format('YYYY-MM-DD')
            ) {
                $("#btn-modal-agregar").attr('disabled', true);
                $("#msg_vidation").addClass('alert alert-danger').text('Solo se puede agendar con 48 horas de anticipacion');
            }


            if (moment().format('YYYY-MM-DD') > selected_date) {
                $("#btn-modal-agregar").attr('disabled', true);
                $("#msg_vidation").addClass('alert alert-danger').text('No sepuede agendar en fechas anteriores');
            }

            // console.log(arg)
            // utils.displayWarningDialog("No se puede agendar en ", "warning", function (data) { })
            // if (title) {
            //     calendar.addEvent({
            //         title: title,
            //         start: arg.start,
            //         end: arg.end,
            //         allDay: arg.allDay
            //     })
            // }
            // calendar.unselect()
        },
        eventClick: function (arg) {
            var selected_date = moment(arg.event.start).format('YYYY-MM-DD HH:mm');
            Swal.fire({
                title: 'Alerta',
                text: "¿Estas seguro de borrar evento? " + selected_date,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar!'
            }).then((result) => {
                if (result.value) {
                    ajax.post('api/agenda-salida-unidades/borrarsalida', {
                        'datetime_inicio': selected_date
                    }, function (response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }

                        utils.displayWarningDialog("Se eliminó evento", "success", function (data) { arg.event.remove() })
                    })
                }
            })
        },
        editable: true,
        dayMaxEvents: true, // allow "more" link when too many events
        events: fechas_from_api
    });

    calendar.render();
});

const agendarsalida = () => {
    const hora_ingresada = document.getElementById('time_inicio').value;
    const fecha_ingresada = document.getElementById('date_inicio').value;

    if (hora_ingresada == '') {
        return toastr.error("Indica una hora");
    }

    if (parseInt(hora_ingresada.substr(3, 3)) > 0) {
        return toastr.error("La hora de salida debe ser cerrada");
    }

    var selected_date = moment(fecha_ingresada).format('YYYY-MM-DD');
    if (selected_date == moment().format('YYYY-MM-DD')) {
        const actual_time = moment(new Date()).format('HH');
        if (parseFloat(actual_time) >= parseFloat(document.getElementById('time_inicio').value)) {
            return toastr.error("La hora ingresada no es valida");
        }
    }

    // return console.log("pasa todo")
    ajax.post('api/agenda-salida-unidades', {
        'datetime_inicio': `${document.getElementById('date_inicio').value} ${parseFloat(document.getElementById('time_inicio').value)}`,
        titulo: document.getElementById('titulo').value,
        id_venta_auto: document.getElementById('id_venta_auto').value
    }, function (response, headers) {
        if (headers.status == 400) {
            return ajax.showValidations(headers);
        }

        utils.displayWarningDialog("Se agendo salida", "success", function (data) {
            return window.location.href = base_url + 'autos/salidas/agendasalidas/' + document.getElementById('id_venta_auto').value;
        })
    })
};