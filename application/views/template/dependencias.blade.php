
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/template/dist/js/scripts.js') ?> "></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/template/dist/assets/demo/datatables-demo.js') ?>"></script>
    <script>
        var site_url = '<?php echo site_url(); ?>';
        var base_url = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url('assets/libraries/sweetalert2/dist/sweetalert2.min.js') ?>"></script>
    
    <script src="<?php echo base_url('js/custom/utils.js') ?>"></script>