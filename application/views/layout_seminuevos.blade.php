<!DOCTYPE html>
<html lang="en">
@include('template/header')
<body class="sb-nav-fixed">
    @include('template/navbar')
    <div id="layoutSidenav">
        @include('template/sidebar_seminuevos')
        <div id="layoutSidenav_content">
            <main>
                @yield('contenido')
            </main>
            @include('template/footer')
        </div>
    </div>
    @include('template/dependencias')
    @yield('scripts')
</body>

</html>