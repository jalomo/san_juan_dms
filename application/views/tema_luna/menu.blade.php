<script>
    var active_item = null;
</script>
<nav>
    <ul class="nav luna-nav">
        <li class="nav-category">
            DMS
        </li>

        @if (!empty($this->session->all_userdata()['menu']))
        @foreach ($this->session->all_userdata()['menu'] as $key_menu => $item_menu)
            <li id="menu_{{$key_menu}}" class="nav-category nav-cat-1 <?php echo $this->uri->segment(1)  == strtolower($item_menu->modulo) ? 'active' : '';?>" style="padding-left: 0px;">
            <a href="#level_one_{{$key_menu}}" data-toggle="collapse" aria-expanded="false">
                <i class="{{ $item_menu->icono != '' ? $item_menu->icono :'fas fa-user-tie' }}"></i>&nbsp {{ $item_menu->modulo }}
            </a>
        </li>
        @if (count($item_menu->contenido_seccion) > 0)
        <div class="nav nav-second collapse nav-cat-2 <?php echo $this->uri->segment(1)  == strtolower($item_menu->modulo) ? 'show' : '';?>" id="level_one_{{$key_menu}}">
            @foreach ($item_menu->contenido_seccion as $key_seccion => $secciones)
            <li class="nav-category">
                {{ $secciones->seccion_nombre }}
            </li>
            @if (count($secciones->contenido_submenus)>0 )
            @foreach ($secciones->contenido_submenus as $key_submenus => $submenu)
            @if ($submenu->visible == 0)
                <?php continue; ?>
            @endif
            <li id="submenu_{{$key_submenus}}">
                <a href="#submenu_{{$key_submenus.$submenu->submenu_id}}" data-toggle="collapse" aria-expanded="false">
                    {{ $submenu->nombre_submenu }}<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                @if (count($submenu->vistas) > 0)
                <ul id="submenu_{{$key_submenus.$submenu->submenu_id}}" class="nav nav-second collapse nav-cat-3">
                    @foreach ($submenu->vistas as $key_vista => $vista)
                        <?php  $activo = false;  ?>
                        @if ($this->uri->segment(2) == $vista->controlador && $this->uri->segment(1) == $vista->modulo && $this->uri->segment(3) == $vista->link && !empty($this->uri->segment(3)))
                            <?php  $activo = true;  ?>
                            <script> 
                                active_item = '<?php echo "submenu_".$key_submenus.$submenu->submenu_id; ?>'; 
                            </script>
                            @elseif($this->uri->segment(2) == $vista->controlador && $this->uri->segment(1) == $vista->modulo && $vista->link == '#')
                            <?php  $activo = true;  ?>
                            <script>
                                active_item = '<?php echo "submenu_".$key_submenus.$submenu->submenu_id; ?>'; 
                            </script>
                        @endif

                        <li class="{{ $activo ? 'active': '' }}" id="vista_{{ $key_vista}}_{{$vista->vista_id}}"  >
                            <?php if (substr($vista->link,0,8) == "https://"): ?>
                                <a href="{{ $vista->link; }}" target="_blank">{{ $vista->nombre_vista }}</a>
                            <?php else: ?>
                                <a href="{{ base_url($vista->modulo.'/'.$vista->controlador.'/'.$vista->link); }}">{{ $vista->nombre_vista }}</a>
                            <?php endif ?>
                        </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
            @endif
            @endforeach
        </div>
        @endif
        @endforeach
        @else
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{ site_url('inicio') }}">
                <i class="fas fa-user-tie"></i> BIENVENIDO
            </a>
        </li>
        @endif
    </ul>
    <br><br>
</nav>
<script>
    let idmenu = $("#"+active_item).parent().parent().addClass('show');
    $("#"+active_item).addClass("show");
</script>