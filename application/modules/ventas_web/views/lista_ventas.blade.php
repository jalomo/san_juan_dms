@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .fa {
            font-size: 20px;
            color: #000;
            margin-right: 5px;
        }

        .ui-datepicker {
            position: relative;
            z-index: 10000 !important;
        }

        .clockpicker-popover {
            z-index: 10000 !important;
        }

        .observaciones {
            background-color: #f2dede !important;
        }

        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }
        td a i{
            font-size: 25px !important;
        }

    </style>
    <input type="hidden" name="vista" id="vista" value="{{ $vista }}">
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        @if ($vista == 'prospectos' || $vista == 'asesores_web')
            <a href="{{ base_url('ventas_web/agregar_venta/0/' . $vista) }}" id="guardar"
                class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agregar venta</strong></a>
        @endif
        <div class="row">
            <div class='col-sm-3'>
                <label for="">Selecciona la fecha</label>
                <input id="fecha" name="fecha" type='date' class="form-control" value="{{ date('Y-m-d') }}" />
                <span class="error_fecha"></span>
            </div>
            @if ($vista == 'prospectos')
                <div class="col-sm-3">
                    <label>Filtrar por</label>
                    {{ $drop_opciones }}
                </div>
            @endif
            <div class="col-sm-2">
                <br>
                <button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
            </div>
        </div>
        <br>
        @if ($vista == 'prospectos')
            <div class="row pull-right">
                <div class="col-sm-12">
                    <div class="alert my_alert-danger" role="alert">
                        Información pendiente por corregir
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl-ventas" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Nombre</th>
                                <th>Teléfono casa</th>
                                <th>Teléfono celular</th>
                                <th>Email</th>
                                <th>Unidad</th>
                                @if ($vista == 'telemarketing')
                                    <th>Asesor</th>
                                    <th>Cita generada</th>
                                @elseif ($vista=='prospectos')
                                    <th>Usuario telemarketing</th>
                                @else
                                    <th>Asesor</th>
                                    <th>Estatus cita</th>
                                    <th>fecha cita</th>
                                    <th>Vehículo vendido</th>
                                    <th>comentario venta</th>
                                @endif
                                <th>Origen</th>
                                <th>Fecha de creación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $clase_erronea = ''; ?>
                            @foreach ($ventas as $v => $venta)
                                @if ($venta->informacion_erronea==1 && $vista == 'prospectos')
                                    <?php $clase_erronea = 'observaciones'; ?>
                                @endif
                                <tr class="{{ $clase_erronea }}">
                                    <td data-info="{{$venta->informacion_erronea}}">{{ $venta->id }}</td>
                                    <td>{{ $venta->nombre . ' ' . $venta->apellido_paterno . ' ' . $venta->apellido_materno }}
                                    </td>
                                    <td>{{ $venta->telefono_secundario }}</td>
                                    <td>{{ $venta->telefono }}</td>
                                    <td>{{ $venta->correo_electronico }}</td>
                                    <td>{{ $venta->unidad }}</td>
                                    @if ($vista == 'telemarketing')
                                        <td>{{ $venta->nombre_usuario_asesor . ' ' . $venta->apellido_paterno_asesor . ' ' . $venta->apellido_materno_asesor }}
                                        </td>
                                        <td>{{ $venta->id_cita != '' ? 'Si' : 'No' }}</td>
                                    @elseif ($vista=='prospectos')
                                        <td>{{ $venta->nombre_usuario_telemarketing . ' ' . $venta->apellido_paterno_telemarketing . ' ' . $venta->apellido_materno_telemarketing }}
                                        </td>
                                    @else
                                        <td>{{ $venta->nombre_usuario_asesor . ' ' . $venta->apellido_paterno_asesor . ' ' . $venta->apellido_materno_asesor }}
                                        </td>
                                        @if ($venta->id_status == 1 || $venta->id_status == '')
                                            <td>Sin estatus</td>
                                        @elseif($venta->id_status==2)
                                            <td>Cliente llegó</td>
                                        @else
                                            <td>Cliente no llegó</td>
                                        @endif
                                        <td>{{ $venta->fecha_cita }}</td>
                                        @if ($venta->vendido == 0)
                                            <td>Sin estatus</td>

                                        @elseif($venta->vendido==1)
                                            <td>Vendido</td>
                                        @else
                                            <td>NO vendido</td>
                                        @endif
                                        <td>{{ $venta->comentario_venta }}</td>

                                    @endif
                                    <td>{{ $venta->origen }}</td>
                                    <td>{{ $venta->created_at }}</td>
                                    <td>
                                        @if ($vista == 'telemarketing')
                                            @if ($venta->id_cita == '')
                                                <a href="" data-id="{{ $venta->id }}" class="js_comentarios"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Comentarios" data-fecha="{{ $venta->fecha_cita_programada }}"
                                                    data-cliente="{{ $venta->nombre . ' ' . $venta->apellido_paterno . ' ' . $venta->apellido_materno }}"
                                                    data-no_contactar="{{ $venta->no_contactar }}">
                                                    <i class="pe pe-7s-comment"></i>
                                                </a>
                                            @endif
                                            @if ($venta->id_cita == '')
                                                <a href="" data-id="{{ $venta->id }}" class=" js_info_erronea"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Información errónea">
                                                    <i class="pe pe-7s-left-arrow"></i>
                                                </a>
                                            @endif
                                            <a href="" data-id="{{ $venta->id }}" class="js_historial" aria-hidden="true"
                                                data-toggle="tooltip" data-placement="top" title="Historial comentarios"
                                                data-tipo_comentario="1">
                                                <i class="pe pe-7s-info"></i>
                                            </a>
                                        @elseif ($vista=='prospectos')
                                            <a href="{{ site_url('ventas_web/agregar_venta/' . $venta->id) }}" name="Editar"
                                                id="Editar" class="" title="Editar">
                                                <i class="pe pe-7s-note"></i>
                                            </a>
                                            @if ($venta->informacion_erronea==1 && $vista == 'prospectos')
                                                <a href="" data-id="{{ $venta->id }}" class="js_historial"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Historial comentarios" data-tipo_comentario="2">
                                                    <i class="pe pe-7s-info"></i>
                                                </a>

                                                <a href="" data-id="{{ $venta->id }}" class=" js_reenviar"
                                                    aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                    title="Enviar información">
                                                    <i class="pe pe-7s-right-arrow"></i>
                                                </a>
                                            @endif

                                        @else
                                            <a href="" data-id="{{ $venta->id }}" class=" js_comentarios_asesor"
                                                aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                                title="Comentarios">
                                                <i class="pe pe-7s-comment"></i>
                                            </a>

                                            <a href="" data-id="{{ $venta->id }}" class="js_historial" aria-hidden="true"
                                                data-toggle="tooltip" data-placement="top" title="Historial comentarios"
                                                data-tipo_comentario="3">
                                                <i class="pe pe-7s-info"></i>
                                            </a>

                                            @if ($venta->vendido == 0)
                                                <a href="" data-id="{{ $venta->id }}" class=" js_vendido" aria-hidden="true"
                                                    data-toggle="tooltip" data-placement="top" title="Estatus venta">
                                                    <i class="fa fa-car"></i>
                                                </a>
                                            @endif
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
		inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        $("body").on("click", '.js_comentarios', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            no_contactar = $(this).data('no_contactar');
            cliente = $(this).data('cliente');
            var url = site_url + "/ventas_web/comentarios/0";
            customModal(url, {
                    "id_venta": id_venta,
                    'fecha': $("#fecha").val(),
                    "cliente": cliente,
                    "no_contactar": no_contactar
                }, "GET", "lg", ingresarComentario, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });
        $("body").on("click", '.js_comentarios_asesor', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            tipo_comentario = 3;
            var url = site_url + "/ventas_web/comentarios_asesor/0";
            customModal(url, {
                    "id_venta": id_venta
                }, "GET", "md", saveComentario, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });
        //Actualizar si se vendió o no el auto
        $("body").on("click", '.js_vendido', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            var url = site_url + "/ventas_web/estatus_venta/0";
            customModal(url, {
                "id_venta": id_venta
            }, "GET", "md", ingresarEstatusVenta, "", "Guardar", "Cancelar", "Estatus venta", "modal1");
        });
        //Actualizar si se vendió o no el auto
        $("body").on("click", '.js_info_erronea', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            var url = site_url + "/ventas_web/informacion_erronea/0";
            customModal(url, {
                    "id_venta": id_venta
                }, "GET", "md", ConfirmarInfoErronea, "", "Guardar", "Cancelar",
                "Comentario de información errónea", "modalComentario");
        });
        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            var tipo_comentario = $(this).data('tipo_comentario');
            var url = site_url + "/ventas_web/historial_comentarios/";
            customModal(url, {
                "id_venta": id_venta,
                "tipo_comentario": tipo_comentario
            }, "POST", "md", "", "", "", "Cerrar", "Historial de comentarios", "modalHistorialComentarios");
        });
        $("body").on("click", '.js_reenviar', function(e) {
            e.preventDefault();
            id_venta = $(this).data('id');
            ConfirmCustom("¿Está seguro enviar la información?", callbackEnviarInformacion, "", "Confirmar",
                "Cancelar");
        });
        function ingresarEstatusVenta() {
            ConfirmCustom("¿Está seguro de guardar?", callbackGuardarEstatusVenta, "", "Confirmar", "Cancelar");
        }

        function ConfirmarInfoErronea() {
            tipo_comentario = 2;
            ConfirmCustom("¿Está seguro de regresar el registro como información errónea para su correción?",
			saveComentarioAndUpdateInfoErronea, "", "Confirmar", "Cancelar");
        }

        function ingresarComentario() {
            const hora = '12:00:00';
            if ($("#cronoHora").val() != '') {
                const hora = $("#cronoHora").val();
            }
            const data = {
                id_venta_web: $("#id_venta").val(),
                comentario: $("#comentario").val(),
                id_usuario: "{{ $this->session->userdata('id') }}",
                fecha_notificacion: $("#cronoFecha").val() + ' ' + hora,
                id_tipo_comentario: 1
            }
            ajax.post('api/telemarketing/historial-comentarios-save', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Comentario guardado con éxito";
                    utils.displayWarningDialog("Comentario guardado con éxito", "success", function(data) {
                        $(".modalComentario").modal('hide')
                    })
                })
        }

        function callbackGuardarEstatusVenta() {
			const data = {
				vendido : $('input:radio[name=vendido]:checked').val(),
				comentario_venta :  $("#comentario").val(),
				fecha_comentario : "{{date('Y-m-d H:i:s')}}"
			}
			ajax.put('api/telemarketing/ventas-web/update-comentario-venta/'+$("#id_venta").val(),data,
				function(response, headers) {
					if (headers.status == 400) {
						return ajax.showValidations(headers);
					}
					var titulo = (headers.status != 200) ? headers.message : "Información guardada con éxito";
					utils.displayWarningDialog("Información guardada con éxito", "success", function(data) {
						$(".modal1 ").modal("hide")
						buscarInformacion();
					})
				})
        }

        function saveComentario() {
            const data = {
                id_venta_web: $("#id_venta").val(),
                comentario: $("#comentario").val(),
                id_usuario: "{{ $this->session->userdata('id') }}",
                id_tipo_comentario: tipo_comentario
            }
            ajax.post('api/telemarketing/historial-comentarios-save', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success", function(data) {
                        $(".modalComentario").modal('hide')
                    })
                })
        }
		function saveComentarioAndUpdateInfoErronea() {
            const data = {
                id_venta_web: $("#id_venta").val(),
                comentario: $("#comentario").val(),
                id_usuario: "{{ $this->session->userdata('id') }}",
                id_tipo_comentario: tipo_comentario
            }
            ajax.post('api/telemarketing/historial-comentarios-info-erronea', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información guardada con éxito";
                    utils.displayWarningDialog("Información guardada con éxito", "success", function(data) {
						window.location.href = base_url + "ventas_web/telemarketing";
                    })
                })
        }

        function callbackEnviarInformacion() {
            const data = {
                id_venta_web: id_venta,
                informacion_erronea: 0
            }
			console.log(data)
            ajax.post('api/telemarketing/ventas-web/update-info-erronea', data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message : "Información enviada con éxito";
                    utils.displayWarningDialog("Información enviada con éxito", "success", function(data) {
                        window.location.href = base_url + "ventas_web/listado";
                    })
			})
        }
        $("body").on('click', '#buscar', function() {
            buscarInformacion();
        });
        $("body").on('click', '#enviar-correo', function() {
            var url = site_url + "/proactivo/correo_cp";
            ajaxJson(url, {}, "POST", "async", function(result) {
                ExitoCustom("Correo enviado correctamente");
            });
        });

        function buscarInformacion() {
            var url = site_url + "/ventas_web/lista_ventas/";
            ajaxLoad(url, {
                "fecha": $("#fecha").val(),
                "informacion_erronea": $("#informacion_erronea").val(),
                "vista": $("#vista").val()
            }, "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }

        function inicializar_tabla_local() {
            $('#tbl-ventas').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }

    </script>
@endsection
