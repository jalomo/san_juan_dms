@layout('tema_luna/layout')
<style>
    .gris {
        color: #000;
    }

    a i{
        font-size: 35px !important;
        margin-right: 5px;
    }

    a.verde i {
        color: #1F7D31 !important;
        background-color: transparent
    }

    a.negro i {
        color: #000;
        background-color: transparent
    }

    .desactivado {
        background-color: #7576F6;
        color: #FFF;
        font-weight: bold;
    }

    .activo {
        background-color: white;
        color: #000;
        font-weight: bold;
    }

    .content-wrapper {
        margin-left: 0px !important;
    }

    td a i {
        font-size: 25px !important;
    }

</style>
@section('contenido')

    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row form-group">
            <div class='col-sm-3'>
                <label for="">Selecciona la fecha</label>
                <input id="fecha" name="fecha" type='date' class="form-control" value="{{ $fecha }}" />
                <span class="error_fecha"></span>
            </div>
            <div class="col-sm-2">
                <br>
                <button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
            </div>
        </div>
        <br>
        <div id="div_tabla">
            <div class="row">
                <div class="col-sm-6">
                    {{ $tabla1 }}
                </div>

                <div class="col-sm-6">
                    {{ $tabla2 }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        var site_url = "{{ site_url() }}";
        var id = '';
        var status = '';
        var aPos = '';
        var fecha_cita = $("#fecha").val();
        var fecha_comparar = "{{ date('Y-m-d') }}";
        var aux1 = $(window).height();
        var aux2 = $(document).height();
        var inicio = 100;
        var heightPage = $(document).height();
        $("#buscar").on('click', buscar);
        if (fecha_cita === fecha_comparar) {

            $("body").on('click', '.js_cambiar_status', function(e) {
                e.preventDefault();
                var id_status_cita = $(this).data('idstatus');
                status = $(this).data('status')
                e.preventDefault();
                var fecha_actual = "{{ date('Y-m-d') }}";
                var fecha = $("#fecha").val();
                if (fecha_actual == fecha) {
                    id = $(this).data('id')
                    status = $(this).data('status')
                    aPos = $(this);
                    ConfirmCustom("¿Está seguro de cambiar el estatus a la cita?", callbackCambiarStatus, "",
                        "Confirmar", "Cancelar");

                } else {
                    ErrorCustom("Solamente puedes hacer check-in el día en que se generó la cita");
                }

            });
        } else {
            $("body").on('click', '.js_cambiar_status', function(e) {
                e.preventDefault();
                ErrorCustom("Sólo puedes hacer check-in el día actual de la cita.")
            });
        }

        function buscar() {
            var url = site_url + "/ventas_web/tabla_horarios_asesores";
            ajaxLoad(url, {
                "fecha": $("#fecha").val()
            }, "div_tabla", "POST", function() {});
        }

        function callbackCambiarStatus() {
            const data = {
                id_cita: id,
                id_status: status
            }
            ajax.post('api/telemarketing/citas-ventas/cambiar-estatus-cita', data,
                function(response, headers) {
                    var titulo = (headers.status != 200) ? headers.message : "Estatus cambiado correctamente";
                    utils.displayWarningDialog("Estatus cambiado correctamente", "success", function(data) {
                        $(".elemento_" + id).removeClass('verde');
                        $(".elemento_" + id).addClass('negro');
                        $(aPos).addClass('verde');
                        $(aPos).removeClass('negro');
                    })
                })
        }

        setInterval(buscar, 150000);

    </script>
@endsection
