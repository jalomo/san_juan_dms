@layout('tema_luna/layout')
@section('contenido')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm" action="ventas_web/reporte_to_excel" method="POST">
            <div class="row">
                <div class="col-sm-4">
                    <label>Unidad</label>
                    {{ $drop_unidad }}
                </div>
                <div class="col-sm-4">
                    <label>Asesor</label>
                    {{ $drop_asesores }}
                </div>
                <div class="col-sm-4">
                    <label>Asesor telemarketing</label>
                    {{ $drop_telemarketing }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label>Origen</label>
                    {{ $drop_origen }}
                </div>
                <div class='col-sm-2'>
					<label for="">Fecha inicio</label>
					<input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control">
                </div>
                <div class='col-sm-2'>
					<label for="">Fecha Fin</label>
					<input type="date" name="fecha_fin" id="fecha_fin" class="form-control">
                </div>
            </div>
            <div class="row text-right">
                <div class='col-sm-12'>
                    <button type="button" class="btn btn-info" id="buscar">Buscar información</button>
                    <!--<button type="button" class="btn btn-success" id="excel">Exportar excel</button>-->
                </div>
            </div>
        </form>
        <br>

        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl-ventas" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>Teléfono</th>
                                <th>Teléfono secundario</th>
                                <th>Email</th>
                                <th>Fecha creción venta</th>
                                <th>Unidad</th>
                                <th>Asesor</th>
                                <th>Asesor telemarketing</th>
                                <th>Origen</th>
                                <th>Fecha comentario venta</th>
                                <th>Comentario venta</th>
                                <th>Estatus cita</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ventas as $v => $venta)
                                <tr>
                                    <td>{{ $venta->nombre.' '.$venta->apellido_paterno.' '.$venta->apellido_materno }}</td>
                                    <td>{{ $venta->telefono }}</td>
                                    <td>{{ $venta->telefono_secundario }}</td>
                                    <td>{{ $venta->correo_electronico }}</td>
                                    <td>{{ $venta->created_at }}</td>
                                    <td>{{ $venta->unidad }}</td>
									<td>{{ $venta->nombre_usuario_asesor.' '.$venta->apellido_paterno_asesor.' '.$venta->apellido_materno_asesor }}</td>
									<td>{{ $venta->nombre_usuario_telemarketing.' '.$venta->apellido_paterno_telemarketing.' '.$venta->apellido_materno_telemarketing }}</td>
                                    <td>{{ $venta->origen }}</td>
                                    <td>{{ $venta->fecha_comentario }}</td>
                                    <td>{{ $venta->comentario_venta }}</td>
                                    <td>
										@if($venta->id_status==1)
											Llegó cliente
										@elseif($venta->id_status==1)
											NO llegó cliente
										@else
											sin estatus
										@endif
									</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
	<script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
	<script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        $(".busqueda").select2();
        $("#buscar").on('click', buscarInformacion);
        $("#excel").on('click', exportarExcel);

        function inicializar_tabla_local() {
            $('#tbl-ventas').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
            $(".tr_principal").addClass('verde');
        }

        function buscarInformacion() {
            var url = site_url + "/ventas_web/buscar_info_reportes";
            ajaxLoad(url, $("#frm").serialize(), "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }

        function exportarExcel() {
            $("#frm").submit();
        }

    </script>
@endsection
