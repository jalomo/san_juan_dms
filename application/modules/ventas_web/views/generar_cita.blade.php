@layout('tema_luna/layout')
@section('contenido')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id_venta" id="id_venta" value="{{ $id_venta }}">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label>Nombre</label>
                    {{ $input_nombre }}
                    <div class="error error_nombre"></div>
                </div>
                <div class="col-sm-4">
                    <label>Apellido paterno</label>
                    {{ $input_ap }}
                    <div class="error error_ap"></div>
                </div>
                <div class="col-sm-4">
                    <label>Apellido materno</label>
                    {{ $input_am }}
                    <div class="error error_am"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Teléfono de casa/oficina</label>
                    {{ $input_telefono_casa }}
                    <div class="error error_casa"></div>
                </div>
                <div class="col-sm-4">
                    <label>Teléfono celular</label>
                    {{ $input_telefono_celular }}
                    <div class="error error_telefono_celular"></div>
                </div>
                <div class="col-sm-4">
                    <label>Email</label>
                    {{ $input_email }}
                    <div class="error error_email"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Unidad de interes</label>
                    {{ $drop_unidad }}
                    <div class="error error_id_unidad"></div>
                </div>
                <div class="col-sm-4">
                    <label>Asesor</label>
                    {{ $drop_asesor }}
                </div>
                <div class="col-sm-4">
                    <label>Fecha cita</label>
                    {{ $drop_fecha }}
                    <div class="error error_fecha"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Horario</label>
                    {{ $drop_horario }}
                    <div class="error error_horario"></div>
                </div>

            </div>
        </form>
        <button id="guardar" class="btn btn-sm btn-primary pull-right m-t-n-xs"
            type="button"><strong>Guardar</strong></button>
        <br><br>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $(".busqueda").select2();
        $("#id_asesor option:not(:selected)").attr("disabled", true);
        var site_url = "{{ site_url() }}";
        $("#guardar").on('click', function() {
            ConfirmCustom("¿Está seguro de agendar la cita?", callbackGuardar, "", "Confirmar", "Cancelar");
        });
        const existeCita = () => {
            const arr_fecha = $("#fecha_cita").val().split("/");
            const fecha = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
            const data = {
                fecha_cita: fecha,
                horario: $("#horario").val(),
                id_cita: $("#id").val(),
            }
            ajax.post('api/telemarketing/citas-ventas/existe-cita', data,
                function(response, headers) {
                    if (response.length > 0) {
                        return true;
                    }
                    return false;
                })
        }

        function callbackGuardar() {
            if (existeCita()) {
                utils.displayWarningDialog("El horario ya fue ocupado", 'warning', function(result) {});
            } else {
                const arr_fecha = $("#fecha_cita").val().split("/");
                const fecha = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
                const data = {
                    id_venta: $("#id_venta").val(),
                    id_asesor: $("#id_asesor").val(),
                    id_status: 1,
                    id_usuario_creo: "{{ $this->session->userdata('id') }}",
                    fecha_cita: fecha,
                    horario: $("#horario").val()
                }
                ajax.post('api/telemarketing/citas-ventas/save-cita', data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message : "Cita generada con éxito";
                        utils.displayWarningDialog("Cita generada con éxito", "success", function(data) {
                            window.location.href = base_url + "ventas_web/telemarketing";
                        })
                    })
            }
        }
        $("#fecha_cita").on("change", function() {
			getTiempoCita()
        });
		$("#id_asesor").on("change", function() {
			getTiempoCita()
        });
		function getTiempoCita(){
			var url = site_url + "/ventas_web/getTiempo";
            fecha = $("#fecha_cita").val();
            id_asesor = $("#id_asesor").val();
            if (fecha != '' && id_asesor != '') {
                ajaxJson(url, {
                    "id_asesor": id_asesor,
                    "fecha": fecha
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.length != 0) {
                        $("#horario").empty();
                        $("#horario").removeAttr("disabled");
                        $("#horario").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#horario").append("<option value= '" + result[i] + "'>" + result[i]
                                .substring(0, 5) + "</option>");
                        });
                    } else {
                        $("#horario").empty();
                        $("#horario").append("<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {
                $("#horario").empty();
                $("#horario").append("<option value=''>-- Selecciona --</option>");
                $("#horario").attr("disabled", true);
            }
		}

    </script>
@endsection
