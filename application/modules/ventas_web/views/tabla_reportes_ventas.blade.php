<table id="tbl-ventas" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Cliente</th>
            <th>Teléfono</th>
            <th>Teléfono secundario</th>
            <th>Email</th>
            <th>Fecha creción venta</th>
            <th>Unidad</th>
            <th>Asesor</th>
            <th>Asesor telemarketing</th>
            <th>Origen</th>
            <th>Fecha comentario venta</th>
            <th>Comentario venta</th>
            <th>Estatus cita</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ventas as $v => $venta)
            <tr>
                <td>{{ $venta->nombre . ' ' . $venta->apellido_paterno . ' ' . $venta->apellido_materno }}</td>
                <td>{{ $venta->telefono }}</td>
                <td>{{ $venta->telefono_secundario }}</td>
                <td>{{ $venta->correo_electronico }}</td>
                <td>{{ $venta->created_at }}</td>
                <td>{{ $venta->unidad }}</td>
                <td>{{ $venta->nombre_usuario_asesor . ' ' . $venta->apellido_paterno_asesor . ' ' . $venta->apellido_materno_asesor }}
                </td>
                <td>{{ $venta->nombre_usuario_telemarketing . ' ' . $venta->apellido_paterno_telemarketing . ' ' . $venta->apellido_materno_telemarketing }}
                </td>
                <td>{{ $venta->origen }}</td>
                <td>{{ $venta->fecha_comentario }}</td>
                <td>{{ $venta->comentario_venta }}</td>
                <td>
                    @if ($venta->id_status == 1)
                        Llegó cliente
                    @elseif($venta->id_status==1)
                        NO llegó cliente
                    @else
                        sin estatus
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
