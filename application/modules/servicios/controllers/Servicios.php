<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Servicios extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  /*
  * funcion que inicia el controlador.
  * desarrollo: jalomo
  * esta funcion estara para  listar los catalogos
  */
  public function index()
  {
  	$data['titulo'] = "";
    $this->blade->render('servicios/index',$data);
  }

  public function combustible()
  {
    $data['titulo'] = "";
    $this->blade->render('servicios/combustible',$data);
  }
}
