<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Caja extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
    	$data['titulo'] = "Caja";
        $data['subtitulo'] = "Recibo de ingreso";

        $this->blade->render('cajas/reciboIngreso',$data);
    }

    public function construccion()
    {
        $this->blade->render('construccion');
    }
    
    /*public function deposito()
    {
    $data['titulo'] = "Caja/deposito";
    $this->blade->render('cajas/caja',$data);
    }

    public function com_bancaria()
    {
    $data['titulo'] = "Caja/Com. Bancaria";
    $this->blade->render('cajas/caja',$data);
    }

    public function mov_bancos()
    {
    $data['titulo'] = "Caja/Mov. Bancos";
    $this->blade->render('cajas/caja',$data);
    }*/
}