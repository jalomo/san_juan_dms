<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Entradas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function pagos()
    {

    	$data['titulo'] = "Caja";
        $data['subtitulo'] = "Entradas / Pagos";
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $estatus_cuentas = $this->curl->curlGet('api/estatus-cuentas');
        $api_cuentas = $this->curl->curlGet('api/catalogo-cuentas');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['estatus_cuentas'] = procesarResponseApiJsonToArray($estatus_cuentas);
        $data['catalogo_cuentas'] = procesarResponseApiJsonToArray($api_cuentas);
        $this->blade->render('entradas/pagos/listado',$data);
    }

    public function tipoPago($folio_id) {
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $usuariosAPI = $this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 5,
        ]);
        $cuentas_por_cobrar = $this->curl->curlGet('api/cuentas-por-cobrar/buscar-por-folio-id/' . $folio_id);


        $decode_forma_pago = procesarResponseApiJsonToArray($tipo_forma_pago);
        $decode_tipo_pago = procesarResponseApiJsonToArray($tipo_pago);
        $decode_plazo_credito = procesarResponseApiJsonToArray($plazo_credito);
        $decode_usuarios = procesarResponseApiJsonToArray($usuariosAPI);
        $decode_cuentas = procesarResponseApiJsonToArray($cuentas_por_cobrar);
        $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $folio_id);
        $parsed_folio = procesarResponseApiJsonToArray($folio_data);
        $data['folio'] = $parsed_folio;

        $totalVenta = $this->curl->curlGet('api/ventas/venta-by-folio/' . $parsed_folio[0]->id);
        $data['venta_total'] = procesarResponseApiJsonToArray($totalVenta);
        
        $data['plazo_credito'] = $decode_plazo_credito;
        $data['tipo_forma_pago'] = $decode_forma_pago;
        $data['tipo_pago'] = $decode_tipo_pago;
        $data['gestores'] = $decode_usuarios;
        $data['cxc'] = $decode_cuentas;
        $data['titulo'] = "Detalle";
        $this->blade->render('entradas/pagos/tipo_pago',$data);

    }

    public function detalle_pago($id)
    {
        $data['titulo'] = "Caja";
        $data['modulo'] = "Caja";
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $enganche_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=1&estatus_abono_id=3');
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=2&estatus_abono_id=1');
        $abonos_pagados_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&estatus_abono_id=3');
        
        $enganche = count(json_decode($enganche_api))>0 ? procesarResponseApiJsonToArray($enganche_api) : [];
        $abonos_pendientes = procesarResponseApiJsonToArray($abonos_pendientes_api);
        $abonos_pagados = procesarResponseApiJsonToArray($abonos_pagados_api);
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $tipo_abono = $this->curl->curlGet('api/tipo-abono');
        $cfdi = $this->curl->curlGet('api/cfdi');
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $tipo_asiento = $this->curl->curlGet('api/tipo-asiento');

        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
        $data['catalogo_caja'] = procesarResponseApiJsonToArray($catalogo_caja);
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        $data['cat_tipo_abono'] = procesarResponseApiJsonToArray($tipo_abono);
        $data['cat_tipo_asiento'] = procesarResponseApiJsonToArray($tipo_asiento);
        $data['cliente'] = current(procesarResponseApiJsonToArray($apiCliente));
        $data['total_abonado'] = $this->procesar_total_abonos($abonos_pagados);
        $data['data_cuentas']  = $data_cuentas;
        $data['enganche'] = $this->procesar_enganche($enganche);
        $data['saldo_actual']  = $data_cuentas->total - $data['total_abonado'];
        $data['abono_mensual'] = $abonos_pendientes ? current($abonos_pendientes)->total_abono : 0;

        $contar_abonos_pendientes = count($abonos_pendientes) - 1; // Se obtiene el total de abonos pendientes menos el que se va realizar
        $interes_abono = ((($data['abono_mensual']) * $data_cuentas->tasa_interes) / 100); // Obtenemos el interes que se aplica a los abonos
        $abono_sin_interes = ($data['abono_mensual'] - $interes_abono) * $contar_abonos_pendientes; // Restamos el interes a los abonos y lo multiplicamos por los abonos pendientes
        $data['liquidar_saldo'] = $abono_sin_interes +  $data['abono_mensual']; // Suma de los abonos pendientes sin el interes + el abono mensual actual.
        
        if ($data_cuentas->tipo_forma_pago_id == 2) {
            $fechaActual = date('Y/m/d');
            $verificaAbonosPendientesApi = $this->curl->curlGet('api/abonos-por-cobrar/verifica-abonos-pendientes?cuenta_por_cobrar_id='. $id . '&fecha_actual='.$fechaActual );
            procesarResponseApiJsonToArray($verificaAbonosPendientesApi);
            
            $data['subtitulo'] = "Entradas / Pago Credito";
            $this->blade->render('entradas/pagos/pago_credito',$data);
        } else {
            $data['subtitulo'] = "Entradas / Pago Contado";
            $unico_pago_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=3');
            $unico_pago = procesarResponseApiJsonToArray($unico_pago_api);
            $api_cuentas = $this->curl->curlGet('api/catalogo-cuentas');
            $data['catalogo_cuentas'] = procesarResponseApiJsonToArray($api_cuentas);
            $data['unico_pago'] = isset($unico_pago) && $unico_pago ? current($unico_pago) : null;
            $this->blade->render('entradas/pagos/pago_contado',$data);
        }
    }

    public function asientos_cuenta($id) {
        $data['titulo'] = "Caja";
        $data['modulo'] = "Caja";
        $data['subtitulo'] = "Asientos contabilidad";

        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        $api_cuentas = $this->curl->curlGet('api/catalogo-cuentas');
        $tipo_asiento = $this->curl->curlGet('api/tipo-asiento');
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_forma_pago_id=1');
        $abonos = procesarResponseApiJsonToArray($abonos_pendientes_api);
       
        $data['cat_tipo_asiento'] = procesarResponseApiJsonToArray($tipo_asiento);
        $data['catalogo_cuentas'] = procesarResponseApiJsonToArray($api_cuentas);
        $data['data_cuentas']  = $data_cuentas;
        $data['abonos']  = current($abonos);


        $this->blade->render('entradas/pagos/asientos_cuentas',$data);

    }

    public function imprime_estado_cuenta() {
        $cuenta_id = base64_decode($this->input->get('cuenta_id'));
        $dataCuentaApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $cuenta_id);
        $dataAbonosApi = $this->curl->curlGet('api/abonos-por-cobrar/listado-abonos-by-orden-entrada?orden_entrada_id='.$cuenta_id);
        $data_cuentas = procesarResponseApiJsonToArray($dataCuentaApi);

        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $data_cliente =  procesarResponseApiJsonToArray($apiCliente);

        $abonos_pagados_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$cuenta_id.'&estatus_abono_id=3');
        $abonos_pagados = procesarResponseApiJsonToArray($abonos_pagados_api);
        $total_abonado = $this->procesar_total_abonos($abonos_pagados);
        $data_cuentas->total_abonado = $total_abonado;
        $data_cuentas->saldo_actual = $data_cuentas->total - $total_abonado;
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$cuenta_id.'&tipo_abono_id=2&estatus_abono_id=1');
        $abonos_pendientes = procesarResponseApiJsonToArray($abonos_pendientes_api);
        $data_cuentas->abono_mensual = $abonos_pendientes ? current($abonos_pendientes)->total_abono : 0;

        $data = [
            'cuenta' => $data_cuentas,
            'cliente' => current($data_cliente),
            'abonos' => procesarResponseApiJsonToArray($dataAbonosApi)
        ];
        $view = $this->load->view('entradas/pagos/formato_estado_cuenta', $data,true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    
    }

    public function imprime_comprobante() {
        $abono_id = base64_decode($this->input->get('abono_id'));
        $abonoFromApi = $this->curl->curlGet('api/abonos-por-cobrar/' . $abono_id);
        $data_abono = procesarResponseApiJsonToArray($abonoFromApi);
        $dataCuentaApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $data_abono->cuenta_por_cobrar_id);
        $data_cuentas = procesarResponseApiJsonToArray($dataCuentaApi);

        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $data_cliente =  procesarResponseApiJsonToArray($apiCliente);

     

        $data = [
            'abono' => $data_abono,
            'cuenta' => $data_cuentas,
            'cliente' => current($data_cliente),
        ];
        $view = $this->load->view('entradas/pagos/formato_comprobante_pago', $data,true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    
    }

    private function procesar_enganche($enganches) {
        $total_enganche = 0;
        if(isset($enganches) && is_array($enganches)) {
            foreach($enganches as $enganche) { 
                if($enganche->estatus_abono_id != 3) {
                    $total_enganche += $enganche->total_pago;
                }
            } 
        }
        return $total_enganche;
    }

    private function verificar_abonos($abonos) {
        foreach($abonos as $abono) {

            if ($abono->estatus_abono_id != 3) {
                utils::pre($abono);

            }
        }
    }
    private function procesar_total_abonos($abonos) {
        $total_abono = 0;
        if(isset($abonos) && is_array($abonos)) {
            foreach($abonos as $abono) { 
                if($abono->estatus_abono_id == 3) {
                    $total_abono += $abono->total_pago;
                }
            } 
        }
        return $total_abono;
    }
   
}