@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="2">Datos del proveedor</th>
				</tr>
				<tr>
					<td width="120px">Proveedor: </td>
					<td><?php echo isset($proveedor->proveedor_nombre) ? $proveedor->proveedor_nombre : ''; ?>
					</td>
				</tr>
				<tr>
					<td width="120px">Núm. proveedor: </td>
					<td><?php echo isset($proveedor->proveedor_numero) ? $proveedor->proveedor_numero : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">RFC: </td>
					<td><?php echo isset($proveedor->proveedor_rfc) ? $proveedor->proveedor_rfc : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Razón social: </td>
					<td><?php echo isset($proveedor->razon_social) ? $proveedor->razon_social : ''; ?></td>
				</tr>
			</table>
		</div>
	</div>
	<h3>Detalle de la compra  [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</h3>
	<div class="row mt-4">
		<div class="col-md-5 mt-4">
			<table width="100%" class="table table-bordered" cellpadding="5">
                <tr>
					<th width="120px">Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
                </tr>
                <tr>
                    <th width="120px">Concepto: </th>
                    <td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
                </tr>
				<tr>
					<th width="120px">Total: </th>
					<td><?php echo isset($data_cuentas->total) ? '$'.(number_format($data_cuentas->total,2)) : ''; ?></td>
                </tr>
				<tr>
                    <th width="120px">Fecha compra: </th>
					<td><?php echo isset($data_cuentas->fecha) ? utils::aFecha($data_cuentas->fecha,true) : ''; ?></td>
                </tr>
			</table>
		</div>
		<div class="col-md-7">
			<div class="form-group">
				<label>Total a pagar:</label>
				<input type="text" readonly="readonly" name="importe" id="importe"
					value="<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>" class="form-control">
			</div>
			<div class="form-group">
				<label>Tipo de pago</label>
				<select class="form-control" id="tipo_pago" name="tipo_pago" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_tipo_pago))
                    @foreach ($cat_tipo_pago as $pago)
                        @if($unico_pago->tipo_pago_id == $pago->id)
                        <option value="{{$pago->id}}" selected="selected">[{{$pago->clave}}] {{$pago->nombre}}</option>
                        @else
                        <option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
                        @endif
                    @endforeach
					@endif
				</select>
			</div>
			<div class="form-group">
				<label>Tipo de CDFI</label>
				<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($cat_cfdi))
					@foreach ($cat_cfdi as $cfdi)
					@if($unico_pago->cfdi_id == $cfdi->id)
					<option value="{{$cfdi->id}}" selected="selected">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
					@else
					<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
					@endif
					@endforeach
					@endif
				</select>
			</div>
			<div class="form-group">
				<label>Caja</label>
				<select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
					<option value="">Selecionar ...</option>
					@if(!empty($catalogo_caja))
					@foreach ($catalogo_caja as $caja)
					@if($unico_pago->caja_id == $caja->id)
					<option value="{{$caja->id}}" selected="selected">{{$caja->nombre}} </option>
					@else
					<option value="{{$caja->id}}">{{$caja->nombre}}</option>
					@endif
					@endforeach
					@endif
				</select>
			</div>
			<?php if($data_cuentas->estatus_cuenta_id == 1) { ?>
			<div class="form-group">
				<label>Su pago:</label>
				<input type="number" name="pago" id="pago" value="<?php echo isset($unico_pago->pago) ? $unico_pago->pago : '';?>" class="form-control">
			</div>
			<div class="form-group">
				<label>Cambio:</label>
				<input type="number" name="cambio" id="cambio" value="<?php echo isset($unico_pago->cambio) ? $unico_pago->cambio : '';?>" class="form-control">
			</div>
			<?php } ?>
            <div class="col-md-12 mt-4 mb-4">
                <div class="text-right">
                    <a href="<?php echo site_url('caja/salidas/pagos');?>" type="button" class="btn btn-secondary col-md-2"><i class="fas fa-chevron-left"></i> Regresar</a>
                    <?php if($data_cuentas->estatus_cuenta_id == 1) { ?>
                        <button id="btn-pagar" type="button" class="btn btn-primary col-md-3"><i class="fas fa-cash-register"></i> Pagar</button>
                    <?php } else { ?>
                        <button id="btn-imprimir_poliza" onclick="imprimir_estado_cuenta(this)" data-cuenta_por_pagar_id="<?php echo $data_cuentas->id;?>" type="button" class="btn btn-primary col-md-3"><i class="fas fa-file-pdf"></i> Estado de cuenta</button>
                    <?php } ?>
                </div>
            </div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var d = new Date();
    var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
	let orden_entrada_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let abono_id = "<?php echo isset($unico_pago->id) ? $unico_pago->id : ''; ?>";

    $("#btn-pagar").on('click', function() {
		$.isLoading({
		text: "Realizando pago ...."
		});
        let dataForm = {
            cuenta_por_pagar_id: orden_entrada_id,
            total_pago: document.getElementById("importe").value,
            tipo_pago_id: document.getElementById("tipo_pago").value,
			tipo_abono_id: 3, //Una sola exhibición
			cfdi_id: document.getElementById("cfdi_id").value,
			caja_id: document.getElementById("caja_id").value,
            fecha_pago: strDate,
			estatus_abono_id:3 //Pagado
        }

        let pago = $("#pago").val();
        if (!parseFloat(pago) && pago < 1) {
			$.isLoading( "hide" );
			toastr.error("Falta indicar el pago");
            return false;
        } 
	
        ajax.put(`api/abonos-por-pagar/${abono_id}`, dataForm, function (response, headers) {
            if (headers.status == 200 || headers.status == 201) {
                ajax.put(`api/cuentas-por-pagar/${orden_entrada_id}`, { estatus_cuenta_id: 2 }, function (response, header) {
                    if (header.status == 400) {
						$.isLoading( "hide" );
                        return ajax.showValidations(header);
					}
					$.isLoading( "hide" );
                    utils.displayWarningDialog(header.message, "success", function (data) {
                        window.location.reload();
                    })
                })
            } else {
				$.isLoading( "hide" );
                return ajax.showValidations(headers);
            }
            
        })
    });

    $("#pago").on('blur', function () {
        let pago = $("#pago").val();
	    let importe = $("#importe").val();
		if (parseFloat(pago) < parseFloat(importe)) {
            toastr.error("El pago es menor al total a pagar")
            $("#btn-pagar").attr('disabled', true);
            $("#pago").val(0);
		} else {
            let cambio = parseFloat(pago) - parseFloat(importe);
            $("#cambio").val(cambio.toFixed(2));
            $("#btn-pagar").attr('disabled', false);
		}

	});
	function  imprimir_comprobante(_this) {
		abono_id = $(_this).data('abono_id');
		window.location.href = PATH + '/caja/salidas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
	}

	function  imprimir_estado_cuenta(_this) {
		cuenta_id = $(_this).data('cuenta_por_pagar_id');
		window.location.href = PATH + '/caja/salidas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
	}

</script>
@endsection
