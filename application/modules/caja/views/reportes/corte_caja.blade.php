@layout('tema_luna/layout')
@section('contenido')
<style>
    .form-control_ {
        background-color: #fff !important;
    }
</style>
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="col-md-12">
        <?php echo '<h4> Corte de caja al día : ' . utils::aFecha(date('Y-m-d')) . '</h4>'; ?>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Fecha corte:</label>
                    <input type="text" id="fecha_transaccion" class="form-control" value="<?php echo $fecha_transaccion; ?>" />
                </div>
                <div class="form-group">
                    <label>Número de caja:</label>
                    <select name="caja_id" onchange="window.location.href = PATH + '/caja/reportes/nuevo_corte?caja_id=' + ($(this).val())"  id="caja_id" class="form-control">
                        <option value="">Selecionar ...</option>
                        @if(!empty($cat_cajas))
                        @foreach ($cat_cajas as $caja)
                        @if($caja_id == $caja->id)
                        <option value="{{$caja->id}}" selected="selected">{{$caja->nombre}}</option>
                        @else 
                        <option value="{{$caja->id}}" >{{$caja->nombre}}</option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label>Usuario:</label>
                    <input type="text" id="usuario" class="form-control" value="<?php echo $this->session->userdata('nombre'); ?>" />
                </div>
                <input type="hidden" id="usuario_registro" class="form-control" readonly="readonly" value="<?php echo $usuario_registro; ?>" />

            </div>
            <div class="col-md-7">
                <?php if (isset($cuentas) && count($cuentas) >= 1) { ?>
                    <table id="corte_caja" class="table col-md-12 table-bordered mt-4" align="right">
                        <tr>
                            <th>Tipo de pago</th>
                            <th>Total vendido</th>
                            <th>Cantidad en caja</th>
                        </tr>
                        <?php
                        foreach ($cuentas as $cuenta) {   ?>
                            <tr>
                                <td><?php echo $cuenta->tipo_pago; ?></td>
                                <td><input type="number" data-tipo_pago_id="<?php echo $cuenta->id; ?>" data-total_caja="<?php echo $cuenta->total; ?>" class="form-control form-control_ total_pago" /></td>
                                <td><?php echo '$' . (number_format($cuenta->total, 2)) ?></td>
                            </tr>
                        <?php } ?>
                    </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
            <a href="{{ base_url('caja/reportes/corteCaja') }}" class="btn btn-default"> <i class="fa fa-arrow-left"></i> &nbsp;Regresar</a>
                <button type="button" id="btn-confirmar" class="btn btn-primary"> <i class="fa fa-file-alt"></i> &nbsp;Confirmar</button>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-md-12">
                <h4>No se han efectuado movimientos</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
            <a href="{{ base_url('caja/reportes/corteCaja') }}" class="btn btn-default"> <i class="fa fa-arrow-left"></i> &nbsp;Regresar</a>
            </div>
        </div>
    <?php } ?>
    </div>
    <div class="sep10"></div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    $("#btn-confirmar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post('api/corte-caja', form(), function(response, headers) {
            if (headers.status == 200 || headers.status == 201) {
                let corte_caja_id = response.id;
                let contador = $('table .total_pago').length;
                let cuantos = 0;
                let total_caja = 0;
                let total_cantidad_reportada = 0;


                $('table .total_pago').each(function(index, _this) {
                    let sobrantes = 0;
                    let faltantes = 0;
                    total_cantidad_reportada = parseFloat($(_this).val());
                    total_caja = parseFloat($(_this).data('total_caja'));
                    if (total_cantidad_reportada > total_caja) {
                        sobrantes = total_cantidad_reportada - total_caja;
                    }
                    if (total_cantidad_reportada < total_caja) {
                        faltantes = total_caja - total_cantidad_reportada;
                    }
                    data = {
                        'corte_caja_id': corte_caja_id,
                        'tipo_pago_id': $(_this).data('tipo_pago_id'),
                        'total_cantidad_caja': total_caja,
                        'total_cantidad_reportada': total_cantidad_reportada,
                        'faltantes': faltantes,
                        'sobrantes': sobrantes
                    }
                    ajax.post('api/detalle-corte-caja', data, function(response, headers) {
                        cuantos++;
                        if (contador == cuantos) {
                            if (headers.status == 200 || headers.status == 201) {
                                toastr.success(headers.message);
                                setTimeout(function() {
                                    location.reload();
                                }, 3000);
                            }
                        }
                    });
                });

            }
        })
    });
    let form = function() {
        return {
            caja_id: $("#caja_id").val(),
            movimiento_caja_id: 1, //ENTRADA
            fecha_transaccion: document.getElementById("fecha_transaccion").value,
            usuario_registro: document.getElementById("usuario_registro").value
        };
    }
</script>

@endsection