@layout('tema_luna/layout')
@section('contenido')
<style>
    .form-control_ {
        background-color: #fff !important;
    }
</style>
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="col-md-12">
        <?php echo '<h4> Corte de caja al día : ' . utils::aFecha($caja->fecha_transaccion) . '</h4>'; ?>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label>Fecha de corte:</label>
                    <input type="text" id="fecha_transaccion" readonly="readonly" class="form-control" value="<?php echo utils::aFecha($caja->fecha_transaccion); ?>" />
                </div>
                <div class="form-group">
                    <label>Número de caja:</label>
                    <select disabled="disabled" name="caja_id" id="caja_id" class="form-control">
                        <option value="">Selecionar ...</option>
                        @if(!empty($cat_cajas))
                        @foreach ($cat_cajas as $val)
                        @if(isset($caja) && $caja->caja_id == $val->id)
                        <option selected="selected" value="{{$val->id}}">{{$val->nombre}}</option>
                        @else
                        <option value="{{$val->id}}">{{$val->nombre}}</option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label>Usuario:</label>
                    <input type="text" id="usuario" readonly="readonly" class="form-control" value="<?php echo $caja->nombre . ' ' . $caja->apellido_paterno . ' ' . $caja->apellido_materno; ?>" />
                </div>

            </div>
            <div class="col-md-7">
                <?php if (isset($detalle_corte) && count($detalle_corte) >= 1) { ?>
                    <table id="corte_caja" class="table col-md-12 table-bordered mt-4" align="right">
                        <tr>
                            <th colspan="5">Movimientos reportados</th>
                        </tr>
                        <tr>
                            <th>Tipo de pago</th>
                            <th>Cantidad reportado</th>
                            <th>Cantidad en caja</th>
                            <th>Faltantes</th>
                            <th>Sobrantes</th>
                        </tr>
                        <?php
                        $total_reportado = 0;
                        $total_cantidad_caja = 0;
                        $total_faltantes = 0;
                        $total_sobrantes = 0;
                        foreach ($detalle_corte as $detalle) {
                            $total_reportado += $detalle->total_cantidad_reportada;
                            $total_cantidad_caja += $detalle->total_cantidad_caja;
                            $total_faltantes += $detalle->faltantes;
                            $total_sobrantes += $detalle->sobrantes;
                        ?>
                            <tr>
                                <td><?php echo $detalle->TipoPagoNombre; ?></td>
                                <td><?php echo '$' . (number_format($detalle->total_cantidad_reportada, 2)) ?></td>
                                <td><?php echo '$' . (number_format($detalle->total_cantidad_caja, 2)) ?></td>
                                <td><?php echo '$' . (number_format($detalle->faltantes, 2)) ?></td>
                                <td><?php echo '$' . (number_format($detalle->sobrantes, 2)) ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                    <table class="table table-bordered" style="width:50%" align="right">
                        <tr>
                            <th colspan="2">Totales</th>
                        </tr>
                        <tr>
                            <td>Cantidad reportado</td>
                            <td><?php echo '$' . (number_format($total_reportado, 2)) ?></td>
                        </tr>
                        <tr>
                            <td>Cantidad en caja</td>
                            <td><?php echo '$' . (number_format($total_cantidad_caja, 2)) ?></td>
                        </tr>
                        <tr>
                            <td>Faltantes</td>
                            <td style="color:red"><?php echo '$' . (number_format($total_faltantes, 2)) ?></td>
                        </tr>
                        <tr>
                            <td>Sobrantes</td>
                            <td style="color:green"><?php echo '$' . (number_format($total_sobrantes, 2)) ?></td>
                        </tr>
                    </table>
            </div>
        </div>

    <?php } else { ?>
        <div class="row">
            <div class="col-md-12">
                <h4>No se reportaron movimientos</h4>
            </div>
        </div>
    <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ base_url('caja/reportes/corteCaja') }}" class="btn btn-default"> <i class="fa fa-arrow-left"></i> &nbsp;Regresar</a>
        </div>
    </div>
    <div class="sep10"></div>
</div>

@endsection