@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <hr />
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label>Caja</label>
                <select class="form-control" id="caja_id" name="caja_id">
                    <option value="">Selecionar ...</option>
                    @if(!empty($catalogo_caja))
                    @foreach ($catalogo_caja as $caja)
                    <option value="{{$caja->id}}">{{$caja->nombre}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Fecha inicio:</label>
                <input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Fecha fin:</label>
                <input type="date" class="form-control" id="fecha_fin" name="fecha_fin" />
            </div>
        </div>

        <div class="col-md-12 text-right">
            <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tabla_caja" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered col-md-5 mt-4" align="right">
                <tr>
                    <th colspan="2">Totales</th>
                </tr>
                <tr>
                    <th class="text-right">Total cantidad en caja:</th>
                    <td id="total_cantidad">-</td>
                </tr>
                <tr>
                    <th class="text-right">Total cantidad reportado:</th>
                    <td id="total_reportado">-</td>
                </tr>
                <tr>
                    <th class="text-right">Total faltantes:</th>
                    <td id="total_faltantes">-</td>
                </tr>
                <tr>
                    <th class="text-right">Total sobrantes:</th>
                    <td id="total_sobrantes">-</td>
                </tr>
            </table>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ base_url('js/caja/corte_caja/listado_poliza_caja.js') }}"></script>
@endsection