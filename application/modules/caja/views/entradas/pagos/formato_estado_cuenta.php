<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Estado de cuenta</title>
	<style>
		.contenedor {
			width: 100%;
		}

		td {
			font-size: 11px !important;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}
		.text-right { text-align: right;}

		table#detalle_cuenta,
		td,
		td {
			border-bottom: 0.1px solid #233a74;
			margin-bottom: 12px
		}

	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12 text-right">
		    <?php echo obtenerFechaEnLetra(date('y-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>FORD</h2>
			<h2>San juan</h2>
			<h4>Estado de cuenta</h4>
		</div>
	</div>
	<div class="col-6">
		<h4>Datos del emisor</h4>
		<table style="width:95%;" class="table" cellpadding="5">
			<tr>
				<td>RFC:</td>
				<td>MYB020125CM3</td>
			</tr>
			<tr>
				<td>Razón Social:</td>
				<td>San juan , S.A. de C.V</td>
			</tr>
			<tr>
				<td>Regimen Fiscal:</td>
				<td>601 - General de Ley Personas Morales</td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="col-6">
		<h4>Receptor</h4>
		<table style="width:100%;" class="table" cellpadding="5">
			<tr>
				<td>Cliente:</td>
				<td><?php echo isset($cliente->nombre) ? $cliente->nombre : '';?></td>
			</tr>
			<tr>
				<td>RFC:</td>
				<td><?php echo isset($cliente->rfc) ? $cliente->rfc : '';?></td>
			</tr>
			<tr>
				<td>Razón Social:</td>
				<td><?php echo isset($cliente->nombre_empresa) ? $cliente->nombre_empresa : '';?></td>
			</tr>
			<tr>
				<td>Uso de CFDI:</td>
				<td><?php echo isset($cliente->cfdi) ? $cliente->cfdi->descripcion : '';?></td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>
	<div class="contenedor">
		<div class="col-12 mt-4">
			<h4>Detalle cuenta</h4>
			<table id="detalle_cuenta" style="width:100%;" class="table" cellpadding="5">
				<tr>
					<td width="120px">Folio: </td>
					<td><?php echo isset($cuenta->folio) ? $cuenta->folio : ''; ?></td>
					<td width="120px">Concepto: </td>
					<td><?php echo isset($cuenta->concepto) ? $cuenta->concepto : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Saldo neto: </td>
					<td><?php echo isset($cuenta->importe) ? '$'.(number_format($cuenta->importe,2)) : ''; ?></td>
					<td width="120px">Tasa de interes %: </td>
					<td><?php echo isset($cuenta->tasa_interes) ? $cuenta->tasa_interes : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Total a pagar: </td>
					<td><?php echo isset($cuenta->total) ? '$'.(number_format($cuenta->total,2)) : ''; ?></td>
					<td width="120px">Plazo: </td>
					<td><?php echo isset($cuenta->plazo_credito) ? $cuenta->plazo_credito : ''; ?></td>

				</tr>
				<tr>
					<td width="120px">Monto actual: </td>
					<td><?php echo '$'.(number_format($cuenta->saldo_actual,2)); ?></td>
					<td width="120px">Monto abonado: </td>
					<td><?php echo isset($cuenta->total_abonado) ? '$'.(number_format($cuenta->total_abonado,2)) : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Abono mensual: </td>
					<?php if($cuenta->tipo_forma_pago_id == 2) { ?>
					<td><?php echo  '$'. (number_format($cuenta->abono_mensual,2)); ?>
					<?php } else { ?>
					<td>0</td>
					<?php } ?>
					</td>
					<td width="120px">Fecha compra: </td>
					<td><?php echo isset($cuenta->fecha) ? utils::aFecha($cuenta->fecha,true) : ''; ?></td>
				</tr>
			</table>
			<div class="clear"></div>
		</div>
			
		</div>
		<div class="col-12">
			<h3>Historico de abonos</h3>
			<table id="detalle_cuenta" style="width:100%;" class="table" cellpadding="5">
					<tr>
						<td>#</td>
						<td>Tipo abono</td>
						<td>Fecha vencimiento</td>
						<td>Fecha pago</td>
						<td>Monto pagado</td>
						<td>Estado</td>
						<td>CFDI</td>
					</tr>
					<?php 
					foreach($abonos->data as $key => $abono) {  ?>
					<tr>
						<td><?php echo $key + 1; ?></td>
						<td><?php echo isset($abono->tipo_abono) ? $abono->tipo_abono : '';?></td>
						<td><?php echo isset($abono->fecha_vencimiento) ? utils::aFecha($abono->fecha_vencimiento,true) : '';?></td>
						<td><?php echo isset($abono->fecha_pago) ? utils::aFecha($abono->fecha_pago,true) : '';?></td>
						<td><?php echo isset($abono->total_pago) ? '$'.(number_format($abono->total_pago,2)) : '';?></td>
						<td><?php echo isset($abono->estatus_abono) ? $abono->estatus_abono : '';?></td>
						<td><?php echo isset($abono->descripcion_cfdi) ? $abono->clave_cfdi . ' - '.$abono->descripcion_cfdi : '';?></td>
					</tr>
					<?php } ?>
			</table>

		</div>
	</div>
</body>

</html>
