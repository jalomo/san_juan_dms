@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : '' }}</li>
    </ol>
    <h4>Estatus de venta : <?php echo isset($folio) && isset($folio[0]->re_ventas_estatus) ?
                                $folio[0]->re_ventas_estatus->estatus_venta->nombre : ''; ?></h4>
    <div class="row mt-2">
        <div class="col-md-4">
            <?php echo renderInputText('text', 'folio', 'Folio', isset($folio) ? $folio[0]->folio->folio
                : '', true); ?>
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->folio_id : '' }}" id="folio_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->id : '' }}" id="venta_id">
            <input type="hidden" value="{{ isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->id : '' }}" id="estatus_venta_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->almacen_id : '' }}" name="almacen_id" id="almacen_id">
            <input type="hidden" value="{{ isset($cxc->id) ? $cxc->id : '' }}" name="cxc_id" id="cxc_id">
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'venta_total', 'Total', isset($folio) ? $folio[0]->venta_total
                : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'cliente', 'Cliente', isset($folio) &&
                isset($folio[0]->cliente) ? $folio[0]->cliente->nombre : '', true); ?>
            <input type="hidden" value="{{ isset($folio) && $folio[0]->cliente ? $folio[0]->cliente->id : '' }}" id="cliente_id" name="cliente_id">
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Concepto</label>
                <textarea name="concepto" id="concepto" class="form-control" rows="3" style="min-height:100px" maxlength="500"><?php echo isset($cxc->concepto) ? $cxc->concepto : ''; ?></textarea>
                <div id='concepto_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">¿Compra de credito ó contado?</label>
                <select name="tipo_forma_pago_id" class="form-control " id="tipo_forma_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_forma_pago as $tipo)
                    @if($tipo->id == 2)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->descripcion}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='tipo_forma_pago_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">¿De que manera realizará el pago?</label>
                <select name="tipo_pago_id" class="form-control " id="tipo_pago_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($tipo_pago as $tipo)
                    @if(isset($cxc->tipo_pago_id) && $cxc->tipo_pago_id == $tipo->id)
                    <option value="{{ $tipo->id}}" selected="selected"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @else
                    <option value="{{ $tipo->id}}"> {{ $tipo->clave}} {{ $tipo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='tipo_pago_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Plazo de credito</label>
                <select name="plazo_credito_id" class="form-control " id="plazo_credito_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($plazo_credito as $plazo)
                    @if($plazo->id != 14)
                    <option data-cantidad_mes="{{$plazo->cantidad_mes}}" value="{{ $plazo->id}}"> {{ $plazo->nombre}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='plazo_credito_id_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Enganche</label>
                <input type="text" name="enganche" id="enganche" class="form-control" value="0" />
                <div id='enganche_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Tasa de interes %</label>
                <input type="text" name="tasa_interes" id="tasa_interes" class="form-control" value="<?php echo isset($cxc->tasa_interes) ? $cxc->tasa_interes : ''; ?>" />
                <div id='tasa_interes_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Cobrador asignar</label>
                <select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($gestores as $gestor)
                    @if(isset($cxc->usuario_gestor_id) && $cxc->usuario_gestor_id == $gestor->id)
                    <option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @else
                    <option value="{{ $gestor->id}}"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='usuario_gestor_id_error' class='invalid-feedback'></div>
            </div>
        </div>
    </div>
    <div class="row mb-4 mt-4">
        <div class="col-md-12 text-right">
            <?php if (isset($folio) && $folio[0]->re_ventas_estatus->estatus_ventas_id == 2) { ?>
                <button class="btn btn-primary col-md-4" id="btn-finalizar"><i class="fas fa-shopping-cart"></i>
                    Cambiar tipo crédito</button>
            <?php } ?>
        </div>
    </div>
    <br>

</div>
@endsection

@section('scripts')
<script>
    $(".container-forma-pago").hide();
    if ($("#tipo_forma_pago_id").val() == 2) {
        $(".container-forma-pago").show();
    }
    $("#tipo_forma_pago_id").on("change", function() {
        let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
        if (tipo_forma_pago_id == 2) {
            $(".container-forma-pago").show();
            $("#plazo_credito_id option[value='']").attr('selected', true);
        } else {
            $(".container-forma-pago").hide();
            $("#plazo_credito_id option[value=14]").attr('selected', true);
        }

    });

    let estatus_venta_id = $('#estatus_venta_id').val();


    $("#btn-finalizar").on("click", function() {
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });
        let continuar = false;

        if (tipo_forma_pago_id.value == 2) {
            ajax.post(`api/clientes/tiene-credito`, {
                id: cliente_id.value
            }, function(response, headers) {
                if (headers.status == 201 || headers.status == 200) {
                    $.isLoading("hide");
                    if (!response.aplica_credito) {
                        utils.displayWarningDialog("El cliente no cuenta con credito. ¿Desea continuar?", "warning", function(data) {
                            if (data.value) {
                                realizarVenta();
                            }
                        }, true)
                    } else {
                        if (response.limite_credito && venta_total.value) {
                            if (parseFloat(venta_total.value) > parseFloat(response.limite_credito)) {
                                utils.displayWarningDialog("El cliente solo cuenta con un límite de crédito de: " + response.limite_credito + " ¿Desea continuar?", "warning", function(data) {
                                    if (data.value) {
                                        realizarVenta();
                                    }
                                }, true)
                            }
                        }
                        if (plazo_credito_id.value && response.cantidad_mes) {
                            if (parseInt($("#plazo_credito_id option:selected").data('cantidad_mes')) > parseInt(response.cantidad_mes)) {
                                utils.displayWarningDialog("El cliente solo cuenta con un plazo de credito de: " + response.plazo_credito + " ¿Desea continuar?", "warning", function(data) {
                                    if (data.value) {
                                        realizarVenta();
                                    }
                                }, true)
                            }
                        }
                    }
                }
            });
        } else if (tipo_forma_pago_id.value == 1) {
            realizarVenta();
        } else {
            $.isLoading("hide");
            toastr.error('Favor de elegir si es compra de credito ó contado');
        }
    });

    function realizarVenta() {
        $.isLoading({
            text: "Efectuando cambio a crédito ...."
        });
        let id_venta = $('#venta_id').val();
        let concepto = '';
        if ($("#concepto").val()) {
            concepto = $('#concepto').val();
        }
        ajax.put(`api/ventas/finalizar/${id_venta}`, {
            folio_id: $('#folio_id').val(),
            venta_total: $('#venta_total').val(),
            tipo_forma_pago_id: $('#tipo_forma_pago_id').val(),
            concepto: concepto,
            tipo_pago_id: $('#tipo_pago_id').val(),
            cliente_id: $('#cliente_id').val(),
            plazo_credito_id: $('#plazo_credito_id').val(),
            enganche: $('#enganche').val(),
            tasa_interes: $('#tasa_interes').val(),
            usuario_gestor_id: $('#usuario_gestor_id').val(),
            estatus_cuenta_id: 5,
        }, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $.isLoading("hide");
                utils.displayWarningDialog('Cambio realizado correctamente..', 'success', function(result) {
                    window.location.href = PATH + 'caja/entradas/detalle_pago/' + $("#cxc_id").val();
                });
            } else {
                $.isLoading("hide");

            }
        })

    }
</script>
@endsection