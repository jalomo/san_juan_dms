@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="4">Datos del cliente</th>
				</tr>
				<tr>
					<th width="10%">Nombre: </th>
					<td width="40%"><?php echo isset($cliente->nombre) ? $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno : ''; ?></td>
					<th width="10%">Núm. cliente: </th>
					<td width="40%"><?php echo isset($cliente->numero_cliente) ? $cliente->numero_cliente : ''; ?></td>
				</tr>
				<tr>
					<th>Domicilio: </th>
					<td><?php echo isset($cliente->direccion) ? $cliente->direccion . ' ' . $cliente->colonia . ' ' . $cliente->municipio . ' ' . $cliente->estado : ''; ?></td>
					<th>Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
				</tr>
				<tr>
					<th>Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
					<th>Total: </th>
					<td><?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Fecha compra: </th>
					<td colspan="3"><?php echo isset($data_cuentas->fecha) ? $data_cuentas->fecha : ''; ?></td>
				</tr>
			</table>
		</div>
	</div>
	<?php if ($data_cuentas->estatus_cuenta_id == 1) { ?>

		<h4>Realizar pago [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</h4>
		<div class="row mt-4">
			<div class="col-md-4">
				<div class="form-group">
					<label>Total a pagar:</label>
					<input type="text" name="importe" id="importe" readonly="readonly" value="<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Tipo de pago</label>
					<select class="form-control" id="tipo_pago" name="tipo_pago" style="width: 100%;">
						<option value="">Selecionar ...</option>
						@if(!empty($cat_tipo_pago))
						@foreach ($cat_tipo_pago as $pago)
						@if($unico_pago->tipo_pago_id == $pago->id)
						<option value="{{$pago->id}}" selected="selected">[{{$pago->clave}}] {{$pago->nombre}}</option>
						@else
						<option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
						@endif
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<?php
			// dd($data_cuentas);
			?>
			<?php if ($data_cuentas->tipo_proceso_id == 10) { ?>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tipo de cuenta:</label>
						<select class="form-control select2" id="cuenta_id_pagar" name="cuenta_id_pagar" style="width:100%">
							<option value="">Selecionar ...</option>
							@if(!empty($catalogo_cuentas))
							@foreach ($catalogo_cuentas as $cuenta)
							<option value="{{ $cuenta->id}}"> {{ $cuenta->no_cuenta}} - {{$cuenta->nombre_cuenta }}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tipo asiento:</label>
						<select class="form-control select2" id="tipo_asiento_id_pagar" name="tipo_asiento_id_pagar" style="width: 100%;">
							<option value="">Selecionar ...</option>
							@if(!empty($cat_tipo_asiento))
							@foreach ($cat_tipo_asiento as $tipo_asiento)
							<option value="{{$tipo_asiento->id}}">{{$tipo_asiento->descripcion}}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
			<?php } ?>
			<div class="col-md-4">
				<div class="form-group">
					<label>Tipo de CDFI</label>
					<select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
						<option value="">Selecionar ...</option>
						@if(!empty($cat_cfdi))
						@foreach ($cat_cfdi as $cfdi)
						@if($unico_pago->cfdi_id == $cfdi->id)
						<option value="{{$cfdi->id}}" selected="selected">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
						@else
						<option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
						@endif
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Caja</label>
					<select class="form-control" id="caja_id" name="caja_id" style="width: 100%;">
						<option value="">Selecionar ...</option>
						@if(!empty($catalogo_caja))
						@foreach ($catalogo_caja as $caja)
						@if($unico_pago->caja_id == $caja->id)
						<option value="{{$caja->id}}" selected="selected">{{$caja->nombre}} </option>
						@else
						<option value="{{$caja->id}}">{{$caja->nombre}}</option>
						@endif
						@endforeach
						@endif
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Fecha pago:</label>
					<input type="date" name="fecha_pago" id="fecha_pago" value="<?php echo isset($unico_pago->fecha_pago) ? $unico_pago->fecha_pago : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Su pago:</label>
					<input type="number" name="pago" id="pago" value="<?php echo isset($unico_pago->pago) ? $unico_pago->pago : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Cambio:</label>
					<input type="number" name="cambio" id="cambio" value="<?php echo isset($unico_pago->cambio) ? $unico_pago->cambio : ''; ?>" class="form-control">
				</div>
			</div>
			<div class="col-md-12 mt-4 mb-4">
				<div class="text-right">
					<button id="btn-pagar" type="button" class="btn btn-primary col-md-2"><i class="fas fa-cash-register"></i> Pagar</button>
				</div>
			</div>
		<?php } else { ?>
			<h3>Asientos contabilidad</h3>
			<div class="row mt-4">
				<div class="col-md-4">
					<div class="form-group">
						<label>Referencia:</label>
						<input type="text" name="referencia" id="referencia" value="<?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?>" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tipo de cuenta:</label>
						<select class="form-control select2" id="cuenta_id" name="cuenta_id" style="width:100%">
							<option value="">Selecionar ...</option>
							@if(!empty($catalogo_cuentas))
							@foreach ($catalogo_cuentas as $cuenta)
							<option value="{{ $cuenta->id}}"> {{ $cuenta->no_cuenta}} - {{$cuenta->nombre_cuenta }}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Tipo asiento:</label>
						<select class="form-control select2" id="tipo_asiento_id" name="tipo_asiento_id" style="width: 100%;">
							<option value="">Selecionar ...</option>
							@if(!empty($cat_tipo_asiento))
							@foreach ($cat_tipo_asiento as $tipo_asiento)
							<option value="{{$tipo_asiento->id}}">{{$tipo_asiento->descripcion}}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Total a pagar:</label>
						<input type="text" name="total_pago" id="total_pago" value="<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Fecha pago:</label>
						<input type="date" name="fecha_pago" id="fecha_pago" class="form-control">
					</div>
				</div>
				<div class="col-md-12 mt-4 mb-4">
					<div class="text-right">
						<button id="btn-agregar" type="button" class="btn btn-primary"><i class="fas fa-cash-register"></i> Agregar cuenta</button>
					</div>
				</div>
			</div>
		<?php } ?>
		<div class="col-md-12 ">
			<?php if ($data_cuentas->estatus_cuenta_id > 1 && $data_cuentas->estatus_cuenta_id <= 3) { ?>
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="tbl_detalle_cuentas" width="100%" cellspacing="0"></table>
				</div>
				<div class="text-right mt-3">
					<button id="btn-imprimir_poliza" onclick="imprimir_estado_cuenta(this)" data-cuenta_por_pagar_id="<?php echo $data_cuentas->id; ?>" type="button" class="btn btn-primary "><i class="fas fa-file-pdf"></i> Imprimir estado cuenta</button>
					<button id="btn-imprimir_factura" onclick="imprimir_factura(this)" data-cuenta_por_pagar_id="<?php echo $data_cuentas->id; ?>" type="button" class="btn btn-success ml-3"><i class="fas fa-file-excel"></i> Imprimir factura</button>
				</div>
			<?php } ?>
		</div>
		</div>
		<input type="hidden" id="abono_id" value="<?php echo isset($unico_pago->id) ? $unico_pago->id : ''; ?>" />
		<input type="hidden" id="tipo_proceso_id" value="<?php echo isset($data_cuentas->tipo_proceso_id) ? $data_cuentas->tipo_proceso_id : ''; ?>" />
		<input type="hidden" id="folio_id" value="{{ isset($data_cuentas->folio_id) ? $data_cuentas->folio_id : ''; }}" />
		
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	crearTabla();
	getBusqueda();
	$("#btn-agregar").on('click', function() {
		if($('small.form-text.text-danger').length > 0){
            $('small.form-text.text-danger').each(function() {
                $( this ).empty();
            });
        }
		$.isLoading({
			text: "Realizando petición ...."
		});
		let dataForm = {
			cuenta_id: document.getElementById("cuenta_id").value,
			cuenta_por_cobrar_id: cuenta_por_cobrar_id,
			abono_id: document.getElementById("abono_id").value,
			total_pago: document.getElementById("total_pago").value,
			tipo_asiento_id: document.getElementById("tipo_asiento_id").value,
			fecha_pago: document.getElementById("fecha_pago").value,
			referencia: document.getElementById("referencia").value,
		}

		let total_pago = $("#total_pago").val();
		let importe = $("#importe").val();
		if (parseFloat(total_pago) > parseFloat(importe)) {
			toastr.error("El total de pago no puede ser mayor al importe de la cuenta " + importe)
			return false;
		}
		ajax.post(`api/detalle-asientos-cuentas`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");
				utils.displayWarningDialog('Cuenta registrada correctamente', "success", function(data) {
					$("#cuenta_id").val('');
					$("#total_pago").val('');
					$("#tipo_asiento_id").val('');
					$("#fecha_pago").val('');
					$("#total_pago").trigger('change');
					$("#tipo_asiento_id").trigger('change');

					getBusqueda();
				})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}

		})
	});

	function crearTabla() {
		$('table#tbl_detalle_cuentas').dataTable(this.configuracionTabla());
	}

	function configuracionTabla() {
		return {
			language: {
				url: PATH_LANGUAGE
			},
			bInfo: false,
			paging: false,
			order: [
				[0, 'asc']
			],
			columns: [
				{
					title: "Referencia",
					data: 'referencia',
				},
				{
					title: "Tipo asiento",
					data: 'tipo_asiento',
				},
				{
					title: "Cuenta",
					render: function(data, type, row) {
						return row.no_cuenta + ' - ' + row.nombre_cuenta;
					},
				},
				{
					title: "Total pago",
					data: 'total_pago',
				},
				{
					title: "Fecha pago",
					data: 'fecha_pago',
				}
			],
		}
	}

	function getBusqueda() {
		ajax.get("api/detalle-asientos-cuentas/buscar-cuenta-por-cobrar/" + cuenta_por_cobrar_id, false, function(response, header) {
			var listado = $('table#tbl_detalle_cuentas').DataTable();
			listado.clear().draw();
			if (response && response.data.length > 0) {
				response.data.forEach(listado.row.add);
				listado.draw();
			}
		});
	}
	var d = new Date();
	var strDate = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
	let orden_entrada_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let dataForm = [];
	$("#btn-pagar").on('click', function() {
		$.isLoading({
			text: "Realizando petición ...."
		});
		if (document.getElementById("tipo_proceso_id").value == 10) {
			dataForm = {
				cuenta_por_cobrar_id: orden_entrada_id,
				total_pago: document.getElementById("importe").value,
				tipo_abono_id: 3,
				tipo_pago_id: document.getElementById("tipo_pago").value,
				cfdi_id: document.getElementById("cfdi_id").value,
				caja_id: document.getElementById("caja_id").value,
				fecha_pago: strDate,
				estatus_abono_id: 3,
				// folio_id: document.getElementById('folio_id').value,
				fecha_vencimiento: strDate,
				tipo_proceso_id: document.getElementById("tipo_proceso_id").value,
				cuenta_id: document.getElementById("cuenta_id_pagar").value,
				tipo_asiento_id: document.getElementById("tipo_asiento_id_pagar").value,
			}
		} else {
			dataForm = {
				cuenta_por_cobrar_id: orden_entrada_id,
				total_pago: document.getElementById("importe").value,
				tipo_abono_id: 3,
				tipo_pago_id: document.getElementById("tipo_pago").value,
				cfdi_id: document.getElementById("cfdi_id").value,
				caja_id: document.getElementById("caja_id").value,
				fecha_pago: strDate,
				estatus_abono_id: 3,
				// folio_id: document.getElementById('folio_id').value,
				fecha_vencimiento: strDate,
				tipo_proceso_id: document.getElementById("tipo_proceso_id").value,
			}
		}

		let pago = $("#pago").val();
		if (!parseFloat(pago) && pago < 1) {
			$.isLoading("hide");
			toastr.error("Falta indicar el pago")
			$.isLoading("hide");
			return false;
		}
		let abono_id = $("#abono_id").val();
		ajax.put(`api/abonos-por-cobrar/${abono_id}`, dataForm, function(response, headers) {
			if (headers.status == 200 || headers.status == 201) {
				$.isLoading("hide");

				ajax.put(`api/cuentas-por-cobrar/${orden_entrada_id}`, {
					estatus_cuenta_id: 2
				}, function(response, header) {
					if (header.status == 400) {
						$.isLoading("hide");
						return ajax.showValidations(header);
					}
					utils.displayWarningDialog(header.message, "success", function(data) {
						window.location.reload();
					})
				})
			} else {
				$.isLoading("hide");
				return ajax.showValidations(headers);
			}

		})
	});

	$("#pago").on('blur', function() {
		let pago = $("#pago").val();
		let importe = $("#importe").val();
		if (parseFloat(pago) < parseFloat(importe)) {
			toastr.error("El pago es menor al total a pagar")
			$("#btn-pagar").attr('disabled', true);
			$("#pago").val(0);
		} else {
			let cambio = parseFloat(pago) - parseFloat(importe);
			$("#cambio").val(cambio.toFixed(2));
			$("#btn-pagar").attr('disabled', false);
		}
	});

	function imprimir_comprobante(_this) {
		abono_id = $(_this).data('abono_id');
		window.location.href = PATH + '/caja/entradas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
	}

	function imprimir_estado_cuenta(_this) {
		cuenta_id = $(_this).data('cuenta_por_pagar_id');
		window.location.href = PATH + '/caja/entradas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
	}
	$(".select2").select2();
</script>
@endsection