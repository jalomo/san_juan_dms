<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RelacionCobranza extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        $this->load->library('curl'); 
        $this->load->helper('general');

        //$dataFromApi = $this->curl->curlGet('api//');
        $dataRegistro = [];
        //$dataRegistro = procesarResponseApiJsonToArray($dataFromApi); 
        $data['data'] = $dataRegistro;
        
        $data['modulo'] = "CXC";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Relación Cobranza";
        $data['subtitulo'] = "Listado";

        $this->blade->render('cobranza/listado', $data);
    }

    public function crear($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos el catalogo de cfdi
        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        //Cargamos el catalogo de formas de pago
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        $data['data'] = [];
        
        $data['modulo'] = "CXC";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Relación Cobranza";
        $data['subtitulo'] = "Registro";

        $this->blade->render('cobranza/formulario', $data);
    }

    public function editar($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos el catalogo de cfdi
        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        //Cargamos el catalogo de formas de pago
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago); 
        
        $data['data'] = [];
        
        $data['modulo'] = "CXC";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Relación Cobranza";
        $data['subtitulo'] = "Edición";

        $this->blade->render('cobranza/formulario', $data);
    }

    public function historialPagos($id='')
    {
        $data['data'] = [];
        
        $data['modulo'] = "CXC";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Relación Cobranza";
        $data['subtitulo'] = "Historial de pagos";

        $this->blade->render('cobranza/historialPagos', $data);
    }

}
