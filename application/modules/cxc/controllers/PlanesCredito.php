<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PlanesCredito extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
    	$this->load->library('curl'); 
        $this->load->helper('general');

        //$dataFromApi = $this->curl->curlGet('api//');
        $dataRegistro = [];
        //$dataRegistro = procesarResponseApiJsonToArray($dataFromApi); 
        $data['data'] = $dataRegistro;
        
        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Planes de Crédito";
        $data['subtitulo'] = "Listado";

        $this->blade->render('planes/listado', $data);
    }

    public function crear($id='')
    {
        $this->load->library('curl'); 
        $this->load->helper('general');

        $data['data'] = [];
        
        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Planes de Crédito";
        $data['subtitulo'] = "Registro";

        $this->blade->render('planes/formulario', $data);
    }

    public function editar($id='')
    {
        $this->load->library('curl'); 
        $this->load->helper('general');

        $data['data'] = [];
        
        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Planes de Crédito";
        $data['subtitulo'] = "Edición";

        $this->blade->render('planes/formulario', $data);
    }

    public function simular($id='')
    {
        $this->load->library('curl'); 
        $this->load->helper('general');

        $data['data'] = [];

        $data['modulo'] = "CXP";
        $data['submodulo'] = "Movimientos";
        $data['titulo'] = "Planes de Crédito";
        $data['subtitulo'] = "Simulador";

        $this->blade->render('planes/simulacion', $data);
    }
}