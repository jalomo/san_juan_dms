@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mt-4">
        <div class="col-md-6">
			<div class="form-group">
				<label for="select">Cobrador asignar</label>
				<select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
					<option value=""> Seleccionar</option>
					@foreach ($gestores as $gestor)
						@if(isset($cxc->usuario_gestor_id) && $this->session->userdata('id') == $gestor->id)
							<option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
						@else
							<option value="{{ $gestor->id}}"> {{ $gestor->nombre }}  - {{ $gestor->apellido_paterno}}  </option>
						@endif
					@endforeach
				</select>
				<div id='usuario_gestor_id_error' class='invalid-feedback'></div>
			</div>
		</div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control"/>
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Número cliente:</label>
                <select class="form-control" id="cliente_id" name="cliente_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_clientes))
                    @foreach ($cat_clientes as $cliente)
                        <option value="{{ $cliente->id}}"> {{$cliente->numero_cliente}} - {{ $cliente->nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="cliente_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Estatus pago:</label>
                <select class="form-control" id="estatus_abono_id" name="estatus_abono_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_estatus_abonos))
                    @foreach ($cat_estatus_abonos as $abonos)
                        <option value="{{ $abonos->id}}"> {{ $abonos->nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="estatus_abono_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Fecha vencimiento:</label>
                <input type="date" name="fecha_vencimiento" id="fecha_vencimiento" value="" class="form-control"/>
            </div>
        </div>
      
        <div class="col-md-12 mt-4">
            <div class="text-right">
                <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary col-md-2">
                    <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
                </button>
                <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary col-md-2">
                    <i class="fa fa-search" aria-hidden="true"></i> Filtrar
                </button>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_gestion" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
        <div class="col-md-12">
        <table class="table table-bordered col-md-5 mt-4" align="right" >
                <tr>
                    <th colspan="2">Totales</th>
                </tr>
                <tr>
                    <th  class="text-right">Monto a abonar:</th>
                    <td id="total_monto_abonar">-</td>
                </tr>
                <tr>
                    <th  class="text-right">Monto pagado:</th>
                    <td id="total_monto_pagado">-</td>
                </tr>
            </table>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ base_url('js/cxc/gestion_cobranza/listado.js') }}"></script>
@endsection