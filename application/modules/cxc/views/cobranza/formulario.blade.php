@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="registro_comprobante"> <!-- enctype="multipart/form-data"  -->
                <h4>Datos del registro (interno)</h4>
                <hr>

                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Folio:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->folio)){print_r($data->folio);} ?>" id="folio" name="folio" placeholder="0001" disable>
                            <div id="folio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de pago:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha)){print_r($data->fecha);}else {echo date('Y-m-d');} ?>" id="fecha" name="fecha" placeholder="">
                            <div id="fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario que realiza el pago:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario)){print_r($data->usuario);} ?>"  id="usuario" name="usuario" placeholder="">
                            <div id="usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Estatus de pago:</label>
                            <select class="form-control" id="estatus" name="estatus" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="estatus_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos generales del proveedor</h4>
                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Buscar proveedor:</label>
                            <input type="text" class="form-control" value="" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-1" align="center">
                        <a href="#" class="btn btn-info" type="button" title="Buscar" style="margin-top: 28px;">
                            <i class="fas fa-search"></i>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Clave:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave_cliente)){print_r($data->clave_cliente);} ?>"  id="clave_cliente" name="clave_cliente" placeholder="">
                            <div id="clave_cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Nombre/Razón Social:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cliente)){print_r($data->cliente);} ?>"  id="cliente" name="cliente" placeholder="">
                            <div id="cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">R.F.C:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->rfc)){print_r($data->rfc);} ?>"  id="rfc" name="rfc" placeholder="">
                            <div id="rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>"  id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Correo Electrónico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo)){print_r($data->correo);} else {echo 'notiene@notiene.com.mx';} ?>"  id="correo" name="correo" placeholder="">
                            <div id="correo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Frecuencias de pagos:</label>
                            <select class="form-control" id="frecuencia" name="frecuencia" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="7">7 días</option>
                                <option value="15">15 días</option>
                                <option value="30">30 días</option>
                                <option value="60">60 días</option>
                                <option value="90">90 días</option>
                                <option value="180">180 días</option>
                                <option value="365">1 año</option>
                            </select>
                            <div id="frecuencia_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Tipo de cuenta:</label>
                            <select class="form-control" id="tipo_cuenta" name="tipo_cuenta" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="tipo_cuenta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Limite de credito:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->limite_credito)){print_r($data->limite_credito);} ?>"  id="limite_credito" name="limite_credito" placeholder="">
                            <div id="limite_credito_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Saldo:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->saldo)){print_r($data->saldo);} ?>"  id="saldo" name="saldo" placeholder="">
                            <div id="saldo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>             

                <br>
                <h4>Datos del pago</h4>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Concepto:</label>
                            <textarea class="form-control" cols="4" id="concepto" name="concepto"><?php if(isset($data->concepto)){print_r($data->concepto);} ?></textarea>
                            <div id="concepto_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Forma de pago:</label>
                            <select class="form-control" id="metodo_pago_id" name="metodo_pago_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_tipo_pago))
                                    @foreach ($cat_tipo_pago as $pago)
                                        <option value="{{$pago->id}}" <?php if(isset($data->metodo_pago_id)){if($data->metodo_pago_id == $pago->id) echo "selected";} ?>>[{{$pago->clave}}] {{$pago->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="metodo_pago_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Método de pago:</label>
                            <select class="form-control" id="forma_pago" name="forma_pago" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="PUE">Pago en una sola exhibición</option>
                                <option value="PPD">Pago en parcialidades o diferido</option>
                            </select>
                            <div id="forma_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">CFDI:</label>
                            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_cfdi))
                                    @foreach ($cat_cfdi as $cfdi)
                                        <option value="{{$cfdi->id}}" <?php if(isset($data->cfdi_id)){if($data->cfdi_id == $cfdi->id) echo "selected";} ?>>[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="cfdi_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Monto:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->pago)){print_r($data->pago);} ?>"  id="pago" name="pago" placeholder="5000">
                            <div id="pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Moneda:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="MXN">MXN (Pesos Mexicanos)</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Estatus del pago:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="Pagado">Pagado</option>
                                <option value="Incompleto">Incompleto</option>
                                <option value="Moratorio">Moratorio</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">N° Documento:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->documento)){print_r($data->documento);} ?>"  id="documento" name="documento" placeholder="0249200126496">
                            <div id="documento_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha del documento:</label>
                            <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->fecha_doc)){print_r($data->fecha_doc);}else {echo '';} ?>" id="fecha_doc" name="fecha_doc" placeholder="">
                            <div id="fecha_doc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Valor del documento:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->pago)){print_r($data->pago);} ?>"  id="pago" name="pago" placeholder="25000">
                            <div id="pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Banco:</label>
                            <select class="form-control" id="banco" name="banco" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="banco_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Sucursal:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->sucursal)){print_r($data->sucursal);} ?>"  id="sucursal" name="sucursal" placeholder="">
                            <div id="sucursal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">N° de cuenta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cuenta)){print_r($data->cuenta);} ?>"  id="cuenta" name="cuenta" placeholder="0249200126496">
                            <div id="cuenta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Ultimo pago:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->ultimo_pago)){print_r($data->ultimo_pago);} ?>"  id="ultimo_pago" name="ultimo_pago" placeholder="">
                            <div id="ultimo_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Fecha ultimo pago:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha_ultimopago)){print_r($data->fecha_ultimopago);}else {echo date('Y-m-d');} ?>" id="fecha_ultimopago" name="fecha_ultimopago" placeholder="">
                            <div id="fecha_ultimopago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--<script src="{{ base_url('js/facturas/index.js') }}"></script>-->
@endsection