@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Fecha inicio:</label> 
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_inicio">
            </div>
        </div>
        <div class="col-md-4">
            <label for="">Fecha fin:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_fin">
            </div>
        </div>

        <div class="col-md-4">
            <label for="">Cliente:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-user"></i>
                    </div>
                </div>

                <select class="form-control" id="cliente" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Estatus cheque:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="fas fa-ellipsis-v"></i>
                    </div>
                </div>

                <select class="form-control" id="estatus" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="" style="margin-top: 30px;">Buscar</a>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Cliente</th>
                            <th>Referencia</th>
                            <th>Proxima de pago</th>
                            <th>Monto del pago</th>
                            <th>Estatus Pago</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->folio}}</td>
                                <td>{{$item->cliente}}</td>
                                <td>{{$item->referencia}}</td>
                                <td>{{$item->fecha_pago}}</td>
                                <td>{{$item->pago}}</td>
                                <td>{{$item->estatus_pago}}</td>
                                <td>
                                    <a href="#" class="btn btn-danger" type="button" title="Devolver">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" type="button" title="Validar">
                                        <i class="fas fa-vote-yea"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td>N° 0001</td>
                                <td>CLIENTE DEMO</td>
                                <td>ABONO MENSUALIDAD</td>
                                <td>04/03/2020</td>
                                <td>$6,000</td>
                                <td>PENDIENTE</td>
                                <td>
                                    <a href="{{ base_url('cxp/Cheques/devolver/1') }}" class="btn btn-danger" type="button" title="Devolver">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('cxp/Cheques/confirmar/1') }}" class="btn btn-success" type="button" title="Validar">
                                        <i class="fas fa-vote-yea"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>N° 0002</td>
                                <td>CLIENTE INTERNO</td>
                                <td>ABONO MENSUALIDAD</td>
                                <td>04/04/2020</td>
                                <td>$6,000</td>
                                <td style="background-color: #f5b7b1;">DEVUELTO/RECHAZADO</td>
                                <td>
                                    <a href="{{ base_url('cxp/Cheques/verDatos/2') }}" class="btn btn-danger" type="button" title="Ver información">
                                        <i class="fas fa-check-double"></i>
                                    </a>
                                </td>
                                <td>
                                    <!--<a href="{{ base_url('cxp/Cheques/devolver/1') }}" class="btn btn-success" type="button" title="Recuperar">
                                        <i class="fas fa-vote-yea"></i>
                                    </a>-->
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>N° 0003</td>
                                <td>CLIENTE DEMO CREDITO</td>
                                <td>ABONO MENSUALIDAD</td>
                                <td>04/03/2020</td>
                                <td>$6,000</td>
                                <td style="background-color: #abebc6 ;">PAGADO</td>
                                <td>
                                    <!--<a href="{{ base_url('cxp/Cheques/devolver/2') }}" class="btn btn-danger" type="button" title="Devolver">
                                        <i class="fas fa-window-close"></i>
                                    </a>-->
                                </td>
                                <td>
                                    <a href="{{ base_url('cxp/Cheques/verDatos/3') }}" class="btn btn-success" type="button" title="Ver información">
                                        <i class="fas fa-check-double"></i>
                                    </a>
                                </td>
                            </tr>

                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Cliente</th>
                            <th>Referencia</th>
                            <th>Proxima de pago</th>
                            <th>Monto del pago</th>
                            <th>Estatus Pago</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection