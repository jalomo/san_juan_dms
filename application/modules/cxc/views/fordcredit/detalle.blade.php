@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "direccion", "Direccion", isset($solicitud->nombre_cliente) ? $solicitud->nombre_cliente : '' , true); ?>
        </div>

        <div class="col-md-6">
            <?php renderInputText("text", "direccion", "Direccion", isset($solicitud->direccion_cliente) ? $solicitud->direccion_cliente : '' , true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "estado", "Estado", isset($solicitud->direccion_cliente) ? $solicitud->estado_cliente : '' , true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("text", "municipio", "Municipio", isset($solicitud->direccion_cliente) ? $solicitud->direccion_cliente : '' , true); ?>
        </div>
        <div class="col-md-12">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="buro_credito">
                <label class="form-check-label" for="buro_credito">Buro de credito</label>
            </div>
        </div>
    </div>
    <hr>
    <h3 class="mt-3 mb-3">Referencias</h3>

    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "referencia_nombre", "Nombre referencia 1", isset($solicitud->referencia_nombre) ? $solicitud->referencia_nombre : '', true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("number", "referencia_telefono", "Telefono referencia 1", isset($solicitud->referencia_telefono) ? $solicitud->referencia_telefono : '', true); ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "referencia_nombre_2", "Nombre referencia 2", isset($solicitud->referencia_nombre_2) ? $solicitud->referencia_nombre_2 : '', true); ?>
        </div>
        <div class="col-md-6">
            <?php renderInputText("number", "referencia_telefono_2", "Telefono referencia 2", isset($solicitud->referencia_telefono_2) ? $solicitud->referencia_telefono_2 : '', true); ?>
        </div>
    </div>
    <hr>

    <div class="row">
        
        @foreach ($solicitud->documentos as $items_files)
           
            <div class="col-md-12">
                <div class="col-md-9">
                    <p style="text-align: justify; margin-bottom: 0px;color:black;">
                        1. Identificación vigente (Ine, Pasaporte )
                    </p>
                    <br>
                    <div class="form-group">
                        <a target="_blank" href="{{ API_URL_DEV."api/solicitud-credito/descargar/".$solicitud->id."/".$items_files->nombre_archivo }}">Descargar documento</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection

@section('scripts')

@endsection