@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Nombre del proyecto</label>
                    {{ $id_proyecto }}
                </div>
                <div class="col-sm-4">
                    <label for="">Cliente</label>
                    {{ $id_cliente }}
                </div>
                <div class="col-sm-4">
                    <label for="">Descripción del proyecto</label>
                    {{ $descripcion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Número de siniestro</label>
                    {{ $numero_siniestro }}
                </div>
                <div class="col-sm-4">
                    <label for="">Número de póliza</label>
                    {{ $numero_poliza }}
                </div>
                <div class="col-sm-4">
                    <label for="">Año del vehículo</label>
                    {{ $id_anio }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Placas</label>
                    {{ $placas }}
                </div>
                <div class="col-sm-4">
                    <label for="">Color</label>
                    {{ $id_color }}
                </div>
                <div class="col-sm-4">
                    <label for="">Modelo</label>
                    {{ $id_modelo }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label for="">¿Cita?</label><br>
                    {{ $cita }}
                </div>
                <div class="col-sm-4">
                    <label for="">Asesor</label>
                    {{ $id_asesor }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha</label>
                    {{ $drop_fecha }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora cita</label>
                    {{ $hora_cita }}
                </div>
                <div class="col-sm-4">
                    <label for="">Estatus</label>
                    {{ $id_status }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha inicio</label>
                    {{ $fecha_inicio }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha fin</label>
                    {{ $fecha_fin }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tipo de golpe</label>
                    {{ $tipo_golpe }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <label for="">Comentarios</label>
                    {{ $comentarios }}
                </div>
            </div>
            <div class="row">
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        const existeCita = () => {
            const arr_fecha = $("#fecha_cita").val().split("/");
            const fecha_cita = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
            const data = {
                fecha_cita,
                hora_cita: $("#hora_cita").val(),
                id: $("#id").val(),
            }
            console.log('data existe',data);
            ajax.post('api/bodyshop/master/existe-reservacion', data,
                function(response, headers) {
                    console.log('response',response);
                    if (response.length > 0) {
                        console.log('entre');
                        return true;
                    }
                    return false;
                })
        }
        $("#guardar").on('click', async function() {
            if (await existeCita()) {
                utils.displayWarningDialog("El horario ya fue ocupado", 'warning', function(result) {});
                return;
            } else {
                const arr_fecha = $("#fecha_cita").val().split("/");
                const fecha_cita = arr_fecha[2] + '-' + arr_fecha[1] + '-' + arr_fecha[0];
                const data = {
                    id_cliente: $("#id_cliente").val(),
                    descripcion: $("#descripcion").val(),
                    numero_siniestro: $("#numero_siniestro").val(),
                    numero_poliza: $("#numero_poliza").val(),
                    id_anio: $("#id_anio").val(),
                    placas: $("#placas").val(),
                    id_color: $("#id_color").val(),
                    id_modelo: $("#id_modelo").val(),
                    serie: $("#serie").val(),
                    cita: ($("#cita").prop('checked'))?1:0,
                    id_asesor: $("#id_asesor").val(),
                    fecha_cita,
                    hora_cita: $("#hora_cita").val(),
                    id_status: $("#id_status").val(),
                    fecha_inicio: $("#fecha_inicio").val(),
                    fecha_fin: $("#fecha_fin").val(),
                    tipo_golpe: $("#tipo_golpe").val(),
                    comentarios: $("#comentarios").val(),
                };
                console.log('data',data);
                if ($("#id").val() != 0) {
                    ajax.put('api/bodyshop/' + $("#id").val(), data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información actualizada con éxito";
                            utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url +
                                    'bodyshop/listado';
                            })
                        })
                } else {
                    ajax.post('api/bodyshop', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información guardada con éxito";
                            utils.displayWarningDialog("Información guardada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url +
                                    'bodyshop/listado';
                            })
                        })
                }
            }

        })
        $("#fecha_cita").on("change", function() {
            getTiempoCita()
        });

        function getTiempoCita() {
            var url = site_url + "/bodyshop/getTiempo";
            fecha = $("#fecha_cita").val();
            id_asesor = $("#id_asesor").val();
            if (fecha != '' && id_asesor != '') {
                ajaxJson(url, {
                    "fecha": fecha,
                    "id_asesor": id_asesor,
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.length != 0) {
                        $("#hora_cita").empty();
                        $("#hora_cita").removeAttr("disabled");
                        $("#hora_cita").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#hora_cita").append("<option value= '" + result[i] + "'>" +
                                result[i]
                                .substring(0, 5) + "</option>");
                        });
                    } else {
                        $("#hora_cita").empty();
                        $("#hora_cita").append("<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {
                $("#hora_cita").empty();
                $("#hora_cita").append("<option value=''>-- Selecciona --</option>");
                $("#hora_cita").attr("disabled", true);
            }
        }
    </script>
@endsection
