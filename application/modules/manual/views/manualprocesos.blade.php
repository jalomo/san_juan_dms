@layout('tema_luna/layout')
@section('contenido')



<div class="container-fluid">

<div class="row">
    <div class="col-lg-12">
        <div class="view-header">
            
            <div class="header-icon">
                <i class="pe page-header-icon pe-7s-bookmarks"></i>
            </div>
            <div class="header-title">
                <h3>Manual de procesos</h3>
                <small>
                    Many panels design give you many possibilities.
                </small>
            </div>
        </div>
        <hr>
    </div>
</div>


<div class="row">

    <div class="col-md-12">

        <div class="panel panel-filled">

            <div class="panel-body">

                <h3>¿En qué podemos ayudarte? </h3>

                <p>
                También puedes revisar los siguientes temas para encontrar la información que buscas.
                </p>

                <div class="form-group">
                    <input class="form-control" placeholder="Escribe palabras de búsqueda">
                </div>

                <div class="searh" id="search">
                    <table class="search-table" id="search-table">
                        
                    </table>
                </div>

            </div>

        </div>

    </div>

</div>
<!-- Fin de buscador-->

<!--Linea de procesos-->

    <!--Areas-->



<div class="row">
    <div class="col-md-4">
               
        <div class="panel-body" style="display: block;">
                <div class="panel-group">
                <ul class="list-unstyled nav nav-tabs">
                    <li class="panel panel-filled support-question" style="display: fleX; width: 100%;">
                        <a href="#Refacciones" data-toggle="tab" class="nav-link active" style="width: 100%;">
                            <div class="panel-body" >
                                <p class="font-bold c-white">Movimientos</p>
                                <p>
                                    Almacen: Productos
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="panel panel-filled support-question" style="display: fleX; width: 100%;">
                        <a href="#Autos" data-toggle="tab" class="nav-link" style="width: 100%;">
                            <div class="panel-body" >
                                <p class="font-bold c-white">Movimientos</p>
                                <p>
                                    Almacen: Traspaso
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="panel panel-filled support-question" style="display: fleX; width: 100%;">
                        <a href="#" data-toggle="tab" class="nav-link" style="width: 100%;">
                            <div class="panel-body" >
                                <p class="font-bold c-white">Movimientos</p>
                                <p>
                                    Almacen: Reemplazos
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="panel panel-filled support-question" style="display: fleX; width: 100%;">
                        <a href="#answer1" data-toggle="tab" class="nav-link" style="width: 100%;">
                            <div class="panel-body" >
                                <p class="font-bold c-white">Movimientos</p>
                                <p>
                                    Salidas: Ventas Mostrador
                                </p>
                            </div>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
        
    </div>



                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="Refacciones" class="tab-pane active">
                                    <h3>
                                        Refacciones
                                    </h3>
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.
                                    </p>
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet, comes from</li>
                                        <li>There are many variations of passages of Lorem Ipsum available</li>
                                        <li>All the Lorem Ipsum generators on the Internet</li>
                                    </ul>
                                    <p class="font-bold c-white">
                                        Contrary to popular belief, Lorem Ipsum is not
                                    </p>
                                    <p>
                                        McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.
                                    </p>
                                    <p>
                                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                    </p>
                                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                    </p>

                                </div>

                                <div id="Autos" class="tab-pane">
                                    <h3>
                                        Autos
                                    </h3>
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.
                                    </p>
                                    <ul>
                                        <li>Lorem ipsum dolor sit amet, comes from</li>
                                        <li>There are many variations of passages of Lorem Ipsum available</li>
                                        <li>All the Lorem Ipsum generators on the Internet</li>
                                        <li>Lorem ipsum dolor sit amet, comes from</li>
                                        <li>All the Lorem Ipsum generators on the Internet</li>
                                        <li>There are many variations of passages of Lorem Ipsum</li>
                                        <li>All the Lorem Ipsum generators on the Internet</li>
                                    </ul>
                                    <p class="font-bold c-white">
                                        Contrary to popular belief, Lorem Ipsum is not
                                    </p>
                                    <p>
                                        McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.
                                    </p>
                                    <p>
                                        McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.
                                    </p>

                                </div>
                                
                    </div>




                </div>

            </div>
    </div>
    <!-- End main content-->
</div>

@endsection