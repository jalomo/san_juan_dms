<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Productos extends MX_Controller
{
  public $modulo;
  public $seccion;
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('general');
    $this->load->library('curl');
    $this->modulo = ucwords($this->uri->segment(1));
    $this->seccion = ucwords($this->uri->segment(2));

    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
    return $this->listado();
  }

  public function construccion()
  {

    $this->blade->render('productos/construccion');
  }

  public function crear()
  {

    $data['titulo'] = "Alta producto";
    $cat_talleres = $this->curl->curlGet('api/talleres');
    $cat_almacenes = $this->curl->curlGet('api/almacen');
    $ubicacion_producto = $this->curl->curlGet('api/catalogo-ubicacion-producto');

    $data['cat_almacenes'] = procesarResponseApiJsonToArray($cat_almacenes);
    $data['cat_talleres'] = procesarResponseApiJsonToArray($cat_talleres);
    $data['cat_ubicaciones'] = procesarResponseApiJsonToArray($ubicacion_producto);

    $this->blade->render('productos/formulario_alta', $data);
  }

  public function polizaProducto($id_producto)
  {
    $data['titulo'] = "ventas mostradorr";
    $detalle_producto = $this->curl->curlGet('api/productos/' . $id_producto);
    $detalle = procesarResponseApiJsonToArray($detalle_producto);
    // $folio_data = $this->curl->curlGet('api/ventas/venta-by-folio/' . $id_producto);
    // $data['folio'] = procesarResponseApiJsonToArray($folio_data);
    $data['producto'] = isset($detalle) ? $detalle : [];

    $view = $this->load->view('polizas/poliza_producto/template', $data, true);
    $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
  }

  public function ficha_tecnica($id = '')
  {

    $detalle_producto = $this->curl->curlGet('api/productos/' . $id);
    $params['data'] = procesarResponseApiJsonToArray($detalle_producto);


    $data = $this->curl->curlGet('api/productos/' . $id);

    $ventas = $this->curl->curlGet('api/ventas/ventas-agrupadas-producto?producto_id=' . $id . '&year=' . date('Y') . '&cantidad_meses=' . date('m'));
    $categoriaSugeridos = $this->curl->curlGet('api/pedido-sugerido/byproducto/' . $id);

    $datos['data'] = procesarResponseApiJsonToArray($data);
    $datos['titulo'] = "Ficha tecnica";
    $cat_precios = $this->curl->curlGet('api/precios');
    $cat_talleres = $this->curl->curlGet('api/talleres');
    $cat_almacenes = $this->curl->curlGet('api/almacen');

    $datos['cat_almacenes'] = procesarResponseApiJsonToArray($cat_almacenes);
    $datos['cat_talleres'] = procesarResponseApiJsonToArray($cat_talleres);
    $datos['precios'] = procesarResponseApiJsonToArray($cat_precios);
    $datos['ventas'] = procesarResponseApiJsonToArray($ventas);
    $datos['categoria_sugerido'] = procesarResponseApiJsonToArray($categoriaSugeridos);
    $dataFromApi = $this->curl->curlGet('api/productos/stockByProductoId/' . $id);
    $datos['stock'] = procesarResponseApiJsonToArray($dataFromApi);
    $this->blade->render('productos/ficha_tecnica', $datos);
  }

  public function editar($id = '')
  {
    if ($id) {
      $data = $this->curl->curlGet('api/productos/' . $id);
      $cat_talleres = $this->curl->curlGet('api/talleres');
      $cat_almacenes = $this->curl->curlGet('api/almacen');
      $ubicacion_producto = $this->curl->curlGet('api/catalogo-ubicacion-producto');

      $dataFromApi = $this->curl->curlGet('api/productos/stockByProductoId/' . $id);
      $datos['stock'] = procesarResponseApiJsonToArray($dataFromApi);

      $datos['cat_almacenes'] = procesarResponseApiJsonToArray($cat_almacenes);
      $datos['cat_talleres'] = procesarResponseApiJsonToArray($cat_talleres);
      $datos['cat_ubicaciones'] = procesarResponseApiJsonToArray($ubicacion_producto);

      $datos['data'] = procesarResponseApiJsonToArray($data);
      $datos['titulo'] = "Editar producto";

      $this->blade->render('productos/formulario_alta', $datos);
    }
  }

  public function testSession()
  {

    echo "<pre>";
    print_r(
      $this->session->all_userdata()
    );
    echo "</pre>";
  }

  public function ajax_aplicar_precio()
  {
    $precio_factura = $this->input->post('precio_factura');
    $id_precio = $this->input->post('id_precio');
    $id_usuario_autoriza = $this->input->post('id_usuario_autoriza');
    if (isset($id_usuario_autoriza)) {
      $this->session->unset_userdata('id_precio');
      $this->session->set_userdata([
        'id_autoriza_venta' => $id_usuario_autoriza,
        'id_precio' => $id_precio
      ]);
    }

    $cat_precios = $this->curl->curlGet('api/precios/' . $id_precio);
    $precios = procesarResponseApiJsonToArray($cat_precios);

    $data =  [
      'precio_publico' => floatval(obtenerPorcentaje($precios->precio_publico, $precio_factura)),
      'precio_mayoreo' => floatval(obtenerPorcentaje($precios->precio_mayoreo, $precio_factura)),
      'precio_interno' => floatval(obtenerPorcentaje($precios->precio_interno, $precio_factura)),
      'precio_taller' => floatval(obtenerPorcentaje($precios->precio_taller, $precio_factura)),
      'precio_otras_distribuidoras' => floatval(obtenerPorcentaje($precios->precio_otras_distribuidoras, $precio_factura)),
      'impuesto' => floatval(obtenerPorcentaje($precios->impuesto, $precio_factura)),
    ];
    $data['porcentaje']  = $precios;

    echo json_encode($data);
  }

  public function listado()
  {
    $this->load->helper('general');
    $this->load->library('curl');
    $dataFromApi = $this->curl->curlGet('api/productos/listadoStock');
    $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);
    $data['precios'] = $this->curl->curlGet('api/precios');
    $this->blade->render('productos/listado', $data);
  }

  public function stock()
  {
    $this->load->helper('general');
    $this->load->library('curl');
    $dataFromApi = $this->curl->curlGet('api/productos/stockActual?compra_realizada=true');
    $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);
    $this->blade->render('productos/stock_productos', $data);
  }

  public function detalle($id)
  {
    if ($id) {
      $this->load->library('curl');
      $this->load->helper('general');
      $dataFromApi = $this->curl->curlGet('api/productos/stockActual?compra_realizada=true&producto_id=' . $id);
      $decode_producto = procesarResponseApiJsonToArray($dataFromApi);
      $data['data'] = count($decode_producto) > 0 ? $decode_producto[0] : [];
      $data['readOnly'] = true;
      $data['titulo'] = 'Detalle de producto';
      $this->blade->render('productos/readonly_producto', $data);
    }
  }

  public function getDataProductos()
  {
    $this->load->library('curl');
    $data = $this->curl->curlGet('api/productos');
    $array = [];
    foreach (json_decode($data) as $key => $value) {
      $prueba  = [
        'id' => $value->id,
        'no_identificacion' => $value->no_identificacion,
        'clave_unidad' => $value->clave_unidad,
        'clave_prod_serv' => $value->clave_prod_serv,
        'descripcion' => $value->descripcion,
        'unidad' => $value->unidad,
        'cantidad' => $value->cantidad,
        'precio' => $value->precio,
        'inventariable' => $value->inventariable,
        'existencia' => $value->existencia,
        'factura_id' => $value->factura_id
      ];

      array_push($array, $prueba);
    }
    $datatable['data'] = $array;
    echo json_encode($datatable);
  }
  public function registroProductos()
  {

    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('producto_concepto', 'Nombre', 'required');
    $this->form_validation->set_rules('producto_id_sat', 'codigo sat', 'required');
    $this->form_validation->set_rules('descripcion', 'descripcion producto', 'required');
    $this->form_validation->set_rules('unidad', 'unidad producto', 'required');
    $this->form_validation->set_rules('inventariable', 'definir inventariable', 'required');
    $this->form_validation->set_rules('precio_id', 'precio', 'required');
    $this->form_validation->set_rules('existencia', 'existencia', 'required');
    if ($this->form_validation->run() == FALSE) {

      $data['errors'] = validation_errors_array();
      $data['status'] = false;
      echo json_encode($data);
      die();
    }
    $data['status'] = true;
    echo json_encode($data);
    die();
  }

  public function subirFactura()
  {
    $data['titulo'] = "Subir factura";
    $this->blade->render('productos/uploadfactura', $data);
  }

  public function busquedaFoliosTipoCuenTa()
  {
    $dataFromApi = $this->curl->curlGet('api/catalogo-cuentas');
    $cuentas = procesarResponseApiJsonToArray($dataFromApi);
    $data['cat_cuentas'] = count($cuentas) > 0 ? $cuentas : [];
    $data['titulo'] = "Consultas";
    $this->blade->render('productos/consultas/formulario_busqueda', $data);
  }

  public function ajax_search_by_folios()
  {
    if ($this->input->post('folio') == '') {
      $data['data'] = [];
      echo json_encode($data);
      die();
    }

    $folio_data = $this->curl->curlGet('api/folio/get-by-folio/' . $this->input->post('folio'));
    $detalle = procesarResponseApiJsonToArray($folio_data);

    $data['data'] = count($detalle) > 0 ? $detalle : [];
    echo json_encode($data);
  }

  public function pedidoSugerido()
  {
    $data['titulo'] = "Pedido sugerido";
    $data['bread_active'] = "Pedido sugerido";
    $this->blade->render('productos/pedido_sugerido/pedido_sugerido', $data);
  }

  public function generarpedidosugerido()
  {
    $data['titulo'] = "Crear Pedido";
    $data['bread_active'] = "Pedido";
    $listado_productos = $this->curl->curlGet('api/pedido-sugerido/listadoproductos');
    $detalle = procesarResponseApiJsonToArray($listado_productos);
    $data['listado_productos'] = isset($detalle)  ? $detalle : [];

    $tipo_pedido = $this->curl->curlGet('api/tipopedido');
    $detalle_tipo_pedido = procesarResponseApiJsonToArray($tipo_pedido);
    $data['tipo_pedido'] = isset($detalle_tipo_pedido)  ? $detalle_tipo_pedido : [];


    $catalogo_proveedor = $this->curl->curlGet('api/catalogo-proveedor');
    $proveedores = procesarResponseApiJsonToArray($catalogo_proveedor);
    $data['catalogo_proveedor'] = isset($proveedores)  ? $proveedores : [];

    $this->blade->render('productos/pedido_sugerido/generar_pedido', $data);
  }

  public function ajax_listado_pedido()
  {
    $listado_productos = $this->curl->curlGet('api/pedido-sugerido/listadoproductos');
    $detalle = procesarResponseApiJsonToArray($listado_productos);
    // $data['listado_productos'] = isset($detalle)  ? $detalle : [];
    $data['data'] = isset($detalle) ? $detalle : [];
    echo json_encode($data);
  }

  public function generarpedido()
  {
    // # code...
    // $length = $this->input->post('productos_length');
    // for ($i=0; $i < $length; $i++) { 
    //   # code...
    //   print_r($i);
    // }
    dd($this->input->post());
  }

  public function ajax_pedido_sugerido()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_sugerido_inactivo()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido/inactivo');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_sugerido_obsoleto()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido/obsoleto');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_sugerido_estancado()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido/estancado');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_sugerido_potencial_obsoleto()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido/potencial-obsoleto');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_sugerido_lento()
  {
    $pedido = $this->curl->curlGet('api/pedido-sugerido/lento');
    $data = procesarResponseApiJsonToArray($pedido);

    $data['data'] = isset($data) ? $data : [];
    echo json_encode($data);
  }

  public function ajax_inventario_producto()
  {
    $query = [];
    if ($this->input->post('descripcion') != '') {
      $query['descripcion'] = $this->input->post('descripcion');
    }

    if ($this->input->post('inventario_id') != '') {
      $query['inventario_id'] = $this->input->post('inventario_id');
    }

    $inventario = $this->curl->curlGet('api/inventario-producto/buscar-productos?' . http_build_query($query));
    $response = procesarResponseApiJsonToArray($inventario);
    $data['data'] = isset($response) ? $response : [];
    echo json_encode($data);
  }

  public function pdfreporteInventario($inventario_id)
  {

    $inventario = $this->curl->curlGet('api/inventario-producto/buscar-productos?inventario_id=' . $inventario_id);
    $response = procesarResponseApiJsonToArray($inventario);
    $data['data'] = isset($response) ? $response : [];
    // $this->load->view('productos/inventario/pdfinventario', $data);

    $query['inventario_id'] = $inventario_id;
    $inventario = $this->curl->curlGet('api/inventario-producto/buscar-productos?' . http_build_query($query));
    $responseInventario = procesarResponseApiJsonToArray($inventario);
    $resultados = [];
    $perdida = 0;
    $residuos = 0;
    foreach ($responseInventario as $key => $elemento) {
      if ($elemento->diferencia < 0) {
        $perdida +=  abs($elemento->diferencia) * $elemento->valor_unitario;
        $resultados['perdidas'] = $perdida;
      } else {
        $residuos +=  $elemento->diferencia * $elemento->valor_unitario;
        $resultados['residuos'] = $residuos;
      }
    }
    $data['resultados'] = $resultados;

    $view = $this->load->view('productos/inventario/pdfinventario', $data, TRUE);
    $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
  }

  public function inventario($inventario_id = '')
  {

    if ($inventario_id == '') {
      redirect(site_url() . 'refacciones/productos/seleccionaInventario');
    }

    $existe_inventario = $this->curl->curlGet('api/inventario/validarid/' . $inventario_id);
    $responseInventario = procesarResponseApiJsonToArray($existe_inventario);

    if (empty($responseInventario)) {
      redirect(site_url() . 'refacciones/productos/seleccionaInventario');
    }
    $inventario_API = $this->curl->curlGet('api/inventario/' . $inventario_id);
    $response_inventrio = procesarResponseApiJsonToArray($inventario_API);
    $data['inventario'] = $response_inventrio;
    $data['data'] = isset($data) ? $data : [];
    $data['titulo'] = "Inventario";
    $data['bread_active'] = "Inventario";
    $data['inventario_id'] = $inventario_id;
    $data['usuario_id'] = $this->session->userdata('id');
    $this->blade->render('productos/inventario/inventario', $data);
  }

  public function reporteInventario($inventario_id = '')
  {
    if ($inventario_id == '') {
      redirect(site_url() . 'refacciones/productos/seleccionaInventario');
    }

    $data['titulo'] = "Inventario";
    $data['bread_active'] = "Inventario";
    $data['usuario_id'] = $this->session->userdata('id');
    $data['inventario_id'] = $inventario_id;


    $query['inventario_id'] = $inventario_id;
    $inventario = $this->curl->curlGet('api/inventario-producto/buscar-productos?' . http_build_query($query));
    $responseInventario = procesarResponseApiJsonToArray($inventario);
    $resultados = [];
    $perdida = 0;
    $residuos = 0;
    foreach ($responseInventario as $key => $elemento) {
      if ($elemento->diferencia < 0) {
        $perdida +=  abs($elemento->diferencia) * $elemento->valor_unitario;
        $resultados['perdidas'] = $perdida;
      } else {
        $residuos +=  $elemento->diferencia * $elemento->valor_unitario;
        $resultados['residuos'] = $residuos;
      }
    }
    $data['resultados'] = $resultados;
    $this->blade->render('productos/inventario/index', $data);
  }

  public function detalleproductoinventario($inventario_id = '', $id_producto = '')
  {
    if ($inventario_id == '' && $id_producto == '') {
      redirect(site_url() . 'refacciones/productos/seleccionaInventario');
    }

    $data['titulo'] = "Detalle";
    $data['bread_active'] = "Inventario";
    $query['producto_id'] = $id_producto;
    $query['inventario_id'] = $inventario_id;
    $pedido = $this->curl->curlGet('api/inventario-producto/buscar-productos?' . http_build_query($query));
    $data_pedido = procesarResponseApiJsonToArray($pedido);
    $data['detalle_producto'] = isset($data_pedido) ? $data_pedido[0] : [];

    $this->blade->render('productos/inventario/detalle', $data);
  }

  public function seleccionaInventario()
  {
    $data['titulo'] = "Inventarios";
    $data['bread_active'] = "Inventario";
    $data['usuario_id'] = $this->session->userdata('id');
    $dataEstatusApi = $this->curl->curlGet('api/catalogo-estatus-inventario');
    $data['estatus_inventario'] = procesarResponseApiJsonToArray($dataEstatusApi);
    $this->blade->render('productos/inventario/listado_inventario', $data);
  }

  public function ajax_mainventario()
  {
    $inventario = $this->curl->curlGet('api/inventario');
    $data_inventario = procesarResponseApiJsonToArray($inventario);
    $data['data'] = isset($data_inventario) ? $data_inventario : [];
    echo json_encode($data);
  }

  public function pedidopiezas($pedido_id = '')
  {
    $data['titulo'] = "Pedido de piezas";
    $data['bread_active'] = "pedido";
    $data['pedido_id'] = $pedido_id;
    $dataFromApi = $this->curl->curlGet('api/productos/stockActual?compra_realizada=true');

    $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);

    $catalogoProveedor = $this->curl->curlGet('api/catalogo-proveedor');
    $data['proveedores'] = procesarResponseApiJsonToArray($catalogoProveedor);

    $infopedido = $this->curl->curlGet('api/masterpedidoproducto/' . $pedido_id);
    $data['infopedido'] = procesarResponseApiJsonToArray($infopedido);

    $this->blade->render('productos/pedidos/index', $data);
  }

  public function crearPedido()
  {
    $data['titulo'] = "Crear pedido de piezas";
    $data['bread_active'] = "pedido";
    $this->blade->render('productos/pedidos/crearpedido', $data);
  }

  public function reportePedidopiezas($pedido_id = '')
  {
    $query = [];
    if ($pedido_id !== '') {
      $query['ma_pedido_id'] = $pedido_id;
    }

    $dataFromApi = $this->curl->curlGet('api/pedidoproducto/listado?' . http_build_query($query));
    $parse = procesarResponseApiJsonToArray($dataFromApi);
    $data['listado'] = isset($parse->data) ? $parse->data : [];
    $view = $this->load->view('productos/pedidos/reporte_pedido', $data, true);
    $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
  }

  public function actualizarprecios()
  {
    $data['titulo'] = "Actualizar precios";
    
    $this->blade->render('productos/inventario/actualizar_precios_archivo', $data);
  }
} //FIN CONTROLADOR
