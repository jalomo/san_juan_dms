@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : '' }}</li>
    </ol>
    <h4>Estatus de venta : <?php echo isset($folio) && isset($folio[0]->re_ventas_estatus) ?
                                $folio[0]->re_ventas_estatus->estatus_venta->nombre : ''; ?></h4>
    <div class="row mt-2">
        <div class="col-md-4">
            <?php echo renderInputText('text', 'folio', 'Folio', isset($folio) ? $folio[0]->folio->folio
                : '', true); ?>
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->folio_id : '' }}" id="folio_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->id : '' }}" id="venta_id">
            <input type="hidden" value="{{ isset($folio[0]->re_ventas_estatus) ? $folio[0]->re_ventas_estatus->estatus_venta->id : '' }}" id="estatus_venta_id">
            <input type="hidden" value="{{ isset($folio) ? $folio[0]->almacen_id : '' }}" name="almacen_id" id="almacen_id">
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'venta_total', 'Total', isset($venta_total[0]->venta_total) ? $venta_total[0]->venta_total  : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText('text', 'cliente', 'Cliente', isset($folio) &&
                isset($folio[0]->cliente) ? $folio[0]->cliente->nombre : '', true); ?>
            <input type="hidden" value="{{ isset($folio) && $folio[0]->cliente ? $folio[0]->cliente->id : '' }}" id="cliente_id" name="cliente_id">
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Concepto</label>
                <textarea name="concepto" id="concepto" class="form-control" rows="3" style="min-height:100px" maxlength="500"><?php echo isset($cxc->concepto) ? $cxc->concepto : ''; ?></textarea>
                <div id='concepto_error' class='invalid-feedback'></div>
            </div>
        </div>
      
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="select">Cobrador asignar</label>
                <select name="usuario_gestor_id" class="form-control " id="usuario_gestor_id">
                    <option value=""> Seleccionar</option>
                    @foreach ($gestores as $gestor)
                    @if(isset($cxc->usuario_gestor_id) && $cxc->usuario_gestor_id == $gestor->id)
                    <option value="{{ $gestor->id}}" selected="selected"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @else
                    <option value="{{ $gestor->id}}"> {{ $gestor->nombre }} - {{ $gestor->apellido_paterno}} </option>
                    @endif
                    @endforeach
                </select>
                <div id='usuario_gestor_id_error' class='invalid-feedback'></div>
            </div>
        </div>
    </div>
    <div class="row mb-4 mt-4">
        <div class="col-md-12 text-right">            
            <?php if (isset($folio) && $folio[0]->re_ventas_estatus->estatus_ventas_id == 2) { ?>
                <a target="_blank" class="btn btn-primary col-md-3" id="btn-poliza" href="{{ base_url('refacciones/polizas/generarPolizaVentasServicioPDF?folio_id='.$folio[0]->folio_id) }}">
                    <i class="fas fa-print"></i> Poliza
                </a>
            <?php } ?>

            <?php if (isset($folio) && $folio[0]->re_ventas_estatus->estatus_ventas_id == 1) { ?>
                <button class="btn btn-primary col-md-4" id="btn-finalizar"><i class="fas fa-shopping-cart"></i>
                    Procesar</button>
            <?php } ?>
        </div>
    </div>
    <br>
    <h3>Productos seleccionados</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Total</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Total</th>
                            <th>Valor unitario</th>
                            <th>Cantidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    $('#tbl_productos').DataTable({
        language: {
            url: PATH_LANGUAGE
        }
    })
    let estatus_venta_id = $('#estatus_venta_id').val();
    var tabla_carrito = $('#tbl_carrito').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "refacciones/salidas/ajax_detalle_venta_mpm",
            type: 'POST',
            data: {
                id: function() {
                    return $('#venta_id').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
                        .no_identificacion : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.descripcion_producto) && data.descripcion_producto ?
                        data
                        .descripcion_producto : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.venta_total) ? "$ " + data.venta_total : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
                }
            },
            {
                'data': function(data) {
                    return utils.isDefined(data.unidad) && data.unidad ? data.unidad : '--'
                }
            },
            {
                'data': function(data) {
                    return '--';
                }
            }
        ]
    });

    $("#btn-finalizar").on("click", function() {
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });
        let continuar = false;
        let table_carrito_length = $("#tbl_carrito").dataTable().fnSettings().aoData.length
        if (table_carrito_length == 0) {
            utils.displayWarningDialog("Seleccionar elementos para comprar");
            return false;
        }
        
        realizarVenta();
    });

    function realizarVenta() {
        $.isLoading({
            text: "Realizando proceso de venta ...."
        });
        let id_venta = $('#venta_id').val();
        let concepto = '';
        if ($("#concepto").val()) {
            concepto = 'Taller - ' + $('#concepto').val();
        }
        console.log(concepto);
        ajax.put(`api/ventas/finalizar/${id_venta}`, {
            folio_id: $('#folio_id').val(),
            venta_total: $('#venta_total').val(),
            tipo_forma_pago_id: 1,
            concepto: concepto,
            tipo_pago_id: 1,
            cliente_id: $('#cliente_id').val(),
            plazo_credito_id: 14,
            enganche:  $('#venta_total').val(),
            tasa_interes: 0,//$('#tasa_interes').val(),
            usuario_gestor_id: $('#usuario_gestor_id').val()
        }, function(response, headers) {
            console.log(response);
            console.log(headers);
            if (headers.status == 201 || headers.status == 200) {
                toastr.info('Realizando proceso de venta')
                return this.addReVentasEstatus(id_venta); //Cambia estatus a vendido
            } else {
                $.isLoading("hide");

            }
        })

    }



    function addReVentasEstatus(id_venta) {
        ajax.post('api/re-estatus-venta', {
            ventas_id: id_venta,
            estatus_ventas_id: 2,
        }, function(response, headers) {
            if (headers.status == 201) {
                let titulo = "La venta se efectuo correctamente!"
                var productosAgregados = $('#tbl_carrito').DataTable();
                toastr.info("Actualizando inventario espere un momento....");
                let total = productosAgregados.data().count();
                let count = 0;
                $(productosAgregados.data()).each(function(index, product) {
                    count++;
                    if (product.producto_id) {
                        ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + product
                            .producto_id, {},
                            function(response, headers) {
                                if (headers.status == 200) {
                                    // count++;
                                    toastr.info("Inventario actualizado para el producto " + product
                                        .descripcion_producto);

                                    // if (total == count) {
                                    //     $.isLoading("hide");
                                    //     utils.displayWarningDialog(titulo, 'success', function(
                                    //         result) {
                                    //         window.location.reload();
                                    //     });
                                    // }
                                }
                            })

                    }

                    if (total == count) {
                        $.isLoading("hide");
                        utils.displayWarningDialog(titulo, 'success', function(result) {
                            window.location.reload();
                        });
                    }
                });



            }
        })
    }
</script>
@endsection