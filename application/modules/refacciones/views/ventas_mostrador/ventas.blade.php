@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item active">{{ isset($titulo) ? $titulo : "" }}</li>
	</ol>
	<div class="row-fluid">
		<div class="col-md-12 text-right">
			<a class="btn btn-primary" href="{{ base_url('refacciones/salidas/ventasMostrador') }}">
				<i class="fa fa-arrow-left"></i> Regresar</a>
		</div>
		<div class="col-md-12 mt-3">
			<form id="frm-producto">
				<div
					style="border:2px solid #ccc; background-color:#f9f9f9; padding:10px; border-radius:6px; margin-bottom:10px;">
					<h4>Datos del producto</h4>
					<div class="row">
						<div class="col-md-6">
							<?php echo renderInputText("text", "descripcion", "Descripcion", isset($producto->descripcion) ? $producto->descripcion : '', true); ?>
							<input type="hidden" name="producto_id" id="producto_id" value="{{ $producto->id}}">
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("text", "unidad", "Unidad", isset($producto->unidad) ? $producto->unidad : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("text", "no_identificacion", "No de producto", isset($producto->no_identificacion) ? $producto->no_identificacion : '', true); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<?php echo renderInputText("text", "ubicacion_producto", "Ubicación producto", isset($producto->ubicacion->nombre) ? $producto->ubicacion->nombre : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia", "Existencia total", isset($producto->desglose_producto->cantidad_actual) ? $producto->desglose_producto->cantidad_actual : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia_almacen_principal", "Existencia Almacen principal", isset($producto->desglose_producto->cantidad_almacen_primario) ? $producto->desglose_producto->cantidad_almacen_primario : '', true); ?>
						</div>
						<div class="col-md-3">
							<?php echo renderInputText("number", "existencia_almacen_secundario", "Existencia Almacen secundario", isset($producto->desglose_producto->cantidad_almacen_secundario) ? $producto->desglose_producto->cantidad_almacen_secundario : '', true); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<?php echo renderInputText("text", "nombre_taller", "Taller", isset($producto->taller) ? $producto->taller->nombre : '', true); ?>
						</div>
						<div class="col-md-6">
							<?php echo renderInputText("text", "ubicacion", "Ubicación taller", isset($producto->taller) ? $producto->taller->ubicacion : '', true); ?>
						</div>
					</div>
				</div>
				<h4 class="mt-3 ">Datos para la venta</h4>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="select">Tipo cliente</label>
							<select name="tipo_cliente_id" class="form-control " id="tipo_cliente_id">
								<option value=""> Seleccionar</option>
								@foreach ($cat_tipo_clientes as $tipo)
								<option value="{{ $tipo->id}}"> {{ $tipo->nombre}} </option>
								@endforeach
							</select>
							<div id='tipo_cliente_id_error' class='invalid-feedback'></div>
						</div>
					</div>
					<div class="row form-registrar-cliente-esporadico"></div>
					<div class="col-md-6 cliente-registrado">
						<div class="form-group">
							<label for="select">Cliente</label>
							<select name="cliente_id" class="form-control" id="cliente_id">
								<option value=""> Seleccionar cliente</option>
								@foreach ($cat_clientes as $cliente)
								<option value="{{ $cliente->id}}">{{ $cliente->numero_cliente}} - {{ $cliente->nombre}} | <?php echo $cliente->aplica_credito && $cliente->aplica_credito == true ? 'Con credito' : 'Sin credito'; ?>
								</option>
								@endforeach
							</select>

							<div id='cliente_id_error' class='invalid-feedback'></div>
						</div>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-md-6">
						<?php echo renderInputText("number", "precio_factura", "Valor unitario", isset($producto->valor_unitario) ? $producto->valor_unitario : '', true); ?>
					</div>
					<div class="col-md-6">
						<?php echo renderInputText("number", "cantidad", "Cantidad", 0); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="select">Aplicar precio</label>
					<select name="precio_id" class="form-control" id="precio_id">
						@foreach ($cat_precios as $precio)
						@if (isset($producto->precio_id) && $producto->precio_id == $precio->id)
						<option selected value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
						@else
						<option value="{{ $precio->id}}"> {{ $precio->descripcion}} </option>
						@endif
						@endforeach
					</select>
					<div id='precio_id_error' class='invalid-feedback'></div>
				</div>
				<div class="row">
					<div class="col-md-4 mt-2">
						<label>Precio al publico <span
								class="porcentaje_publico"><?php echo '<b>' . $producto->rel_precio->precio_publico . "%</b>"; ?></span>
							&nbsp;<input type="radio" name="tipo_precio[]" id="tipo_precio" data-precio="precio_publico"
								value="1" /></label>
						<input type="text" name="precio_publico" id="precio_publico" class="form-control" value="" />
						<div id='tipo_precio_id_error' class='invalid-feedback'></div>
					</div>
					<div class="col-md-4 mt-2">
						<label>Precio mayoreo <span
								class="porcentaje_mayoreo"><?php echo '<b>' . $producto->rel_precio->precio_mayoreo . "%</b>"; ?></span>
							&nbsp;<input type="radio" name="tipo_precio[]" id="tipo_precio" data-precio="precio_mayoreo"
								value="2" /></label>
						<input type="text" name="precio_mayoreo" id="precio_mayoreo" class="form-control"
							value="" />
					</div>
					<div class="col-md-4 mt-2">
						<label>Precio interno <span
								class="porcentaje_interno"><?php echo '<b>' . $producto->rel_precio->precio_interno . "%</b>"; ?></span>
							&nbsp;<input type="radio" name="tipo_precio[]" id="tipo_precio" data-precio="precio_interno"
								value="3" /></label>
						<input type="text" name="precio_interno" id="precio_interno" class="form-control" value="" />
					</div>
					<div class="col-md-4 mt-4">
						<label>Precio taller <span
								class="porcentaje_taller"><?php echo '<b>' . $producto->rel_precio->precio_taller . "%</b>"; ?></span>
							&nbsp;<input type="radio" name="tipo_precio[]" id="tipo_precio" data-precio="precio_taller"
								value="4" /></label>
						<input type="text" name="precio_taller" id="precio_taller" class="form-control" value="" />
					</div>
					<div class="col-md-4 mt-4">
						<label>Precio otras distribuidoras <span
								class="porcentaje_otras_distribuidoras"><?php echo '<b>' . $producto->rel_precio->precio_otras_distribuidoras . "%</b>"; ?></span>
							&nbsp;<input type="radio" id="tipo_precio" name="tipo_precio[]"
								data-precio="precio_otras_distribuidoras" value="5" /></label>
						<input type="text" name="precio_otras_distribuidoras" id="precio_otras_distribuidoras"
							class="form-control" value="" />
					</div>
					<div class="col-md-4 mt-4">
						<label>Impuesto <span
								class="porcentaje_impuesto"><?php echo '<b>' . $producto->rel_precio->impuesto . "%</b>"; ?></span>
							&nbsp;<input type="radio" name="tipo_precio[]" id="tipo_precio" data-precio="impuesto"
								value="6" /></label>
						<input type="text" name="impuesto" id="impuesto" class="form-control" value="" />
					</div>
				</div>
				<input type="hidden" id="status" name="status" value="2">
				<input type="hidden" id="almacen_id" name="almacen_id" value="{{ AlmacenPrincipal }}">
				
				<input type="hidden" id="vendedor_id" name="vendedor_id" value="{{ $vendedor_id }}">
				<input type="hidden" value="<?php echo isset($tipo_venta_id) ? $tipo_venta_id : false; ?>"
					name="tipo_venta_id" id="tipo_venta_id">
				<div class="row mt-5 mb-4">
					<div class="col-md-12 text-right">
						<button class="btn btn-primary col-md-4" id="agregar_vender" type="button"> <i
								class="fas fa-shopping-cart"></i> Continuar </button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
	$(document).ready(function () {
		$("#cliente_id").select2();
		$(".form-registrar-cliente-esporadico").hide();
		$(".cliente-registrado").hide();


		$("#tipo_cliente_id").on("change", function () {
			if ($("#tipo_cliente_id").val() == '') {
				$(".form-registrar-cliente-esporadico").hide();
				$(".cliente-registrado").hide();
			} else if ($("#tipo_cliente_id").val() == 1) {
				utils.displayWarningDialog("¿Desea ir a Registrar el cliente?", null, function (data) {
					if (data.value) {
						return window.location.href = base_url +
							'catalogos/clientesController/crear';
					}
				}, true);
			} else if ($("#tipo_cliente_id").val() == 2) {
				$(".form-registrar-cliente-esporadico").hide();
				$(".cliente-registrado").show();
			} else {
				$(".form-registrar-cliente-esporadico").hide();
				$(".cliente-registrado").hide();
			}
		});



		$("#agregar_vender").on("click", function () {
			if ($("#cantidad").val() == 0) {
				return utils.displayWarningDialog("Indicar numero de piezas", "warning", function (
					data) {})
			}

			if (document.getElementById("cliente_id").value == '' && $('.hidden-dinamic-client-id')
				.val() == '') {
				$("#tipo_cliente_id_error").text('seleccionar tipo de cliente').css('display', 'block');
				$('html,body').animate({
					scrollTop: $('#tipo_cliente_id_error').offset().top
				}, 1000);
				return false;
			}

			let form = $("#frm-producto").serializeArray();
			$(".invalid-feedback").html("");
			let dinamic_client_id = document.getElementById("cliente_id").value !== '' ? document
				.getElementById("cliente_id").value : $('.hidden-dinamic-client-id').val();
			let precio = $('#tipo_precio:checked').data('precio');

			if (!precio) {
				return utils.displayWarningDialog("Indicar un precio", "warning", function (data) {});
			}

			ajax.post(`api/ventas`, {
				cliente_id: dinamic_client_id,
				venta_total: 0,
				valor_unitario: $('#' + precio).val(),
				tipo_venta_id: document.getElementById("tipo_venta_id").value,
				precio_id: document.getElementById("precio_id").value,
				// tipo_pago_id: document.getElementById("tipo_pago_id").value,
				vendedor_id: document.getElementById("vendedor_id").value,
				producto_id: document.getElementById("producto_id").value,
				cantidad: document.getElementById("cantidad").value,
				almacen_id: document.getElementById("almacen_id").value,
				tipo_precio_id: $('#tipo_precio:checked').val(),
			}, function (response, headers) {
				if (headers.status == 200 || headers.status == 201) {
					utils.displayWarningDialog('Procesando compra..', "success", function (data) {
						if (response.folio_id) {
							return window.location.href = base_url +
								`refacciones/salidas/detalleVenta/` + response.folio_id;
						}
					})
				} else {
					return false;
				}
			});
		});

		$("#cantidad").on('blur', function () {
			let cantidad = $("#cantidad").val();
			let almacen_id = $("#almacen_id").val();
			let producto_id = $("#producto_id").val();
			let cantidad_almacen_principal = $("#almacen_1").val();
			validar_cantidad_productos(cantidad, almacen_id, producto_id);

		});

		function validar_cantidad_productos(cantidad, almacen_id, producto_id) {
			ajax.get(`api/desglose-producto/getStockByProducto?producto_id=` + producto_id, {}, function (response,
				headers) {
				if (headers.status == 200) {
					if (response.cantidad_almacen_primario >= cantidad) {
						$(".validation_error").html('');
						$("#agregar_vender").attr('disabled', false);
					} else {
						toastr.error("No existe cantidad sufiente en el stock: " + response
							.cantidad_almacen_primario);
						$("#agregar_vender").attr('disabled', true);
						$("#cantidad").val(0);
						$("#cantidad").focus();
					}
				}
			})
		}

		$("#modal-permiso").on('click', function () {
			ajax.post(`api/usuarios/login`, {
				usuario: document.getElementById('usuario').value,
				password: document.getElementById('password').value
			}, function (response, header) {
				if (header.status == 200) {
					aplicarPrecio(response);
				} else {
					utils.displayWarningDialog("Credenciales incorrectas",
						"warning",
						function (data) {})
				}
			});

		});

		let aplicarPrecio = ({
			id
		}) => {
			let precio_factura = $("#precio_factura").val();
			$.ajax({
				type: 'POST',
				url: base_url + "refacciones/productos/ajax_aplicar_precio",
				data: {
					precio_factura,
					id_precio: $("#precio_id").val(),
					id_usuario_autoriza: id
				},
				dataType: "json",
				success: function (response) {
					$("#modal-permiso-precio").modal('hide');
					document.getElementById('password').value = "";
					document.getElementById('usuario').value = "";
					$("#precio_publico").val(response.precio_publico);
					$("#precio_mayoreo").val(response.precio_mayoreo);
					$("#precio_interno").val(response.precio_interno);
					$("#precio_taller").val(response.precio_taller);
					$("#precio_otras_distribuidoras").val(response.precio_otras_distribuidoras);
					$("#impuesto").val(response.impuesto);

					$(".porcentaje_publico").html('<b>' + response.porcentaje.precio_publico +
						'%</b> ');
					$(".porcentaje_mayoreo").html('<b>' + response.porcentaje.precio_mayoreo +
						'%</b> ');
					$(".porcentaje_interno").html('<b>' + response.porcentaje.precio_interno +
						'%</b> ');
					$(".porcentaje_taller").html('<b>' + response.porcentaje.precio_taller +
						'%</b> ');
					$(".porcentaje_otras_distribuidoras").html('<b>' + response.porcentaje
						.precio_otras_distribuidoras + '%</b> ');
					$(".porcentaje_impuesto").html('<b>' + response.porcentaje.impuesto +
						'%</b> ');

				}
			});
		}

		$("#precio_id").on('change', function () {
			let id_precio = $("#precio_id").val();
			if (id_precio > 1) {
				utils.displayWarningDialog("Atención esta aplicando un descuento distinto al establecido",
					"warning",
					function (data) {
						if (data.value) {
							return $("#modal-permiso-precio").modal('show');
						}
						$("#precio_id").val(1);
						swal.close()
					}, true)
			} else if (id_precio == 1) {
				aplicarPrecio({})
			}
		});
		aplicarPrecio(1);

	});

</script>
@endsection


@section('modal')
<div class="modal fade" id="modal-permiso-precio" data-toggle="modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">
					Permisos para asignar precios
				</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<?php echo renderInputText("text", "usuario", "usuario", false); ?>
					</div>
					<div class="col-md-12">
						<?php echo renderInputText("password", "password", "password", false); ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button id="modal-permiso" type="button" class="btn btn-primary">
					<i class="fas fa-lock-open"></i> Continuar
				</button>
			</div>
		</div>
	</div>
</div>
@endsection
