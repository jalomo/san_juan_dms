@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-success" href="{{ site_url('refacciones/entradas/listadoEntradas/') }}"> 
                Listado 
            </a>
        </div>
    </div>
    <h4>Estatus de la compra : <b>	<?php echo isset($orden_compra) && isset($orden_compra->re_compras_estatus) ? $orden_compra->re_compras_estatus->estatus_compra->nombre : ''; ?></b></h4>
	<div class="row mt-2">
		<div class="col-md-4">
			<?php echo renderInputText("text", "folio", "Folio", isset($orden_compra->folios) ? $orden_compra->folios->folio : '', true); ?>
		</div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "factura", "Factura", isset($orden_compra->factura) ? $orden_compra->factura : '',true); ?>
		</div>
		<div class="col-md-4">
			<?php echo renderInputText("text", "proveedor", "Proveedor", isset($orden_compra->proveedor) ? $orden_compra->proveedor->proveedor_nombre : '', true); ?>
		</div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "fecha", "Fecha pago", isset($orden_compra->fecha_pago) ? utils::aFecha($orden_compra->fecha_pago,true) : '',true); ?>
		</div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "observaciones", "Observaciones", isset($orden_compra->observaciones) ? $orden_compra->observaciones : '',true); ?>
		</div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "total", "Total", isset($cxp->total) ? $cxp->total : '',true); ?>
		</div>
	</div>
    <hr/>
	<div class="row">
		<div class="col-md-4">
            <?php echo renderInputText("text", "concepto", "Concepto", isset($cxp->concepto) ? $cxp->concepto : '',true); ?>
		</div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "compra_credito", "¿Compra de credito ó contado?", isset($cxp->tipo_forma_pago) ? $cxp->tipo_forma_pago : '',true); ?>
		</div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "tipo_pago", "¿De que manera realizará el pago?", isset($cxp->tipo_pago) ? $cxp->tipo_pago : '',true); ?>
		</div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "plazo_credito", "Plazo de credito", isset($cxp->plazo_credito) ? $cxp->plazo_credito : '',true); ?>
        </div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "enganche", "Enganche", isset($cxp->enganche) ? $cxp->enganche : '',true); ?>
        </div>
		<div class="col-md-4">
            <?php echo renderInputText("text", "tasa_interes", "Tasa de interes", isset($cxp->tasa_interes) ? $cxp->tasa_interes : '',true); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Productos</h3>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla_ventas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Num. Orden</th>
                        <th>Descripcion</th>
                        <th>Ubicación</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($detalle->productos as $item)    
                            <tr>
                                <td>{{ $item->productos->id }}</td>
                                <td>{{ $item->productos->no_identificacion }}</td>
                                <td>{{ $item->productos->descripcion }}</td>
                                <td>{{ $item->productos->ubicacion->nombre }}</td>
                                <td>{{ $item->productos_orden_compra->cantidad }}</td>
                                <td> {{ $item->productos_orden_compra->total }}</td>
                            </tr>
                        @endforeach
                       
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Num. Orden</th>
                        <th>Descripcion</th>
                        <th>Ubicación</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$('#tabla_ventas').DataTable({
    language: {
		url: PATH_LANGUAGE
	},
})
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_entradas").addClass("show");
            $("#refacciones_entradas").addClass("active");
            $("#tables").addClass("show");
            $("#tables").addClass("active");
            $("#menu_entradas_listado_compras").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection