@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <form enctype="multipart/form-data" id="subir_factura">
                <div class="form-group">
                    <label for="factura">Factura</label>
                    <input type="file" class="form-control-file" id="archivo_factura" name="archivo_factura">
                    <div id="archivo_factura_error" class="invalid-feedback"></div>
                </div>
                <button type="button" id="subirfactura" class="btn btn-primary">Subir</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/facturas/index.js') }}"></script>
@endsection