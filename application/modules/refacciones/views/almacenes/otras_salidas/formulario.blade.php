@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
            <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
            <li class="breadcrumb-item active">Otras salidas Serv. Exce</li>
        </ol>

        <div class="row">
            <div class="col-md-12">
                <form id="frm-productos">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="orden">Orden</label>
                                <input id="orden" placeholder="174708" value="" name="orden" type="text"
                                    class="form-control">
                                <div id="orden_error" class="invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="col-md-8 mt-4">
                            <button type="button" id="btn-buscar" onclick="cargar_operaciones($('#orden').val());"
                                class="btn-primary btn col-md-4">
                                <i class="fas fa-search"></i> buscar
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="select">Cliente</label>
                                <input class="form-control" disabled type="text" id="clave_cliente" value="">
                                <input class="form-control" type="hidden" id="id_cliente" value="">
                                <div id='id_cliente_error' class='invalid-feedback'></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="select">Nombre completo</label>
                                <input class="form-control" disabled type="text" id="nombre_cliente" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="select">RFC</label>
                                <input class="form-control" type="text" disabled id="rfc_cliente">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'asesor', 'Asesor', '', true); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'tecnico', 'Técnico', '', true); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'modelo', 'Unidad', '', true); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'placas', 'Placas', '', true); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'no_serie', 'No de serie', '', true); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo renderInputText('text', 'kilometraje', 'Kilometraje', '', true); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class=" table table-bordered" id="tbl_salidas">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Descripción</th>
                                        <th>Número de pieza</th>
                                        <th>Precio unitario</th>
                                        <th>Cantidad solicitada</th>
                                        <th>Cantidad en stock</th>
                                        <th>Inventario </th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Descripción</th>
                                        <th>Número de pieza</th>
                                        <th>Precio unitario</th>
                                        <th>Cantidad solicitada</th>
                                        <th>Cantidad en stock</th>
                                        <th>Inventario </th>
                                        <th>-</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div id="msg_confirmar">
                            </div>
                        </div>
                        <div class="col-md-8 text-right mt-2">
                            <button id="btn-confirmar-venta-mpm" disabled="disabled" type="button" class="btn btn-primary"> Aceptar y continuar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h3>Listado ordenes abiertas</h3>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class=" table table-bordered" id="tbl_ordenes_abiertas">
                    <thead>
                        <tr>
                            <th>No. Orden</th>
                            <th>Cliente</th>
                            <th>Asesor</th>
                            <th>Técnico</th>
                            <th>Serie</th>
                            <th>Placas</th>
                            <th>Estatus</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No. Orden</th>
                            <th>Cliente</th>
                            <th>Asesor</th>
                            <th>Técnico</th>
                            <th>Serie</th>
                            <th>Placas</th>
                            <th>Estatus</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        let orden = $("#orden").val();
		let band = false;

        var tabla_salidas = $('#tbl_salidas').DataTable({
            "ajax": {
                url: base_url + "refacciones/salidas/ajax_data_salidas_producto",
                type: 'POST',
                data: {
                    orden: function() {
                        return $('#orden').val()
                    }
                }
            },
            columns: [{
                    'data': function(data) {
                        if (data.id) {
                            return data.id
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.descripcion) {
                            return data.descripcion
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.descripcion) {
                            return data.num_pieza
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function(data) {
                        // console.log(data)
                        if (data.tipo == 'REFACCION' && data.precio_unitario) {
                            return data.precio_unitario
                        } else if (data.tipo == 'OPERACION') {
                            return parseFloat(data.costo_mo) + parseFloat(data.precio_unitario);
                        } else {
                            return '--'
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.cantidad) {
                            return data.cantidad
                        } else {
                            return "--"
                        }
                    }
                },
                {
                    'data': function({
                        cantidad_stock,
                        descripcion,
                        tipo,
                        costo_mo,
                        precio_unitario
                    }) {
                        if (cantidad_stock !== 0 && tipo == 'REFACCION') {
                            return cantidad_stock
                        } else if (cantidad_stock == 0 && tipo == 'REFACCION') {
                            return 0
                        } else if (tipo == 'OPERACION') {
                            return tipo;
                        } else {
                            return 0
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.se_registro) {
                            return "Pieza actualizada"
                        } else {
                            return "<span> -- </span>"
                        }
                    }
                },
                {
                    'data': function(data) {
                        if (data.se_registro || data.tipo == 'OPERACION') {
                            return "<span> -- </span>"
                        } else {
                            return "<button data-descripcion=" + btoa(data.descripcion) +
                                " data-cantidad=" + data.cantidad + " data-num_pieza=" + btoa(data.num_pieza) + " data-producto_servicio_id=" + data.id +
                                " type='button' class='btn-servicio-producto btn btn-primary'><i class='fas fa-list'></i></button>";
                        }
                    }
                }
            ],
              "createdRow": function(row, data, dataIndex) {
            if (data.tipo == 'REFACCION' && !data.se_registro) {
                band = true;
            }

            if (data.cantidad_stock == 0 && data.tipo == 'REFACCION') {

                toastr.error(`No hay <strong>${data.descripcion} </strong> en stock.`);
                $("#msg_confirmar").text("No hay " + data.descripcion + " en stock.");
                $("#msg_confirmar").addClass("alert alert-danger");
                if (!data.se_registro) {
                    $("#btn-confirmar-venta-mpm").attr('disabled', true)
                }
                band = true;
            }
            
            if (data['se_registro']) {
                $(row).find('td:eq(6)').css('background-color', '#8cdd8c');

            } else {
                if(data.tipo == 'OPERACION'){
                    $(row).find('td:eq(6)').css('background-color', '#2f71d480');
                }else{

                    $(row).find('td:eq(6)').css('background-color', '#db524d80');
                }
            }
        }
        });


        var tbl_busqueda_pieza = $("#tbl_busqueda_pieza").DataTable({
            "ajax": {
                url: base_url + "refacciones/salidas/ajax_busqueda_pieza",
                type: 'POST',
                data: {
                    descripcion_pieza: function() { return $('#descripcion_pieza').val() },
                    no_identificacion: function() { return $('#busqueda_no_identificacion').val() },
                }
            },
            columns: [{
                    'data': function(data) {
                        return data.id
                    }
                },
                {
                    'data': function(data) {
                        return data.no_identificacion
                    }
                },
                {
                    'data': function(data) {
                        return data.descripcion
                    }
                },
                {
                    'data': function(data) {
                        return data.cantidad_actual
                    }
                },
                {
                    'data': function(data) {
                        return data.valor_unitario
                    }
                },
                {
                    'data': function(data) {
                        return "<button data-descripcion=" + btoa(data.descripcion) + " data-no_pieza=" +
                            btoa(data.no_identificacion) + " data-producto_id=" + data.id +
                            " type='button' class='btn-selecciona-producto btn btn-primary'><i class='fas fa-list'></i></button>";
                    }
                }
            ]
        });

        //continuar flujo de venta
        $("#btn-confirmar-venta-mpm").on('click', function() {

            if ($("#orden").val() == '') {
                return utils.displayWarningDialog('Indicar numero de orden ...', "warning", function(data) {})
            }

            if ($("#id_cliente").val() == '') {
                return utils.displayWarningDialog('Selecciona una cita ...', "warning", function(data) {})
            }

            ajax.post('api/ventas/mpm', {
                numero_orden: $('#orden').val(),
                cliente_id: $("#id_cliente").val(),
                venta_total: 0,
                tipo_venta_id: 2, //ventanilla taller
                almacen_id: 1,
                tipo_precio_id: 1,
                precio_id: 1
            }, (data, headers) => {

                toastr.warning('<strong>Procesando ...</strong>');
                if (headers.status == 200) {
                    utils.displayWarningDialog('Venta registrada ...', "success", function(data_alert) {
                        if (data.folio_id) {
                            window.location.href = PATH +
                                "/refacciones/salidas/detalleVentaServiceExcellent/" + data.folio_id
                        } else {
                            toastr.error('<strong>Algo salio mal ...</strong>');
                        }
                    })
                }
            });
        });

        $("#tbl_salidas").on('click', '.btn-servicio-producto', function() {

            // const producto_id = $(this).data('producto_id');
            let num_pieza = $(this).data('num_pieza');
            let producto_servicio_id = $(this).data('producto_servicio_id');
            let cantidad = $(this).data('cantidad');
            let descripcion = $(this).data('descripcion');
            $("#cantidad_servicio").val(cantidad);
            $("#producto_servicio_id").val(producto_servicio_id);
            $("#no_identificacion").val(atob(num_pieza));
            $("#descripcion").val(atob(descripcion));
            $("#modal-producto-detalle").modal('show');
            $("#title_modal").text('Detalle');
            tbl_busqueda_pieza.clear().draw();
            $("#descripcion_pieza").val('');

        });

        $("#tbl_busqueda_pieza").on('click', '.btn-selecciona-producto', function() {
            let producto_id = $(this).data('producto_id');
            let no_pieza = $(this).data('no_pieza');
            let descripcion = $(this).data('descripcion');

            $("#no_pieza").val(atob(no_pieza));
            $("#descripcion_producto").val(atob(descripcion));
            $("#producto_id").val(producto_id);
        });

        $("#btn-continuar").on('click', function() {

            if ($("#producto_id").val() == '') {
                toastr.error('<strong>Seleccionar producto ...</strong>');
                return false;
            }
            // $("#no_identificacion").val()
            // $("#no_pieza").val(),
            agregarElementoHistoricoProducto({

                num_pieza: $("#no_identificacion").val(),
                producto_servicio_id: $("#producto_servicio_id").val(),
                cantidad: $("#cantidad_servicio").val(),
                producto_id: $("#producto_id").val()
            });
        });

        function agregarElementoHistoricoProducto({
            num_pieza,
            producto_servicio_id,
            cantidad
        }) {
			band = false;

            let data = {
                no_identificacion: $("#no_identificacion").val(),
                producto_servicio_id: $("#producto_servicio_id").val(),
                cantidad: $("#cantidad_servicio").val(),
                producto_id: $("#producto_id").val()
            };

            ajax.post(`api/producto-servicio`, data, function(response, headers) {
                if (headers.status == 201 || headers.status == 204) {
                    toastr.success('<strong>Producto registrado ...</strong>');

                    $("#producto_id").val('');
                    $("#descripcion_producto").val('');
                    $("#no_pieza").val('');
                    $("#modal-producto-detalle").modal('hide');
                    $('#descripcion_pieza').val('')
                    $("#descripcion_pieza").val('');
                    tabla_salidas.ajax.reload();
					setTimeout(() => {
                    if (!band) {
                        $("#btn-confirmar-venta-mpm").attr('disabled', false);
                    }
                }, 5000);
                }
            })
        }

        //llamar cuando se compre
        function callbackStockproductos(producto_id) {
            ajax.get('api/desglose-producto/actualizaStockByProducto?producto_id=' + producto_id, {},
                function(response, headers) {
                    if (headers.status == 200) {
                        toastr.info("Inventario actualizado para el producto " + $("#descripcion_producto").val());
                        $.isLoading("hide");
                        window.location.reload();
                    }
                })
        }



        $("#btn_buscar_pieza").on('click', function() {
            tbl_busqueda_pieza.ajax.reload();
            // $("#descripcion_pieza").val();
        });

        $("#id_cliente").on('change', function(e) {
            let id_cliente = $("#id_cliente").val();
            ajax.get(`api/clientes/${id_cliente}`, {}, (data, headers) => {
                let response = data[0];
                $("#nombre_cliente").val(
                    `${response.nombre} ${response.apellido_materno} ${response.apellido_paterno}`);
                $("#rfc_cliente").val(response.rfc);
            })
        });

        var tabla_ordenes = $('#tbl_ordenes_abiertas').DataTable({
            ajax: base_url + "refacciones/Salidas/ajax_ordenes_abiertas",
            columns: [{
                    'data': function(data) {
                        return data.id_cita
                    }
                },
                {
                    'data': function(data) {
                        return data.cliente
                    }
                },
                {
                    'data': function(data) {
                        return data.asesor
                    }
                },
                {
                    'data': function(data) {
                        return data.tecnico
                    }
                },
                {
                    'data': function(data) {
                        return data.serie
                    }
                },
                {
                    'data': function(data) {
                        return data.placas
                    }
                },
                {
                    'data': function(data) {
                        return data.estatus_orden
                    }
                },
                {
                    'data': function(data) {
                        return "<button type='button' onclick='cargar_operaciones(" + data.id_cita +
                            ")' class='btn btn-primary'>" +
                            "<i class='fas fa-tasks'></i>" +
                            "</button>";
                    }
                }
            ]
        });

        function cargar_operaciones(id_cita) {
			band = false;
			$("#btn-confirmar-venta-mpm").attr('disabled', true);
			$("#msg_confirmar").text("");
			$("#msg_confirmar").removeClass("alert alert-danger");
            if (id_cita != "") {
                $("#orden").val(id_cita);
            }
            //console.log($("#orden").val());

            $.ajax({
                type: 'GET',
                url: base_url + "refacciones/salidas/ajax_data_salidas/" + $("#orden").val(),
                dataType: "json",
                success: function(response) {
                    try {
                        if (response.asesor.length > 0) {

                            let asesor = response.asesor;
                            if (asesor[0].numero_cliente) {

                                const cliente = asesor[0].numero_cliente;
                                const patt = new RegExp(/^[0-9\s]+$/g);
                                const numero_cliente = patt.test(cliente) ? cliente : cliente.substr(2);
                                recuperar_numero_cliente(numero_cliente);
                                $("#clave_cliente").val(asesor[0].numero_cliente);
                                $("#id_cliente").val(asesor[0].id_cita);
                            } else {
                                numero_cliente_ramdon();
                            }

                            //$("#id_cliente").val(asesor[0].num_Cliente_orden);

                            if (asesor[0].nombre_compania != "") {
                                $("#nombre_cliente").val(asesor[0].nombre_compania);
                            } else {
                                $("#nombre_cliente").val(asesor[0].datos_nombres + " " + asesor[0]
                                    .datos_apellido_paterno + " " + asesor[0].datos_apellido_materno);
                            }

                            $("#rfc_cliente").val(asesor[0].rfc);
                            $("#asesor").val(asesor[0].asesor);
                            $("#tecnico").val(asesor[0].tecnico);
                            $("#modelo").val(asesor[0].vehiculo_modelo);
                            $("#placas").val(asesor[0].vehiculo_placas);
                            $("#kilometraje").val(asesor[0].km);

                            if (asesor[0].vehiculo_numero_serie != "") {
                                $("#no_serie").val(asesor[0].vehiculo_numero_serie);
                            } else {
                                $("#no_serie").val(asesor[0].vehiculo_identificacion);
                            }

                            tabla_salidas.ajax.reload();
							 setTimeout(() => {
                            if (!band) {
                                $("#btn-confirmar-venta-mpm").attr('disabled', false);
                            }
                        }, 5000);
                            toastr.warning('<strong>Cargando productos ...</strong>');
                        } else {
                            utils.displayWarningDialog('No se encontraron resultados ...', "warning", function(
                                data) {})
                        }
                    } catch (error) {
                        utils.displayWarningDialog('Se produjo un error ...', "warning", function(data) {})
                    }
                }
            });
        }

        function recuperar_numero_cliente(clave_cliente) {
            $.ajax({
                type: 'GET',
                url: base_url + "refacciones/salidas/ajax_data_id_cliente/" + clave_cliente,
                dataType: "json",
                success: function({
                    cliente
                }) {
                    if (cliente.length > 0) {
                        let cliente_info = cliente[0];
                        $("#id_cliente").val(cliente_info.id);
                    } else {
                        utils.displayWarningDialog('No se encontro el cliente...', "warning", function(data) {})
                    }
                }
            });
        }

        function numero_cliente_ramdon() {
            $.ajax({
                type: 'GET',
                url: base_url + "refacciones/salidas/ajax_data_cliente/1",
                dataType: "json",
                success: function(response) {
                    if (response.cliente) {
                        let cliente = response.cliente;
                        $("#id_cliente").val(cliente[0].id);
                        $("#clave_cliente").val(cliente[0].numero_cliente);
                        //toastr.warning('<strong>Cargando productos ...</strong>');
                    } else {
                        utils.displayWarningDialog('No se encontro el cliente...', "warning", function(data) {})
                    }
                }
            });
        }

    </script>
@endsection



@section('modal')
    <div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="modalProductos" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-10">
                            <?php echo renderInputText('text', 'descripcion', 'Descripcion', '', true); ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo renderInputText('text', 'cantidad_servicio', 'Cantidad', 0, true); ?>
                        </div>
                        <div class="col-md-12">
                            <?php echo renderInputText('text', 'no_identificacion', 'No identificación', 0,
                            true); ?>
                        </div>
                        <hr>
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'descripcion_producto', 'Descripción', '',
                            true); ?>
                            <input type="hidden" name="producto_id" id="producto_id">
                            <input type="hidden" name="producto_servicio_id" id="producto_servicio_id">

                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'no_pieza', 'No de pieza', '', true); ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <?php echo renderInputText('text', 'descripcion_pieza', 'Buscar producto', '');?>
                            
                        </div>
                        <div class="col-md-6">

                            <?php echo renderInputText('text', 'busqueda_no_identificacion', 'No pieza', '');?>
                            
                        </div>
                        <div class="col-md-2 mt-4">
                            <button id="btn_buscar_pieza" class="btn btn-primary btn-block"> Buscar</button>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12 ">
                            <table class="table table-bordered" style="width:100%;" id="tbl_busqueda_pieza">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>No. Identificacion</th>
                                        <th>Descripcion</th>
                                        <th>Stock</th>
                                        <th>Precio </th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cerrar
                    </button>
                    <button id="btn-continuar" type="button" class="btn btn-primary">
                        <i class="fas fa-shopping-cart"></i> Empatar con stock
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
