@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4">Almacenes</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-10">
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary" href="<?php echo base_url('almacenes/almacenes/agregarAlmacen') ?>">Agregar</a>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_almacenes" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Codigo almacén</th>
                            <th>Nombre</th>
                            <th>Encargado</th>
                            <th>Ubicacion</th>
                            <th>Status</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Codigo almacén</th>
                            <th>Nombre</th>
                            <th>Encargado</th>
                            <th>Ubicacion</th>
                            <th>Status</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			$('#tbl_almacenes').DataTable({
				"ajax": `${base_url}almacenes/almacenes/getDataAlmacenes`,
				columns: [
					{ 'data':'id'},
					{ 'data':'clave_prod_serv'},
					{ 'data':'descripcion'},
					{ 'data':'precio'},
					{ 'data':'unidad'},
					{'data':function(data)
						{
							return "<a class='btn btn-success' href='"+base_url+'refacciones/productos/editar/'+data.id+"'> Editar </a>";
						}
					},
					{'data':function(data)
						{
                            return "<button type='button' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
						}
					}
				]
			})
		});

        $("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });

            function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                            $.ajax({
                            url: base_url+'refacciones/productos/delProducto/'+id,
                            success: function(respuesta) {
                                Swal.fire(
                              'Borrado!',
                              'El registro se ha borrado',
                              'success'
                            )
                            location.reload(true)
                           // 
                            },
                            error: function() {
                                console.log("No se ha podido obtener la información");
                            }
                            });   
                         }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }


	</script>
@endsection