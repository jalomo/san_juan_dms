@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Traspasar producto</li>
    </ol>
    
    <div class="row">
        <div class="col-md-12">
            <form id="frm-productos">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo renderInputText("text", "descripcion", "Actual nombre", isset($producto->descripcion) ? $producto->descripcion : '', true); ?>
                    <input type="hidden" name="producto_id" value="{{$producto->id}}" id="producto_id">
                    </div>
                    <div class="col-md-3">
                        <?php echo renderInputText("text", "no_identificacion", "Numero identificación actual", isset($producto->no_identificacion) ? $producto->no_identificacion : '', true); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "no_identificacion_nuevo", "Numero de identificación", ''); ?>
                    </div>
                    <div class="col-md-8">
                        <?php echo renderInputText("text", "nombre_producto_actual", "Nuevo nombre", ''); ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <button id="btn-remplazo" class="btn btn-primary col-md-6" type="button"> Remplazo </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $("#btn-remplazo").on('click', function() {
            ajax.post('api/producto-remplazo-almacen', {
                producto_id:document.getElementById("producto_id").value,
                nombre_producto_anterior:document.getElementById("descripcion").value,
                nombre_producto_actual:document.getElementById("nombre_producto_actual").value,
                no_identificacion:document.getElementById("no_identificacion_nuevo").value,
                no_identificacion_anterior:document.getElementById("no_identificacion").value,
            }, function(response, header) {
                if (header.status == 200 || header.status == 201) {
                    let titulo = "Remplazo actulizado con exito!"
                    utils.displayWarningDialog(titulo, 'success', function(result) {
                        return window.location.href = base_url + "refacciones/almacenes/listadoremplazos";
                    });

                    if (header.status == 500) {
                        let titulo = "Error en remplazo de  producto!"
                        utils.displayWarningDialog(titulo, 'warning', function(result) {});
                    }
                }
            })
        })
    });

</script>
@endsection