@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <form enctype="multipart/form-data" id="subir_factura">
                <button type="button" id="subirfactura" class="btn btn-primary">Generar reporte</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_salidas").addClass("show");
            $("#refacciones_salidas").addClass("active");
            $("#refacciones_salidas_sub").addClass("show");
            $("#refacciones_salidas_sub").addClass("active");
            $("#poliza_dev_prov").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection