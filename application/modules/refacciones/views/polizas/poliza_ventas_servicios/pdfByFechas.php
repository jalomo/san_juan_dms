<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Poliza Service Excelent</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		td {
			font-size: 11px !important;
			text-align: left !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<div style="text-align:right; font-size: 6px !important">Reporte elaborado: <?php echo obtenerFechaEnLetra(date('Y-m-d')); ?></div>
		<div class="col-12">
			<h2>Poliza Service Excelent.</h2>
		</div>
	</div>
	<div class="contenedor">
		<div class="col-12">
			<b>Refacciones</b>
			<table style="margin-top:10px" class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripción</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha venta</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($ventas) { ?>
						<?php foreach ($ventas as $key => $item) { ?>
							<tr>
								<td><?php echo  $item->folio; ?></td>
								<td><?php echo  $item->no_identificacion; ?></td>
								<td><?php echo  $item->descripcion; ?></td>
								<td><?php echo  $item->unidad; ?></td>
								<td><?php echo  in_array($item->estatusId, [2, 3]) ?  $item->cantidad : '-' . $item->cantidad; ?></td>
								<td><?php echo  '$' . number_format($item->valor_unitario); ?></td>
								<td><?php echo  '$' . number_format($item->venta_total); ?></td>
								<td><?php echo  $item->estatusVenta; ?></td>
								<td>
									<?php echo date('d-m-y', strtotime($item->created_at)) ?>
								</td>
							</tr>
						<?php  }
					} else {
						?>
						<tr>
							<td style="text-align:center" colspan="9">No se solicitarón refacciones</td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripción</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha venta</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-12">
			<b>Servicios</b><br />
			<table style="margin-top:10px" class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">No identificación</th>
						<th style="font-size: 11px" scope="col">Descripción</th>
						<th style="font-size: 11px" scope="col">Mano obra</th>
						<th style="font-size: 11px" scope="col">IVA</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha venta</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sum_servicios = 0;
					$total_precio = 0;
					if ($servicios) {
						foreach ($servicios as $key => $item) {
							$sum_servicios++; 
							$total_precio += $item->precio;
						?>
							<tr>
								<td><?php echo  $item->folio; ?></td>
								<td><?php echo  $item->no_identificacion; ?></td>
								<td><?php echo  $item->nombre_servicio; ?></td>
								<td><?php echo  '$' . number_format($item->costo_mo); ?></td>
								<td><?php echo  '$' . number_format($item->iva); ?></td>
								<td><?php echo  '$' . number_format($item->precio); ?></td>
								<td><?php echo  $item->estatus; ?></td>
								<td>
									<?php echo date('d-m-y', strtotime($item->created_at)) ?>
								</td>
							</tr>
						<?php  }
					} else { ?>
						<tr>
							<td style="text-align:center" colspan="9">No se solicitarón servicios</td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">No identificación</th>
						<th style="font-size: 11px" scope="col">Descripción</th>
						<th style="font-size: 11px" scope="col">Mano obra</th>
						<th style="font-size: 11px" scope="col">IVA</th>
						<th style="font-size: 11px" scope="col">Total</th>
						<th style="font-size: 11px" scope="col">Estatus</th>
						<th style="font-size: 11px" scope="col">Fecha venta</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-12">
			<h3>Resultados totales</h3>
			<table style="width:50%;" align="right" class="table" cellpadding="5">
				<tbody>
					<tr>
						<td style="text-align:right">Productos vendidos: </td>
						<td>
							<?php echo isset($totales->total_cantidad) ? $totales->total_cantidad : ''; ?>
						</td>
					</tr>
					<tr>
						<td style="text-align:right">Total refacciones: </td>
						<td>
							<?php echo isset($totales->sum_venta_total) ? '$' . number_format($totales->sum_venta_total) : ''; ?>
						</td>
					</tr>
					<tr>
						<td style="text-align:right">Servicios solicitados: </td>
						<td>
							<?php echo isset($sum_servicios) ? $sum_servicios : ''; ?>
						</td>
					</tr>
					<tr>
						<td style="text-align:right">Total servicios: </td>
						<td>
							<?php echo isset($total_precio) ? '$' . number_format($total_precio) : ''; ?>
						</td>
					</tr>
					<tr>
						<td style="text-align:right">Total: </td>
						<td>
							<?php 
							$total_refacciones = isset($totales->sum_venta_total) ? $totales->sum_venta_total : 0;
							$total_totales = $total_precio + $total_refacciones;
							echo isset($total_totales) ? '$' . number_format($total_totales) : ''; 
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>