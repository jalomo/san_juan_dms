<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Poliza devolución proveedor</title>
	<style>
		.contenedor {
			width: 100%;
		}

		.col-12 {
			width: 100%;
			padding: 3px;
		}

		td {
			font-size: 11px !important;
		}

		.col-6 {
			float: left;
			width: 49%;
			padding: 3px;
		}

		.col-5 {
			float: left;
			width: 40%;
			padding: 3px;
		}

		table {
			border-collapse: collapse;
			width: 100%;
		}

		table,
		th,
		td {
			border: 1px solid #233a74;
			margin-bottom: 12px
		}
	</style>
</head>

<body>
	<div class="contenedor">
		<div class="col-12">
			Mexico D.F. <?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
		</div>
		<div class="col-12">
			<h2>Poliza de Devolución Proveedor</h2>
		</div>
	</div>
	<div class="contenedor">
		<div class="col-12">
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripcion</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Observaciones</th>
						<th style="font-size: 11px" scope="col">Total devolución</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($devolucion_proveedor as $key => $item) { ?>
						<tr>
							<td><?php echo  $item->folio; ?></td>
							<td><?php echo  $item->no_identificacion; ?></td>
							<td><?php echo  $item->descripcion; ?></td>
							<td><?php echo  $item->cantidad; ?></td>
							<td><?php echo  $item->unidad; ?></td>
							<td><?php echo  '$' . number_format($item->precio); ?></td>
							<td><?php echo  $item->observaciones; ?></td>
							<td><?php echo  '$' . number_format($item->total_compra); ?></td>
						</tr>
					<?php  } ?>
				</tbody>
				<tfoot>
					<tr>
						<th style="font-size: 11px" scope="col">Folio</th>
						<th style="font-size: 11px" scope="col">Cve Producto</th>
						<th style="font-size: 11px" scope="col">Descripcion</th>
						<th style="font-size: 11px" scope="col">Cantidad</th>
						<th style="font-size: 11px" scope="col">Unidad</th>
						<th style="font-size: 11px" scope="col">V/Unitario</th>
						<th style="font-size: 11px" scope="col">Observaciones</th>
						<th style="font-size: 11px" scope="col">Total devolución</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<div class="col-12">
			<h3>Resultados totales</h3>
			<table class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Total Ordenes</th>
						<th style="font-size: 11px" scope="col">Total Cantidad</th>
						<th style="font-size: 11px" scope="col">Suma Total</th>
						<?php if ($fecha_inicio && $fecha_fin) { ?>
							<th style="font-size: 11px" scope="col">Periodo</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td id="totalOrdenes"><?php echo isset($totales->total_compras) ? $totales->total_compras : ''; ?>
						</td>
						<td id="totalCantidad"><?php echo isset($totales->total_cantidad) ? $totales->total_cantidad : ''; ?>
						</td>
						<td id="sumaTotal"><?php echo isset($totales->sum_compra_total) ? '$' . number_format($totales->sum_compra_total) : ''; ?></td>
						<?php if ($fecha_inicio && $fecha_fin) { ?>
							<td>
								<span id=""><?php echo obtenerFechaEnLetra($fecha_inicio); ?></span> AL <span id="">
									<?php echo obtenerFechaEnLetra($fecha_fin); ?>
							</td>
						<?php } ?>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>

</html>