<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Traspaso</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        .col-5 {
            float: left;
            width: 40%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            Mexico D.F. <?php echo obtenerFechaEnLetra(date('yy-m-d')); ?>
        </div>
        <div class="col-12">
            <h2>Poliza de traspasos</h2>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th style="font-size: 11px" scope="col">Orden</th>
                        <th style="font-size: 11px" scope="col">Cantidad</th>
                        <th style="font-size: 11px" scope="col">Cve Producto</th>
                        <th style="font-size: 11px" scope="col">Descripcion</th>
                        <th style="font-size: 11px" scope="col">V/Unitario</th>
                        <th style="font-size: 11px" scope="col">Total</th>
                        <th style="font-size: 11px" scope="col">Al. origen</th>
                        <th style="font-size: 11px" scope="col">Al. destino</th>
                        <th style="font-size: 11px" scope="col">Fecha compra</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detalle_movimientos as $key => $item) { ?>
                        <tr>
                            <td><?php echo  $item->folios->folio; ?></td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div style="font-size: 11px">
                                        <?php echo $productos->cantidad; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div style="font-size: 11px">
                                        <?php echo $productos->no_identificacion; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div style="font-size: 11px">
                                        <?php echo $productos->descripcion; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div>
                                        <?php echo $productos->valor_unitario; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div>
                                        <?php echo $productos->total; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div>
                                        <?php echo $productos->codigo_almacen_orig; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($item->detalle_productos_almacen as $productos) { ?>
                                    <div>
                                        <?php echo $productos->codigo_almacen_dest; ?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php echo date('d-m-yy', strtotime($item->created_at)) ?>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th style="font-size: 11px" scope="col">Orden</th>
                        <th style="font-size: 11px" scope="col">Cantidad</th>
                        <th style="font-size: 11px" scope="col">Cve Producto</th>
                        <th style="font-size: 11px" scope="col">Descripcion</th>
                        <th style="font-size: 11px" scope="col">V/Unitario</th>
                        <th style="font-size: 11px" scope="col">Total</th>
                        <th style="font-size: 11px" scope="col">Al. origen</th>
                        <th style="font-size: 11px" scope="col">Al. destino</th>
                        <th style="font-size: 11px" scope="col">Fecha compra</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">AFECTACION</th>
                        <th scope="col">ALMACEN ENVIA</th>
                        <th scope="col">HABER</th>
                        <th scope="col">ALMACEN RECIBE</th>
                        <th scope="col">DEBE</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($poliza as $key => $poliza_item) { ?>
                        <tr>
                            <td>0000</td>
                            <td><?php echo $poliza_item->almacen_origen ?></td>
                            <td>$ <?php echo $poliza_item->total ?></td>
                            <td><?php echo $poliza_item->almacen_dest ?></td>
                            <td>$ <?php echo $poliza_item->total ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SUMA TOTAL</th>
                        <th scope="col">CONCEPTO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>$ <?php echo $total_generado; ?></td>
                        <td>TRASPASO DE HOY FECHA DEL: <span id=""><?php echo obtenerFechaEnLetra($fecha_inicio); ?></span> AL <span id=""> <?php echo obtenerFechaEnLetra($fecha_fin); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>