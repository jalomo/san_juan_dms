<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Traspaso</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        .col-5 {
            float: left;
            width: 40%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            Mexico D.F. <?php echo obtenerFechaEnLetra($traspaso->created_at) ?>
        </div>
        <div class="col-12">
            <h2>Reporte de traspasos</h2>
        </div>
        <div class="col-12">
            Folio <?php echo isset($traspaso->folios) ? $traspaso->folios->folio : '--' ?>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead class="">
                    <tr>
                        <th scope="col"> # </th>
                        <th scope="col"> No identificación </th>
                        <th scope="col"> Producto </th>
                        <th scope="col"> Cantidad </th>
                        <th scope="col"> Origen </th>
                        <th scope="col"> Destino </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data_traspaso as $key => $item) { ?>
                        
                        <tr>
                            <td style="text-align:center"> <?php echo $item->id; ?></td>
                            <td style="text-align:center"> <?php echo $item->producto->no_identificacion; ?></td>
                            <td style="text-align:center"> <?php echo $item->producto->descripcion; ?></td>
                            <td style="text-align:center"> <?php echo $item->total_prod_almacen_dest; ?></td>
                            <td style="text-align:center"> <?php echo $item->almacen_origen->nombre; ?></td>
                            
                            <td style="text-align:center"> <?php echo $item->almacen_destino->nombre; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
                
            </table>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-6">
            SUMAS IGUALES :
        </div>
        <div class="col-6">
           <div class="contenedor">
               <div style="text-align:center" class="col-5">
                $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
               </div>
               <div style="text-align:center"  class="col-5">
                $ <?php echo isset($total_traspaso->total) ? $total_traspaso->total : '--' ?>
               </div>
           </div>
        </div>
    </div>
    <div class="contenedor">
        <div class="col-12">
           CONCEPTO: Traspaso de <?php echo obtenerFechaEnLetra($traspaso->created_at) ?>
        </div>
    </div>
</body>

</html>