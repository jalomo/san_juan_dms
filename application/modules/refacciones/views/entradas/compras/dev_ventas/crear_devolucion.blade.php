@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h2 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h2>
    <h4><?php echo isset($subtitulo) ? $subtitulo : "" ?></h4>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control"/>
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Número cliente:</label>
                <select class="form-control" id="cliente_id" name="cliente_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_clientes))
                    @foreach ($cat_clientes as $cliente)
                        <option value="{{ $cliente->id}}"> {{$cliente->numero_cliente}} - {{ $cliente->nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="cliente_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4 mt-4">
            <button type="button" id="btn-limpiar" onclick="limpiarFiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_ventas_realizadas" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/entradas/devoluciones/creardevolucion.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_entradas").addClass("show");
            $("#refacciones_entradas").addClass("active");
            $("#tables").addClass("show");
            $("#tables").addClass("active");
            $("#menu_entradas_dev_crear").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection