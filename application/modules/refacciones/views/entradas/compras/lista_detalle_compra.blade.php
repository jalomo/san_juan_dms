@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-10"></div>
        <div class="col-md-2">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php renderInputText("date","total", "Fecha entrada", '', true); ?>
        </div>
        <div class="col-md-3">
            <?php renderInputText("date","total", "Orden entrada",'', true); ?>
        </div>
        <div class="col-md-3">
            <?php renderInputText("date","no_factura", "No Factura", "12312", true); ?>
        </div>
        <div class="col-md-3">
            <?php renderInputText("date","no_factura", "No Pedido", "12312", true); ?>
        </div>
    </div>
    <hr>
    <h3>Productos</h3>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-detalle">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Total</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">-</th>
                  </tr>
                </thead>
                <tbody>
                    @if (!empty($lista))
                        @foreach ($lista as $item)
                            <tr>
                                <td scope="row">{{ $item->id }}</td>
                                <td>$ {{ $item->total }}</td>
                                <td>{{ $item->subtotal }}</td>
                                <td><a href="{{ base_url('refacciones/factura/vistafactura/'.$item->id) }}" class="btn btn-primary" type="button">Ver</a></td>
                            </tr>
                        @endforeach
                        @else
                        <tr>
                            <td scope="row">1</td>
                            <td>$ 100</td>
                            <td>$ 100</td>
                            <td>producto 1 </td>
                            <td><a href="#" class="btn btn-primary" type="button">detalle</a></td>
                        </tr>
                        <tr>
                            <td scope="row">2</td>
                            <td>$ 200</td>
                            <td>$ 200</td>
                            <td>producto 2 </td>
                            <td><a href="#" class="btn btn-primary" type="button">detalle</a></td>
                        </tr>
                    @endif
                </tbody>
              </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php renderInputText("text","total", "Total", "$4000", true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text","subtotal", "Sub total", "$4000", true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text","iva", "IVA", "$4000", true); ?>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#tabla-detalle").DataTable({});
</script>
@endsection