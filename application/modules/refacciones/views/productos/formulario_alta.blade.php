@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
    </ol>
    <div class="row">
        <div class="col-md-2">
            <?php echo renderInputText("text", "prefijo", "Prefijo", isset($data->prefijo) ? $data->prefijo : '', false); ?>
        </div>
        <div class="col-md-2">
            <?php echo renderInputText("text", "sufijo", "Sufijo", isset($data->sufijo) ? $data->sufijo : '', false); ?>
        </div>
        <div class="col-md-2">
            <?php echo renderInputText("text", "basico", "Basico", isset($data->basico) ? $data->basico : '', false); ?>
        </div>
        <div class="col-md-6">
            <?php echo renderInputText("text", "no_identificacion", "No identificacion", isset($data->no_identificacion) ? $data->no_identificacion : '', false); ?>
        </div>
        <div class="col-md-6">
            <?php echo renderInputText("text", "clave_unidad", "Clave unidad", isset($data->clave_unidad) ? $data->clave_unidad : '', false); ?>
        </div>
        <div class="col-md-6">
            <?php echo renderInputText("text", "clave_prod_serv", "Clave producto servicio", isset($data->clave_prod_serv) ? $data->clave_prod_serv : '', false); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?php echo renderInputText("text", "descripcion", "Descripcion", isset($data->descripcion) ? $data->descripcion : '', false); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "unidad", "Unidad", isset($data->unidad) ? $data->unidad : '', false); ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-4">
        <?php echo renderInputText("number", "cantidad", "Cantidad", isset($stock->desglose_producto->cantidad_actual) ? $stock->desglose_producto->cantidad_actual : ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("number", "valor_unitario", "Valor unitario", isset($data->valor_unitario) ? $data->valor_unitario: '', false); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("number", "precio_factura", "Precio factura", isset($data->precio_factura) ? $data->precio_factura : '', isset($data->precio_factura) ?  true : false); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Taller</label>
                <select name="taller_id" class="form-control" id="taller_id">
                    <option value=""> Seleccionar taller</option>
                    @foreach ($cat_talleres as $taller)

                    @if (isset($data->taller_id) && $data->taller_id == $taller->id)
                    <option selected value="{{ $taller->id}}"> {{ $taller->nombre}}</option>
                    @else
                    <option value="{{ $taller->id}}"> {{ $taller->nombre}}</option>
                    @endif
                    @endforeach
                </select>
                <div id='taller_id_error' class='invalid-feedback'></div>
            </div>
        </div>
       <div class="col-md-4">
        <div class="form-group">
            <label for="select">Almacen</label>
            <select name="almacen_id" class="form-control" id="almacen_id">
                <option value=""> Seleccionar almacen</option>
                @foreach ($cat_almacenes as $almacen)
                    @if (isset($data->almacen_id) && $data->almacen_id == $almacen->id)
                        <option selected value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                    @else
                        <option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                    @endif
                @endforeach
            </select>
            <div id='almacen_id_error' class='invalid-feedback'></div>
        </div>
       </div>
       <div class="col-md-4">
        <div class="form-group">
                <label for="select">Ubicación producto</label>
                <select name="ubicacion_producto_id" class="form-control" id="ubicacion_producto_id">
                    <option value=""> Seleccionar ubicación</option>
                    @foreach ($cat_ubicaciones as $ubicacion)
                        @if (isset($data->ubicacion_producto_id) && $data->ubicacion_producto_id == $ubicacion->id)
                            <option selected value="{{ $ubicacion->id}}"> {{ $ubicacion->nombre}}</option>
                        @else
                            <option value="{{ $ubicacion->id}}"> {{ $ubicacion->nombre}}</option>
                        @endif
                    @endforeach
                </select>
                <div id='ubicacion_producto_id_error' class='invalid-feedback'></div>
            </div>
       </div>
    </div>
    <div class="row">
        <div class="col-md-12 mt-4 mb-4 text-right" id="div-productos" data-id="{{ isset($data->id) ? $data->id : '' }}">
            @if ($this->uri->segment(4))
                <button type="button" id="editar_producto" class="btn btn-primary col-md-3"> 
                    <i class="fas fa-shopping-cart"></i> Editar
                </button>
            @else
                <button type="button" id="guardar_producto" class="btn btn-primary col-md-3">
                    <i class="fas fa-shopping-cart"></i> Guardar
                </button>
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let onload = $("#precio_factura").val();
    disableElement(onload);
    $("#guardar_producto").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post(`api/productos`, procesarForm(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            utils.displayWarningDialog(headers.message, "success", function(data) {
                return window.location.href = base_url + `refacciones/productos/listado`;
            })
        })
    })

    $("#precio_factura").on('keyup', function() {
        let data = $("#precio_factura").val();
        disableElement(data);
    });

    function disableElement(data = '') {
        if (data === '') {
            $('#precio_id').attr('disabled', true)
        } else {
            $('#precio_id').attr('disabled', false)
        }
    }

    $("#editar_producto").on('click', function() {

        $(".invalid-feedback").html("");
        var id = $("#div-productos").data('id');
        ajax.put(`api/productos/${id}`, procesarForm(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            utils.displayWarningDialog(headers.message, "success", function(data) {
                return window.location.href = base_url + `refacciones/productos/listado`;
            })
        })

    })

    let procesarForm = function() {
        let form = $('#registro_producto').serializeArray();
        let newArray = {
            no_identificacion: document.getElementById("no_identificacion").value,
            clave_unidad: document.getElementById("clave_unidad").value,
            clave_prod_serv: document.getElementById("clave_prod_serv").value,
            descripcion: document.getElementById("descripcion").value,
            unidad: document.getElementById("unidad").value,
            cantidad: document.getElementById("cantidad").value,
            prefijo: document.getElementById("prefijo").value,
            sufijo: document.getElementById("sufijo").value,
            basico: document.getElementById("basico").value,
            precio_factura: parseInt(document.getElementById("precio_factura").value),
            inventariable: 1,
            existencia: 1,
            valor_unitario:document.getElementById("valor_unitario").value,
            precio_id: 1,
            taller_id: document.getElementById("taller_id").value !== '' ? parseInt(document.getElementById("taller_id").value) : '',
            almacen_id: document.getElementById("almacen_id").value !== '' ? parseInt(document.getElementById("almacen_id").value) : '',
            ubicacion_producto_id: document.getElementById("ubicacion_producto_id").value !== '' ? parseInt(document.getElementById("ubicacion_producto_id").value) : ''
        };
        return newArray;
    }
</script>

@endsection