@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="form-readonly" data-readonly="{{$readOnly}}">
                <?php renderInputText("text","no_identificacion","No identificacion",$data->no_identificacion, true); ?>
                <?php renderInputText("text","clave_unidad","Clave unidad",$data->clave_unidad, true); ?>
                <?php renderInputText("text","producto_id_sat","Clave unidad",$data->clave_prod_serv, true); ?>
                <?php renderInputText("text","descripcion","Descripcion",$data->descripcion, true); ?>
                <?php renderInputText("text","unidad","Unidad",$data->unidad, true); ?>
                <?php renderInputText("text","cantidad","Cantidad",$data->cantidad, true); ?>
                <?php renderInputText("text","precio","Precio",$data->valor_unitario, true); ?>
                <br>
                @if (!$readOnly)
                    <button type="button" id="guardar_producto" class="btn btn-primary">Guardar</button>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script >
</script>
@endsection
