@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Stock productos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>

    <div class="row">
        <div class="col-md-3">
            <?php echo renderInputText("text", "no_identificacion", "No identificacion", '', false); ?>
        </div>
        <div class="col-md-7">
            <?php echo renderInputText("text", "descripcion", "Descripción", '', false); ?>
        </div>
        <div class="col-md-2 mt-4">
            <button onclick="buscarproducto()" class="btn btn-primary"> 
                <i class="fa fa-search"></i> Buscar
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_stock" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/productos/stock.js') }}"></script>
@endsection