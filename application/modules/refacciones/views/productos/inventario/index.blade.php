@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <?php echo renderInputText("text", "descripcion", "Buscar", '', false); ?>
        </div>
        <div class="col-md-4 mt-4">
            <button id="btn-buscar" class="btn btn-primary"> Buscar</button>
            <button id="btntodos" class="btn btn-primary"> Todos</button>
        </div>
        <div class="col-md-2 mt-4 mb-4">
            <a class="btn btn-primary col-md-12" href="{{ base_url('refacciones/productos/inventario/').$inventario_id }}"> 
                <i class="fas fa-arrow-left"></i> Regresar
            </a>
            <a target="_blank" class="btn btn-primary mt-1 col-md-12" href="{{ base_url('refacciones/productos/pdfreporteInventario/').$inventario_id }}"> 
                <i class="fas fa-file"></i> Reporte
            </a>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <input type="hidden" name="usuario_id" id="usuario_id" value="{{ $usuario_id }}">
            <input type="hidden" name="inventario_id" id="inventario_id" value="{{ $inventario_id }}">
            <table class="table table-bordered" id="tbl_inventario" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Unidad</th>
                        <th>Valor Unitario</th>
                        <th>Stock</th>
                        <th>Inventario</th>
                        <th>Valor</th>
                        <th>Diferencia</th>
                        <th> - </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Producto</th>
                        <th>Unidad</th>
                        <th>Valor Unitario</th>
                        <th>Stock</th>
                        <th>Inventario</th>
                        <th>Valor</th>
                        <th>Diferencia</th>
                        <th> - </th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-md-4">
            <table class="table table-bordered" id="tbl_inventario" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Perdida</th>
                        <th>Diferencia</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>{{ isset($data['resultados']['perdidas']) ? $data['resultados']['perdidas'] : 0 }}</th>
                        <th>{{ isset($data['resultados']['residuos']) ? $data['resultados']['residuos'] : 0 }}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
let tabla_inventario = $('#tbl_inventario').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: base_url + "refacciones/productos/ajax_inventario_producto",
            type: 'POST',
            data: {
                descripcion: () => $('#descripcion').val(),
                inventario_id: () => $('#inventario_id').val()
            }
		},
		columns: [{
				'data': 'producto_id'
			},
			
			{
				'data': 'no_identificacion'
			},
			{
				'data': 'descripcion'
			},
            {
				'data': 'unidad'
			},
            {
				'data': 'valor_unitario'
			},
            {
				'data': 'cantidad_stock_inventario'
			},
            {
				'data': function(data){
                    if(data.cantidad_inventario){
                        return data.cantidad_inventario
                    }else{
                        return 'N/A'
                    }
                }
			},
            {
				'data':function(data){
                    if(data.valor_residuo){
                        return data.valor_residuo
                    }else{
                        return 'N/A'
                    }
                }
			},
            {
				'data':function(data){
                    if(data.diferencia){
                        return data.diferencia
                    }else{
                        return 'N/A'
                    }
                }
			},
            {
				
                'data': function(data) {
                    let html =  "";
                    let inventario_id = document.getElementById('inventario_id').value;
                    if(data.valor_residuo && data.diferencia){
                        html +="<a href="+ base_url + "refacciones/productos/detalleproductoinventario/"+inventario_id+"/"+data.producto_id+" class='btn btn-primary ' >";
                        html += "<i class='fa fa-bars'></i></a>";
                    }
                    return html;
                }
			}
		],
        "createdRow": function(row, data, dataIndex) {
            
            if(Math.sign(data.valor_residuo) == -1){
                $(row).find('td:eq(7)').css('background-color', '#ff000073');
            }

            if(Math.sign(data.diferencia) == -1){
                $(row).find('td:eq(8)').css('background-color', '#ff000073');
            }
        }
    });
    
    $('#tbl_inventario').on('click','.btn-modal',function(){
        $("#modalInventario").modal('show');
        let producto_id = $(this).data('producto_id');
        let cantidad_anterior = $(this).data('cantidad_anterior');
        let descripcion = $(this).data('descripcion');
        let valor_unitario = $(this).data('valor_unitario');
        let no_identificacion = $(this).data('no_identificacion');
        $("#producto_id").val(producto_id);
        $("#cantidad_anterior").val(cantidad_anterior);
        $("#descripcion").val(descripcion);
        $("#no_identificacion").val(no_identificacion);
        $("#valor_unitario").val(valor_unitario);
    });

    // btn-buscar
    $('#btn-buscar').on('click',function(){
        if($("#descripcion").val() == ''){
            toastr.error("Indicar nombre de producto!")
            return false;
        }
        
        tabla_inventario.ajax.reload();
    });

    $('#btntodos').on('click',function(){
        $("#descripcion").val('')
        tabla_inventario.ajax.reload();
    });
    

    $('#btn-confirmar').on('click',function(){    
        if($("#cantidad_actual").val() == ''){
            toastr.error("Indicar la cantidad!")
            return false;
        }

        let data = {
            "cantidad_actual": $("#cantidad_actual").val(),
            "cantidad_anterior": $("#cantidad_anterior").val(),
            "valor_unitario": $("#valor_unitario").val(),
            "producto_id": $("#producto_id").val(),
            "usuario_id": $("#usuario_id").val(),
        };
        
        ajax.post(`api/inventario-producto`, data, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $("#modalInventario").modal('hide');
                $("#frm_agregar_inventario")[0].reset();
                tabla_inventario.ajax.reload();
            }
        })
    });

</script>
@endsection


@section('modal')
<div class="modal fade" id="modalInventario" tabindex="-1" role="dialog" aria-labelledby="modalinventario" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title_modal">Actualizar Inventario</h5>
        </div>
        <div class="modal-body">
            <form id="frm_agregar_inventario" class="row">
                <div class="col-md-12">
                    <?php echo renderInputText("text", "descripcion", "Producto", '', true); ?>
                </div>
                <div class="col-md-8">
                    <?php echo renderInputText("text", "no_identificacion", "No identificacion", '', true); ?>
                </div>
                <div class="col-md-4">
                    <?php echo renderInputText("text", "valor_unitario", "Valor unitario", '', true); ?>
                </div>
                <div class="col-md-4">
                    <?php echo renderInputText("text", "cantidad_anterior", "Cantidad sistema", '', true); ?>
                </div>
                
                <div class="col-md-8">
                    <?php echo renderInputText("numeric", "cantidad_actual", "Inventario", ''); ?>
                    <input type="hidden" name="producto_id" id="producto_id">    
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button id="btn-confirmar" type="button" class="btn btn-primary">
                <i class="fas fa-list"></i>  Aceptar
            </button>
        </div>
      </div>
    </div>
  </div>
@endsection