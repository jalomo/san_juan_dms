@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>
    <div class="row">
        <div class="col-md-6">
            <?php echo renderInputText("text", "descripcion_busqueda", "Buscar", '', false); ?>
        </div>
        <div class="col-md-2 mt-4">
            <button id="btn-buscar" class="btn btn-primary"> Buscar</button>
            <button id="btntodos" class="btn btn-primary"> Limpiar</button>
        </div>
        <div class="col-md-4 text-right mt-4">
            <a class="btn btn-primary" href="{{ base_url('refacciones/productos/seleccionaInventario') }}"> <i class="fas fa-arrow-left"></i> Regresar</a>
            <button id="btnreporte" class="btn btn-primary"> Reporte inventario</button>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <input type="hidden" name="usuario_id" id="usuario_id" value="{{ $usuario_id }}">
            <input type="hidden" name="inventario_id" id="inventario_id" value="{{ $inventario_id  }}">
            <input type="hidden" name="estatus_inventario_id" id="estatus_inventario_id" value="{{ $inventario->estatus_inventario_id  }}">
            <table class="table table-bordered" id="tbl_inventario" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Unidad</th>
                        <th>Valor Unitario</th>
                        <th>Ubicacion</th>
                        <th>Cantidad reportada</th>
                        <th>Inventariado</th>
                        <th> - </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Unidad</th>
                        <th>Valor Unitario</th>
                        <th>Ubicacion</th>
                        <th>Cantidad reportada</th>
                        <th>Inventariado</th>
                        <th> - </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
let tabla_inventario = $('#tbl_inventario').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: base_url + "refacciones/productos/ajax_inventario_producto",
            type: 'POST',
            data: {
                descripcion: () => $('#descripcion_busqueda').val(),
                inventario_id: () => $('#inventario_id').val(),
                
            }
		},
		columns: [{
				'data': 'producto_id'
			},
			
			{
				'data': 'no_identificacion'
			},
			{
				'data': 'descripcion'
			},
            {
				'data': 'unidad'
			},
            {
				'data': 'valor_unitario'
			},
            {
				'data': 'ubicacionProducto'
			},
            {
				'data': 'cantidad_inventario'
			},
            {
				'data': function(data){
                    if(data.cantidad_inventario){
                        return 'inventariado';
                    }else{
                        return 'sin realizar';
                    }
                }
			},
            {
				
                'data': function(data) {
                    if ($("#estatus_inventario_id").val() == 1) {
                        let html =  "<button class='btn btn-primary btn-modal'  data-producto_id=" + data.producto_id + ">";
                        html += "<i class='fa fa-list'></i></button>";
                        return html;
                    } 
                    return '';
                }
			}
		],
        "createdRow": function(row, data, dataIndex) {
            if(!data.cantidad_inventario){
                $(row).find('td:eq(7)').css('background-color', '#e37f7f');
            }else{
                $(row).find('td:eq(7)').css('background-color', '#8cdd8c');
            }
        }
    });
    
    $('#tbl_inventario').on('click','.btn-modal',function(){
        $("#modalInventario").modal('show');
        $("#frm_agregar_inventario")[0].reset();
        let producto_id = $(this).data('producto_id');
        ajax.get(`api/productos/stockActual?producto_id=${producto_id}`,{}, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                let data = response[0];
                $("#producto_id").val(producto_id);
                $("#descripcion").val(data.descripcion);
                $("#no_identificacion").val(data.no_identificacion);
                $("#valor_unitario").val(data.valor_unitario);
                $("#ubicacion").val(data.ubicacionProducto);
                $("#cantidad_stock").val(data.cantidad_actual);
            }
        })
    });

    // btn-buscar
    $('#btn-buscar').on('click',function(){
        if($("#descripcion_busqueda").val() == ''){
            toastr.error("Indicar nombre de producto!")
            return false;
        }
        
        tabla_inventario.ajax.reload();
    });

    $('#btntodos').on('click',function(){
        $("#descripcion_busqueda").val('')
        tabla_inventario.ajax.reload();
    });
    

    $('#btn-confirmar').on('click',function(){    
        if($("#cantidad_inventario").val() == ''){
            toastr.error("Indicar la cantidad!")
            return false;
        }

       
        let data = {
            "cantidad_inventario": $("#cantidad_inventario").val(),
            "cantidad_stock": $("#cantidad_stock").val(),
            "valor_unitario": $("#valor_unitario").val(),
            "producto_id": $("#producto_id").val(),
            "usuario_id": $("#usuario_id").val(),
            "inventario_id": $("#inventario_id").val()
        };
       
        ajax.post(`api/inventario-producto`, data, function(response, headers) {
            if (headers.status == 201 || headers.status == 200) {
                $("#modalInventario").modal('hide');
                $("#frm_agregar_inventario")[0].reset();
                tabla_inventario.ajax.reload();
            }
        })
    });

    // 
    $('#btnreporte').on('click',function(){    
        let inventario = $("#inventario_id").val();
        if(!inventario){
            toastr.error("No se ha cargado toda la informacion!")
            return false;
        }

        ajax.get(`api/inventario-producto/inventario-completo/${inventario}`, {}, function(response, headers) {
            if (headers.status == 204 ) {
                utils.displayWarningDialog('Generando reporte inventario', "success", function(data) {
                    return window.location.href = base_url + 'refacciones/productos/reporteInventario/'+$("#inventario_id").val();
                })
            }
        })

    });

</script>
@endsection


@section('modal')
<div class="modal fade" id="modalInventario" tabindex="-1" role="dialog" aria-labelledby="modalinventario" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title_modal">Actualizar Inventario</h5>
        </div>
        <div class="modal-body">
            <form id="frm_agregar_inventario" class="row">
                <div class="col-md-12">
                    <?php echo renderInputText("text", "descripcion", "Producto", '', true); ?>
                </div>
                <div class="col-md-8">
                    <?php echo renderInputText("text", "no_identificacion", "No identificacion", '', true); ?>
                </div>
                <div class="col-md-4">
                    <?php echo renderInputText("text", "valor_unitario", "Valor unitario", '', true); ?>
                </div>
                <div class="col-md-4">
                    <?php echo renderInputText("text", "ubicacion", "Ubicación", '', true); ?>
                </div>
                
                <div class="col-md-8">
                    <?php echo renderInputText("number", "cantidad_inventario", "Inventario", ''); ?>
                    <input type="hidden" name="producto_id" id="producto_id">    
                    <input type="hidden" name="cantidad_stock" id="cantidad_stock">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button id="btn-confirmar" type="button" class="btn btn-primary">
                <i class="fas fa-check-circle"></i>  Aceptar
            </button>
        </div>
      </div>
    </div>
  </div>
@endsection