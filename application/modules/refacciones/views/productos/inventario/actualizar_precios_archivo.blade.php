@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?></li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <form enctype="multipart/form-data" id="subir_factura">
                    <div class="form-group">
                        <label for="factura">Universo de partes</label>
                        <input type="file" class="form-control-file" id="archivo_universo_partes" name="archivo_universo_partes">
                        <div id="archivo_universo_partes_error" class="invalid-feedback"></div>
                    </div>
                    <button type="button" id="subir_archivo" class="btn btn-primary">
                        Subir archivo
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection



@section('scripts')
<script src="{{ base_url('js/refacciones/productos/actualizar_precios.js') }}"></script>
@endsection
