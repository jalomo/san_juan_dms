@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">Productos</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">Pedido de piezas</li>
    </ol>
    <div class="row">
        <div class="col-md-8 mb-4">
            <h2>Piezas pedidas</h2>
        </div>
        <div class="col-md-2 mb-4">
                
                <a target="_blank" href="{{ base_url('refacciones/productos/reportePedidopiezas/').$data['pedido_id'] }}" class="btn btn-success col-md-12" id="generar-reporte"> 
                    <i class="far fa-file-word"></i> Reporte 
                </a>

        </div>
        <div class="col-md-2">
            <a href="{{ base_url('refacciones/productos/crearPedido') }}" class="btn btn-success col-md-12" > 
                <i class="fas fa-arrow-left"></i> Regresar 
            </a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-5">
            <input type="hidden"  id="ma_pedido_id" value="{{ $data['pedido_id']}}">
            <?php renderSelectArray('filtro_proveedor_id', 'Proveedor', $proveedores, 'id', 'proveedor_nombre', null) ?>
        </div>
        <div class="col-md-4 mt-4">
            <button class="btn btn-success col-md-12" id="btn-filtrar"> <i class="fas fa-search"></i> Filtrar </button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_piezas" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Listado productos</h2>
        </div>
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_stock" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/custom/pedido_piezas/pedido.js')}}"></script>

@endsection

@section('modal')

@include('productos/pedidos/modaldetalle')
@include('productos/pedidos/modalproveedor')
{{-- @include('productos/pedidos/modalproductos') --}}
{{-- @include('productos/pedidos/modalautorizar') --}}


@endsection