
<div class="modal fade" id="modaldetalle" tabindex="-1" role="dialog" aria-labelledby="modalcomprasLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal">Producto</h5>
            </div>
            <div class="modal-body">
                <form id="frm-producto" class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "descripcion", "Producto", '', true); ?>
                    </div>
                    <div class="col-md-8">
                        <?php echo renderInputText("text", "no_identificacion", "No identificacion", '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("text", "valor_unitario", "Valor unitario", '', true); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo renderInputText("number", "cantidad_solicitada", "Cantidad", ''); ?>
                        <input type="hidden" name="producto_id" id="producto_id">
                    </div>
                    <div class="col-md-8">
                        <?php renderSelectArray('proveedor_id', 'Proveedor', $proveedores, 'id', 'proveedor_nombre', null) ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-confirmar" type="button" class="btn btn-primary">
                    <i class="fas fa-list"></i> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
