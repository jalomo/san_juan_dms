@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <hr>
    <div class="row">
        <div class="col-md-12 text-right">
            <a target="blank" href="{{ base_url('refacciones/kardex/generarPolizaPorFechasTraspasos/'.$fecha_inicio.'/'.$fecha_fin) }}" class="btn btn-primary">
                <i class="fas fa-file-pdf" aria-hidden="true"></i> Generar PDF
            </a>
        </div>
        
        <div class="col-md-12 mt-2 text-right">
            <a href="{{ base_url('refacciones/kardex/') }}" class="btn btn-primary">
                <i class="fas fa-list" aria-hidden="true"></i> Regresar
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4>HORA: {{ date('H:i') }}</h4>
        </div>
        <div class="col-md-4 text-center">
            <h4>DMS FORD</h4>
            <div>REFACCIONES</div>
        </div>
        <div class="col-md-4 text-right">
            <h4>FECHA: {{ date('d/m/yy') }}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div>
                Fecha del: <span id="fecha_inicio_label">{{ $fecha_inicio }}</span> al <span id="fecha_fin_label"> {{ $fecha_fin }}</span>
            </div>
        </div>
        <div class="col-md-4 text-center"> </div>
        <div class="col-md-4 text-right"> </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                      <th scope="col">Orden</th>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Clave Producto</th>
                      <th scope="col">Descripcion</th>
                      <th scope="col">Valor unitario</th>
                      <th scope="col">Total</th>
                      <th scope="col">Almacen origen</th>
                      <th scope="col">Almacen destino</th>
                      <th scope="col">Fecha compra</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalle_movimientos as $item)
                        <tr>
                            <td scope="row">{{ $item->folios->folio }}</td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->cantidad }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->no_identificacion }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->descripcion }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->valor_unitario }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->total }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->codigo_almacen_orig }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                @foreach ($item->detalle_productos_almacen as $productos)
                                    <div>
                                        {{ $productos->codigo_almacen_dest }}
                                    </div>
                                @endforeach
                            </td>
                            <td scope="row">
                                {{  date('d-m-yy',strtotime($item->created_at)) }}
                            </td>
                        </tr>
                    @endforeach 
                </tbody>
                <tfoot>
                    <tr>
                      <th scope="col">Orden</th>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Clave Producto</th>
                      <th scope="col">Descripcion</th>
                      <th scope="col">Valor unitario</th>
                      <th scope="col">Total</th>
                      <th scope="col">Almacen origen</th>
                      <th scope="col">Almacen destino</th>
                      <th scope="col">Fecha compra</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           <h3>
                Mexico D.F. <?php echo obtenerFechaEnLetra(date('yy-m-d')) ?>
           </h3>
        </div>
        <div class="col-md-12">
            <h3>Poliza de traspasos</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">AFECTACION</th>
                        <th scope="col">ALMACEN ENVIA</th>
                        <th scope="col">HABER</th>
                        <th scope="col">ALMACEN RECIBE</th>
                        <th scope="col">DEBE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($poliza as $poliza_item)    
                        <tr>
                            <td>0000</td>
                            <td>{{ $poliza_item->almacen_origen }}</td>
                            <td>$ {{ $poliza_item->total}}</td>
                            <td>{{ $poliza_item->almacen_dest }}</td>
                            <td>$ {{ $poliza_item->total}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SUMA TOTAL</th>
                        <th scope="col">CONCEPTO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${{$total_generado }}</td>
                        <td>TRASPASO DE HOY FECHA DEL: <span id="">{{ obtenerFechaEnLetra($fecha_inicio) }}</span> AL <span id=""> {{ obtenerFechaEnLetra($fecha_fin) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
