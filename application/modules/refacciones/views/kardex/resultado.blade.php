@layout('tema_luna/layout')
<style>
    	th{
			font-size:12px !important;
		}
    	td{
			font-size:12px !important;
		}
</style>
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <hr>
    <div class="row">
        <!-- <div class="col-md-12 text-right">
            <a href="#" class="btn btn-primary" onclick="alert('en proceso');">
                <i class="fas fa-file-pdf" aria-hidden="true"></i> Generar PDF
            </a>
        </div> -->
        <div class="col-md-12 mt-2 text-right">
            <a href="{{ base_url('refacciones/kardex/') }}" class="btn btn-primary">
                <i class="fas fa-list" aria-hidden="true"></i> Regresar
            </a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?php renderInputText("text", "articulo", "Articulo",  isset($kardex->producto[0]->no_identificacion) ? $kardex->producto[0]->no_identificacion : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "descripcion", "Descripcion",  isset($kardex->producto[0]->descripcion) ? $kardex->producto[0]->descripcion : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "costo", "Costo p.",  isset($kardex->producto[0]->valor_unitario) ? $kardex->producto[0]->valor_unitario : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "existencia", "Existencia actual",  isset($kardex->producto[0]->cantidad_actual) ? $kardex->producto[0]->cantidad_actual : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "almacen", "Almacen primario",  isset($kardex->producto[0]->cantidad_almacen_primario) ? $kardex->producto[0]->cantidad_almacen_primario : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "almacen", "Almacen secundario",  isset($kardex->producto[0]->cantidad_almacen_secundario) ? $kardex->producto[0]->cantidad_almacen_secundario : '', true); ?>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla_ventas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Estatus</th>
                        <th>Cliente/Proveedor</th>
                        <th>Folio</th>
                        <th>Salida</th>
                        <th>Entrada</th>
                        <th>Exist.</th>
                        <th>Costo</th>
                        <th>Total</th>
                        <th>Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kardex->kardex as $item)
                        <tr>
                            <td>{{ $item->fecha}}</td>
                            <td>{{ $item->estatus}}</td>
                            <td>{{ $item->cliente}}</td>
                            <td>{{ $item->folio}}</td>
                            <td>{{ $item->tipo_movimiento == 'salida' ? $item->cantidad : '-'  }}</td>
                            <td>{{ $item->tipo_movimiento == 'entrada' ? $item->cantidad : '-' }}</td>
                            <td>{{ $item->existencia}}</td>
                            <td>{{ '$'. number_format($item->valor_unitario)}}</td>
                            <td>{{ '$'. number_format($item->total)}}</td>
                            <td>{{ '$'. number_format($item->totalAcomulado)}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                        <th>Fecha</th>
                        <th>Estatus</th>
                        <th>Cliente/Proveedor</th>
                        <th>Factura</th>
                        <th>Salida</th>
                        <th>Entrada</th>
                        <th>Exist.</th>
                        <th>Costo</th>
                        <th>Total</th>
                        <th>Saldo</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-6 mt-3">
			<h4>Cantidades totales</h4>
			<table style="width:100%;" class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Productos comprados</th>
						<th style="font-size: 11px" scope="col">Productos vendidos</th>
						<th style="font-size: 11px" scope="col">Total productos</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo isset($kardex->totales->CantidadTotalCompras) ? $kardex->totales->CantidadTotalCompras : '' ; ?></td>
						<td><?php echo isset($kardex->totales->CantidadTotalVentas) ? $kardex->totales->CantidadTotalVentas : '' ; ?></td>
						<td><?php echo isset($kardex->totales->CantidadTotal) ? $kardex->totales->CantidadTotal : '' ; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
        <div class="col-6 mt-3">
			<h4>Saldos totales</h4>
			<table style="width:100%;" class="table" cellpadding="5">
				<thead>
					<tr>
						<th style="font-size: 11px" scope="col">Saldo en compras</th>
						<th style="font-size: 11px" scope="col">Saldo en ventas</th>
						<th style="font-size: 11px" scope="col">Saldo total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo  isset($kardex->totales->SaldoTotalCompras) ? '$' .number_format($kardex->totales->SaldoTotalCompras) : '' ; ?></td>
						<td><?php echo  isset($kardex->totales->SaldoTotalVentas) ? '$' .number_format($kardex->totales->SaldoTotalVentas) : '' ; ?></td>
						<td><?php echo  isset($kardex->totales->SaldoTotal) ? '$' .number_format($kardex->totales->SaldoTotal) : '' ; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
    </div>
</div>
@endsection
