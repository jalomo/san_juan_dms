@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control"/>
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Número proveedor:</label>
                <select class="form-control" id="proveedor_id" name="proveedor_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_proveedor))
                    @foreach ($cat_proveedor as $proveedor)
                        <option value="{{ $proveedor->id}}"> {{$proveedor->proveedor_numero}} - {{ $proveedor->proveedor_nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="proveedor_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Estatus de cuenta:</label>
                <select class="form-control" id="estatus_cuenta_id" name="estatus_cuenta_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($estatus_cuentas))
                    @foreach ($estatus_cuentas as $estatus)
                        <option value="{{ $estatus->id}}"> {{ $estatus->nombre }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-12 mt-4 text-right">
            <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tbl_cxp" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered col-md-5 mt-4" align="right" >
                <tr>
                    <th colspan="2">Totales</th>
                </tr>
                <tr>
                    <th class="text-right">Saldo neto:</th>
                    <td id="total_saldo_neto_tabla" class="money_format">-</td>
                </tr>
                <tr>
                    <th class="text-right">Monto a pagar:</th>
                    <td id="total_pagar_tabla" class="money_format">-</td>
                </tr>
                <tr>
                    <th  class="text-right">Monto pagado:</th>
                    <td id="total_abonado_tabla" class="money_format">-</td>
                </tr>
            </table>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ base_url('js/cxp/cuentas/listado.js') }}"></script>
@endsection