<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        ini_set('max_execution_time',300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['titulo'] = "Cuenta por pagar";
        $data['subtitulo'] = "Listado";
        $proveedores = $this->curl->curlGet('api/catalogo-proveedor');
        $estatus_cuentas = $this->curl->curlGet('api/estatus-cuentas');
        
        $data['estatus_cuentas'] = procesarResponseApiJsonToArray($estatus_cuentas);
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($proveedores);

        $this->blade->render('cuentas_por_pagar/cuentas/listado', $data);
    }

    public function relacion_pagos()
    {
        $data['titulo'] = "Cuenta por pagar";
        $data['subtitulo'] = "Relación pagos";
        $proveedores = $this->curl->curlGet('api/catalogo-proveedor');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($proveedores);
        $cat_estatus_abonos = $this->curl->curlGet('api/estatus-abono');
        $data['cat_estatus_abonos'] = procesarResponseApiJsonToArray($cat_estatus_abonos);
        $data['fecha_inicio'] = $this->_data_first_month_day();
        $data['fecha_fin'] = $this->_data_last_month_day();

        $this->blade->render('cuentas_por_pagar/listado', $data);
    }

    public function detalle_cuenta($id)
    {
    	$data['titulo'] = "Cuentas por pagar";
        $data['modulo'] = "Detalle cuenta";
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-pagar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        $proveedor_id = isset($data_cuentas) ? $data_cuentas->proveedor_id : null;
        $apiProveedor = $this->curl->curlGet('api/catalogo-proveedor/' . $proveedor_id);
        $enganche_api = $this->curl->curlGet('api/abonos-por-pagar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=1&estatus_abono_id=3');
        $abonos_api = $this->curl->curlGet('api/abonos-por-pagar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=2&estatus_abono_id=3');
        
        $enganche = procesarResponseApiJsonToArray($enganche_api);
        $abonos = procesarResponseApiJsonToArray($abonos_api);
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $tipo_abono = $this->curl->curlGet('api/tipo-abono');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        $data['cat_tipo_abono'] = procesarResponseApiJsonToArray($tipo_abono);
        $data['proveedor'] = current(procesarResponseApiJsonToArray($apiProveedor));
        $data['total_abonado'] = $this->procesar_total_abonos($abonos);
        $data['total_abonos'] = isset($abonos) && $abonos ? count($abonos): 0;
        $data['data_cuentas']  = $data_cuentas;
        $data['detalle_enganche'] = isset($enganche) && $enganche ? current($enganche) : null;
        $data['enganche'] = $this->procesar_enganche($enganche);
        $data['saldo_actual']  = $data_cuentas->total - $data['total_abonado'] - $data['enganche'];
        if ($data_cuentas->tipo_forma_pago_id == 2) {
            $abonos_restantes = $data_cuentas->cantidad_mes - $data['total_abonos'];
            $data['abono_mensual'] = $abonos_restantes >= 1 ? $data['saldo_actual'] / $abonos_restantes : 0;
            $data['subtitulo'] = "Cuentas / Detalle Credito";
            $this->blade->render('cuentas_por_pagar/cuentas/pago_credito',$data);
        } 
    }


    private function procesar_enganche($enganches) {
        $total_enganche = 0;
        if(isset($enganches) && is_array($enganches)) {
            foreach($enganches as $enganche) { 
                if($enganche->estatus_abono_id == 3) {
                    $total_enganche += $enganche->total_pago;
                }
            } 
        }
        return $total_enganche;
    }

    private function procesar_total_abonos($abonos) {
        $total_abono = 0;
        if(isset($abonos) && is_array($abonos)) {
            foreach($abonos as $abono) { 
                if($abono->estatus_abono_id == 3) {
                    $total_abono += $abono->total_pago;
                }
            } 
        }
        return $total_abono;
    }
   
    function _data_last_month_day() { 
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
   
        return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    }

    function _data_first_month_day() {
        $month = date('m');
        $year = date('Y');
        return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }

    public function polizapedidoproducto()
    {
        $data['titulo'] = "Poliza pedidos";
        $data['subtitulo'] = "Poliza por proveedor";
        $proveedores = $this->curl->curlGet('api/catalogo-proveedor');
        $data['cat_proveedor'] = procesarResponseApiJsonToArray($proveedores);
        $this->blade->render('cuentas_por_pagar/pedido_pieza/index', $data);
    }
}