<?php

defined('BASEPATH') or exit('No direct script access allowed');

class NotaCredito extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/nota-credito/');
        $dataRegistro = [];
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = $dataRegistro;

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Nota de Crédito";
        $data['subtitulo'] = "Listado";

        $this->blade->render('nota_credito/listado', $data);
    }

    public function alta($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos los catalogos
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Nota de Crédito";
        $data['subtitulo'] = "Registro";

        $data['data'] = [];
        $this->blade->render('nota_credito/formulario', $data);
    }

    public function editar($id='')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Cargamos los catalogos
            $marcas = $this->curl->curlGet('api/catalogo-marcas');
            $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

            $anio = $this->curl->curlGet('api/catalogo-anio');
            $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

            $color = $this->curl->curlGet('api/catalogo-colores');
            $data['cat_color'] = procesarResponseApiJsonToArray($color);

            $cfdi = $this->curl->curlGet('api/cfdi');
            $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

            $tipo_pago = $this->curl->curlGet('api/tipo-pago');
            $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

            $data['modulo'] = "Autos";
            $data['submodulo'] = "Ventas";
            $data['titulo'] = "Nota de Crédito";
            $data['subtitulo'] = "Edición";

            $data['data'] = [];
            $this->blade->render('nota_credito/formulario', $data);
        }
    }
}
