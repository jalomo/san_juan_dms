<?php

defined('BASEPATH') or exit('No direct script access allowed');

class TablaIsan extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/impuesto/');
        $dataRegistro = [];
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = $dataRegistro;

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Catálogos";
        $data['titulo'] = "Impuesto";
        $data['subtitulo'] = "Tabla Isan";
        $this->blade->render('impuestos/listado', $data);
    }

    public function entradaTabla($id='')
    {
        $data['data'] = [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Catálogos";
        $data['titulo'] = "Impuesto";
        $data['subtitulo'] = "Tabla Isan";
        $this->blade->render('impuestos/tabla', $data);
    }
}
