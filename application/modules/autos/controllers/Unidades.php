<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Unidades extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Catálogos";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Catalogo de autos";

        $this->blade->render('unidades/listado', $data);
    }



    public function ajax_unidades()
    {
        $responseData = $this->curl->curlGet('api/unidades');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }

    public function ajax_catalogo_autos()
    {
        $responseData = $this->curl->curlGet('api/cat-lineas');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }

    public function alta()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Catálogos";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Registro";

        $this->blade->render('unidades/alta', $data);
    }

    public function editar($id = '')
    {
        if (!$id) {
            return $this->index();
        }

        $responseData = $this->curl->curlGet('api/catalogo-autos/' . $id);
        $dataregistro = procesarResponseApiJsonToArray($responseData);
        $data['detalle'] = isset($dataregistro)  ? $dataregistro : [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Catálogos";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Edición";

        $this->blade->render('unidades/alta', $data);
    }
}
