@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
        
            <form id="alta_unidad"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="producto_id_sat">No. de Orden:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->orden_folio)){print_r($data->orden_folio);} ?>"  id="orden_folio" name="orden_folio" placeholder="">
                            <div id="orden_folio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

				<div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="producto_id_sat">Usuario que solicita:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->orden_usuario)){print_r($data->orden_usuario);} ?>"  id="orden_usuario" name="orden_usuario" placeholder="">
                            <div id="orden_usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="producto_id_sat">Fecha de solicitud:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->orden_fecha)){print_r($data->orden_fecha);}else {echo date('Y-m-d');} ?>" id="orden_fecha" name="orden_fecha" placeholder="">
                            <div id="orden_fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <h4>Equipo a solicitar</h4>
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" align="center">#</th>
                                    <th scope="col" align="center">Equipo</th>
                                    <th scope="col" align="center">Clave</th>
                                    <th scope="col" align="center">Cantidad</th>
                                    <th scope="col" align="center">
                                        <a title="Agregar equipo" class="btn btn-success" data-target="#cargar" data-toggle="modal" style="cursor: pointer;color:white;">
                                            <i class="fas fa-plus"></i>
                                        </a>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row" align="center">1</th>
                                    <td align="center">Demo</td>
                                    <td align="center">0001</td>
                                    <td align="center">1</td>
                                    <td align="center">
                                        <a title="Eliminar equipo" data-value="1" class="btn btn-danger" data-target="#eliminar" data-toggle="modal" style="cursor: pointer;color:white;">
                                            <i class='fas fa-trash'></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" align="center">2</th>
                                    <td>
                                        <input type="text" class="form-control" value="" placeholder="Demo 2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="" placeholder="0002">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="" placeholder="1">
                                    </td>
                                     <td align="center">
                                         <a title="Eliminar equipo" data-value="2" class="btn btn-danger" data-target="#eliminar" data-toggle="modal" style="cursor: pointer;color:white;">
                                            <i class='fas fa-trash'></i>
                                        </a>
                                     </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


            	<div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection