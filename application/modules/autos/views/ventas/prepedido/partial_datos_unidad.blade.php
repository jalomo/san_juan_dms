<div class="row">
    <div class="col-md-4">
        <input type="hidden" name="id_unidad" id="id_unidad" value="{{ isset($detalle_unidad) ? $detalle_unidad->id :'' }}">
        <?php renderInputText("text", "modelo", "Modelo",  isset($detalle_unidad) ? $detalle_unidad->modelo : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "modelo_descripcion", "Modelo descripcion",  isset($detalle_unidad) ? $detalle_unidad->modelo_descripcion : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "linea", "Linea",  isset($detalle_unidad) ? $detalle_unidad->linea : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "economico", "No economico",  isset($detalle_unidad) ? $detalle_unidad->economico : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : null, true); ?>
    </div>
    <div class="col-md-2">
        <?php renderInputText("text", "cilindros", "Cilindros",  isset($detalle_unidad) ? $detalle_unidad->cilindros : null, true); ?>
    </div>
    <div class="col-md-2">
        <?php renderInputText("text", "puertas", "Puertas",  isset($detalle_unidad) ? $detalle_unidad->puertas : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "combustible", "Combustible",  isset($detalle_unidad) ? $detalle_unidad->combustible : null, true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "motor", "Motor",  isset($detalle_unidad) ? $detalle_unidad->motor : '', true); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("text", "transmision", "Transmision",  isset($detalle_unidad) ? $detalle_unidad->transmision : '', true); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <input type="hidden" name="precio_venta" value="{{ isset($detalle_unidad->precio_venta) ? $detalle_unidad->precio_venta : '' }}" id="precio_venta">
        <input type="hidden" name="id_estatus" id="id_estatus" value="1">
        <?php renderInputText("text", "descuento", "Descuento",  isset($detalle_unidad->descuento) ? $detalle_unidad->descuento : ''); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("number", "impuesto_isan", "Impuesto ISAN", isset($detalle_unidad->impuesto_isan) ? $detalle_unidad->impuesto_isan : ''); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputText("number", "iva", "IVA", isset($detalle_unidad->iva) ? $detalle_unidad->iva : '',false,'', 'onChange="validariva()"'); ?>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="id_tipo_auto">Tipo de auto</label>
            <select disabled class="form-control" name="id_tipo_auto" id="id_tipo_auto">
                <option value=""> Seleccionar ..</option>
                <option value="1" selected >Nuevo</option>
            </select>
            <div id="id_tipo_auto_error" class="invalid-feedback"></div>
        </div>
        <input type="hidden" name="moneda" id="moneda" value="MXN">
    </div>
    <div class="col-md-2">
        <?php renderInputText("text", "tasa_interes", "Tasa de interes",  1); ?>
    </div>
    <div class="col-md-4">
        <?php renderInputTextArea("unidad_descripcion", "Descripcion de unidad",  isset($detalle_unidad) ? $detalle_unidad->unidad_descripcion : '', true); ?>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="precio" class="">Precio venta</label>
            <div>
                <h2 class="text-danger">
                    <span>$</span>
                    <span id="precio_number"> -- </span>
                    <span>MXN</span>
                </h2>
            </div>
        </div>
    </div>
</div>
