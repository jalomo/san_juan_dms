@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>
        @include('ventas/partial_venta_steps')
        <div class="row mt-3">
            @if (isset($venta->id))
                <div class="col-md-12 text-right">
                    <a target="_blank" href="{{ base_url('autos/ventas/pdfcontrato/' . $venta->id) }}" class="btn btn-w-md btn-success">
                        Generar Contrato
                    </a>
                </div>
            @else
                <div class="col-md-8"></div>
                <div class="col-md-4 text-right">
                    <div class="alert alert-danger">
                        Firmar el documento para generar contrato
                    </div>
                </div>
            @endif
        </div>
        <h3>DATOS DEL CLIENTE (CONSUMIDOR)</h3>
        <div class="row mt-3">
            <div class="col-md-6">
                <?php renderInputText('text', 'nombre_cliente', 'Nombre', $nombre_cliente, true); ?>
                <input type="hidden" name="id_venta_auto" id="id_venta_auto" value="{{ $venta->id_venta_auto }}">
            </div>
            <div class="col-md-6">
                <?php renderInputText('text', 'rfc', 'RFC', $venta->cliente_rfc, true); ?>
            </div>
            <div class="col-md-3">
                <?php renderInputText('text', 'direccion', 'Domicilio', $venta->direccion, true); ?>
            </div>
            <div class="col-md-2">
                <?php renderInputText('text', 'numero_ext', 'No. Ext.', !empty($venta->numero_ext) ?
                $venta->numero_ext : '-', true); ?>
            </div>
            <div class="col-md-3">
                <?php renderInputText('text', 'colonia', 'Colonia', $venta->colonia, true); ?>
            </div>
            <div class="col-md-2">
                <?php renderInputText('text', 'cp', 'Codigo Postal', $venta->codigo_postal, true); ?>
            </div>
            <div class="col-md-2">
                <?php renderInputText('text', 'estado', 'Estado', $venta->estado, true); ?>
            </div>
        </div>
        <hr>
        <h3>CARACTERISTICAS DEL VEHICULO</h3>
        <div class="row mt-3">
            <div class="col-md-2">
                <?php renderInputText('text', 'linea', 'Linea', $venta->linea, true); ?>
            </div>
            <div class="col-md-5">
                <?php renderInputText('text', 'descripcion', 'Descripción', $venta->descripcion_linea, true);
                ?>
            </div>
            <div class="col-md-5">
                <?php renderInputText('text', 'color', 'Color', $venta->color_interior . ' / ' .
                $venta->color_exterior, true); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText('text', 'economico', 'No. economico', $venta->economico, true); ?>
            </div>
            <div class="col-md-2">
                <?php renderInputText('text', 'anio', 'Año', $venta->anio_modelo, true); ?>
            </div>
            <div class="col-md-2">
                <?php renderInputText('text', 'capacidad', 'Capacidad', $venta->capacidad, true); ?>
            </div>
        </div>
        <hr>
        <h3>MONTO DE LA OPERACIÓN</h3>
        <div class="row mt-3">
            <div class="col-md-6">
                <?php renderInputText('text', 'precio_venta', 'Precio de venta', '$ ' . $venta->precio_venta,
                true); ?>
            </div>
            <div class="col-md-6">
                <?php renderInputText('text', 'forma_pago', 'Forma de pago', $venta->plazo_credito, true);
                ?>
            </div>
            <div class="col-md-6">
                <?php renderInputText('text', 'precio_venta', 'Monto de la opreración', '$ ' .
                $venta->precio_venta, true); ?>
            </div>
        </div>
        <hr>
        <h3>EQUIPO Y ACCESORIOS ADICIONALES</h3>
        <div class="row mt-3">
            <div class="col-md-12">
                @if (empty($equipo_opcional))
                    <div class="alert alert-success">
                        Sin elementos
                    </div>
                @else
                    <ul>
                        @foreach ($equipo_opcional as $item)
                            <li>{{ $item->descripcion }} <b> ${{ $item->valor_unitario * $item->cantidad }} </b></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6 text-center">
                <label for=""> FIRMA DE CLIENTE </label>
                <br>
                <div class="mb-3">
                    <img class="img_firma_cliente"
                        src="{{ isset($venta->firma_cliente) ? $venta->firma_cliente : '' }}" id="img_firma_cliente"
                        alt="" style="height:2cm;width:4cm;">
                    <input type="hidden" name="base_64_img_firma_cliente" id="base_64_img_firma_cliente"
                        value="{{ isset($venta->firma_cliente) ? $venta->firma_cliente : '' }}">
                </div>
                <button onclick="tipofirma(1)" class="cuadroFirma btn btn-w-md btn-info" data-toggle="modal"
                    data-target="#firmaDigital">
                    Firmar
                </button>
            </div>
            <div class="col-md-6 text-center">
                <label for=""> GERENTE DE VENTAS </label>
                <br>
                <div class="mb-3">
                    <img class="img_firma_gerente_ventas"
                        src="{{ isset($venta->firma_gerente_ventas) ? $venta->firma_gerente_ventas : '' }}"
                        id="img_firma_gerente_ventas" alt="" style="height:2cm;width:4cm;">
                    <input type="hidden" name="base_64_img_firma_gerente_ventas" id="base_64_img_firma_gerente_ventas"
                        value="{{ isset($venta->firma_gerente_ventas) ? $venta->firma_gerente_ventas : '' }}">
                </div>
                <button onclick="tipofirma(2)" class="cuadroFirma btn btn-w-md btn-info" data-toggle="modal"
                    data-target="#firmaDigital">
                    Firmar
                </button>
            </div>

            @if ($venta->plazo_credito_id !== 14)
                <div class="col-md-12 text-center">
                    <label for=""> GERENTE DE CREDITO </label>
                    <br>
                    <div class="mb-3">
                        <img class="img_firma_gerente_credito"
                            src="{{ isset($venta->firma_gerente_credito) ? $venta->firma_gerente_credito : '' }}"
                            id="img_firma_gerente_credito" alt="" style="height:2cm;width:4cm;">
                        <input type="hidden" name="base_64_img_firma_gerente_credito" id="base_64_img_firma_gerente_credito"
                            value="{{ isset($venta->firma_gerente_credito) ? $venta->firma_gerente_credito : '' }}">
                    </div>
                    <button onclick="tipofirma(3)" class="cuadroFirma btn btn-w-md btn-info" data-toggle="modal"
                        data-target="#firmaDigital">
                        Firmar
                    </button>
                </div>
            @endif
            <input type="hidden" id="data_firma_cliente" name="data_firma_cliente" value="">
            <input type="hidden" id="data_firma_gerente_ventas" name="data_firma_gerente_ventas" value="">
            <input type="hidden" id="data_firma_gerente_credito" name="data_firma_gerente_credito" value="">
        </div>
        <div class="row">
            <div class="col-md-12">
                <button onclick="guardarcontrato()" class="btn btn-primary">
                    Guardar y continuar
                </button>
            </div>
        </div>
    </div>

    <!-- Modal para la firma eléctronica-->
    <div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false"
        style="z-index:3000;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title" id="firmaDigitalLabel">Firmar</h4>
                </div>
                <div class="modal-body">
                    <!-- signature -->
                    <div class="text-center">
                        <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                            Su navegador no soporta canvas
                        </canvas>
                    </div>
                    <!-- signature -->
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="firma_actual" value="">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        id="cerrarCuadroFirma">Cerrar</button>
                    <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                    <button type="button" class="btn btn-primary" name="btnSign" data-dismiss="modal"
                        id="btnSign">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script>
        var signaturePad = new SignaturePad(document.getElementById('canvas'));
        $("#btnSign").on('click', function() {
            //Recuperamos la ruta de la imagen
            var data = signaturePad.toDataURL('image/png');
            //Comprobamos a donde se enviara
            const firma_actual = parseInt(document.getElementById('firma_actual').value);
            switch (firma_actual) {
                case 1:
                    $('#data_firma_cliente').val(data);
                    $("#img_firma_cliente").attr("src", data);
                    $("#base_64_img_firma_cliente").val(data);

                    break;
                case 2:
                    $('#data_firma_gerente_ventas').val(data);
                    $("#img_firma_gerente_ventas").attr("src", data);
                    $("#base_64_img_firma_gerente_ventas").val(data);
                    break;
                case 3:
                    $('#data_firma_gerente_credito').val(data);
                    $("#img_firma_gerente_credito").attr("src", data);
                    $("#base_64_img_firma_gerente_credito").val(data);
                    break;
                default:
                    break;
            }

            signaturePad.clear();
        });

        const tipofirma = (id_firma) => {
            document.getElementById('firma_actual').value = id_firma;
        }

        const guardarcontrato = () => {

            ajax.post('api/contrato-venta/createupdate', {
                id_venta_auto: document.getElementById('id_venta_auto').value,
                firma_cliente: document.getElementById('base_64_img_firma_cliente') ? document.getElementById(
                    'base_64_img_firma_cliente').value : '',
                firma_gerente_credito: document.getElementById('base_64_img_firma_gerente_credito') ? document
                    .getElementById('base_64_img_firma_gerente_credito').value : '',
                firma_gerente_ventas: document.getElementById('base_64_img_firma_gerente_ventas') ? document
                    .getElementById('base_64_img_firma_gerente_ventas').value : '',
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                utils.displayWarningDialog("Contrato actualizado", "success", function(data) {
                    return window.location.href = base_url + 'autos/ventas/index';
                })
            })
        }

    </script>
@endsection
