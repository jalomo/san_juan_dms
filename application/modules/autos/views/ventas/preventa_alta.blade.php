@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <div class="row">
        <div class="col-md-10">
            <?php 
            // dd($detalle_unidad);    
            ?>
        </div>
        <div class="col-md-2 text-rigth">
            <a class="btn btn-primary" href="{{ base_url('autos/ventas/unidades') }}">
                <i class="fas fa-list"></i> Regresar a listado
            </a>
        </div>
    </div>
    <h3>Datos de vendedor</h3>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "vendedor", "Vendedor",  isset($data_venta) ? $data_venta->nombre_vendedor . ' ' . $data_venta->apellido_paterno_vendedor : $nombre_usuario, isset($data_venta) ? true : false); ?>
            <input type="hidden" id="id_asesor" name="id_asesor" value="{{ isset($data_venta) ? $data_venta->id_asesor : $id_asesor }}">

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="fecha_alta">Fecha de alta:</label>
                <input type="date" class="form-control" min="{{ date('Y-m-d') }}" value="{{ isset($data->created_at) ? $data->created_at :  date('Y-m-d') }}" id="orden_fecha" name="orden_fecha" placeholder="">
                <div id="fecha_alta_error" class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <h3>Datos del Cliente</h3>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Cliente</label>
                <select name="id_cliente" class="form-control " id="id_cliente">
                    <option value=""> Seleccionar</option>
                    @foreach ($catalogo_clientes as $cliente)
                    @if (isset($data_venta) && $data_venta->cliente_id == $cliente->id)
                    <option selected value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>
                    @else
                    <option value="{{ $cliente->id}}"> {{ $cliente->numero_cliente .' - '. $cliente->nombre .' '. $cliente->apellido_materno}} </option>

                    @endif
                    @endforeach
                </select>
                <div id='id_cliente_error' class='invalid-feedback'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">Nombre completo</label>
                <input class="form-control" type="text" {{ isset($data_venta) ? 'disabled': '' }} id="nombre_cliente" value="{{ isset($data_venta) ? $data_venta->nombre_cliente . ' '. $data_venta->cliente_apellido_paterno : '' }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="select">RFC</label>
                <input class="form-control" type="text" id="rfc_cliente" {{ isset($data_venta) ? 'disabled': '' }} value="{{ isset($data_venta) ? $data_venta->cliente_rfc  : '' }}">
            </div>
        </div>
    </div>
    @if (empty($detalle_unidad))
        <h3 class="mt-3">Datos de la Unidad</h3>
        <div class="row">
            <div class="col-md-4 ">
                <?php renderInputText("text", "descripcion_modelo", "Descripción Modelo",  isset($data_venta) ? $data_venta->descripcion_modelo : ''); ?>
            </div>    
            <div class="col-md-8 mt-4">
                <div class="alert alert-warning">
                    La unidad aun no se ha cargado en inventario, indica el modelo.
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php renderInputText("number", "precio_venta", "Precio de venta",  '', false, '', 'onKeyup="validarprecioventa()"'); ?>
            </div>
            <div class="col-md-4">
                <input type="hidden" name="id_estatus" id="id_estatus" value="17">
                <?php renderInputText("number", "descuento", "Descuento", 0); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText("number", "impuesto_isan", "Impuesto ISAN", 0); ?>
            </div>
            <div class="col-md-4">
                <?php renderInputText("number", "iva", "IVA", 0,false,'', 'onChange="validariva()"'); ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="id_tipo_auto">Tipo de auto</label>
                    <select disabled class="form-control" name="id_tipo_auto" id="id_tipo_auto">
                        <option value=""> Seleccionar ..</option>
                        <option value="1" selected >Nuevo</option>
                    </select>
                    <div id="id_tipo_auto_error" class="invalid-feedback"></div>
                </div>
                <input type="hidden" name="moneda" id="moneda" value="MXN">
            </div>
            <div class="col-md-2">
                <?php renderInputText("text", "tasa_interes", "Tasa de interes",  1); ?>
            </div>
        </div>
        @else

        <h3 class="mt-3">Datos de la Unidad</h3>
        @include('ventas/prepedido/partial_datos_unidad')
       
    @endif
    <h3>Formas de pago</h3>
    <div class="row">
        <div class="col-md-4">
        <?php renderSelectArray('tipo_forma_pago_id', '¿Compra de credito ó contado?', $tipo_forma_pago, 'id', 'descripcion', isset($data_venta) ? $data_venta->tipo_forma_pago_id : null) ?>
            
        </div>
        <div class="col-md-4">
            <?php renderSelectArray('tipo_pago_id', '¿De que manera realizará el pago?', $tipo_pago, 'id', 'nombre', isset($data_venta) ? $data_venta->tipo_pago_id : null) ?>
        </div>
        <div class="col-md-4 container-forma-pago">
            <?php renderSelectArray('plazo_credito_id', 'Plazo de credito', $plazo_credito, 'id', 'nombre', isset($data_venta) ? $data_venta->plazo_credito_id : null) ?>
        </div>
        <div class="col-md-4 ">
            <?php renderInputText("text", "enganche", "Enganche",  0); ?>
        </div>
        <div class="col-md-4 container-forma-pago">
            <div class="form-group">
                <label for="precio" class="">Precio total a financiar</label>
                <div>
                    <h2 class="text-danger">
                        <span>$</span>
                        <span id="total_con_enganche"> -- </span>
                        <span>MXN</span>
                    </h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-4 text-center container-precio-venta">
            <h3>El plan de pago que tenemos:</h3>
            <div class="row">
                <div class="col-md-6 text-center">
                    <span id="label_tipo_pago">Pago mensual:</span>
                    <h4>
                        <span>$</span>
                        <span class="text-danger" id="precio_mensualidad"> -- </span>
                        <span>MXN</span>
                    </h4>
                </div>
                <div class="col-md-6 text-center">
                    <span>Tasa fija:</span>
                    <h4 class="">
                        <span></span>
                        <span class="text-danger" id="label_tasa_fija"> -- </span>
                        <span>%</span>
                    </h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="accordion" id="acordeonPagos">
                <div class="card">
                    <div class="card-header" id="detalle_pagos">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Detalle de pagos
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="detalle_pagos" data-parent="#acordeonPagos">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Enganche</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_enganche">
                                            --
                                        </label>
                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Mensualidad</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_mensualidad">
                                            --
                                        </label>

                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 pl-3">
                                    <h3>Precio total a financiar</h3>
                                </div>
                                <div class="col-md-4 text-right">
                                    <h2 class="text-danger">
                                        $
                                        <label for="enganche" id="lbl_total_con_enganche">
                                            --
                                        </label>

                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="collapse_tabla_amortizacion">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Tabla de amortización
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="collapse_tabla_amortizacion" data-parent="#acordeonPagos">
                        <div class="card-body">
                            <table class="table table-bordered" id="tabla_amortizacion" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Plazo de credito</th>
                                        <th>Saldo insoluto</th>
                                        <th>Pago total en periodo</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Plazo de credito</th>
                                        <th>Saldo insoluto</th>
                                        <th>Pago total en periodo</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 row">
       @if (empty($data_venta->id))
       <div class="col-md-12">
           
            <button class="btn btn-success" id="procesar_venta">
               <i class="fas fa-car"></i> Crear Preventa
            </button>

        </div> 
        @else 
        <div class="col-md-12">
            <button class="btn btn-success" id="confirmar_venta">
               <i class="fas fa-car"></i> Confirmar venta
            </button>
        </div>
       @endif

    </div>
</div>
@endsection

@section('scripts')
<script>
    // nombre_usuario
    $(".container-forma-pago").hide();
    var tabla_amortizacion = $('#tabla_amortizacion').DataTable({
        // "order": [[ 0, "desc" ]],
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: base_url + "autos/ventas/ajax_tabla_amortizacion",
            type: 'POST',
            data: {
                precio_venta: () => $('#precio_venta').val(),
                plazo: () => $("#plazo_credito_id").val() ? $("#plazo_credito_id").val() : 0,
                enganche: () => $("#enganche").val()
            }
        },
        columns: [
            {
                'data': function(data) {
                    return data.id;
                }
            },
            {
                'data': function(data) {
                    return data.plazo_credito;
                }
            },
            {
                'data': function(data) {
                    return data.saldo_insoluto;
                }
            },
            {
                'data': function(data) {
                    return data.pago_periodo;
                }
            }
        ]
    });

    $("#tipo_forma_pago_id").on("change", function() {
        let tipo_forma_pago_id = $("#tipo_forma_pago_id").val();
        if (tipo_forma_pago_id == 2) {
            $(".container-forma-pago").show();
        } else {
            $("#plazo_credito_id").val(null)
            $(".container-forma-pago").hide();
        }
    });

    $("#plazo_credito_id").on('change', function(e) {
        calcular_pago();
    });


    let load_data_unidad = () => {
        let id_unidad = $("#id_unidad").val();
        let tasa = $("#tasa_interes").val();
        $("#label_tasa_fija").text(tasa);
        ajax.get(`api/unidades/${id_unidad}`, {}, (data, headers) => {
            $("#precio_number").text(data.precio_venta);
            calcular_pago();
            bloquear_campos();
        })
      
    };

    const validarprecioventa = ()=>{
        let unidad_value = $("#precio_venta").val();
        if (unidad_value == '') {
            $("#tipo_forma_pago_id").attr('disabled', true);
            $("#tipo_pago_id").attr('disabled', true);
        }else{
            $("#tipo_forma_pago_id").attr('disabled', false);
            $("#tipo_pago_id").attr('disabled', false);
        }
    }
    
    if($("#id_unidad").val() && $("#id_unidad").val() !== ''){
        load_data_unidad();
    }

    if (!document.getElementById('id_unidad') && document.getElementById('descripcion_modelo')) {
        if(document.getElementById('descripcion_modelo').value == ''){
            validarprecioventa();
        }
    }

    $("#tasa_interes").on('change', function(e) {
        let tasa = $("#tasa_interes").val();
        $("#label_tasa_fija").text(tasa);
    });

    $("#id_cliente").on('change', function(e) {
        let id_cliente = $("#id_cliente").val();
        ajax.get(`api/clientes/${id_cliente}`, {}, (data, headers) => {
            let response = data[0];
            $("#nombre_cliente").val(`${response.nombre} ${response.apellido_materno} ${response.apellido_paterno}`);
            $("#rfc_cliente").val(response.rfc);
        })
    });

    let bloquear_campos = () => {
        let unidad_value = $("#id_unidad").val();
        if (unidad_value.length == 0) {
            return $("#tipo_forma_pago_id").attr('disabled', true);
        }
        return $("#tipo_forma_pago_id").attr('disabled', false);
    };

    if($("#id_unidad").val()){
        bloquear_campos();
    }

    let calcular_pago = (valor_enganche = null) => {
        let plazo = $("#plazo_credito_id").val();
        let precio = $("#precio_venta").val();
        if (plazo !== '' && plazo !== null) {
            let obtener_enganche = valor_enganche === null ? precio * 10 / 100 : valor_enganche;
            let precio_con_enganche = precio - obtener_enganche;
            $("#total_con_enganche").text(precio_con_enganche);
            $("#lbl_total_con_enganche").text(precio_con_enganche);

            $("#enganche").val(obtener_enganche);
            $("#lbl_enganche").text(obtener_enganche);

            let valor_mensualidad = Math.round(precio_con_enganche / plazo);
            $("#precio_mensualidad").text(valor_mensualidad);
            $("#lbl_mensualidad").text(valor_mensualidad);

            $("#label_tipo_pago").text("Pago mensual");
            tabla_amortizacion.ajax.reload();
            return false;
        }else{
            // precio * 10 / 100
            let obtener_enganche = valor_enganche === null ? precio * 10 / 100 : valor_enganche;
            $("#enganche").val(obtener_enganche);
        }

        tabla_amortizacion.ajax.reload();
        $("#precio_number").text(precio);
        $("#precio_mensualidad").text(precio);
        $("#label_tipo_pago").text("Pago contado");
    }

    $("#enganche").on('keyup', function(e) {
        let precio = $("#precio_venta").val();
        let enganche_minimo = precio * 10 / 100;
        let valor_enganche = $("#enganche").val();
        // if (valor_enganche < enganche_minimo) {

        // }
        calcular_pago(valor_enganche);
    });

    $("#procesar_venta").on('click', function(e) {
        if ($("#id_cliente").val() == "") {
            toastr.error("Seleccionar o registrar cliente")
            return false;
        }

        if ($("#precio_venta").val() == "") {
            toastr.error("Indicar el precio de venta")
            return false;
        }
        
        if (!document.getElementById('id_unidad') && document.getElementById('descripcion_modelo')) {
            if(document.getElementById('descripcion_modelo').value == ''){
                toastr.error("Proporciona una descripción de la unidad")
                document.getElementById("descripcion_modelo").focus();
                return false;
            }
        }else if(document.getElementById('id_unidad') && $("#id_unidad").val() == ""){
            toastr.error("Seleccionar unidad");
            return false;
        }

        if ($("#tipo_forma_pago_id").val() == "" || $("#tipo_pago_id").val() == '') {
            utils.displayWarningDialog("Seleccionar la forma de pago. ", "warning", function(data) {})
            return false;
        }

        let plazo_credito = $("#tipo_forma_pago_id").val() == 1 ? 14 : $("#plazo_credito_id").val();

        if ($("#tipo_forma_pago_id").val() == 2 && $("#plazo_credito_id").val() == '') {
            utils.displayWarningDialog("Indicar plazo de pago. ", "warning", function(data) {})
            return false;
        }

        if($("#iva").val()  > 100){
            utils.displayWarningDialog("el valor de iva debe ser de 1 - 100", "warning", function(data) {})
            return false;
        }
       
        ajax.post(`api/venta-unidades`, {
            "id_unidad": document.getElementById('id_unidad') ? $("#id_unidad").val() : null,
            "descripcion_modelo": document.getElementById('descripcion_modelo') ? $("#descripcion_modelo").val() : '',
            "id_cliente": $("#id_cliente").val(),
            "id_estatus": $("#id_estatus").val(),
            "id_tipo_auto": $("#id_tipo_auto").val(),
            "tipo_forma_pago_id": $("#tipo_forma_pago_id").val(),
            "tipo_pago_id": $("#tipo_pago_id").val(),
            "plazo_credito_id": plazo_credito,
            "moneda": $("#moneda").val(),
            "tasa_interes": $("#tasa_interes").val(),
            "enganche": $("#enganche").val(),
            "total": $("#precio_venta").val(),
            "impuesto_isan": $("#impuesto_isan").val(),
            "descuento": $("#descuento").val(),
            "id_asesor": $("#id_asesor").val(),
            "iva": $("#iva").val()           
        }, (data, headers) => {
            
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            
            utils.displayWarningDialog("Folio de Preventa: "+ data.folio, "success", function(datamodal) {
                return window.location.href = base_url + `autos/ventas/index`;              
            })
        })
    });

    const validariva = ()=> {
        const iva = document.getElementById('iva').value;
        
        if(iva > 100){
            $("#procesar_venta").attr('disabled', true)
            return toastr.error("el valor de iva debe ser de 1 - 100");
        }else{
            $("#procesar_venta").attr('disabled', false)
        }
    }

</script>
@endsection