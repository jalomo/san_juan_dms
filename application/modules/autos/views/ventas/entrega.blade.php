@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="entrega"> <!-- enctype="multipart/form-data"  -->
				<div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Usuario que entrega:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_usuario)){print_r($data->entrega_usuario);} ?>"  id="entrega_usuario" name="entrega_usuario" placeholder="">
                            <div id="entrega_usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de entrega:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->entrega_fecha)){print_r($data->entrega_fecha);}else {echo date('Y-m-d');} ?>" id="entrega_fecha" name="entrega_fecha" placeholder="">
                            <div id="entrega_fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">No de pedido:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_pedido)){print_r($data->entrega_pedido);} ?>"  id="entrega_pedido" name="entrega_pedido" placeholder="">
                            <div id="entrega_pedido_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h3 style="text-align: center;">Datos de la unidad</h3>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Categoria:</label>
                            <select class="form-control" id="entrega_categoria" name="entrega_categoria" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="entrega_categoria_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Sub-categoria:</label>
                            <select class="form-control" id="entrega_subcategoria" name="entrega_subcategoria" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="entrega_subcategoria_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Marca:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_marca)){print_r($data->entrega_marca);} ?>"  id="entrega_marca" name="entrega_marca" placeholder="">
                            <div id="entrega_marca_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Modelo:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_modelo)){print_r($data->entrega_modelo);} ?>"  id="entrega_modelo" name="entrega_modelo" placeholder="">
                            <div id="no_identificacion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Año:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_anio)){print_r($data->entrega_anio);} ?>"  id="entrega_anio" name="entrega_anio" placeholder="">
                            <div id="entrega_anio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Color:</label>
                            <select class="form-control" id="entrega_color" name="entrega_color" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="entrega_color_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Motor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_motor)){print_r($data->entrega_motor);} ?>"  id="entrega_motor" name="entrega_motor" placeholder="">
                            <div id="entrega_motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Transmisión:</label>
                            <!--<input type="text" class="form-control" value="<?php if(isset($data->entrega_transmision)){print_r($data->entrega_transmision);} ?>"  id="entrega_transmision" name="entrega_transmision" placeholder="">-->
                            <select class="form-control" id="entrega_transmision" name="entrega_transmision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="AUTOMÁTICA" <?php if(isset($data->entrega_transmision)){ if($data->entrega_transmision == "AUTOMÁTICA") echo "selected";} ?> >AUTOMÁTICA </option>
                                <option value="ESTÁNDAR" <?php if(isset($data->entrega_transmision)){ if($data->entrega_transmision == "ESTÁNDAR") echo "selected";} ?> >ESTÁNDAR </option>
                                <option value="CVT" <?php if(isset($data->entrega_transmision)){ if($data->entrega_transmision == "CVT") echo "selected";} ?> >CVT </option>
                                <option value="SEMI-AUTOMÁTICA" <?php if(isset($data->entrega_transmision)){ if($data->entrega_transmision == "SEMI-AUTOMÁTICA") echo "selected";} ?> >SEMI-AUTOMÁTICA </option>
                            </select>
                            <div id="entrega_transmision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>   
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">VIN:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_vin)){print_r($data->entrega_vin);} ?>"  id="entrega_vin" name="entrega_vin" placeholder="">
                            <div id="entrega_vin_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Cantidad a ingresar:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->entrega_cantidad)){print_r($data->entrega_cantidad);} ?>"  id="entrega_cantidad" name="entrega_cantidad" placeholder="">
                            <div id="entrega_cantidad_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

            	<div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection