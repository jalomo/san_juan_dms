@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>        
       @include('ventas/partial_venta_steps')

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

    </script>
@endsection
