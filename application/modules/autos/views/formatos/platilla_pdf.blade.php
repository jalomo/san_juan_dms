<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
    <head>
        <title>Checklist de Entrega de Vehículos <?php if (isset($pedido)) echo $pedido; else echo "0"; ?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page {
                sheet-size: A4;
                size: portrait; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 10mm; /* <any of the usual CSS values for margins> */
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>

        <style>
            .contenedor {
                width: 100%;
                font-size: 10px;
            }

            .col-12 {
                width: 100%;
                padding: 3px;
            }

            .col-6 {
                float: left;
                width: 49%;
                padding: 3px;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th{
                font-size: 12px;
            }

            td{
                font-size: 10px;
            }

            .table.titles {
                /*background: black;*/
                background: black;
                color: white;
            }

            .col.noborders-topleft {
                border-top: 1px solid white !important;
                border-left: 1px solid white !important;
            }
        </style>
    </head>
     
    <body>
		<div class="contenedor">
            <table>
                <tr>
                    <td style="width: 25%;"  align="right">
                        <img src="<?php echo base_url().'assets/imgs/logos/logo_queretaro_2.png' ?>" alt="" style="width: 200px;">
                    </td>
                    <td style="width: 50%;font-size: 17px;font-weight: bold;" align="center">
                        Checklist de Entrega de Vehículos
                    </td>
                    <td style="width: 25%;"  align="right">
                        <img src="<?php echo base_url().'assets/imgs/logos/ford.png' ?>" alt="" style="width: 100px;height: 40px;">
                    </td>
                </tr>
            </table>

            
            <br>
            <table>
                <tr>
                    <td style="width: 10%;font-weight: bold;" align="left">
                        PEDIDO :
                    </td>
                    <td style="width: 30%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($pedido)) echo $pedido; ?>
                    </td>
                    <td style="width: 15%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 25%;font-weight: bold;" align="left">
                        Fecha de previa:
                    </td>
                    <td style="width: 20%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($fecha_previa_f)) echo $fecha_previa_f->format('d/m/Y'); ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;font-weight: bold;" align="left">
                        SERIE :
                    </td>
                    <td style="width: 30%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($serie)) echo $serie; ?>
                    </td>
                    <td style="width: 15%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 25%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 20%;color: darkblue;"  align="left">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;font-weight: bold;" align="left">
                        VEHÍCULO :
                    </td>
                    <td style="width: 30%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($vehiculo)) echo $vehiculo; ?>
                    </td>
                    <td style="width: 15%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 25%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 20%;color: darkblue;"  align="left">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;font-weight: bold;" align="left">
                        COLOR :
                    </td>
                    <td style="width: 25%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($color)) echo $color; ?>
                    </td>
                    <td style="width: 15%;font-weight: bold;" align="left">
                        <br>
                    </td>
                    <td style="width: 30%;font-weight: bold;" align="left">
                        Fecha de Elaboración de la Comanda:
                    </td>
                    <td style="width: 20%;color: darkblue;border-bottom: 0.5px solid black;"  align="left">
                        <?php if(isset($fecha_comanda_f)) echo $fecha_comanda_f->format('d/m/Y'); ?>
                    </td>
                </tr>
            </table>

            <br>
            <table>
                <tr>
                    <td style="width: 100%;background-color: #666699;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table>
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 36%;">
                        <table>
                            <tr>
                                <td style="width: 100%;font-size: 11px;font-weight: bold;" align="center" colspan="2">
                                    COMPROMISO DE ENTREGA A CLIENTE
                                </td>
                            </tr>
                            <tr>
                                <td style="" align="center" colspan="2">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 9px;font-weight: bold;" align="right">
                                    FECHA DE ENTREGA
                                </td>
                                <td style="width: 50%;color: darkblue;border-bottom:0.5 solid black;" align="left">
                                    <?php if(isset($fecha_entrada_f)) echo $fecha_entrada_f->format('d/m/Y'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 9px;font-weight: bold;" align="right">
                                    HORA DE ENTREGA
                                </td>
                                <td style="width: 50%;color: darkblue;border-bottom:0.5 solid black;" align="left">
                                    <?php if(isset($hora_entrada)) echo $hora_entrada; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="" align="center" colspan="2">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;" align="center" colspan="2">
                                    <?php if (isset($ruta_gerente_experiencia)): ?>
                                        <?php if ($ruta_gerente_experiencia != ''): ?>
                                            <img src="<?php if(isset($ruta_gerente_experiencia)) echo base_url().$ruta_gerente_experiencia; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center" colspan="2">
                                    <?php if(isset($nombre_gerente)) echo $nombre_gerente; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center" colspan="2">
                                    FIRMA DE GERENTE DE EXPERIENCIA
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 15%;">
                        <br>
                    </td>
                    <td style="width: 45%;">
                        <table>
                            <tr>
                                <td style="width: 100%;font-size: 11px;font-weight: bold;" align="center" colspan="2">
                                    COMPROMISO DE ENTREGA GERENTE DE EXPERIENCIA
                                </td>
                            </tr>
                            <tr>
                                <td style="" align="center" colspan="2">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 9px;font-weight: bold;" align="right">
                                    FECHA DE ENTREGA
                                </td>
                                <td style="width: 50%;color: darkblue;border-bottom:0.5 solid black;" align="left">
                                    <?php if(isset($fecha_entrada_logistica_f)) echo $fecha_entrada_logistica_f->format('d/m/Y'); ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 9px;font-weight: bold;" align="right">
                                    HORA DE ENTREGA
                                </td>
                                <td style="width: 50%;color: darkblue;border-bottom:0.5 solid black;" align="left">
                                    <?php if(isset($hora_entrada_logistica)) echo $hora_entrada_logistica; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="" align="center" colspan="2">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;" align="center" colspan="2">
                                    <?php if (isset($ruta_logistica)): ?>
                                        <?php if ($ruta_logistica != ''): ?>
                                            <img src="<?php echo base_url().$ruta_logistica; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center" colspan="2">
                                    <?php if(isset($nombre_logistica)) echo $nombre_logistica; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center" colspan="2">
                                    FIRMA DE LOGÍSTICA
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table>
                <tr>
                    <td style="width: 100%;background-color: #666699;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Extintor
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($extintor)) if($extintor == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($extintor)) if($extintor == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($extintor)) if($extintor == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Gato
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($gato)) if($gato == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($gato)) if($gato == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($gato)) if($gato == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Llanta de Refacción
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($llanta)) if($llanta == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($llanta)) if($llanta == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($llanta)) if($llanta == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Reflejantes
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($reflejante)) if($reflejante == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($reflejante)) if($reflejante == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($reflejante)) if($reflejante == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Llave "L"
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($llave_l)) if($llave_l == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($llave_l)) if($llave_l == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($llave_l)) if($llave_l == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Cables pasa corriente
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($cables)) if($cables == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($cables)) if($cables == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($cables)) if($cables == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Controles
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($controles)) if($controles == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($controles)) if($controles == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($controles)) if($controles == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Seguro de ruedas
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($seguro_ruedas)) if($seguro_ruedas == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($seguro_ruedas)) if($seguro_ruedas == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($seguro_ruedas)) if($seguro_ruedas == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Tapones (si aplica)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($tapones)) if($tapones == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($tapones)) if($tapones == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($tapones)) if($tapones == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Rines
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($rines)) if($tapones == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($rines)) if($tapones == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($rines)) if($rines == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Antena
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($antena)) if($antena == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($antena)) if($antena == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($antena)) if($antena == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Tapón de Gasolina
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($tapon_gasolina)) if($tapon_gasolina == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($tapon_gasolina)) if($tapon_gasolina == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($tapon_gasolina)) if($tapon_gasolina == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Encendedor
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($encendedor)) if($encendedor == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($encendedor)) if($encendedor == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($encendedor)) if($encendedor == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Cenicero
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($cenicero)) if($cenicero == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($cenicero)) if($cenicero == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($cenicero)) if($cenicero == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Tapetes (si aplica)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($tapetes)) if($tapetes == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($tapetes)) if($tapetes == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($tapetes)) if($tapetes == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Embudo
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($embudo)) if($embudo == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($embudo)) if($embudo == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($embudo)) if($embudo == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Llaves 2
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($llaves_2)) if($llaves_2 == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($llaves_2)) if($llaves_2 == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($llaves_2)) if($llaves_2 == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Luces
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($luces)) if($luces == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($luces)) if($luces == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($luces)) if($luces == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4">
                                    DOCUMENTOS
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Tarjeta de acceso
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($tarjeta_acceso)) if($tarjeta_acceso == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($tarjeta_acceso)) if($tarjeta_acceso == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($tarjeta_acceso)) if($tarjeta_acceso == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Póliza de garantía
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($poliza_garantia)) if($poliza_garantia == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($poliza_garantia)) if($poliza_garantia == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($poliza_garantia)) if($poliza_garantia == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Tarjeta de asistencia
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($tarjeta_asistencia)) if($tarjeta_asistencia == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($tarjeta_asistencia)) if($tarjeta_asistencia == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($tarjeta_asistencia)) if($tarjeta_asistencia == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Manual de propietario
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($manual)) if($manual == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($manual)) if($manual == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($manual)) if($manual == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Manual de propietario
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($manual)) if($manual == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($manual)) if($manual == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($manual)) if($manual == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 42%;">
                        <table>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4">
                                    LIMPIEZA
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Motor limpio
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($motor)) if($motor == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($motor)) if($motor == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($motor)) if($motor == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Carrocería limpia
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($carroceria)) if($carroceria == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($carroceria)) if($carroceria == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($carroceria)) if($carroceria == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Golpes
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($golpes)) if($golpes == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($golpes)) if($golpes == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($golpes)) if($golpes == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Rayones
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($rayones)) if($rayones == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($rayones)) if($rayones == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($rayones)) if($rayones == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Interiores limpios
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($interiores)) if($interiores == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($interiores)) if($interiores == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($interiores)) if($interiores == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Niveles
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($niveles)) if($niveles == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($niveles)) if($niveles == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($niveles)) if($niveles == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 5%;">
                        <br>
                    </td>
                    <td style="width: 49%;">
                        <table>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4">
                                    ADICIONALES
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Accesorios
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($accesorio)) if($accesorio == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($accesorio)) if($accesorio == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($accesorio)) if($accesorio == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4">
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="3">
                                    <img src="<?php echo base_url().'assets/imgs/indicador_gas.png'; ?>" style="width:200px;" alt="">
                                </td>
                                <td style="font-size: 10px;font-weight: bold;">
                                    <?php if(isset($gas_p1)) echo $gas_p1."/".$gas_p2;?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 49%;">
                        <table>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" align="">
                                    <img src="<?php echo base_url().'assets/imgs/carroceria.png'; ?>" alt="" style="width:200px;">

                                    <?php if (isset($danosMarcas)): ?>
                                        <?php if ($danosMarcas != ''): ?>
                                            <img src="<?php echo base_url().$danosMarcas; ?>" alt="" style="width:250px;height: 210px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:260px;height: 210px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:260px;height: 210px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 5%;">
                        <br>
                    </td>
                    <td style="width: 42%;">
                        <table>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4" align="center">
                                    RECIBO DE UNIDAD
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" align="right">
                                    FECHA:&nbsp;&nbsp;
                                </td>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;"  align="left" colspan="2">
                                    <?php if(isset($fecha_recibo_f)) echo $fecha_recibo_f->format('d/m/Y'); ?>
                                </td>
                                <td style="font-size: 10px;font-weight: bold;" align="right">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" align="right">
                                    HORA:&nbsp;&nbsp;
                                </td>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;"  align="left" colspan="2">
                                    <?php if(isset($hora_recibo)) echo $hora_recibo; ?>
                                </td>
                                <td style="font-size: 10px;font-weight: bold;" align="right">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4" align="center">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 12px;" align="center" colspan="4">
                                    <?php if (isset($ruta_asesor)): ?>
                                        <?php if ($ruta_asesor != ''): ?>
                                            <img src="<?php if(isset($ruta_asesor)) echo base_url().$ruta_asesor; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center" colspan="4">
                                    <?php if(isset($nombre_asesor)) echo $nombre_asesor; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center" colspan="4">
                                    NOMBRE Y FIRMA DE GERENTE DE EXPERIENCIA Y ASESOR 
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4" align="center">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 12px;" align="center" colspan="4">
                                    <?php if (isset($ruta_encargado_previas)): ?>
                                        <?php if ($ruta_encargado_previas != ''): ?>
                                            <img src="<?php if(isset($ruta_encargado_previas)) echo base_url().$ruta_encargado_previas; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center" colspan="4">
                                    <?php if(isset($encargado_previas)) echo $encargado_previas; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center" colspan="4">
                                    NOMBRE Y FIRMA DE ENCARGADO DE PREVIAS
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;font-weight: bold;" colspan="4" align="center">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;" colspan="4" align="justify">
                                    COMENTARIOS: <br>
                                    <p align="justify" style="text-align: justify;font-size: 7px;color:darkblue;">
                                        <?php if(isset($comentarios)) echo $comentarios; ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table>
                <tr>
                    <td style="width: 100%;background-color: #666699;">
                        <br>
                    </td>
                </tr>
            </table>

            <table style="width: 100%;">
                <tr>
                    <td><br></td>
                    <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                        DOCUMENTACIÓN REQUERIDA
                    </td>
                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Póliza
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($poliza)) if($poliza == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($poliza)) if($poliza == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($poliza)) if($poliza == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Carta Factura
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($carta_factura)) if($carta_factura == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($carta_factura)) if($carta_factura == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($carta_factura)) if($carta_factura == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Seguro
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($seguro)) if($seguro == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($seguro)) if($seguro == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($seguro)) if($seguro == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Permiso
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($permiso)) if($permiso == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($permiso)) if($permiso == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($permiso)) if($permiso == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Placas
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($placas)) if($placas == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($placas)) if($placas == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($placas)) if($placas == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Verificación
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($verificacion)) if($verificacion == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($verificacion)) if($verificacion == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($verificacion)) if($verificacion == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%;font-size: 8px;">
                                    Papelería
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    SI (&nbsp;<?php if(isset($papeleria)) if($verificacion == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NO (&nbsp;<?php if(isset($papeleria)) if($verificacion == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 15%;font-size: 7.5px;">
                                    NA (&nbsp;<?php if(isset($papeleria)) if($papeleria == "NA") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 36%;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table>
                <tr>
                    <td style="width: 100%;background-color: #666699;">
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <br>
                    </td>
                    <td style="font-size: 12px;font-weight: bold;" colspan="4" align="center">
                        AUTORIZACIÓN DE ENTREGA FUERA DE PROCESO
                    </td>
                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                    <td style="font-size: 10px;font-weight: bold;" colspan="3" align="center">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 60%;">
                                    REQUIERE AUTORIZACIÓN:
                                </td>
                                <td style="width: 40%;">
                                    SI 
                                    (&nbsp;<?php if(isset($autorizacion)) if($autorizacion == "SI") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                                <td style="width: 40%;">
                                    NO 
                                    (&nbsp;<?php if(isset($autorizacion)) if($autorizacion == "NO") echo "<u style='color:darkblue;'>X</u>";?>&nbsp;)
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 100%;font-size: 10px;" align="center">
                                    <?php if (isset($ruta_gerente_general)): ?>
                                        <?php if ($ruta_gerente_general != ''): ?>
                                            <img src="<?php if(isset($ruta_gerente_general)) echo base_url().$ruta_gerente_general; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center">
                                    <?php if(isset($gerente_general)) echo $gerente_general; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center">
                                    GERENTE GENERAL
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <table>
                            <tr>
                                <td style="width: 100%;font-size: 10px;" align="center">
                                    <?php if (isset($ruta_logistica_02)): ?>
                                        <?php if ($ruta_logistica_02 != ''): ?>
                                            <img src="<?php if(isset($ruta_logistica_02)) echo base_url().$ruta_logistica_02; ?>" style="width: 80px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                        <?php endif ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="width: 80px;">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: darkblue;border-bottom: 0.5px solid black;" align="center">
                                    <?php if(isset($logistica_2)) echo $logistica_2; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;font-size: 10px;font-weight: bold;" align="center">
                                    LOGÍSTICA
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                    <td style="width: 30%;" align="justify">
                        MOTIVO: <br>
                        <p align="justify" style="text-align: justify;font-size: 7px;color:darkblue;">
                            <?php if(isset($motivo)) echo $motivo; ?>
                        </p>
                    </td>
                    <td style="width: 2%;">
                        <br>
                    </td>
                </tr>
            </table>
		</div>
     </body>
</html>

<?php 
    try {      

        $html = ob_get_clean();
        ob_clean();

        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        $mpdf = new \mPDF('utf-8', 'A4-P');

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        $mpdf->Output();

    } catch (Exception $e) {
        //$html2pdf->clean();
        //$formatter = new ExceptionFormatter($e);
        //echo $formatter->getHtmlMessage();
        echo "NO SE PUDO CARGAR CORRECTAMENTE EL PDF";
    }
 ?>


