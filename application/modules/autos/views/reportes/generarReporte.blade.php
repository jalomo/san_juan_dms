@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Fecha inicio:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_inicio">
            </div>
        </div>
        <div class="col-md-4">
            <label for="">Fecha fin:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_fin">
            </div>
        </div>

        <div class="col-md-4">
            <label for="">Usuario:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-user"></i>
                    </div>
                </div>

                <select class="form-control" id="salida_unidad" name="salida_unidad">
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
        </div>
        <div class="col-md-4" align="center">
            <a class="btn btn-primary" href="#">
                <i class="fas fa-file-download"></i>
                Generar reporte
            </a>
        </div>
        <div class="col-md-4">
        </div>
    </div>

    
</div>

@endsection

@section('scripts')
	<script>
		/*$(document).ready(function() {
            try {
                $('#tbl_unidades').DataTable({
                    "ajax": `${base_url}autos/Unidades/getDataUnidades`,
                    columns: [
                        { 'data':'id'},
                        { 'data':'categoria'},
                        { 'data':'subcategoria'},
                        { 'data':'anio'},
                        { 'data':'color'},
                        { 'data':'transmision'},
                        { 'data':'motor'},
                        {'data':function(data)
                            {
                                return "<a class='btn btn-success' href='"+base_url+'autos/Unidades/editar/'+data.id+"'> Editar </a>";
                            }
                        },
                        {'data':function(data)
                            {
                                return "<button type='button' onclick='borrar("+data.id+")' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
                            }
                        }
                    ]
                })
            }
            catch(err) {
              Swal.fire(
                  'Borrado!',
                  'El registro se ha borrado',
                  'success'
                );
            }
			
        });
        /*$("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });*/

        /*    function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                    url: base_url+'refacciones/productos/delProducto/'+id,
                    success: function(respuesta) {
                    Swal.fire(
                      'Borrado!',
                      'El registro se ha borrado',
                      'success'
                    )
                    location.reload(true)
                   // 
                    },
                    error: function() {
                        console.log("No se ha podido obtener la información");
                    }
                    });   
                 }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }
            */


	</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#menu_autos").addClass("show");
            $("#menu_reportes").addClass("show");
            $("#menu_reportes").addClass("active");
            $("#autos_reportes").addClass("show");
            $("#autos_reportes").addClass("active");
            $(<?php echo isset($menu) ? $menu : "" ?>).addClass("active");
            $("#M03").addClass("active");
        });
    </script>
@endsection