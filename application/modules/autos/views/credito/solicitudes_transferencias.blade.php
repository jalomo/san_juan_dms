@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    @include('ventas/partial_venta_steps')
    <div class="col-md-12 mt-4">
        <div class="table-responsive">
            <table class="table table-bordered" id="tbl_solicitudes"></table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var tabla_pre_ventas = $('#tbl_solicitudes').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "autos/credito/ajax_listado_transferencias",
                type: 'GET'
            },
            columns: [{
                    title: "#",
                    data: function({id_cxc}) {
                        return id_cxc
                    }
                },
                {
                    title: "Concepto",
                    data: function({concepto}) {
                        return concepto;
                    }
                },
                {
                    title: "Unidad",
                    data: 'unidad_descripcion'
                },
                {
                    title: "Total",
                    data: 'total'
                },
                {
                    title: "Folio",
                    data: 'folio'
                },
                {
                    title: "Estatus",
                    data: function({transferencia_autorizada}) {
                        if(transferencia_autorizada == 1){
                            return 'POR AUTORIZAR';
                        }if(transferencia_autorizada == 2){
                            return 'TRANSFERENCIA AUTORIZADA';
                        }
                        else if(transferencia_autorizada == 3){
                            return 'TRANSFERENCIA NO AUTORIZADA';
                        }else{
                            return 'NO APLICA';
                        }
                        
                    }
                },
                {
                    title: "autorizar",
                    data: function({id_cxc}) {
                        return "<a alt='autorizar' href='" + base_url + 'autos/credito/autorizatransferencia/' + id_cxc + "' class='btn btn-primary'><i class='far fa-file-alt'></i></a>";
                        
                    }
                }
            ]
        });

        $("#btn-filtrar").on('click', function() {
            tabla_pre_ventas.ajax.reload();
        });

    </script>
@endsection
