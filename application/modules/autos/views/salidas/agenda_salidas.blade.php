@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-8"></div>
            <div class="col-md-4 text-right">
                <a class="btn btn-primary" href="<?php echo base_url('autos/ventas'); ?>">Pre
                    pedidos</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" id="calendar"></div>
            <input type="hidden" name="id_venta_auto" id="id_venta_auto" value="{{ $detalle_unidad->id }}">
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ base_url('/js/custom/moment.js') }}"></script>
    <link rel="stylesheet" href="{{ base_url('/assets/libraries/calendar/main.css') }}">
    <script src="{{ base_url('/assets/libraries/calendar/main.js') }}"></script>
    <script src="{{ base_url('/assets/libraries/calendar/custom_calendar.js') }}"></script>
@endsection


@section('modal')
    <div class="modal fade" id="modal_agregar_salida" data-toggle="modal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo renderInputText('text', 'titulo', 'Nombre de evento', ''); ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('date', 'date_inicio', 'Fecha de salida', '', true);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo renderInputText('time', 'time_inicio', 'Hora de salida', ''); ?>
                        </div>
                        <div class="col-md-12">
                            <div id="msg_vidation" class=""></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button onclick="agendarsalida()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i
                            class="fas fa-calendar"></i> Agendar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
