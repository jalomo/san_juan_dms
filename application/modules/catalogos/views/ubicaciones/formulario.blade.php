@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12" align="center">
            <form id="form-ubicacion" data-id="<?php echo isset($data->id) ? $data->id : ''?>" method="post">
                <h3 style="text-align: left;">Ubicación</h3>
                <?php echo renderInputText("text", "nombre", "", isset($data->nombre) ? $data->nombre : '', false); ?>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button id="btn-actualizar" class="btn btn-success" type="button">Actualizar</button>
                        @else
                            <button id="btn-guardar" class="btn btn-success" type="button">Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    //Para guardar registro por primera vez
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post('api/catalogo-ubicacion', form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/UbicacionController/index';
            })
        })
    });

    //Para actualizar un registro
    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-ubicacion").data('id');
        ajax.put('api/catalogo-ubicacion/'+id, form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            
            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/UbicacionController/index';
            })
        })

    });

    let form = function() {
         return  {
            nombre: document.getElementById("nombre").value,
        };
    }

</script>
@endsection