@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('catalogos/UbicacionController/crear') ?>">Registrar</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ubicacion" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ubicación</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Ubicación</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
            try {
                var tabla_clientes = $('#tbl_ubicacion').DataTable({
                    ajax: base_url + "catalogos/UbicacionController/ajax_catalogo",
                    columns: [{
                            'data': function(data) {
                                return data.id
                            }
                        },
                        {
                            'data': function(data) {
                                return data.nombre
                            }
                        },
                        {
                            'data': function(data) {
                                return "<a href='" + site_url + 'catalogos/UbicacionController/editar/' + data.id + "' class='btn btn-success'><i class='fas fa-pen'></i></a>";
                            }
                        },
                        {
                            'data': function(data) {
                                return "<a href='#' class='btn btn-danger'><i class='far fa-trash-alt'></i></a>";
                            }
                        }
                    ]
                });
            }
            catch(err) {
                alert("ok");
                console.log(err);
            }
            
        });
	</script>
@endsection