@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="alta_registro" data-id="{{ isset($data->id) ? $data->id : '' }}"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-12" align="right">
                        <a href="#" onclick="recuperar_datos_cliente()" class="btn btn-info" type="button">
                            Recuperar datos cliente
                        </a>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>" id="nombre" name="nombre" placeholder="">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Paterno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?>" id="apellido_paterno" name="apellido_paterno" placeholder="">
                            <div id="apellido_paterno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Materno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?>" id="apellido_materno" name="apellido_materno" placeholder="">
                            <div id="apellido_materno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <!--<div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Dirección:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->direccion)){print_r($data->direccion);} ?>" id="direccion" name="direccion" placeholder="">
                            <div id="direccion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->estado)){print_r($data->estado);} ?>" id="estado" name="estado" placeholder="">
                            <div id="estado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Municipio:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->municipio)){print_r($data->municipio);} ?>" id="municipio" name="municipio" placeholder="">
                            <div id="municipio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Colonial:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->colonia)){print_r($data->colonia);} ?>" id="colonia" name="colonia" placeholder="">
                            <div id="colonia_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Código postal:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->codigo_postal)){print_r($data->codigo_postal);} ?>" id="codigo_postal" name="codigo_postal" placeholder="">
                            <div id="codigo_postal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Celular:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>" id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Telefono Casa:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_secundario)){print_r($data->telefono_secundario);} ?>" id="telefono_secundario" name="telefono_secundario" placeholder="">
                            <div id="telefono_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo)){print_r($data->correo);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo" name="correo" placeholder="">
                            <div id="correo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico secundario:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_secundario)){print_r($data->correo_secundario);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_secundario" name="correo_secundario" placeholder="">
                            <div id="correo_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="4" id="notas" name="notas"><?php if(isset($data->notas)){print_r($data->notas);} ?></textarea>
                            <div id="notas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <input type="hidden" id="id_cliente" value="<?php if(isset($data->id_cliente)){print_r($data->id_cliente);} else{ if (isset($id_cliente)) {echo $id_cliente;} else {echo '0';} } ?>">

                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar</button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            var id_cliente = document.getElementById("id_cliente").value;
            if (validaciones) {
                ajax.post('api/contactos', procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }

                    var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog(titulo, "success", function(data) {
                        return window.location.href = base_url + 'catalogos/clientesController/listado_contacto/'+id_cliente;
                    })
                })
            }
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            var id_cliente = document.getElementById("id_cliente").value;
            if (validaciones) {
                var id = $("#alta_registro").data('id');
                ajax.put('api/contactos/'+id, procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    
                    var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog(titulo, "success", function(data) {
                        return window.location.href = base_url + 'catalogos/clientesController/listado_contacto/'+id_cliente;
                    })
                })
            }
        });
        
        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let form = $('#alta_registro').serializeArray();
            let newArray = {
                nombre : document.getElementById("nombre").value,
                apellido_paterno : document.getElementById("apellido_paterno").value,
                apellido_materno : document.getElementById("apellido_materno").value,
                //direccion : document.getElementById("direccion").value,
                //estado : document.getElementById("estado").value,
                //municipio : document.getElementById("municipio").value,
                //colonia : document.getElementById("colonia").value,
                //codigo_postal : document.getElementById("codigo_postal").value,
                telefono : document.getElementById("telefono").value,
                telefono_secundario : document.getElementById("telefono_secundario").value,
                correo : document.getElementById("correo").value,
                correo_secundario : document.getElementById("correo_secundario").value,
                notas : document.getElementById("notas").value,
                id_cliente : document.getElementById("id_cliente").value,
            };

            return newArray;
        }

        function validar_campos() {
            var validacion = true;

            var correo_1 = $("#correo").val();
            if (!validar_email(correo_1)) {
                var error_1 = $("#correo_error");
                $("#correo_error").css("display","inline-block");
                error_1.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_1 = $("#correo_error");
                error_1.empty();
            }

            var correo_2 = $("#correo_secundario").val();
            if (!validar_email(correo_2)) {
                var error_2 = $("#correo_secundario_error");
                $("#correo_secundario_error").css("display","inline-block");
                error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_2 = $("#correo_secundario_error");
                error_2.empty();
            }

            var telefono_1 = document.getElementById('telefono').value;
            if (telefono_1.length < 10) {
                var error_3 = $("#telefono_error");
                $("#telefono_error").css("display","inline-block");
                error_3.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_3 = $("#telefono_error");
                error_3.empty();
            }

            var telefono_2 = document.getElementById('telefono_secundario').value;
            if (telefono_2.length < 10) {
                var error_4 = $("#telefono_secundario_error");
                $("#telefono_secundario_error").css("display","inline-block");
                error_4.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_4 = $("#telefono_secundario_error");
                error_4.empty();
            }

            return validacion;
        }

        function validar_email( email ) 
        {
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email) ? true : false;
        }

        function recuperar_datos_cliente() {
            var cliente = document.getElementById("id_cliente").value;

            $.ajax({
                url: base_url+"catalogos/clientesController/prellenado_contacto/"+cliente,
                method: 'post',
                /*data: {
                    base_64: base_64,
                },*/
                success:function(resp){
                    console.log(resp);

                    if (resp.indexOf("handler           </p>")<1) {
                        var campos = resp.split("=");
                        console.log(campos);

                        if (campos.length >5) {
                            $('#nombre').val(campos[0]);
                            $('#apellido_paterno').val(campos[1]);
                            $('#apellido_materno').val(campos[2]);
                            $('#telefono').val(campos[3]);
                            $('#telefono_secundario').val(campos[4]);
                            $('#correo').val(campos[5]);
                            $('#correo_secundario').val(campos[6]);
                        } 
                    }

                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

    </script>
@endsection