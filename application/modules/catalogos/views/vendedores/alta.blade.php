@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item" active><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="alta_registro" data-id="{{ isset($data->id) ? $data->id : '' }}"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Número de vendedor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_vendedor)){print_r($data->numero_vendedor);}else{ echo $num_vendedor;} ?>"  id="numero_vendedor" name="numero_vendedor" placeholder="">
                            <div id="numero_vendedor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">RFC:</label>
                            <input type="text" class="form-control" maxlength="20" value="<?php if(isset($data->rfc)){print_r($data->rfc);} ?>" id="rfc" name="rfc" placeholder="">
                            <div id="rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)){print_r($data->nombre);} ?>"  id="nombre" name="nombre" placeholder="">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Paterno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?>"  id="apellido_paterno" name="apellido_paterno" placeholder="">
                            <div id="apellido_paterno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Materno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?>"  id="apellido_materno" name="apellido_materno" placeholder="">
                            <div id="apellido_materno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Dirección:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->direccion)){print_r($data->direccion);} ?>" id="direccion" name="direccion" placeholder="">
                            <div id="direccion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->estado)){print_r($data->estado);} ?>" id="estado" name="estado" placeholder="">
                            <div id="estado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Municipio:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->municipio)){print_r($data->municipio);} ?>" id="municipio" name="municipio" placeholder="">
                            <div id="municipio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Colonial:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->colonia)){print_r($data->colonia);} ?>" id="colonia" name="colonia" placeholder="">
                            <div id="colonia_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Código postal:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->codigo_postal)){print_r($data->codigo_postal);} ?>" id="codigo_postal" name="codigo_postal" placeholder="">
                            <div id="codigo_postal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Celular:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>" id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Telefono (secundario):</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_secundario)){print_r($data->telefono_secundario);} ?>" id="telefono_secundario" name="telefono_secundario" placeholder="">
                            <div id="telefono_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico 1:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico)){print_r($data->correo_electronico);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico" name="correo_electronico" placeholder="">
                            <div id="correo_electronico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico 2:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico_secundario)){print_r($data->correo_electronico_secundario);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico_secundario" name="correo_electronico_secundario" placeholder="">
                            <div id="correo_electronico_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="4" id="notas" name="notas"><?php if(isset($data->notas)){print_r($data->notas);} ?></textarea>
                            <div id="notas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar</button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            ajax.post('api/vendedor', procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                //var alerta = (headers.status != 200) ? "warning" : "success";

                utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                    return window.location.href = base_url + 'catalogos/VendedoresController/index';
                })
            })
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#alta_registro").data('id');
            ajax.put('api/vendedor/'+id, procesarRegistro(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                var alerta = (headers.status != 200) ? "warning" : "success";

                utils.displayWarningDialog(titulo, alerta, function(data) {
                    return window.location.href = base_url + 'catalogos/VendedoresController/index';
                })
            })

        });

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let form = $('#alta_registro').serializeArray();
            let newArray = {
                //numero_vendedor : document.getElementById("numero_vendedor").value,
                rfc : document.getElementById("rfc").value,
                nombre : document.getElementById("nombre").value,
                apellido_paterno : document.getElementById("apellido_paterno").value,
                apellido_materno : document.getElementById("apellido_materno").value,
                direccion : document.getElementById("direccion").value,
                estado : document.getElementById("estado").value,
                municipio : document.getElementById("municipio").value,
                colonia : document.getElementById("colonia").value,
                codigo_postal : document.getElementById("codigo_postal").value,
                telefono : document.getElementById("telefono").value,
                telefono_secundario : document.getElementById("telefono_secundario").value,
                correo_electronico : document.getElementById("correo_electronico").value,
                correo_electronico_secundario : document.getElementById("correo_electronico_secundario").value,
                notas : document.getElementById("notas").value,

                //id_tipo_pago : document.getElementById("pago").value !== '' ? document.getElementById("pago").value : '0',
                //metodo_pago : document.getElementById("metodo").value,
                //id_cfdi : document.getElementById("cfdi").value !== '' ? document.getElementById("cfdi").value : '0',
            };

            return newArray;
        }
    </script>
@endsection