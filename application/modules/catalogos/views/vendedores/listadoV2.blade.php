@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-8">
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('catalogos/VendedoresController/crear') ?>">Agregar vendedor</a>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_vendedor" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <!--<th>N° Vendedor</th>-->
                            <th>Nombre</th>
                            <th>RFC</th>                            
                            <th>Tel.</th>
                            <th>Correo</th>
                            <th>Observaciones</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            <?php $indice = 1; ?>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $indice++ }} </td>
                                <!--<td>{{$item->clave}}</td>-->
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->rfc}}</td>
                                <td>{{$item->telefono}}</td>
                                <td>{{$item->correo_electronico}}</td>
                                <td>{{$item->notas}}</td>
                                <td>
                                    <a href="{{ base_url('catalogos/VendedoresController/editar/'.$item->id) }}" class="btn btn-primary" type="button" title="Editar">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <button class="btn btn-danger" type="button" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <!--<th>N° Vendedor</th>-->
                            <th>Nombre</th>
                            <th>RFC</th>                            
                            <th>Tel.</th>
                            <th>Correo</th>
                            <th>Observaciones</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		/*$(document).ready(function() {
            try {
                $('#tbl_vendedor').DataTable({
                    "ajax": `${base_url}autos/Vendedores/getDataVendedores`,
                    columns: [
                        { 'data':'id'},
                        { 'data':'clave'},
                        { 'data':'nombre'},
                        { 'data':'rfc'},
                        { 'data':'tel'},
                        { 'data':'correo'},
                        {'data':function(data)
                            {
                                return "<a class='btn btn-success' href='"+base_url+'autos/Vendedores/editar/'+data.id+"'> Editar </a>";
                            }
                        },
                        {'data':function(data)
                            {
                                return "<button type='button' onclick='borrar("+data.id+")' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
                            }
                        }
                    ]
                })
            }
            catch(err) {
                alert("ok");
            }
			
        });
        /*$("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });*/

        /*    function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                    url: base_url+'refacciones/productos/delProducto/'+id,
                    success: function(respuesta) {
                    Swal.fire(
                      'Borrado!',
                      'El registro se ha borrado',
                      'success'
                    )
                    location.reload(true)
                   // 
                    },
                    error: function() {
                        console.log("No se ha podido obtener la información");
                    }
                    });   
                 }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }
            */


	</script>
@endsection