@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12" align="center">
            <form id="form-anio" data-id="<?php echo isset($data->id) ? $data->id : ''?>" method="post">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="text-align: left;">Año</h3>
                        <div class="form-group">
                            <input type="text" maxlength="4" minlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->nombre)) {print_r($data->nombre);} ?>" id="nombre" name="nombre">
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button id="btn-actualizar" class="btn btn-success" type="button">Actualizar</button>
                        @else
                            <button id="btn-guardar" class="btn btn-success" type="button">Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
            </form>
            <hr>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    //Para guardar registro por primera vez
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post('api/catalogo-anio', form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/AnioController/index';
            })
        })
    });

    //Para actualizar un registro
    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-anio").data('id');
        ajax.put('api/catalogo-anio/'+id, form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            
            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/AnioController/index';
            })
        })

    });

    let form = function() {
         return  {
            nombre: document.getElementById("nombre").value,
        };
    }
</script>
@endsection