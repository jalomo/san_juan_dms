@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-3">
        <div class="col-md-8">
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('catalogos/ProveedoresController/crear') ?>">Agregar proveedor</a>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_vendedor" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>RFC</th>                            
                            <th>Tel.</th>
                            <th>Correo</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            <?php $indice = 1; ?>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $indice++ }} </td>
                                <td>{{$item->clave_identificador}}</td>
                                <td>{{($item->razon_social != '') ? $item->razon_social : $item->proveedor_nombre}}</td>
                                <td>{{$item->proveedor_rfc}}</td>
                                <td>{{$item->telefono}}</td>
                                <td>{{$item->correo}}</td>
                                <td>
                                    <a href="{{ base_url('catalogos/ProveedoresController/editar/'.$item->id) }}" class="btn btn-primary" type="button" title="Editar">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <button class="btn btn-danger" type="button" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>RFC</th>                            
                            <th>Tel.</th>
                            <th>Correo</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        
    </script>
@endsection