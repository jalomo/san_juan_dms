<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AlmacenesController extends MX_Controller
{


    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/almacen');
        $dataVendedores = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataVendedores) ? $dataVendedores : [];
        $data['titulo'] = "Almacenes";
        $this->blade->render('almacenes/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['titulo'] = "Registrar almacen";
        $this->blade->render('almacenes/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/almacen/' . $id);
            $datavendedor = procesarResponseApiJsonToArray($dataFromApi);
            
            $data['data'] = isset($datavendedor)  ? $datavendedor : [];
            $data['titulo'] = "Editar de almacen";
            $this->blade->render('almacenes/formulario', $data);
        }
    }
}

/* End of file VendedoresController.php */
