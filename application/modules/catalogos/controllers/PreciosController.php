<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PreciosController extends MX_Controller
{


    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/precios');
        
        $dataVendedores = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataVendedores) ? $dataVendedores : [];
        $data['titulo'] = "Precios";
        $this->blade->render('precios/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['titulo'] = "Registrar precios";
        $this->blade->render('precios/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/precios/' . $id);
            $datavendedor = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($datavendedor)  ? $datavendedor : [];
            $data['titulo'] = "Editar de Vendedor";
            $this->blade->render('precios/formulario', $data);
        }
    }
}

/* End of file VendedoresController.php */
