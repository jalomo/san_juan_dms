<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Catalogos extends MX_Controller
{

    public function index()
    {
        $data['titulo'] = "Catalogos";
        $array = [
            [
                'nombre' => 'Precios',
                'url' => base_url('catalogos/preciosController'),
                'id' => 1
            ],
            [
                'nombre' => 'Clientes',
                'url' => base_url('catalogos/ClientesController'),
                'id' => 1
            ],
            [
                'nombre' => 'Claves Cliente',
                'url' => base_url('catalogos/ClavesClienteController'),
                'id' => 1
            ],
            [
                'nombre' => 'Vendedores',
                'url' => base_url('catalogos/VendedoresController'),
                'id' => 1
            ],
            [
                'nombre' => 'Proveedores',
                'url' => base_url('catalogos/ProveedoresController'), 
                'id' => 1
            ],
            [
                'nombre' => 'Almacenes',
                'url' => base_url('catalogos/almacenesController'),
                'id' => 1
            ],
            [
                'nombre' => 'Talleres',
                'url' => base_url('catalogos/tallerController'),
                'id' => 1
            ],
            
            [
                'nombre' => 'Colores Exteriores',
                'url' => base_url('catalogos/coloresController'),
                'id' => 1
            ],
            [
                'nombre' => 'Colores Interiores (Vestiduras)',
                'url' => base_url('catalogos/vestidurasController'),
                'id' => 1
            ],
            [
                'nombre' => 'Estatus',
                'url' => base_url('catalogos/estatusController'),
                'id' => 1
            ],
            [
                'nombre' => 'Marca',
                'url' => base_url('catalogos/marcasController'),
                'id' => 1
            ],
            [
                'nombre' => 'Año',
                'url' => base_url('catalogos/anioController'),
                'id' => 1
            ],
            [
                'nombre' => 'Modelos',
                'url' => base_url('catalogos/autoController'),
                'id' => 1
            ],
            [
                'nombre' => 'Ubicación (unidad)',
                'url' => base_url('catalogos/ubicacionController'),
                'id' => 1
            ],
            [
                'nombre' => 'Ubicación llaves',
                'url' => base_url('catalogos/ubicacionLlavesController'),
                'id' => 1
            ],
            [
                'nombre' => 'Ubicación Productos',
                'url' => base_url('catalogos/ubicacionProductosController'),
                'id' => 1
            ],
        ];

        $data['data'] = $array;

        $this->blade->render('catalogos', $data);
    }

       
}

/* End of file Catalogos.php */
