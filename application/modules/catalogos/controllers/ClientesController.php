<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ClientesController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

    }

    public function index()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        //Cargamos la lista de clientes
        $clientes = $this->curl->curlGet('api/clientes');
        $data['data'] = procesarResponseApiJsonToArray($clientes);

        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');
        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo'] = "Listado";

        $this->blade->render('clientes/listadoV2', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos el catalogo de cfdi
        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        //Cargamos el catalogo de formas de pago
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        //Cargamos el catalogo de claves de cliente
        $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');
        $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);

        $dataFromApi = $this->curl->curlGet('api/clientes/ultimo-registro');
        $dataregistro = json_decode($dataFromApi, TRUE);

        if (isset($dataregistro["id"])) {
            $data['consecutivo'] = $dataregistro["id"]; 
        }

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo']  = "Registro";

        $this->blade->render('clientes/alta', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Cargamos el catalogo de cfdi
            $cfdi = $this->curl->curlGet('api/cfdi');
            $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

            //Cargamos el catalogo de formas de pago
            $tipo_pago = $this->curl->curlGet('api/tipo-pago');
            $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

            //Cargamos el catalogo de claves de cliente
            $clave_cliente = $this->curl->curlGet('api/catalogo-clave-cliente');
            $data['cat_clave'] = procesarResponseApiJsonToArray($clave_cliente);
        
            $dataFromApi = $this->curl->curlGet('api/clientes/' . $id);
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataRegistro)  ? $dataRegistro[0] : [];

            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Clientes";
            $data['subtitulo']  = "Edición";

            $this->blade->render('clientes/alta', $data);
        }
    }

    public function recuperar_cliente($num_cliente)
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $valores = "";

        try {
            $dataFromApi = $this->curl->curlPost('api/clientes/numero-cliente',[
                'numero_cliente' => $num_cliente,
            ]);
            
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data = isset($dataRegistro)  ? $dataRegistro[0] : [];

            if (isset($data->id)) {
                $id = $data->id;
            }

            if (isset($id)) {
                $dataFromApi_1 = $this->curl->curlGet('api/clientes/' . $id);
                $dataRegistro_1 = procesarResponseApiJsonToArray($dataFromApi_1);
                $data_2 = isset($dataRegistro_1)  ? $dataRegistro_1[0] : [];
                $contenedor = [];

                if (isset($data_2)) {
                    $valores .= $data_2->nombre."=";
                    $valores .= $data_2->apellido_paterno."=";
                    $valores .= $data_2->apellido_materno."=";
                    $valores .= $data_2->regimen_fiscal."=";
                    $valores .= $data_2->nombre_empresa."=";
                    $valores .= $data_2->rfc."=";
                    $valores .= $data_2->direccion."=";
                    $valores .= $data_2->numero_int."=";
                    $valores .= $data_2->numero_ext."=";
                    $valores .= $data_2->colonia."=";
                    $valores .= $data_2->municipio."=";
                    $valores .= $data_2->codigo_postal."=";
                    $valores .= $data_2->telefono."=";
                    $valores .= $data_2->telefono_secundario."=";
                    $valores .= $data_2->telefono_oficina."=";
                    $valores .= $data_2->flotillero."=";
                    $valores .= $data_2->correo_electronico."=";
                    $valores .= $data_2->correo_electronico_secundario."=";
                    $valores .= $data_2->fecha_nacimiento."=";
                    $valores .= $data_2->cfdi_id."=";
                    $valores .= $data_2->metodo_pago_id."=";
                    $valores .= $data_2->forma_pago."=";
                    $valores .= $data_2->saldo."=";
                    $valores .= $data_2->limite_credito."=";
                    $valores .= $data_2->notas."=";
                    $valores .= $data_2->estado."=";
                    $valores .= $data_2->tipo_registro."=";
                    $valores .= $data_2->id."=";
                }

                //Recuperamos los clientes parecidos
                $dataFromApi_2 = $this->curl->curlGet('api/clave-cliente/cliente-lista/'.$id);
                $dataRegistro_2 = procesarResponseApiJsonToArray($dataFromApi_2);

                for ($i=0; $i < count($dataRegistro_2) ; $i++) { 
                    foreach ($dataRegistro_2[$i] as $key => $value) {
                        if ($key == "id_clave") {
                            $valores .= $value.",";
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $valores = "ERROR";
        }
        
        echo $valores;
    }

    public function recuperar_id_cliente()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $valores = "";

        try {
            $num_cliente = $_POST['clave'];
            $id_clave = $_POST['cat_clave'];

            $dataFromApi = $this->curl->curlPost('api/clientes/numero-cliente',[
                'numero_cliente' => $num_cliente,
            ]);
            
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data = isset($dataRegistro)  ? $dataRegistro[0] : [];

            if (isset($data->id)) {
                $dataFromApi = $this->curl->curlPost('api/clave-cliente',[
                    "id_cliente" => $data->id,
                    "id_clave" => $id_clave
                ]);

            }

        } catch (Exception $e) {
            $valores = "ERROR";
        }
        
        echo $valores;
    }

    /******************** Proceso para contactos *************************/
    public function listado_contacto($id_cliente)
    {
        $this->load->helper('general');
        $this->load->library('curl');

        if ($id_cliente) {
            //Recuperar los datos del cliente
            $contactos = $this->curl->curlGet('api/contactos/lista-cliente/'.$id_cliente);
            $data['data'] = procesarResponseApiJsonToArray($contactos);
        }

        $data['id_cliente'] = $id_cliente;

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo'] = "Listado";
        $data['submodulo'] = "Contactos";

        $this->blade->render('clientes/contactos/listado_contactos', $data);
    }

    public function crear_contacto($id_cliente)
    {
        //$this->load->library('curl');
        //$this->load->helper('general');

        $data['id_cliente'] = $id_cliente;

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo'] = "Contactos";
        $data['submodulo']  = "Registrar contacto";

        $this->blade->render('clientes/contactos/alta_contacto', $data);
    }

    public function prellenado_contacto($id)
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $dataFromApi = $this->curl->curlGet('api/clientes/' . $id);
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data = isset($dataRegistro)  ? $dataRegistro[0] : [];

        $valores = "";
        //var_dump($data);
        foreach ($data as $key => $value) {
            if ($key == "nombre") {
                $valores .= $value."=";
            }
            if ($key == "apellido_paterno") {
                $valores .= $value."=";
            }
            if ($key == "apellido_materno") {
                $valores .= $value."=";
            }
            if ($key == "telefono") {
                $valores .= $value."=";
            }
            if ($key == "telefono_secundario") {
                $valores .= $value."=";
            }
            if ($key == "correo_electronico") {
                $valores .= $value."=";
            }
            if ($key == "correo_electronico_secundario") {
                $valores .= $value."=";
            }
        }
        
        echo $valores;
    }

    public function editar_contacto($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            $dataFromApi = $this->curl->curlGet('api/contactos/' . $id);
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataRegistro)  ? $dataRegistro : [];

            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Clientes";
            $data['subtitulo'] = "Contactos";
            $data['submodulo']  = "Editar contacto";
            
            $this->blade->render('clientes/contactos/alta_contacto', $data);
        }
    }

/******************** Proceso para vehiculos del cliente *************************/
    public function listado_vehiculo($id_cliente)
    {
        $this->load->helper('general');
        $this->load->library('curl');

        if ($id_cliente) {
            //Recuperar los datos del cliente
            $vehiculo = $this->curl->curlGet('api/vehiculos-clientes/lista-cliente/'.$id_cliente);
            $data['data'] = procesarResponseApiJsonToArray($vehiculo);
        }

        $data['id_cliente'] = $id_cliente;

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo'] = "Listado";
        $data['submodulo'] = "Vehículos";

        $this->blade->render('clientes/vehiculos/listado_vehiculo', $data);
    }

    public function crear_vehiculo($id_cliente)
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos los catalogos
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $autos = $this->curl->curlGet('api/catalogo-autos');
        $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

        $catalogos = $this->pseudo_catalogos();
        $data['cat_combustible'] = $catalogos['cat_combustible'];

        $catalogo_transmision = $this->catalogos_transmision();
        $data['cat_transmision'] = $catalogo_transmision['cat_transmision'];

        $data['id_cliente'] = $id_cliente;

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Clientes";
        $data['subtitulo'] = "Vehículos";
        $data['submodulo']  = "Registrar vehículo";

        $this->blade->render('clientes/vehiculos/alta_vehiculo',$data);
    }

    public function editar_vehiculo($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Cargamos los catalogos
            $marcas = $this->curl->curlGet('api/catalogo-marcas');
            $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

            $anio = $this->curl->curlGet('api/catalogo-anio');
            $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

            $color = $this->curl->curlGet('api/catalogo-colores');
            $data['cat_color'] = procesarResponseApiJsonToArray($color);

            $autos = $this->curl->curlGet('api/catalogo-autos');
            $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

            $catalogos = $this->pseudo_catalogos();
            $data['cat_combustible'] = $catalogos['cat_combustible'];

            $catalogo_transmision = $this->catalogos_transmision();
            $data['cat_transmision'] = $catalogo_transmision['cat_transmision'];

            $dataFromApi = $this->curl->curlGet('api/vehiculos-clientes/' . $id);
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataRegistro)  ? $dataRegistro : [];

            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Clientes";
            $data['subtitulo'] = "Vehículos";
            $data['submodulo']  = "Editar vehículo";

            $this->blade->render('clientes/vehiculos/alta_vehiculo',$data);
        }
    }

    //Recupéramos el ultimo id registrado
    public function ultimo_id_cliente()
    {
        $respuesta = "0";
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/clientes/ultimo-registro');
        $dataregistro = json_decode($dataFromApi, TRUE);

        if (isset($dataregistro["id"])) {
            $respuesta = $dataregistro["id"];
        }

        echo $respuesta;
    }

    public function pseudo_catalogos()
    {
        $data_combustible =  [
            ['id' => 1, 'nombre' => 'Gasolina'], 
            ['id' => 2, 'nombre' => 'Diesel'],
            ['id' => 3, 'nombre' => 'Híbrido'],
            ['id' => 4, 'nombre' => 'Eléctrico']
        ];
        
        $new_combustible = [];
        foreach ($data_combustible as $key => $value) {
            $item = new \stdClass;
            $item->id = $value['id'];
            $item->nombre = $value['nombre'];
            array_push($new_combustible, $item);
        }

        return [
            'cat_combustible' => $new_combustible
        ];
    }

    public function catalogos_transmision()
    {
        $data_transmision =  [
            ['id' => 1, 'nombre' => 'AUTOMÁTICA'], 
            ['id' => 2, 'nombre' => 'STANDAR'],
            ['id' => 3, 'nombre' => 'CVT'],
            ['id' => 4, 'nombre' => 'SEMI-AUTOMÁTICA']
        ];
        
        $new_transmision = [];
        foreach ($data_transmision as $key => $value) {
            $item = new \stdClass;
            $item->id = $value['id'];
            $item->nombre = $value['nombre'];
            array_push($new_transmision, $item);
        }

        return [
            'cat_transmision' => $new_transmision
        ];
    }

    public function ajax_catalogo_clientes()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/clientes');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }

    public function ajax_catalogo_contactos($id_cliente)
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/contactos/lista-cliente/'.$id_cliente);
        $response = procesarResponseApiJsonToArray($responseData);
        
        echo json_encode($response);
    }

}



/* End of file ClientesController.php */
