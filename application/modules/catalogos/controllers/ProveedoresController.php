<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProveedoresController extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        //Cargamos la lista de clientes
        $provvedores = $this->curl->curlGet('api/catalogo-proveedor');
        $data['data'] = procesarResponseApiJsonToArray($provvedores);

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Proveedores";
        $data['subtitulo'] = "Listado";
        $this->blade->render('proveedores/listado', $data); 
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        //Cargamos el catalogo de cfdi
        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        //Cargamos el catalogo de formas de pago
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        //Creamos numero del cliente
        $str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $clave = "P".date("is");
        for($j=0; $j<=2; $j++ ){
           $clave .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $data['clave'] = $clave;

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Proveedores";
        $data['subtitulo']  = "Registro";

        $this->blade->render('proveedores/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Cargamos el catalogo de cfdi
            $cfdi = $this->curl->curlGet('api/cfdi');
            $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

            //Cargamos el catalogo de formas de pago
            $tipo_pago = $this->curl->curlGet('api/tipo-pago');
            $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

            $dataFromApi = $this->curl->curlGet('api/catalogo-proveedor/' . $id);
            $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataRegistro)  ? $dataRegistro[0] : [];

            if (!isset($data['data']->clave_identificador)) {
                //Creamos numero del cliente
                $str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $clave = "PX".date("is");
                for($j=0; $j<=1; $j++ ){
                   $clave .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                $data['clave'] = $clave;
            }

            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Proveedores";
            $data['subtitulo']  = "Edición";
            
            $this->blade->render('proveedores/formulario', $data);
        }
    }
}

/* End of file CobradoresController.php */
