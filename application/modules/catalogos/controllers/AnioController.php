<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AnioController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300); 
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/catalogo-anio');
        
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Año";
        $data['subtitulo'] = "Listado";

        $this->blade->render('anio/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Año";
        $data['subtitulo'] = "Registrar Año";

        $this->blade->render('anio/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/catalogo-anio/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Año";
            $data['subtitulo'] = "Editar Año";
            
            $this->blade->render('anio/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-anio');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file AnioController.php */
