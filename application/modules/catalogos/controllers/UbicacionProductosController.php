<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UbicacionProductosController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $dataFromApi = $this->curl->curlGet('api/catalogo-ubicacion-producto');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Ubicación Productos";
        $data['subtitulo'] = "Listado";

        $this->blade->render('ubicacion_producto/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Ubicación Productos";
        $data['subtitulo'] = "Registro ubicación";

        $this->blade->render('ubicacion_producto/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/catalogo-ubicacion-producto/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Ubicación Productos";
            $data['subtitulo'] = "Editar Ubicación";

            $this->blade->render('ubicacion_producto/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-ubicacion-producto');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file UbicacionLlavesController.php */
