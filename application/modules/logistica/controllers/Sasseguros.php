<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sasseguros extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/sas-seguros/get-all', []));
        $data['titulo'] = "Listado Sas Seguros";
        $data['bitacora'] = "inicial";
        $this->blade->render('sas_seguros/listado', $data);
    }
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
            $data['files'] = array();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/sas-seguros/' . $id));
            $data['files'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/archivos-sas-seguros/'.$id));
        }

        $data['reclamo'] = form_input('reclamo', set_value('reclamo', exist_obj($info, 'reclamo')), 'class="form-control" id="reclamo"');
        $data['tipo'] = form_input('tipo', set_value('tipo', exist_obj($info, 'tipo')), 'class="form-control" id="tipo"');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/unidades'));
        $data['id_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'modelo_descripcion', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda" id="id_unidad"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['id_color'] = form_dropdown('id_color', array_combos($colores, 'id', 'nombre', TRUE), set_value('id_color', exist_obj($info, 'id_color')), 'class="form-control busqueda" id="id_color"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $anios = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-anio'));
        $data['id_anio'] = form_dropdown('id_anio', array_combos($anios, 'id', 'nombre', TRUE), set_value('id_anio', exist_obj($info, 'id_anio')), 'class="form-control busqueda" id="id_anio"');
        $data['observaciones'] = form_textarea('observaciones', set_value('observaciones', exist_obj($info, 'observaciones')), 'class="form-control" id="observaciones"');
        
        $data['danio'] = form_input('danio', set_value('danio', exist_obj($info, 'danio')), 'class="form-control" id="danio"');
        
        $data['fecha_alta'] = form_input('fecha_alta', set_value('fecha_alta', exist_obj($info, 'fecha_alta')), 'class="form-control" id="fecha_alta"', 'date');
        $data['fecha_entrega_asesor'] = form_input('fecha_entrega_asesor', set_value('fecha_entrega_asesor', exist_obj($info, 'fecha_entrega_asesor')), 'class="form-control" id="fecha_entrega_asesor"', 'date');
        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/catalogos/estatus-danios-bodyshop'));
        $data['id_estatus'] = form_dropdown('id_estatus', array_combos($estatus, 'id', 'estatus', TRUE), set_value('id_estatus', exist_obj($info, 'id_estatus')), 'class="form-control busqueda" id="id_estatus"');

        $asesores_web = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 13
        ]));
        $data['id_asesor_venta'] = form_dropdown('id_asesor_venta', array_combos($asesores_web, 'id', 'nombre', TRUE), set_value('id_asesor_venta', exist_obj($info, 'id_asesor_venta')), 'class="form-control busqueda" id="id_asesor_venta"');

        $data['finalizado'] = form_input('finalizado', set_value('finalizado', exist_obj($info, 'finalizado')), 'class="form-control" id="finalizado"');
        $data['fotos'] = form_input('fotos', set_value('fotos', exist_obj($info, 'fotos')), 'class="form-control" id="fotos"');
        $data['documentos'] = form_input('documentos', set_value('documentos', exist_obj($info, 'documentos')), 'class="form-control" id="documentos"');
        $data['presupuestos'] = form_input('presupuestos', set_value('presupuestos', exist_obj($info, 'presupuestos')), 'class="form-control" id="presupuestos"');
        $data['recuperado'] = form_input('recuperado', set_value('recuperado', exist_obj($info, 'recuperado')), 'class="form-control" id="recuperado"');
        $data['cobrado'] = form_input('cobrado', set_value('cobrado', exist_obj($info, 'cobrado')), 'class="form-control" id="cobrado"');

        $data['id'] = $id;
        $data['titulo'] = 'Sas Seguros';
        $this->blade->render('sas_seguros/agregar', $data);
    }
}