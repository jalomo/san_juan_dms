@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }

    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Fecha</label>
                <input type="date" id="fecha_entrega_cliente" name="fecha_entrega_cliente" class="form-control"
                    value="{{ date('Y-m-d') }}">
                <span class="error error_fecha_entrega_cliente"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Carril</label>
                {{ $id_carril }}
                <span class="error error_id_carril"></span>
            </div>
            <div class="col-sm-4">
                <button id="buscar" style="margin-top:30px" class="btn btn btn-info">Buscar</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        $("#buscar").on('click', function() {
            if ($("#fecha_entrega_cliente").val() == '') {
                $(".error_fecha_entrega_cliente").empty().append('El campo es requerido').css('color', 'red');
            } else if ($("#id_carril").val() == '') {
                $(".error_id_carril").empty().append('El campo es requerido').css('color', 'red');
            } else {
                buscarInformacion();
            }
        });

        function buscarInformacion() {
            var url = site_url + "/logistica/buscar_tablero/";
            ajaxLoad(url, {
                "fecha_entrega_cliente": $("#fecha_entrega_cliente").val(),
                "id_carril": $("#id_carril").val(),
            }, "tabla-items", "POST", function() {

            });
        }
        $("body").on('click', '.ver_detalles', function() {
            var url = site_url + "/logistica/detalles/";
            customModal(url, {
                "id": $(this).data('id')
            }, "POST", "lg", "", "", "", "Cerrar", "Detalles", "modal1");
        });

    </script>
@endsection
