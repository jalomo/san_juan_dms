<div>
    <table class="table table-bordered">
        <thead class="titulo_pdf">
            <tr>
                <td>
                    Cajuela
                    <div id="cajuela_error"></div>
                </td>
                <td class="text-center">
                    Si
                </td>
                <td class="text-center">
                    No
                </td>
                <td class="text-center">
                    No cuenta<br>
                    (NC)
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Herramienta.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="" name="herramienta"  value="1" {{ (isset($datos->cajuela) && $datos->cajuela->herramienta == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="" name="herramienta"  value="2" {{ (isset($datos->cajuela) && $datos->cajuela->herramienta == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="" name="herramienta" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->herramienta == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Gato / Llave.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="gato_llave" value="1" {{ (isset($datos->cajuela) && $datos->cajuela->gato_llave == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="gato_llave" value="2" {{ (isset($datos->cajuela) && $datos->cajuela->gato_llave == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="gato_llave" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->gato_llave == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Reflejantes.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="reflejantes" value="1" {{ (isset($datos->cajuela) && $datos->cajuela->reflejantes == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="reflejantes" value="2" {{ (isset($datos->cajuela) && $datos->cajuela->reflejantes == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="reflejantes" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->reflejantes == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Cables.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cables" value="1" {{ (isset($datos->cajuela) && $datos->cajuela->cables == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cables" value="2" {{ (isset($datos->cajuela) && $datos->cajuela->cables == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cables" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->cables == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Extintor.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ExtintorCheck cajuela"  name="extintor" value="1" {{ (isset($datos->cajuela) && $datos->cajuela->extintor == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="extintor" value="2" {{ (isset($datos->cajuela) && $datos->cajuela->extintor == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="extintor" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->extintor == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
            <tr>
                <td>Llanta Refacción.</td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="llanta_refaccion" value="1" {{ (isset($datos->cajuela) && $datos->cajuela->llanta_refaccion == '1')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="llanta_refaccion" value="2" {{ (isset($datos->cajuela) && $datos->cajuela->llanta_refaccion == '2')  ? 'checked' : '' }}>
                </td>
                <td class="text-center">
                    <input type="radio" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="llanta_refaccion" value="3" {{ (isset($datos->cajuela) && $datos->cajuela->llanta_refaccion == '3')  ? 'checked' : '' }}>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div>
    <table class="table table-bordered">
        <tbody>
            <tr>
                <td colspan="2" class="text-center">
                    <div>
                        <b>Nivel de Gasolina:</b>
                    </div>
                    <div>
                        <img src="<?php echo base_url('img/inventario/gas.jpeg'); ?>" style="width:50px;" id="nivel_gasolina_img">
                    </div>
                    <div>
                        <p>
                            <b>4/4</b>
                        </p>
                        <input type="text" name="nivel_gasolina" id="nivel_gasolina"  value="{{  isset($datos) ? $datos->gasolina->nivel_gasolina : null }}">
                    </div>
                </td>
            </tr>
            <tr>
                <td>¿Deja artículos personales?</td>
                <td>
                    (Si&nbsp;&nbsp;
                    <input type="radio" class="" style="transform: scale(1.5);" id="" name="articulos_personales" value="1" {{ (isset($datos->gasolina) && $datos->gasolina->articulos_personales == '1')  ? 'checked' : '' }}>&nbsp;&nbsp; No&nbsp;&nbsp;
                    <input type="radio" class="" style="transform: scale(1.5);" id="" name="articulos_personales" value="2" {{ (isset($datos->gasolina) && $datos->gasolina->articulos_personales == '2')  ? 'checked' : '' }} > &nbsp;&nbsp;)
                </td>
            </tr>
            <tr>
                <td>¿Cuales?</td>
                <td>
                    
                    <?php renderInputTextArea("cuales", "",  isset($datos->gasolina) ? $datos->gasolina->cuales : ''); ?>
                </td>
            </tr>
            <tr>
                <td>¿Desea reportar algo más?</td>
                <td>
                    <?php renderInputTextArea("reporte_algo_mas", "",  isset($datos->gasolina) ? $datos->gasolina->reporte_algo_mas : ''); ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>