<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formato Inventario Diagnóstico</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        td {
            font-size: 10px !important;
            text-align: right !important;
            color: #4e7291 !important;
        }

        .col-6 {
            float: left;
            width: 49%;
            /* padding: 3px; */
        }

        .col-5 {
            float: left;
            width: 40%;
            padding: 3px;
        }

        .col-3 {
            float: left;
            width: 24.6%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .text-center {
            text-align: center !important;
        }

        table,
        th,
        td {
            border: 1px solid #4e7191;
            margin-bottom: 12px;
            text-align: left;
            font-size: 10px !important;

        }

        .sep10 {
            width: 100%;
            height: 10px;
            clear: both;
        }
    </style>
</head>

<body>
    <header>
        <table style="border:0px !important">
            <tr>
                <td style="width:15%; border:0px !important" class="text-center">
                    <img style="height:50px;" src="<?php echo base_url('img/logo_queretaro_2.png'); ?>" class="img-fluid" />
                </td>
                <td class="text-center" style="width:70%; border:0px !important">
                    <div style="font-size:12px; font-weight:bold; color:#4e7191">
                        FORD PLASENCIA SANTA ANITA, S.A. DE C.V
                    </div>
                    <div style="font-size:10px; width:95%; text-align:center; color:#4e7199">
                        <p>AV. LOPEZ MATEOS SUR NO 4115, ZAPOPAN, JALISCO C.P. 45603</p>
                        <p>Tels. 33 44 47 00, R.F.C. PMG-1807068H5 CED. EMP. 8752</p>
                        <p>REG. CAM. NAL. COM. 151032 E-MAIL: drocha@fordplasencia.com WEB: www.fordsantaanita.mx</p>
                        <p> Horario Recepción y Entrega de Unidades:</p>
                        <p>Lunes a Viernes de 8:00 AM a 6:00 A 7:30 PM Sábado 8:00 AM a 6:00 PM </p>
                        <p>Horario de Caja: Lunes a Viernes 9:00 AM a 7:30 PM Sábado 8:00 AM a 2:00 PM</p>
                    </div>

                </td>
                <td class="text-center" style="width:15%; border:0px !important; font-weight:bold; text-align:right">
                    <img style="height:30px;" src="<?php echo base_url('img/logo.png'); ?>" class="img-fluid" />
                </td>
            </tr>
        </table>
    </header>
    <div class="contenedor">
        <div class="col-12 text-center" style="font-size: 12px; color:#4e7191">
            <h4 style="margin: 1px;">DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</h4>
        </div>
        <div class="col-12" style="font-size: 9px; color:#4e7199">
            <p style="margin: 1px;">Para elaborar el diagnóstico de del vehículo, autorizo en forma expresa a desarmar las partes indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él, en el entendido de
                que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en caso de que como consecuencia inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, por
                causa no imputable al proveedor, lo cual si ( ) no( ) acepto. En todo caso me obligo a pagar el importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la reparación en
                términos del contrato y acepto que el diagnóstico se realice en un plazo de 0 días hábiles a partir de la firma del presente.
            </p>
        </div>
        <table style="width:100%; color:#4e7199; border:0px !important">
            <tr>
                <td style="width:25%; border:0px !important; border-bottom:1px solid #4e7199; padding-top:30px" class="text-center">
                    <img style="height:100px;width: 200px;" class="img-fluid img-firma-big" src="<?php echo isset($datos->firma_elabora) ?  $datos->firma_elabora : ''; ?>">
                <td style="width:25%; border:0px !important; border-bottom:1px solid #4e7199; padding-top:30px" class="text-center">
                    <?php echo isset($datos->fecha_elaboracion) ?  utils::aFecha($datos->fecha_elaboracion, true) : ''; ?>
                </td>
                <td style="width:25%; border:0px !important; border-bottom:1px solid #4e7199; padding-top:30px" class="text-center">
                    <img style="height:100px;width:200px" class="img-fluid img-firma-big" src="<?php echo isset($datos->firma_consumidor) ?  $datos->firma_consumidor : ''; ?>">
                </td>
                <td style="width:25%; border:0px !important; border-bottom:1px solid #4e7199; padding-top:30px" class="text-center">
                    <?php echo isset($datos->fecha_aceptacion) ?  utils::aFecha($datos->fecha_aceptacion, true) : ''; ?>
                </td>
            </tr>
            <tr>
                <td style="width:25%; border:0px !important; font-weight:bold; font-size:8px;" class="text-center">
                    Nombre y firma de quien elabora el diagnóstico.
                </td>
                <td style="width:25%; border:0px !important; font-weight:bold; font-size:8px;" class="text-center">
                    Fecha de elaboración del diagnóstico.
                </td>
                <td style="width:25%; border:0px !important; font-weight:bold; font-size:8px;" class="text-center">
                    Nombre y firma del consumidor aceptando diagnóstico.
                </td>
                <td style="width:25%; border:0px !important; font-weight:bold; font-size:8px;" class="text-center">
                    Fecha aceptación.
                </td>
            </tr>
        </table>
    </div>
    <div class="contenedor">
        <div class="col-12 text-center" style="font-size: 12px; color:#4e7191">
            <h4 style="margin: 1px;">INSPECCI&Oacute;N VISUAL E INVENTARIO EN RECEPCI&Oacute;N</h4>
        </div>
    </div>
    <div class="contenedor">
        <table style="border:1px !important;">
            <tr>
                <td class="text-center" style="border:0px !important; width:60%">
                    <table cellpadding="3" class="table" style="width:100% !important">
                        <tr>
                            <td>
                                <table class="table">
                                    <!-- <tr>
                                        <td>
                                            <input type="radio"  id="golpes" name="marcasRadio" checked value="hit">
                                            &nbsp;&nbsp;
                                            Golpes
                                        </td>
                                        <td>
                                            <input type="radio"  id="roto" name="marcasRadio" value="broken">
                                            &nbsp;&nbsp;
                                            Roto / Estrellado
                                        </td>
                                        <td>
                                            <input type="radio"  id="rayones" name="marcasRadio" value="scratch">
                                            &nbsp;&nbsp;
                                            Rayones
                                        </td>
                                    </tr> -->
                                    <tr style="border:0px !important">
                                        <td  style="border:0px !important" colspan="3">
                                        <img style="height:180px; width:250px;" class="img-fluid img-firma-big" src="<?php echo isset($datos->firma_consumidor) ?  $datos->firma_consumidor : ''; ?>">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="3" class="table table-bordered" style="width:100% !important">
                        <thead class="titulo_pdf">
                            <tr>
                                <td align="center" class="text-center">
                                    Interiores
                                    <div id="interiores_error"></div>
                                </td>
                                <td align="center" class="text-center">
                                    Si
                                </td>
                                <td align="center" class="text-center">
                                    No
                                </td>
                                <td align="center" class="text-center">
                                    (No cuenta)&nbsp;
                                    NC
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Llavero.</td>
                                <td class="text-center">
                                    <input type="checkbox"  class="llaveroCheck interiores" <?php echo isset($datos->interiores->llavero) && $datos->interiores->llavero == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" class="llaveroCheck interiores"  <?php echo isset($datos->interiores->llavero) && $datos->interiores->llavero == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" class="llaveroCheck interiores"  <?php echo isset($datos->interiores->llavero) && $datos->interiores->llavero == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Seguro rines.</td>
                                <td class="text-center">
                                    <input type="checkbox"  class="SeguroRinesCheck interiores" name="interiores[]" <?php echo isset($datos->interiores->seguro_rines) && $datos->interiores->seguro_rines == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" class="SeguroRinesCheck interiores" name="interiores[]" <?php echo isset($datos->interiores->seguro_rines) && $datos->interiores->seguro_rines == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" class="SeguroRinesCheck interiores" name="interiores[]" <?php echo isset($datos->interiores->seguro_rines) && $datos->interiores->seguro_rines == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Indicadores de falla Activados:</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/05.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/02.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/07.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/01.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/06.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/04.png'); ?>" style="width:1.2cm;">
                                            </td>
                                            <td class="text-center">
                                                <img src="<?php echo base_url('img/inventario/03.png'); ?>" style="width:1.2cm;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-right: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.Si" <?php if (isset($interiores)) if (in_array("IndicadorGasolina.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.Si" <?php if (isset($interiores)) if (in_array("IndicadorMantenimento.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.Si" <?php if (isset($interiores)) if (in_array("SistemaABS.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.Si" <?php if (isset($interiores)) if (in_array("IndicadorFrenos.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.Si" <?php if (isset($interiores)) if (in_array("IndicadorBolsaAire.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.Si" <?php if (isset($interiores)) if (in_array("IndicadorTPMS.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td class="text-center">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.Si" <?php if (isset($interiores)) if (in_array("IndicadorBateria.Si", $interiores)) echo "checked"; ?>>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Indicador de falla activados.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="IndicadorDeFallaCheck interiores" <?php echo isset($datos->interiores->indicadores) && $datos->interiores->indicadores == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="IndicadorDeFallaCheck interiores" <?php echo isset($datos->interiores->indicadores) && $datos->interiores->indicadores == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="IndicadorDeFallaCheck interiores" <?php echo isset($datos->interiores->indicadores) && $datos->interiores->indicadores == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Rociadores y Limpiaparabrisas.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="RociadoresCheck interiores" <?php echo isset($datos->interiores->rociador) && $datos->interiores->rociador == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="RociadoresCheck interiores" <?php echo isset($datos->interiores->rociador) && $datos->interiores->rociador == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="RociadoresCheck interiores" <?php echo isset($datos->interiores->rociador) && $datos->interiores->rociador == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Claxon.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="ClaxonCheck interiores" <?php echo isset($datos->interiores->claxon) && $datos->interiores->claxon == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="ClaxonCheck interiores" <?php echo isset($datos->interiores->claxon) && $datos->interiores->claxon == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="ClaxonCheck interiores" <?php echo isset($datos->interiores->claxon) && $datos->interiores->claxon == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    Luces
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Delanteras.
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesDelanterasCheck interiores" <?php echo isset($datos->interiores->luces_del) && $datos->interiores->luces_del == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesDelanterasCheck interiores" <?php echo isset($datos->interiores->luces_del) && $datos->interiores->luces_del == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesDelanterasCheck interiores" <?php echo isset($datos->interiores->luces_del) && $datos->interiores->luces_del == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Traseras.
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesTraserasCheck interiores" <?php echo isset($datos->interiores->luces_tras) && $datos->interiores->luces_tras == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesTraserasCheck interiores" <?php echo isset($datos->interiores->luces_tras) && $datos->interiores->luces_tras == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesTraserasCheck interiores"  <?php echo isset($datos->interiores->luces_tras) && $datos->interiores->luces_tras == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Stop.
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesStopCheck interiores" <?php echo isset($datos->interiores->luces_stop) && $datos->interiores->luces_stop == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesStopCheck interiores" <?php echo isset($datos->interiores->luces_stop) && $datos->interiores->luces_stop == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="LucesStopCheck interiores" <?php echo isset($datos->interiores->luces_stop) && $datos->interiores->luces_stop == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Radio / Caratulas.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CaratulasCheck interiores" <?php echo isset($datos->interiores->radio) && $datos->interiores->radio == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CaratulasCheck interiores" <?php echo isset($datos->interiores->radio) && $datos->interiores->radio == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CaratulasCheck interiores" <?php echo isset($datos->interiores->radio) && $datos->interiores->radio == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Pantallas.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="PantallasCheck interiores" value="Pantallas.Si" <?php echo isset($datos->interiores->pantallas) && $datos->interiores->pantallas == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="PantallasCheck interiores" value="Pantallas.No" <?php echo isset($datos->interiores->pantallas) && $datos->interiores->pantallas == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="PantallasCheck interiores" value="Pantallas.NC" <?php echo isset($datos->interiores->pantallas) && $datos->interiores->pantallas == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>A/C.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="AACheck interiores" value="AA.Si" <?php echo isset($datos->interiores->ac) && $datos->interiores->ac == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="AACheck interiores" value="AA.No" <?php echo isset($datos->interiores->ac) && $datos->interiores->ac == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="AACheck interiores" value="AA.NC" <?php echo isset($datos->interiores->ac) && $datos->interiores->ac == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Encendedor.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EncendedorCheck interiores" value="Encendedor.Si" <?php echo isset($datos->interiores->encendedor) && $datos->interiores->encendedor == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EncendedorCheck interiores" value="Encendedor.No" <?php echo isset($datos->interiores->encendedor) && $datos->interiores->encendedor == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EncendedorCheck interiores" value="Encendedor.NC" <?php echo isset($datos->interiores->encendedor) && $datos->interiores->encendedor == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Vidrios.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VidriosCheck interiores"  value="Vidrios.Si" <?php echo isset($datos->interiores->vidrios) && $datos->interiores->vidrios == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VidriosCheck interiores"  value="Vidrios.No" <?php echo isset($datos->interiores->vidrios) && $datos->interiores->vidrios == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VidriosCheck interiores"  value="Vidrios.NC" <?php echo isset($datos->interiores->vidrios) && $datos->interiores->vidrios == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Espejos.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EspejosCheck interiores"  value="Espejos.Si" <?php echo isset($datos->interiores->espejos) && $datos->interiores->espejos == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EspejosCheck interiores"  value="Espejos.No" <?php echo isset($datos->interiores->espejos) && $datos->interiores->espejos == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="EspejosCheck interiores"  value="Espejos.NC" <?php echo isset($datos->interiores->espejos) && $datos->interiores->espejos == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Seguros eléctricos.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="SegurosEléctricosCheck interiores"  value="SegurosEléctricos.Si" <?php echo isset($datos->interiores->seguros_electricos) && $datos->interiores->seguros_electricos == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="SegurosEléctricosCheck interiores"  value="SegurosEléctricos.No" <?php echo isset($datos->interiores->seguros_electricos) && $datos->interiores->seguros_electricos == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="SegurosEléctricosCheck interiores"  value="SegurosEléctricos.NC" <?php echo isset($datos->interiores->seguros_electricos) && $datos->interiores->seguros_electricos == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Disco compacto.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CDCheck interiores"  value="CD.Si" <?php echo isset($datos->interiores->disco_compato) && $datos->interiores->disco_compato == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CDCheck interiores"  value="CD.No" <?php echo isset($datos->interiores->disco_compato) && $datos->interiores->disco_compato == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="CDCheck interiores"  value="CD.NC" <?php echo isset($datos->interiores->disco_compato) && $datos->interiores->disco_compato == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Asientos y vestiduras.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VestidurasCheck interiores"  value="Vestiduras.Si" <?php echo isset($datos->interiores->asiento_vestidura) && $datos->interiores->asiento_vestidura == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VestidurasCheck interiores"  value="Vestiduras.No" <?php echo isset($datos->interiores->asiento_vestidura) && $datos->interiores->asiento_vestidura == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="VestidurasCheck interiores"  value="Vestiduras.NC" <?php echo isset($datos->interiores->asiento_vestidura) && $datos->interiores->asiento_vestidura == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Tapetes.</td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="TapetesCheck interiores"  value="Tapetes.Si" <?php echo isset($datos->interiores->tapetes) && $datos->interiores->tapetes == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="TapetesCheck interiores"  value="Tapetes.No" <?php echo isset($datos->interiores->tapetes) && $datos->interiores->tapetes == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="text-center">
                                    <input type="checkbox"  class="TapetesCheck interiores"  value="Tapetes.NC" <?php echo isset($datos->interiores->tapetes) && $datos->interiores->tapetes == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td class="text-center" style="border:0px !important">
                    <table class="table table-bordered table-responsive" style="width: 100%;">
                        <thead class="titulo_pdf">
                            <tr>
                                <td>
                                    Cajuela
                                    <div id="cajuela_error"></div>
                                </td>
                                <td align="center" class="td_tablas">
                                    Si
                                </td>
                                <td align="center" class="td_tablas">
                                    No
                                </td>
                                <td align="center" class="td_tablas">
                                    No cuenta<br>
                                    (NC)
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Herramienta.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.Si" <?php echo isset($datos->cajuela->herramienta) && $datos->cajuela->herramienta == 3 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.No" <?php echo isset($datos->cajuela->herramienta) && $datos->cajuela->herramienta == 3 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.NC" <?php echo isset($datos->cajuela->herramienta) && $datos->cajuela->herramienta == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Gato / Llave.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.Si" <?php echo isset($datos->cajuela->gato_llave) && $datos->cajuela->gato_llave == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.No" <?php echo isset($datos->cajuela->gato_llave) && $datos->cajuela->gato_llave == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.NC" <?php echo isset($datos->cajuela->gato_llave) && $datos->cajuela->gato_llave == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Reflejantes.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.Si" <?php echo isset($datos->cajuela->reflejantes) && $datos->cajuela->reflejantes == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.No" <?php echo isset($datos->cajuela->reflejantes) && $datos->cajuela->reflejantes == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.NC" <?php echo isset($datos->cajuela->reflejantes) && $datos->cajuela->reflejantes == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Cables.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="CablesCheck cajuela" name="cajuela[]" value="Cables.Si" <?php echo isset($datos->cajuela->cables) && $datos->cajuela->cables == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="CablesCheck cajuela" name="cajuela[]" value="Cables.No" <?php echo isset($datos->cajuela->cables) && $datos->cajuela->cables == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="CablesCheck cajuela" name="cajuela[]" value="Cables.NC" <?php echo isset($datos->cajuela->cables) && $datos->cajuela->cables == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Extintor.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.Si" <?php echo isset($datos->cajuela->extintor) && $datos->cajuela->extintor == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.No" <?php echo isset($datos->cajuela->extintor) && $datos->cajuela->extintor == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.NC" <?php echo isset($datos->cajuela->extintor) && $datos->cajuela->extintor == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Llanta Refacción.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.Si" <?php echo isset($datos->cajuela->llanta_refaccion) && $datos->cajuela->llanta_refaccion == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.No" <?php echo isset($datos->cajuela->llanta_refaccion) && $datos->cajuela->llanta_refaccion == 2 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.NC" <?php echo isset($datos->cajuela->llanta_refaccion) && $datos->cajuela->llanta_refaccion == 3 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td colspan="2" class="text-center">
                                    <h5>Nivel de Gasolina:</h5>
                                    <div>
                                        <img src="<?php echo base_url('img/inventario/gas.jpeg'); ?>" style="width:50px;" id="nivel_gasolina_img">
                                    </div>
                                    <div>
                                        <p>
                                            <b>4/4</b>
                                        </p>
                                        <?php echo  isset($datos->gasolina->nivel_gasolina) ? $datos->gasolina->nivel_gasolina : ''; ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>¿Deja artículos personales?</td>
                                <td>
                                    (Si&nbsp;&nbsp;
                                    <input type="radio" <?php echo isset($datos->gasolina->articulos_personales) && $datos->gasolina->articulos_personales == 1 ? "checked='checked'" : '' ?> class="articulos"  value="1" name="articulos"> No
                                    <input type="radio" <?php echo isset($datos->gasolina->articulos_personales) && $datos->gasolina->articulos_personales == 2 ? "checked='checked'" : '' ?> class="articulos"  value="0" name="articulos"> &nbsp;&nbsp;)
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>¿Cuales?</td>
                                <td>
                                <?php echo  isset($datos->gasolina->cuales) ? $datos->gasolina->cuales : ''; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>¿Desea reportar algo más?</td>
                                <td>
                                <?php echo  isset($datos->gasolina->reporte_algo_mas) ? $datos->gasolina->reporte_algo_mas : ''; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <thead class="titulo_pdf">
                            <tr>
                                <td>
                                    Exteriores
                                    <div id="exteriores_error"></div>
                                </td>
                                <td align="center" class="td_tablas">
                                    Si
                                </td>
                                <td align="center" class="td_tablas">
                                    No
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tapones rueda.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="TaponesRuedaCheck exteriores"  value="TaponesRueda.Si" <?php echo isset($datos->exteriores->tapones_rueda) && $datos->exteriores->tapones_rueda == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="TaponesRuedaCheck exteriores"  value="TaponesRueda.No" <?php echo isset($datos->exteriores->tapones_rueda) && $datos->exteriores->tapones_rueda == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Gomas de limpiadores.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="GotasCheck exteriores"  value="Gomas.Si" <?php echo isset($datos->exteriores->goma_limpiadores) && $datos->exteriores->goma_limpiadores == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="GotasCheck exteriores"  value="Gomas.No" <?php echo isset($datos->exteriores->goma_limpiadores) && $datos->exteriores->goma_limpiadores == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Antena.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="AntenaCheck exteriores"  value="Antena.Si" <?php echo isset($datos->exteriores->antena) && $datos->exteriores->antena == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="AntenaCheck exteriores"  value="Antena.No" <?php echo isset($datos->exteriores->antena) && $datos->exteriores->antena == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Tapón de gasolina.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="TaponGasolinaCheck exteriores"  value="TaponGasolina.Si" <?php echo isset($datos->exteriores->tapon_gasolina) && $datos->exteriores->tapon_gasolina == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="TaponGasolinaCheck exteriores"  value="TaponGasolina.No" <?php echo isset($datos->exteriores->tapon_gasolina) && $datos->exteriores->tapon_gasolina == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <thead class="titulo_pdf">
                            <tr>
                                <td>
                                    Documentación
                                    <div id="documentacion_error"></div>
                                </td>
                                <td align="center" class="td_tablas">
                                    Si
                                </td>
                                <td align="center" class="td_tablas">
                                    No
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Póliza garantía/Manual prop.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="PolizaGarantiaCheck documentacion"  value="PolizaGarantia.Si" <?php echo isset($datos->documentacion->poliza_garantia) && $datos->documentacion->poliza_garantia == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="PolizaGarantiaCheck documentacion"  value="PolizaGarantia.No" <?php echo isset($datos->documentacion->poliza_garantia) && $datos->documentacion->poliza_garantia == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Seguro de Rines.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="SeguroRinesDocCheck documentacion"  value="SeguroRinesDoc.Si" <?php echo isset($datos->documentacion->seguro_rines) && $datos->documentacion->seguro_rines == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="SeguroRinesDocCheck documentacion"  value="SeguroRinesDoc.No" <?php echo isset($datos->documentacion->seguro_rines) && $datos->documentacion->seguro_rines == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Certificado verificación.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="cVerificacionCheck documentacion"  value="cVerificacion.Si" <?php echo isset($datos->documentacion->certificado_verificacion) && $datos->documentacion->certificado_verificacion == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="cVerificacionCheck documentacion"  value="cVerificacion.No" <?php echo isset($datos->documentacion->certificado_verificacion) && $datos->documentacion->certificado_verificacion == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Tarjeta de circulación.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="tCirculacionCheck documentacion"  value="tCirculacion.Si" <?php echo isset($datos->documentacion->tarjeta_circulacion) && $datos->documentacion->tarjeta_circulacion == 1 ? "checked='checked'" : '' ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox"  class="tCirculacionCheck documentacion"  value="tCirculacion.No" <?php echo isset($datos->documentacion->tarjeta_circulacion) && $datos->documentacion->tarjeta_circulacion == 2 ? "checked='checked'" : '' ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height:235px"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>