@layout('layout')

@section('included_css')
<!-- inventario -->
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/css_inventario.css">
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">

<style type="text/css">
    #cargaIcono {
        /*-webkit-animation: rotation 1s infinite linear;*/
        font-size: 55px !important;
        color: darkblue;
        display: none;
    }
</style>
@endsection

@section('contenido')
<form id="formulario" method="post" action="" autocomplete="on" enctype="multipart/form-data">
    <div class="row">
        <div class="col-sm-12">
            <div class="titulo_pdf" align="center" style="margin-left: 15px; margin-right: 15px;">
                <h5>DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</h5>

                <br>
            </div>

            <div class="panel-body cuadro_pdf">
                <div class="col-sm-12 table-responsive" style="width: 100%;" align="center">
                    <table class="table table-bordered" style="background-color: white;">
                        <tr>
                            <td style="font-size: 14px;width: 30%;">
                                No. Orden / Placas:
                            </td>
                            <td style="font-size: 14px;">
                                <input type="text" class="form-control" name="orden" value="<?php if (isset($folio_info)) echo $folio_info; ?>" onblur="revisar_datos()" style="width: 5cm;">
                                <label for="" id="errorConsultaServer" class="error"></label>
                                <div id="orden_error"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Si&nbsp;&nbsp;
                                <input type="radio" class="danos" style="transform: scale(1.5);" value="1" name="danos" <?php if (isset($marca_danos)) {
                                                                                                                            if ($marca_danos == "1") echo "checked";
                                                                                                                        } else {
                                                                                                                            echo 'checked';
                                                                                                                        } ?>>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                No&nbsp;&nbsp;
                                <input type="radio" class="danos" style="transform: scale(1.5);" value="0" name="danos" <?php if (isset($marca_danos)) if ($marca_danos == "0") echo "checked"; ?>>

                                <input type="hidden" name="marca_danos" value="<?php if (isset($marca_danos)) echo $marca_danos;
                                                                                else echo '1' ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="radio" style="transform: scale(1.5);" id="golpes" name="marcasRadio" checked value="hit">
                                &nbsp;&nbsp;
                                Golpes
                                &nbsp;&nbsp;
                                <input type="radio" style="transform: scale(1.5);" id="roto" name="marcasRadio" value="broken">
                                &nbsp;&nbsp;
                                Roto / Estrellado
                                &nbsp;&nbsp;
                                <input type="radio" style="transform: scale(1.5);" id="rayones" name="marcasRadio" value="scratch">
                                &nbsp;&nbsp;
                                Rayones
                            </td>
                        <tr>
                            <td align="center" colspan="2">
                                <img src="<?php if (isset($img_diagrama)) {
                                                echo (($img_diagrama != "") ? $img_diagrama : base_url() . 'statics/inventario/imgs/car_1.jpg');
                                            } else {
                                                echo base_url() . 'statics/inventario/imgs/car_1.jpg';
                                            } ?>" id="img_diagrama" style="width:420px; height:500px;" <?php if (!isset($img_diagrama)) echo 'hidden'; ?>>

                                <canvas id="canvas_3" width="420" height="500" <?php if (isset($img_diagrama)) echo 'hidden'; ?>>
                                    Su navegador no soporta canvas.
                                </canvas>
                            </td>
                        </tr>
                    </table>

                    <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url() . 'statics/inventario/imgs/car_1.jpg'; ?>">
                    <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if (isset($img_diagrama)) echo $img_diagrama; ?>">
                </div>

                <div class="col-sm-12 table-responsive" align="center">
                    <br>
                    <a href="<?php if (isset($img_diagrama)) echo base_url() . $img_diagrama;
                                else echo '#'; ?>" class="btn btn-default" id="btn-download" download="Diagrama_Diagnostico">
                        <i class="fa fa-download" aria-hidden="true"></i>
                        Descargar diagrama
                    </a>

                    <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;" <?php if (isset($img_diagrama)) echo 'hidden'; ?>>
                        <i class="fa fa-refresh" aria-hidden="true"></i>
                        Restaurar diagrama
                    </button>
                </div>


                <div class="col-sm-12 table-responsive">
                    <br><br>
                    <table class="table table-bordered" style="min-width: 100%;background-color: white;border-right: 10px;">
                        <thead class="titulo_pdf">
                            <tr>
                                <td align="center" class="td_tablas">
                                    Interiores
                                    <div id="interiores_error"></div>
                                </td>
                                <td align="center" class="td_tablas">
                                    Si
                                </td>
                                <td align="center" class="td_tablas">
                                    No
                                </td>
                                <td align="center" class="td_tablas">
                                    (No cuenta)&nbsp;
                                    NC
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Llavero.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.Si" <?php if (isset($interiores)) if (in_array("Llavero.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.No" <?php if (isset($interiores)) if (in_array("Llavero.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck interiores" name="interiores[]" value="Llavero.NC" <?php if (isset($interiores)) if (in_array("Llavero.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Seguro rines.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.Si" <?php if (isset($interiores)) if (in_array("SeguroRines.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.No" <?php if (isset($interiores)) if (in_array("SeguroRines.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck interiores" name="interiores[]" value="SeguroRines.NC" <?php if (isset($interiores)) if (in_array("SeguroRines.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">Indicadores de falla Activados:</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="width:100%;">
                                        <tr>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_5.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_2.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_7.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_1.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_6.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_4.png" style="width:1.2cm;">
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <img src="<?php echo base_url(); ?>statics/inventario/imgs/icono_3.png" style="width:1.2cm;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-right: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.Si" <?php if (isset($interiores)) if (in_array("IndicadorGasolina.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.No" <?php if (isset($interiores)) if (in_array("IndicadorGasolina.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" class="IndicadorGasolinaCheck interiores" style="margin-left: 6px; transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina.NC" <?php if (isset($interiores)) if (in_array("IndicadorGasolina.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.Si" <?php if (isset($interiores)) if (in_array("IndicadorMantenimento.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.No" <?php if (isset($interiores)) if (in_array("IndicadorMantenimento.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck interiores" name="interiores[]" value="IndicadorMantenimento.NC" <?php if (isset($interiores)) if (in_array("IndicadorMantenimento.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.Si" <?php if (isset($interiores)) if (in_array("SistemaABS.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.No" <?php if (isset($interiores)) if (in_array("SistemaABS.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="SistemaABSCheck interiores" name="interiores[]" value="SistemaABS.NC" <?php if (isset($interiores)) if (in_array("SistemaABS.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.Si" <?php if (isset($interiores)) if (in_array("IndicadorFrenos.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.No" <?php if (isset($interiores)) if (in_array("IndicadorFrenos.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck interiores" name="interiores[]" value="IndicadorFrenos.NC" <?php if (isset($interiores)) if (in_array("IndicadorFrenos.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.Si" <?php if (isset($interiores)) if (in_array("IndicadorBolsaAire.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.No" <?php if (isset($interiores)) if (in_array("IndicadorBolsaAire.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck interiores" name="interiores[]" value="IndicadorBolsaAire.NC" <?php if (isset($interiores)) if (in_array("IndicadorBolsaAire.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.Si" <?php if (isset($interiores)) if (in_array("IndicadorTPMS.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.No" <?php if (isset($interiores)) if (in_array("IndicadorTPMS.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck interiores" name="interiores[]" value="IndicadorTPMS.NC" <?php if (isset($interiores)) if (in_array("IndicadorTPMS.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                            <td align="center" class="td_tablas">
                                                <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.Si" <?php if (isset($interiores)) if (in_array("IndicadorBateria.Si", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.No" <?php if (isset($interiores)) if (in_array("IndicadorBateria.No", $interiores)) echo "checked"; ?>>
                                                <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck interiores" name="interiores[]" value="IndicadorBateria.NC" <?php if (isset($interiores)) if (in_array("IndicadorBateria.NC", $interiores)) echo "checked"; ?>>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Indicador de falla activados.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.Si" <?php if (isset($interiores)) if (in_array("IndicadorDeFalla.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.No" <?php if (isset($interiores)) if (in_array("IndicadorDeFalla.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck interiores" name="interiores2[]" value="IndicadorDeFalla.NC" <?php if (isset($interiores)) if (in_array("IndicadorDeFalla.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Rociadores y Limpiaparabrisas.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.Si" <?php if (isset($interiores)) if (in_array("Rociadores.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.No" <?php if (isset($interiores)) if (in_array("Rociadores.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck interiores" name="interiores2[]" value="Rociadores.NC" <?php if (isset($interiores)) if (in_array("Rociadores.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Claxon.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.Si" <?php if (isset($interiores)) if (in_array("Claxon.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.No" <?php if (isset($interiores)) if (in_array("Claxon.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck interiores" name="interiores2[]" value="Claxon.NC" <?php if (isset($interiores)) if (in_array("Claxon.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    Luces
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    Delanteras.
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.Si" <?php if (isset($interiores)) if (in_array("LucesDelanteras.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.No" <?php if (isset($interiores)) if (in_array("LucesDelanteras.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck interiores" name="interiores2[]" value="LucesDelanteras.NC" <?php if (isset($interiores)) if (in_array("LucesDelanteras.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    Traseras.
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.Si" <?php if (isset($interiores)) if (in_array("LucesTraseras.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.No" <?php if (isset($interiores)) if (in_array("LucesTraseras.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck interiores" name="interiores2[]" value="LucesTraseras.NC" <?php if (isset($interiores)) if (in_array("LucesTraseras.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    Stop.
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.Si" <?php if (isset($interiores)) if (in_array("LucesStop.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.No" <?php if (isset($interiores)) if (in_array("LucesStop.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck interiores" name="interiores2[]" value="LucesStop.NC" <?php if (isset($interiores)) if (in_array("LucesStop.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Radio / Caratulas.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.Si" <?php if (isset($interiores)) if (in_array("Caratulas.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.No" <?php if (isset($interiores)) if (in_array("Caratulas.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck interiores" name="interiores2[]" value="Caratulas.NC" <?php if (isset($interiores)) if (in_array("Caratulas.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Pantallas.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores2[]" value="Pantallas.Si" <?php if (isset($interiores)) if (in_array("Pantallas.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores2[]" value="Pantallas.No" <?php if (isset($interiores)) if (in_array("Pantallas.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck interiores" name="interiores[]" value="Pantallas.NC" <?php if (isset($interiores)) if (in_array("Pantallas.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>A/C.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.Si" <?php if (isset($interiores)) if (in_array("AA.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.No" <?php if (isset($interiores)) if (in_array("AA.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="AACheck interiores" name="interiores2[]" value="AA.NC" <?php if (isset($interiores)) if (in_array("AA.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Encendedor.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.Si" <?php if (isset($interiores)) if (in_array("Encendedor.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.No" <?php if (isset($interiores)) if (in_array("Encendedor.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck interiores" name="interiores3[]" value="Encendedor.NC" <?php if (isset($interiores)) if (in_array("Encendedor.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Vidrios.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.Si" <?php if (isset($interiores)) if (in_array("Vidrios.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.No" <?php if (isset($interiores)) if (in_array("Vidrios.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck interiores" name="interiores3[]" value="Vidrios.NC" <?php if (isset($interiores)) if (in_array("Vidrios.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Espejos.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.Si" <?php if (isset($interiores)) if (in_array("Espejos.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.No" <?php if (isset($interiores)) if (in_array("Espejos.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck interiores" name="interiores3[]" value="Espejos.NC" <?php if (isset($interiores)) if (in_array("Espejos.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Seguros eléctricos.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.Si" <?php if (isset($interiores)) if (in_array("SegurosEléctricos.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.No" <?php if (isset($interiores)) if (in_array("SegurosEléctricos.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck interiores" name="interiores3[]" value="SegurosEléctricos.NC" <?php if (isset($interiores)) if (in_array("SegurosEléctricos.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Disco compacto.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.Si" <?php if (isset($interiores)) if (in_array("CD.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.No" <?php if (isset($interiores)) if (in_array("CD.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="CDCheck interiores" name="interiores3[]" value="CD.NC" <?php if (isset($interiores)) if (in_array("CD.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Asientos y vestiduras.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.Si" <?php if (isset($interiores)) if (in_array("Vestiduras.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.No" <?php if (isset($interiores)) if (in_array("Vestiduras.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck interiores" name="interiores3[]" value="Vestiduras.NC" <?php if (isset($interiores)) if (in_array("Vestiduras.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                            <tr>
                                <td>Tapetes.</td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.Si" <?php if (isset($interiores)) if (in_array("Tapetes.Si", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.No" <?php if (isset($interiores)) if (in_array("Tapetes.No", $interiores)) echo "checked"; ?>>
                                </td>
                                <td align="center" class="td_tablas">
                                    <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck interiores" name="interiores3[]" value="Tapetes.NC" <?php if (isset($interiores)) if (in_array("Tapetes.NC", $interiores)) echo "checked"; ?>>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-12 table-responsive">
                    <div class="titulo_pdf" align="center" style="margin-left: 15px; margin-right: 15px;">
                        <h5>Cajuela / Exteriores / Documentación</h5>
                    </div>

                    <div class="panel-body" style="background-color: white;">
                        <div class="col-sm-4">
                            <table class="table table-bordered table-responsive" style="width: 100%;">
                                <thead class="titulo_pdf">
                                    <tr>
                                        <td>
                                            Cajuela
                                            <div id="cajuela_error"></div>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            Si
                                        </td>
                                        <td align="center" class="td_tablas">
                                            No
                                        </td>
                                        <td align="center" class="td_tablas">
                                            No cuenta<br>
                                            (NC)
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Herramienta.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.Si" <?php if (isset($cajuela)) if (in_array("Herramienta.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.No" <?php if (isset($cajuela)) if (in_array("Herramienta.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck cajuela" name="cajuela[]" value="Herramienta.NC" <?php if (isset($cajuela)) if (in_array("Herramienta.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gato / Llave.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.Si" <?php if (isset($cajuela)) if (in_array("eLlave.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.No" <?php if (isset($cajuela)) if (in_array("eLlave.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck cajuela" name="cajuela[]" value="eLlave.NC" <?php if (isset($cajuela)) if (in_array("eLlave.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Reflejantes.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.Si" <?php if (isset($cajuela)) if (in_array("Reflejantes.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.No" <?php if (isset($cajuela)) if (in_array("Reflejantes.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck cajuela" name="cajuela[]" value="Reflejantes.NC" <?php if (isset($cajuela)) if (in_array("Reflejantes.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Cables.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.Si" <?php if (isset($cajuela)) if (in_array("Cables.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.No" <?php if (isset($cajuela)) if (in_array("Cables.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck cajuela" name="cajuela[]" value="Cables.NC" <?php if (isset($cajuela)) if (in_array("Cables.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Extintor.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.Si" <?php if (isset($cajuela)) if (in_array("Extintor.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.No" <?php if (isset($cajuela)) if (in_array("Extintor.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck cajuela" name="cajuela[]" value="Extintor.NC" <?php if (isset($cajuela)) if (in_array("Extintor.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Llanta Refacción.</td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.Si" <?php if (isset($cajuela)) if (in_array("LlantaRefaccion.Si", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.No" <?php if (isset($cajuela)) if (in_array("LlantaRefaccion.No", $cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" class="td_tablas">
                                            <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck cajuela" name="cajuela[]" value="LlantaRefaccion.NC" <?php if (isset($cajuela)) if (in_array("LlantaRefaccion.NC", $cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6 table-responsive  table-responsive" style="width: 100%;">
                                    <table class="table table-bordered">
                                        <thead class="titulo_pdf">
                                            <tr>
                                                <td>
                                                    Exteriores
                                                    <div id="exteriores_error"></div>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    Si
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    No
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tapones rueda.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores" name="exteriores[]" value="TaponesRueda.Si" <?php if (isset($exteriores)) if (in_array("TaponesRueda.Si", $exteriores)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck exteriores" name="exteriores[]" value="TaponesRueda.No" <?php if (isset($exteriores)) if (in_array("TaponesRueda.No", $exteriores)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Gomas de limpiadores.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck exteriores" name="exteriores[]" value="Gomas.Si" <?php if (isset($exteriores)) if (in_array("Gomas.Si", $exteriores)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck exteriores" name="exteriores[]" value="Gomas.No" <?php if (isset($exteriores)) if (in_array("Gomas.No", $exteriores)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Antena.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="exteriores[]" value="Antena.Si" <?php if (isset($exteriores)) if (in_array("Antena.Si", $exteriores)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck exteriores" name="exteriores[]" value="Antena.No" <?php if (isset($exteriores)) if (in_array("Antena.No", $exteriores)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tapón de gasolina.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="exteriores[]" value="TaponGasolina.Si" <?php if (isset($exteriores)) if (in_array("TaponGasolina.Si", $exteriores)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck exteriores" name="exteriores[]" value="TaponGasolina.No" <?php if (isset($exteriores)) if (in_array("TaponGasolina.No", $exteriores)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-sm-6 table-responsive  table-responsive" style="width: 100%;">
                                    <table class="table table-bordered">
                                        <thead class="titulo_pdf">
                                            <tr>
                                                <td>
                                                    Documentación
                                                    <div id="documentacion_error"></div>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    Si
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    No
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Póliza garantía/Manual prop.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="documentacion[]" value="PolizaGarantia.Si" <?php if (isset($documentacion)) if (in_array("PolizaGarantia.Si", $documentacion)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck documentacion" name="documentacion[]" value="PolizaGarantia.No" <?php if (isset($documentacion)) if (in_array("PolizaGarantia.No", $documentacion)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Seguro de Rines.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="documentacion[]" value="SeguroRinesDoc.Si" <?php if (isset($documentacion)) if (in_array("SeguroRinesDoc.Si", $documentacion)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck documentacion" name="documentacion[]" value="SeguroRinesDoc.No" <?php if (isset($documentacion)) if (in_array("SeguroRinesDoc.No", $documentacion)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Certificado verificación.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="documentacion[]" value="cVerificacion.Si" <?php if (isset($documentacion)) if (in_array("cVerificacion.Si", $documentacion)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck documentacion" name="documentacion[]" value="cVerificacion.No" <?php if (isset($documentacion)) if (in_array("cVerificacion.No", $documentacion)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tarjeta de circulación.</td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="documentacion[]" value="tCirculacion.Si" <?php if (isset($documentacion)) if (in_array("tCirculacion.Si", $documentacion)) echo "checked"; ?>>
                                                </td>
                                                <td align="center" class="td_tablas">
                                                    <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck documentacion" name="documentacion[]" value="tCirculacion.No" <?php if (isset($documentacion)) if (in_array("tCirculacion.No", $documentacion)) echo "checked"; ?>>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <h5>Nivel de Gasolina:</h5>
                                                    <img src="<?php echo base_url(); ?>statics/inventario/imgs/gasolina_a.png" style="width:100px;" id="nivel_gasolina_img">
                                                    <br>
                                                    <div id="slider-2" style="width:200px;"></div>
                                                    <input type="hidden" name="nivel_gasolina" style="width:1.5cm;" value="<?php if (isset($nivel_gasolina)) echo $nivel_gasolina;
                                                                                                                            else echo "0"; ?>">
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Deja artículos personales?</td>
                                                <td>
                                                    (Si&nbsp;&nbsp;
                                                    <input type="radio" class="articulos" style="transform: scale(1.5);" value="1" name="articulos" <?php if (isset($articulos_vehiculo)) if ($articulos_vehiculo == "1") echo "checked" ?>>&nbsp;&nbsp;
                                                    No&nbsp;&nbsp;
                                                    <input type="radio" class="articulos" style="transform: scale(1.5);" value="0" name="articulos" <?php if (isset($articulos_vehiculo)) {
                                                                                                                                                        if ($articulos_vehiculo == "0") echo "checked";
                                                                                                                                                    } else {
                                                                                                                                                        echo "checked";
                                                                                                                                                    } ?>>
                                                    &nbsp;&nbsp;)
                                                    <br>
                                                    <input type="hidden" name="articulos_vehiculo" value="0">
                                                    <div id="articulos_error"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Cuales?</td>
                                                <td>
                                                    <input type="text" class="form-control" value="<?php if (isset($articulos)) echo $articulos; ?>" name="articulos" style="width: 100%;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Desea reportar algo más?</td>
                                                <td>
                                                    <textarea name="reporte_articulos" class="form-control" rows="2"><?php if (isset($reporte_articulos)) echo $reporte_articulos; ?></textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 table-responsive" style="margin-top: 30px;">
                    <div class="titulo_pdf" align="center">
                        <h4>IMPORTANTE</h4>
                    </div>

                    <div class="panel-body" style="background-color: white;">
                        <p align="justify" style="text-align: justify;">
                            Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para utilizarlo y ordenar éste servicio, en su representación, autorizando en su nombre y en el propio la realización de los trabajos descritos, así como el uso de materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar el importe del servicio; también manifiesto mi conformidad con el inventario realizado y que consta en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante previo acuerdo u orden del CONSUMIDOR por escrito en la orden de servicio digital vía App. O habiendo pasado los filtros de seguridad, para el debido resguardo del vehículo y debidamente aceptada por el PROVEEDOR, con un costo de
                            $ <input type="text" class="input_field" name="costo_diagnostico" style="width:3cm;" value="<?php if (isset($costo_diagnostico)) echo $costo_diagnostico;
                                                                                                                        else echo 'Por definir'; ?>">
                            (MONEDA NACIONAL); será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.
                            <br>
                            Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito en el presente, el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
                            <input type="text" class="input_field" name="dia_diagnostico" style="width:1cm;" value="<?php if (isset($dia_diagnostico)) echo $dia_diagnostico;
                                                                                                                    else echo date('d'); ?>">
                            de
                            <input type="text" class="input_field" name="mes_diagnostico" style="width:3cm;" value="<?php if (isset($mes_diagnostico)) echo $mes_diagnostico;
                                                                                                                    else echo $mes ?>">
                            de
                            <input type="text" class="input_field" name="anio_diagnostico" style="width:2cm;" value="<?php if (isset($anio_diagnostico)) echo $anio_diagnostico;
                                                                                                                        else echo date("Y"); ?>"> .
                        </p>
                    </div>

                    <div class="col-sm-12" style="background-color: white;" align="right">
                        <h5>Evidencias</h5>
                        <input type="file" class="btn btn-default" name="archivo[]" value="" multiple>
                        <br>

                        <?php if (isset($evidencias)) : ?>
                            <?php for ($x = 0; $x < count($evidencias); $x++) : ?>
                                <a href="<?php echo base_url() . $evidencias[$x]; ?>" class="btn btn-success" target="_blank">Archivo (<?php echo $x + 1; ?>)</a>
                            <?php endfor ?>
                        <?php endif ?>
                    </div>

                    <div class="col-sm-12" style="background-color: white;">
                        <div class="col-sm-6" align="center">
                            <img class="marcoImg" src="<?php if (isset($firma_asesor)) {
                                                            echo (($firma_asesor != "") ? $firma_asesor : base_url() . 'statics/inventario/imgs/fondo_bco.jpeg');
                                                        } else {
                                                            echo base_url() . 'statics/inventario/imgs/fondo_bco.jpeg';
                                                        } ?>" id="firma_asesor_Img" style="width: 2.5cm;">

                            <input type="hidden" id="ruta_firma_asesor" name="ruta_firma_asesor" value="<?php if (isset($firma_asesor)) echo $firma_asesor; ?>">

                            <br>
                            <input type="text" onblur='llenadoAsesor()' class="input_field" name="nombre_asesor" style="width:100%;" value="<?php if (isset($nombre_asesor)) echo $nombre_asesor; ?>" <?php if (isset($id)) echo 'disabled' ?>>

                            <div id="ruta_firma_asesor_error"></div>
                            <div id="nombre_asesor_error"></div>
                            <br>
                            Nombre y firma del operador de servicio.
                        </div>

                        <div class="col-sm-6" align="center">
                            <img class="marcoImg" src="<?php if (isset($firma_cliente)) {
                                                            echo (($firma_cliente != "") ? $firma_cliente : base_url() . 'statics/inventario/imgs/fondo_bco.jpeg');
                                                        } else {
                                                            echo base_url() . 'statics/inventario/imgs/fondo_bco.jpeg';
                                                        } ?>" id="firma_cliente_Img" style="width: 2.5cm;">

                            <input type="hidden" id="ruta_firma_cliente" name="ruta_firma_cliente" value="<?php if (isset($firma_cliente)) echo $firma_cliente; ?>">

                            <br>
                            <input type="text" onblur='llenadoCliente()' class="input_field" name="nombre_cliente" style="width:100%;" value="<?php if (isset($nombre_cliente)) echo $nombre_cliente; ?>" <?php if (isset($id)) echo 'disabled' ?>>

                            <div id="ruta_firma_cliente_error"></div>
                            <div id="nombre_cliente_error"></div>
                            <br>
                            Firma del consumidor
                        </div>
                    </div>

                    <div class="col-sm-12" style="background-color: white;">
                        <div class="col-sm-6" align="center">
                            <?php if (!isset($id)) : ?>
                                <a class="cuadroFirma btn btn-primary" data-value="Asesor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif ?>
                        </div>

                        <div class="col-sm-6" align="center">
                            <?php if (!isset($id)) : ?>
                                <a class="cuadroFirma btn btn-primary" data-value="Cliente_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif ?>
                        </div>

                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 30000;">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 30001;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body" align="center">
                <div class="signatureparent_cont_1">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection
@section('included_js')
@include('main/scripts_dt')
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>

<script src="{{ base_url('statics/js/inventario/interacciones_check.js') }}"></script>
<script src="{{ base_url('statics/js/inventario/envio_formulario.js') }}"></script>
<script src="{{ base_url('statics/js/inventario/diagrama_danos.js') }}"></script>
<script src="{{ base_url('statics/js/inventario/firma.js') }}"></script>

<script type="text/javascript">
    var site_url = "{{site_url()}}";
    var notnotification = true;
</script>

<script>
    function revisar_datos() {
        var base = $("#sitio").val();
        var servicio = $("input[name='orden']").val();

        if (servicio != "") {
            $.ajax({
                url: base + "inventario/recuperar_datos",
                method: 'post',
                data: {
                    orden: servicio,
                },
                success: function(resp) {
                    console.log(resp);
                    if (resp.indexOf("handler			</p>") < 1) {
                        var respuesta = resp.split("=");

                        if (respuesta[0] == "EDITA") {
                            //Redireccionamos al formulario de alta
                            location.href = base + "index.php/inventario/index/" + respuesta[1];
                        } else {
                            if (respuesta[1] == "") {
                                $("#errorConsultaServer").text("No se encontro ningun servicio abierto con ese folio");
                                $("input").attr("disabled", true);
                                $("input[name='orden']").attr("disabled", false);
                            } else {
                                $("#errorConsultaServer").text("");
                                $("input").attr("disabled", false);

                                $("input[name='id_infop']").val(respuesta[1]);
                                //$("input[name='orden']").val(respuesta[1]);
                                //$("input[name='folio']").val(respuesta[1]);

                                $("input[name='nombre_asesor']").val(respuesta[2]);
                                $("input[name='cnombre_asesor']").val(respuesta[2]);

                                $("input[name='nombre_cliente']").val(respuesta[3]);
                                $("#h_cliente").text(respuesta[3]);
                                $("input[name='cnombre_cliente']").val(respuesta[3]);

                                $("input[name='costo_diagnostico']").val(respuesta[4]);
                                $("input[name='cantidad_contrato']").val(respuesta[4]);

                                $("input[name='orden']").val(respuesta[5]);
                                $("input[name='folio']").val(respuesta[5]);
                            }
                        }
                    }
                    //Cierre de success
                },
                error: function(error) {
                    console.log(error);
                    //Cierre del error
                }
                //Cierre del ajax
            });
        } else {
            $("#errorConsultaServer").text("No se indco el No. de orden");
        }
    }
</script>
@endsection