<div class="col-md-12">
    <p>
        Para elaborar el diagnóstico de del vehículo, autorizo en forma expresa a desarmar las partes
        indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él,
        en el
        entendido de
        que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en
        caso de
        que como consecuencia inevitable resulte imposible o ineficaz para su funcionamiento así
        entregarlo,
        por
        causa no imputable al proveedor, lo cual si ( ) no( X ) acepto. En todo caso me obligo a pagar
        el
        importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la
        reparación en
        términos del contrato y acepto que el diagnóstico se realice en un plazo de 
        <input type="number" name="dias_habiles" id="dias_habiles" value="{{ isset($datos->dias_habiles) ? $datos->dias_habiles : 0  }}">
        días hábiles a
        partir
        de la firma del presente.
    </p>
</div>

<div class="col-md-3 text-center">
    <img style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big"
        id="firma_elabora_diagnostico_img"
        src="{{ isset($datos->firma_elabora) ? $datos->firma_elabora : '' }}">
    <input type="hidden" id="firma_elabora" name="firma_elabora" value="{{ isset($datos->firma_elabora) ?  $datos->firma_elabora : '' }}" >
    <b>
        Nombre y firma de quien elabora el diagnóstico.
    </b>
    <a class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_diagnostico" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Diagnóstico</a>

</div>
<div class="col-md-3 text-center">
    <?php renderInputText('date', 'fecha_elaboracion', 'Fecha de elaboración del diagnóstico', isset($datos->fecha_elaboracion) ? $datos->fecha_elaboracion : ''); ?>
    <b>Fecha de elaboración del diagnóstico.</b>
</div>
<div class="col-md-3 text-center">
    <img style="height:120px;width: 90%; border:1px solid #ddd" class="img-fluid img-firma-big" id="firma_consumidor_img"
        src="{{ isset($datos->firma_consumidor) ? $datos->firma_consumidor : '' }}">
    <input type="hidden" id="firma_consumidor" name="firma_consumidor" value="{{ isset($datos->firma_consumidor) && $datos->firma_consumidor }}" >
    <b>
        Nombre y firma del consumidor aceptando
        diagnóstico.
    </b>
    <a class="cuadroFirma btn btn-w-md btn-info" style="color:darkblue !important;" data-value="firma_consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Consumidor</a>

</div>
<div class="col-md-3 text-center">
    <?php renderInputText('date', 'fecha_aceptacion', 'Fecha aceptación.', isset($datos->fecha_aceptacion) ? $datos->fecha_aceptacion : ''); ?>
    <b> Fecha aceptación.</b>
</div>
