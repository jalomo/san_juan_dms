@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('seminuevos/unidades/alta') ?>">Registrar</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_unidades" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th># Económico</th>
                            <th>Fecha recepción</th>
                            <th>VIN</th>
                            <th>Serie Corta</th>
                            <th>Kilometraje</th>
                            <th>Precio Costo</th>
                            <th>Precio venta</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            <?php $indice = 1; ?>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $indice++ }} </td>
                                    <td>{{$item->numero_economico}}</td>
                                    <td>{{$item->fecha_recepcion}}</td>
                                    <td>{{$item->vin}}</td>
                                    <td>{{$item->serie_corta}}</td>
                                    <td>{{$item->precio_costo}}</td>
                                    <td>{{$item->precio_venta}}</td>
                                    <td>
                                        <a href="{{ base_url('seminuevos/Unidades/editar/'.$item->id) }}" class="btn btn-success" type="button" title="Editar">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-danger" type="button" title="Eliminar" disabled>
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                    <!--<td>
                                        <a href="#" class="btn btn-warning" type="button" title="Cambio">
                                            <i class="fas fa-exchange-alt"></i>
                                        </a>
                                    </td>-->
                                    <td>
                                        <a href="{{ base_url('seminuevos/ControlDocumentos/carga_documentacion/'.$item->numero_economico) }}" class="btn btn-warning" type="button" title="Documentación">
                                            <i class="fa fa-list-ol" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary" type="button" title="Imprimir QR" disabled>
                                            <i class="fas fa-qrcode"></i>
                                        </a>
                                    </td>
                                    <!--<td>
                                        <a href="#" class="btn btn-info" type="button" title="Ver Estatus" disabled>
                                            <i class="fas fa-info"></i>
                                        </a>
                                    </td>-->
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="12" align="center">
                                    No se encontraron resultados
                                </td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th># Económico</th>
                            <th>Fecha recepción</th>
                            <th>VIN</th>
                            <th>Serie Corta</th>
                            <th>Kilometraje</th>
                            <th>Precio Costo</th>
                            <th>Precio venta</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                            <th align="center">-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		/*$(document).ready(function() {
            try {
                $('#tbl_unidades').DataTable({
                    "ajax": `${base_url}autos/Unidades/getDataUnidades`,
                    columns: [
                        { 'data':'id'},
                        { 'data':'categoria'},
                        { 'data':'subcategoria'},
                        { 'data':'anio'},
                        { 'data':'color'},
                        { 'data':'transmision'},
                        { 'data':'motor'},
                        {'data':function(data)
                            {
                                return "<a class='btn btn-success' href='"+base_url+'autos/Unidades/editar/'+data.id+"'> Editar </a>";
                            }
                        },
                        {'data':function(data)
                            {
                                return "<button type='button' onclick='borrar("+data.id+")' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
                            }
                        }
                    ]
                })
            }
            catch(err) {
              Swal.fire(
                  'Borrado!',
                  'El registro se ha borrado',
                  'success'
                );
            }
			
        });
        /*$("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });*/

        /*    function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                    url: base_url+'refacciones/productos/delProducto/'+id,
                    success: function(respuesta) {
                    Swal.fire(
                      'Borrado!',
                      'El registro se ha borrado',
                      'success'
                    )
                    location.reload(true)
                   // 
                    },
                    error: function() {
                        console.log("No se ha podido obtener la información");
                    }
                    });   
                 }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }
            */
	</script>
@endsection