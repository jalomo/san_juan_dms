@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('seminuevos/ventas/preventa_formulario') ?>">Nueva Pre-venta</a>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Fecha inicio:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_inicio">
            </div>
        </div>
        <div class="col-md-4">
            <label for="">Fecha fin:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-calendar-alt"></i>
                    </div>
                </div>

                <input type="date" class="form-control" max="<?= date('Y-m-d'); ?>" value="" id="fecha_fin">
            </div>
        </div>

        <div class="col-md-4">
            <label for="">Usuario:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-user"></i>
                    </div>
                </div>

                <select class="form-control" id="usuario" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Estatus:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="fas fa-ellipsis-v"></i>
                    </div>
                </div>

                <select class="form-control" id="estatus" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="" style="margin-top: 30px;">Consultar</a>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fecha solicitud</th>
                            <th>Cliente</th>
                            <th>Unidad</th>
                            <th>Serie</th>
                            <th>Usuario</th>
                            <th>Estatus</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->fecha_solicitud}}</td>
                                <td>{{$item->cliente}}</td>
                                <td>{{$item->unidad}}</td>
                                <td>{{$item->serie}}</td>
                                <td>{{$item->usuario}}</td>
                                <td>{{$item->estatus}}</td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/preventa_formulario/'.$item->id) }}" title="Editar" class="btn btn-success" type="button">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" title="Cancelar" class="btn btn-danger" type="button">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/salida_unidad/'.$item->id) }}" title="Salida de la Unidad" class="btn btn-warning" type="button">
                                        <i class="fas fa-car-side"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/carta_factura/'.$item->id) }}" title="Carta Factura" class="btn btn-default" type="button">
                                        <i class="fas fa-file-alt"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" title="Imprimir Factura" class="btn btn-info" type="button">
                                        <i class="fas fa-file-invoice"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td>04/04/2020</td>
                                <td>CLIENTE DEMO</td>
                                <td>VFIGO - FIGO IMPULSE 4PS</td>
                                <td>BDN9XXXX845JO</td>
                                <td>ADMIN</td>
                                <td>PENDIENTE</td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/preventa_formulario/1') }}" title="Editar" class="btn btn-success" type="button">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" title="Cancelar" class="btn btn-danger" type="button">
                                        <i class="fas fa-window-close"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/salida_unidad/1') }}" title="Salida de la Unidad" class="btn btn-warning" type="button">
                                        <i class="fas fa-car-side"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('seminuevos/ventas/carta_factura/1') }}" title="Carta Factura" class="btn btn-default" type="button">
                                        <i class="fas fa-file-alt"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" title="Imprimir Factura" class="btn btn-info" type="button">
                                        <i class="fas fa-file-invoice"></i>
                                    </a>
                                </td>
                            </tr>

                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Fecha solicitud</th>
                            <th>Cliente</th>
                            <th>Unidad</th>
                            <th>Serie</th>
                            <th>Usuario</th>
                            <th>Estatus</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		/*$(document).ready(function() {
            try {
                $('#tbl_unidades').DataTable({
                    "ajax": `${base_url}autos/Unidades/getDataUnidades`,
                    columns: [
                        { 'data':'id'},
                        { 'data':'categoria'},
                        { 'data':'subcategoria'},
                        { 'data':'anio'},
                        { 'data':'color'},
                        { 'data':'transmision'},
                        { 'data':'motor'},
                        {'data':function(data)
                            {
                                return "<a class='btn btn-success' href='"+base_url+'autos/Unidades/editar/'+data.id+"'> Editar </a>";
                            }
                        },
                        {'data':function(data)
                            {
                                return "<button type='button' onclick='borrar("+data.id+")' class='btn-borrar btn btn-danger' data-id="+data.id+">Borrar</button>";
                            }
                        }
                    ]
                })
            }
            catch(err) {
              Swal.fire(
                  'Borrado!',
                  'El registro se ha borrado',
                  'success'
                );
            }
			
        });
        /*$("#tbl_productos").on("click", ".btn-borrar", function(){
            var id = $(this).data('id')
            borrar(id) 
        });*/

        /*    function borrar(id){
                Swal.fire({
              title: 'Estás seguro?',
              text: "Esta acción no se puede revertir!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, borrar!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                    url: base_url+'refacciones/productos/delProducto/'+id,
                    success: function(respuesta) {
                    Swal.fire(
                      'Borrado!',
                      'El registro se ha borrado',
                      'success'
                    )
                    location.reload(true)
                   // 
                    },
                    error: function() {
                        console.log("No se ha podido obtener la información");
                    }
                    });   
                 }
            })
             }
             function fnDelay() { // Function is defined here
            setTimeout(function(){ alert("ok");}, 5000);
                                 }
            */
    </script>
@endsection