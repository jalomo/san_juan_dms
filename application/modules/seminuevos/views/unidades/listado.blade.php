@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?>
            </li>
            <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <div class="row mb-3">
            <div class="col-md-8"></div>
            <div class="col-md-4" align="right">
                <a class="btn btn-primary"
                    href="<?php echo base_url('autos/Recepcion/alta'); ?>">Recibir unidad</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tbl_recepcion" width="100%" cellspacing="0"> </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        var recepcion = $('#tbl_recepcion').DataTable({
            language: {
                url: PATH_LANGUAGE
            },
            "ajax": {
                url: base_url + "seminuevos/unidades/ajax_recepcion",
                type: 'GET'
            },
            columns: [{
                    title: "#",
                    data: 'id',
                },
                {
                    title: "Unidad",
                    render: function(data, type, row) {
                        return row.unidad_descripcion + ' ' + row.transmision + ' ' + row.nombre_color;
                    }
                },
                {
                    title: "VIN",
                    data: 'vin',
                },
                {
                    title: "Ingresado por: ",
                    data: 'usuario_recibe',
                },
                {
                    title: "Valor unidad: ",
                    data: 'valor_unidad_venta',
                },

                {
                    title: "-",
                    render: function(data, type, {id}) {
                        let btn_open = '<a class="btn btn-primary" href="'+ base_url + 'autos/Recepcion/editar/'+ id +'"><i class="fas fa-pen"></i> </a>';
                        let btn_formato = '<a class="btn btn-primary" href="'+ base_url + 'seminuevos/reportes/formatoinventario/'+ id +'"><i class="fa fa-file"></i> </a>';
                        // let btn_list = '<a class="btn btn-primary" href="'+ base_url + 'autos/Checklist/alta/'+ row.id_unidad +'"><i class="fas fa-clipboard-list"></i> </a>';
                        // let btn_car_alt = '<a class="btn btn-primary" href="'+ base_url + 'autos/Salidas/alta/'+ row.id_unidad +'"><i class="fas fa-car-alt"></i> </a>';

                        return btn_open + ' ' + btn_formato;// + ' ' +btn_car_alt;
                        
                    }
                }


            ]
        });

    </script>
@endsection
