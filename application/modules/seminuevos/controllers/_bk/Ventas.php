<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ventas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/ventas_seminuevos/');
        $dataRegistro = [];
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = $dataRegistro;
        
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Consultas";
        $data['subtitulo'] = "Listado";

        $this->blade->render('ventas/consulta', $data);
    }

    public function preventa_formulario($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $cfdi = $this->curl->curlGet('api/cfdi');
        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);

        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pre-venta";

        if ($id) {
            $data['data'] = [];
            $data['subtitulo'] = "Edición";
        }else{
            $data['data'] = [];
            $data['subtitulo'] = "Registro";
        }

        $this->blade->render('ventas/preventa_alta', $data);
    }

    public function salida_unidad($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $data['data'] = [];

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Salida Unidad";
        
        if ($id) {
            $data['data'] = [];
            $data['subtitulo'] = "Edición";
        }else{
            $data['data'] = [];
            $data['subtitulo'] = "Registro";
        }

        $this->blade->render('ventas/salida', $data);
    }

    public function carta_factura($id='')
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $data['meses'] = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $data['data'] = [];

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Salida Unidad";
        $data['subtitulo'] = "Carta Factura";

        $this->blade->render('ventas/carta_factura', $data);
    }
}
