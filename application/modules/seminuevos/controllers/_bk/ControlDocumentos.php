<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ControlDocumentos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        
    }

    public function carga_documentacion($id='')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Recuperamos la informacion basados en el # economico
            if($this->session->userdata('nombre')){
                $data['usuario_recibe'] = $this->session->userdata('nombre')." ".$this->session->userdata('apellido_paterno')." ".$this->session->userdata('apellido_materno');
            }else{
                $data['usuario_recibe'] = "Visitante"; 
           }
           
           try {
               //Recuperamos la informacion del registro
                $dataFromApi = $this->curl->curlGet('api/seminuevos/' . $id);
                $valores = json_decode($dataFromApi,TRUE);            

                if (isset($valores[0]["id"])) {
                    $data["id_seminuevo"] = $valores[0]['id'];
                    $data["economico"] = $valores[0]['n_economico'];
                }
           } catch (Exception $e) {
               $data['usuario_recibe'] = "Visitante"; 
           }
        }

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Recepción";
        $data['titulo'] = "Control de Documentos";
        $data['subtitulo'] = "Documentación";

        $this->blade->render('unidades/documentacion', $data);
    }

    //Validamos y guardamos los documentos
    public function cargar_documentos()
    {
        $path = "";
        //300 segundos  = 10 minutos
        //ini_set('max_execution_time',600);
        
        //Revisamos que la imagen se envio
        if ($_FILES['archivo']['name'] != "") {

           //Url donde se guardara la imagen
           // $urlDoc ="videos";
           $urlDoc ="archivos/unidades_seminuevos";
           //Generamos un nombre random
           $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
           $urlNombre = date("YmdHi")."_";
           for($j=0; $j<=8; $j++ ){
              $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
           }

          //Validamos que exista la carpeta destino   base_url()
          if(!file_exists($urlDoc)){
              mkdir($urlDoc, 0647, true);
          }

           //Configuramos las propiedades permitidas para la imagen
           $config['upload_path'] = $urlDoc;
           $config['file_name'] = $urlNombre;
           $config['allowed_types'] = "*";
           $config['max_size'] = 0;

           //Cargamos la libreria y la inicializamos
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('archivo')) {
                $data['uploadError'] = $this->upload->display_errors();
                $path .= $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $path .= "";
           }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['archivo']['name']);
                $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
           }
        }else{
            $path = "";
        } 

        echo $path;
    }

    //Validamos y guardamos los documentos
    public function multiples_archivos($nombreDoc = NULL)
    {
        $path = "";
        $imgBase64 = $_POST['archivo'];
        $economico = $_POST['economico'];
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="archivos/unidades_seminuevos/";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($j=0; $j<=3; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "=";
            }
        }

        return $path;
    }
}
