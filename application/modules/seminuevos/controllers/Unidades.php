<?php

class Unidades extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function recepcion()
    {
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Recepción";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Listado";

        $this->load->library('curl');
        $this->load->helper('general');

        // esto puede ir en el constructor y reutilizar codigo
        $usuario_recibe = !empty($this->session->userdata('nombre')) ?
            $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno')
            : "Visitante";

        $data['usuario'] = $usuario_recibe;
        $data['id_asesor'] = $this->session->userdata('id');

        //Cargamos los catalogos
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

        // catalogo-modelos
        $modelo = $this->curl->curlGet('api/catalogo-modelos');
        $data['cat_modelo'] = procesarResponseApiJsonToArray($modelo);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $interiores = $this->curl->curlGet('api/catalogo-vestiduras');
        $data['cat_interiores'] = procesarResponseApiJsonToArray($interiores);

        $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
        $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

        $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
        $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

        $autos = $this->curl->curlGet('api/catalogo-autos');
        $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

        $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/ultimo-registro');
        $dataregistro = json_decode($dataFromApi, TRUE);

        if (isset($dataregistro["id"])) {
            $data['consecutivo'] = $dataregistro["id"];
        }

        $catalogos = $this->pseudo_catalogos();
        $data['cat_combustible'] = $catalogos['cat_combustible'];

        $this->blade->render('unidades/alta', $data);
    }

    public function editar($id = '')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Recuperamos la informacion del registro
            $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro[0] : [];

            //Cargamos los catalogos
            $marcas = $this->curl->curlGet('api/catalogo-marcas');
            $data['cat_marcas'] = procesarResponseApiJsonToArray($marcas);

            $anio = $this->curl->curlGet('api/catalogo-anio');
            $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

            // catalogo-modelos
            $modelo = $this->curl->curlGet('api/catalogo-modelos');
            $data['cat_modelo'] = procesarResponseApiJsonToArray($modelo);

            $color = $this->curl->curlGet('api/catalogo-colores');
            $data['cat_color'] = procesarResponseApiJsonToArray($color);

            $interiores = $this->curl->curlGet('api/catalogo-vestiduras');
            $data['cat_interiores'] = procesarResponseApiJsonToArray($interiores);

            $ubicacion = $this->curl->curlGet('api/catalogo-ubicacion');
            $data['cat_ubicacion'] = procesarResponseApiJsonToArray($ubicacion);

            $llaves = $this->curl->curlGet('api/catalogo-ubicacion-llaves');
            $data['cat_llaves'] = procesarResponseApiJsonToArray($llaves);

            $autos = $this->curl->curlGet('api/catalogo-autos');
            $data['cat_unidades'] = procesarResponseApiJsonToArray($autos);

            if (!isset($data['data']->usuario)) {
                if ($this->session->userdata('id_sesion_dms')) {
                    $data['usuario'] = $this->session->userdata('user_sesion_dms');
                } else {
                    $data['usuario'] = "Visitante";
                }
            }
            $catalogos = $this->pseudo_catalogos();
            // dd($data);
            $data['cat_combustible'] = $catalogos['cat_combustible'];
            $data['modulo'] = "Autos";
            $data['submodulo'] = "Inventario";
            $data['titulo'] = "Recepción de Unidades";
            $data['subtitulo'] = "Edición";

            $this->blade->render('unidades/alta', $data);
        }
    }

    public function pseudo_catalogos()
    {
        $data_combustible =  [
            ['id' => 1, 'nombre' => 'Gasolina'],
            ['id' => 2, 'nombre' => 'Diesel'],
            ['id' => 3, 'nombre' => 'Híbrido'],
            ['id' => 4, 'nombre' => 'Eléctrico']
        ];
        $new_combustible = [];
        foreach ($data_combustible as $key => $value) {
            $item = new \stdClass;
            $item->id = $value['id'];
            $item->nombre = $value['nombre'];
            array_push($new_combustible, $item);
        }

        return [
            'cat_combustible' => $new_combustible
        ];
    }

    //preventas 
    public function listadorecepcion()
    {
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Recepción";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Listado";
        $this->blade->render('unidades/listado', $data);
    }

    public function unidadesventa()
    {
        $api_response = $this->curl->curlGet('api/seminuevos/resumen-contabilidad');
        $data_response = procesarResponseApiJsonToArray($api_response);
        $data['total_unidades'] = isset($data_response) ? $data_response[0] : [];

        $data['modulo'] = "Sminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Unidades disponibles";

        $this->blade->render('unidades/catalogo_unidades', $data);
    }

    public function prepedidos()
    {
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "listado";

        $this->blade->render('unidades/listado_prepedidos', $data);
    }

    public function preventa_formulario($id_unidad)
    {
        if (!$id_unidad) {
            return $this->unidadesventa();
        }

        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_asesor'] = $catalogos['id_asesor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        $data['id_tipo_auto'] = 1; //seminuevo

        if ($id_unidad != '') {
            $detalle_unidad = $this->curl->curlGet('api/seminuevos/' . $id_unidad);
            $unidad = procesarResponseApiJsonToArray($detalle_unidad);
            $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
        }

        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pre-venta";
        $data['subtitulo'] = "Registro";
        $this->blade->render('unidades/preventa_alta', $data);
    }

    public function returnCatalogosForVentas()
    {
        $cfdi = $this->curl->curlGet('api/cfdi');
        $unidades = $this->curl->curlGet('api/seminuevos');
        $modelos = $this->curl->curlGet('api/catalogo-modelos');
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $anio = $this->curl->curlGet('api/catalogo-anio');
        $color = $this->curl->curlGet('api/catalogo-colores');
        $clientes = $this->curl->curlGet('api/clientes');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');

        $data['nombre_usuario'] = $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno');
        $data['id_asesor'] = $this->session->userdata('id');

        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
        $data['catalogo_unidades'] = procesarResponseApiJsonToArray($unidades);
        $data['catalogo_modelos'] = procesarResponseApiJsonToArray($modelos);
        $data['catalogo_marcas'] = procesarResponseApiJsonToArray($marcas);
        $data['catalogo_anio'] = procesarResponseApiJsonToArray($anio);
        $data['catalogo_color'] = procesarResponseApiJsonToArray($color);
        $data['catalogo_clientes'] = procesarResponseApiJsonToArray($clientes);
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);;
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        return $data;
    }

    public function ajax_recepcion()
    {
        $this->load->library('curl');
        $request = $this->curl->curlGet('api/seminuevos');
        $data_reporte = procesarResponseApiJsonToArray($request);
        $data['data'] = isset($data_reporte) ? $data_reporte : [];
        echo json_encode($data);
    }

    public function ajax_preventas()
    {
        $responseData = $this->curl->curlGet('api/seminuevos/pre-pedidos');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }

    public function formatosalida($id = '')
    {
        if (!$id) {
            return $this->index();
        }

        $request = $this->curl->curlGet('api/recepcion-unidades/' . $id);
        $responseRequest =  procesarResponseApiJsonToArray($request);
        $dataFromApi = count($responseRequest) > 0 ? $responseRequest[0] : [];

        // dd();
        $modelos = $this->curl->curlGet('api/catalogo-modelos');
        $data['cat_modelos'] = procesarResponseApiJsonToArray($modelos);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);

        $data["unidad"] = $dataFromApi;

        $request_formato = $this->curl->curlGet('api/venta-unidades/formatosalida/' . $id);
        $response_formato =  procesarResponseApiJsonToArray($request_formato);
        $data_formato = count($response_formato) > 0 ? $response_formato[0] : [];

        $nombre_cliente = $data_formato->cliente->nombre . ' ' . $data_formato->cliente->apellido_paterno . ' ' . $data_formato->cliente->apellido_materno;
        $data_formato->cliente->nombre_cliente = $nombre_cliente;

        $data["formato"] = $data_formato;
        $data['modulo'] = "Seminuevos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Salida de Unidades";
        $data['subtitulo'] = "Registro";

        $this->blade->render('unidades/formato_salida', $data);
    }


    public function documentacionpreventaunidad($id_venta_unidad)
    {
        if ($id_venta_unidad == '') {
            return $this->index();
        }

        $data['modulo'] = "Checklist";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre-pedidos";
        $data['subtitulo'] = "Documentación";
        $data['id_venta_unidad'] = $id_venta_unidad;

        $dataInfoVentaDocs = $this->curl->curlGet('api/informaciondocumentosventa/byidventaunidad/' . $id_venta_unidad);
        $datainfoVentas = procesarResponseApiJsonToArray($dataInfoVentaDocs);
        $data['info_ventas'] = isset($datainfoVentas) ? $datainfoVentas : [];
        // dd($data['info_ventas']);
        $dataFromApi = $this->curl->curlGet('api/documentosventa/byidunidad/' . $id_venta_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['archivos_subidos'] = isset($dataregistros) ? (array)$dataregistros : [];
        $this->blade->render('unidades/documentacion', $data);
    }


    
}
