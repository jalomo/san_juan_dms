@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    
    <div class="row">
        <div class="col-md-4">
            <?php echo renderInputText("text", "nombre", "Nombre", isset($usuarios->nombre) ? $usuarios->nombre : ''); ?>
            <input type="hidden" name="usuario_id" value="{{ $usuarios->id}}" id="usuario_id">
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "apellido_paterno", "Apellido paterno", isset($usuarios->apellido_paterno) ? $usuarios->apellido_paterno : ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "apellido_materno", "Apellido materno", isset($usuarios->apellido_materno) ? $usuarios->apellido_materno : ''); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php echo renderInputText("text", "telefono", "Telefono", isset($usuarios->telefono) ? $usuarios->telefono : ''); ?>
        </div>
        <div class="col-md-4">
            <?php echo renderInputText("text", "usuario", "Usuario", isset($usuarios->usuario) ? $usuarios->usuario : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderSelectArray('rol_id', 'Rol', $roles, 'id', 'rol',  isset($usuarios->rol_id) ? $usuarios->rol_id : null) ?>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12">
            <button id="btn-update" class="btn btn-primary" type="button">
               Actualizar usuario
            </button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
     $("#btn-update").on('click', function(){
            let usuario_id = $("#usuario_id").val();
            ajax.put('api/usuarios/'+usuario_id, {
                'nombre':$("#nombre").val(),
                'apellido_paterno':$("#apellido_paterno").val(),
                'apellido_materno':$("#apellido_materno").val(),
                'telefono':$("#telefono").val(),
                'rol_id':$("#rol_id").val()
            }, function(response, headers){
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Usuario actualizado", "success", function(data) {
                    return window.location.href = base_url + 'usuarios/administrarUsuarios';
                })
            })
        });
</script>
@endsection