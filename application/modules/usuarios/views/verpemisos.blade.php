@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Usuario:</label>
                <select class="form-control" id="usuario_id" name="usuario_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($usuarios))
                    @foreach ($usuarios as $usuario)
                    <option value="{{ $usuario->id}}"> {{$usuario->usuario}} - {{ $usuario->nombre. " ".$usuario->apellido_paterno }}</option>
                    @endforeach
                    @endif
                </select>
                <div id="usuario_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Modulos:</label>
                <select class="form-control dinamic-seccion" name="modulo_id" id="modulo_id">
                </select>
                <div id="modulo_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Secciones</label>
                <select class="form-control dinamic-seccion" name="seccion_id" id="seccion_id">
                </select>
                <div id="seccion_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Submenus</label>
                <select class="form-control dinamic-submenu-vista" name="submenu_id" id="submenu_id">
                </select>
                <div id="submenu_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        
    </div>
    
    <div class="row">
        <div class="col-md-3">
            <h3 class="mt-4">Modulos</h3>
            <div class="ml-1" id="modulos"></div>
        </div>
        <div class="col-md-3">
            <h3 class="mt-4">Secciones</h3>
            <div class="ml-1" id="secciones"></div>
        </div>
        <div class="col-md-3">
            <h3 class="mt-4">Sub menu</h3>
            <div class="ml-1" id="submenus"></div>
        </div>
        <div class="col-md-3">
            <h3 class="mt-4">Vistas</h3>
            <div class="ml-1" id="vistas"></div>
        </div>
    </div>
    <hr>

</div>
@endsection

@section('scripts')
<script>


    $("#usuario_id").on('change', function(){
        let usuario_id = $("#usuario_id").val();
        ajax.get('api/menu/modulos-userid/' + usuario_id, {}, function(response, headers) {
            if (response.length > 0) {
                $('#modulo_id').html('');
                $('#modulo_id').append('<option value=""> Seleccionar ...</option>');
                let html = "<ul class='list-group'>";
                response.map(item => {
                    if(item.default != 1){
                        $('#modulo_id').append('<option value="' + item.modulo_id + '">' + item.nombre_modulo + '</option>');
                        html += "<li class='list-group-item'>"+item.nombre_modulo+"</li>";
                    }
                })
                html  +=  "</ul>";
                $("#modulos").html(html);
            } else {
                $("#secciones").html('');
                $("#submenus").html('');
                $("#vistas").html('');
                $("#modulos").html('<h4>Sin modulos</h4>');
            }
        })
    });

    $("#modulo_id").on('change', function() {
        let modulo_id = $("#modulo_id").val();

        ajax.get('api/menu-secciones?modulo_id=' + modulo_id, {}, function(response, headers) {
            $('#seccion_id').html('');
            $('#seccion_id').append('<option value=""> Seleccionar ...</option>');
            let html = "<ul class='list-group'>";
                response.map(item => {
                    if(item.default != 1){
                        $('#seccion_id').append('<option value="' + item.id + '">' + item.nombre + '</option>');
                        html += "<li class='list-group-item'>"+item.nombre+"</li>";
                    }
                })
            html  +=  "</ul>";
            $("#secciones").html(html);
        });
    });

    $("#seccion_id").on('change', function() {
        let seccion_id = $("#seccion_id").val();
        ajax.get('api/menu-submenu?seccion_id=' + seccion_id, {}, function(response, headers) {
            // console.log(seccion_id,response)
            $('#submenu_id').html('');
            $('#submenu_id').append('<option value=""> Seleccionar ...</option>');
            let html = "<ul class='list-group'>";
                response.map(item => {
                    $('#submenu_id').append('<option value="' + item.id + '">' + item.nombre_submenu + '</option>');
                    html += "<li class='list-group-item'>"+item.nombre_submenu+"</li>";
                })
            html  +=  "</ul>";
            $("#submenus").html(html);
        });
    });

    $("#submenu_id").on('change', function() {
        let submenu_id = $("#submenu_id").val();
        ajax.get('api/menu-vistas?submenu_id=' + submenu_id, {}, function(response, headers) {
            // console.log(response)
            $("#vistas").html('');
            let html = "<ul class='list-group'>";
                response.map(item => {
                    html += "<li class='list-group-item'>"+item.nombre_vista+"</li>";
                })
            html  +=  "</ul>";
            $("#vistas").html(html);
        });
    });
</script>
@endsection