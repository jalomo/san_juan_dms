@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <button type="button" id="btn-modal-rol" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalRoles">
                Agregar rol
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tabla-roles" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre rol</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($roles))
                    @foreach ($roles as $key => $rol)
                    <tr>
                        <td>{{ $rol->id }} </td>
                        <td>{{ $rol->rol }} </td>
                        <td>
                            <button data-id="{{ $rol->id }}" class="btn btn-primary btn-editar">
                                <i class="fa fa-list"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            No se encontraron resultados
                        </td>
                    </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Nombre rol</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalRoles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Rol</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("text", "rol", "Nombre de rol", ''); ?>
                        <input type="hidden" name="rol_id" value="" id="rol_id">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-agregar" class="btn btn-primary">Guardar rol</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#tabla-roles").on("click", ".btn-editar", function() {
        var id = $(this).data('id');
        $("#modalRoles").modal('hide');
        ajax.get(`api/roles/${id}`, null, function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            $("#rol").val(response.rol);
            $("#rol_id").val(response.id);
            $("#modalRoles").modal('show');
        })
    });

    $("#btn-agregar").on('click', function() {

        if ($("#rol_id").val() == '') {
            ajax.post('api/roles', {
                'rol': $("#rol").val()
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                $("#rol").val('');
                $("#modalRoles").modal('hide');
                utils.displayWarningDialog("Rol creado", "success", function(data) {
                    return window.location.href = base_url + 'usuarios/roles';
                })
            });
        } else {
            let rol_id = $("#rol_id").val();
            ajax.put(`api/roles/${rol_id}`, {
                'rol': $("#rol").val()
            }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }

                $("#rol").val('');
                $("#modalRoles").modal('hide');

                return window.location.href = base_url + 'usuarios/roles';
            })
        }
    });
</script>
@endsection