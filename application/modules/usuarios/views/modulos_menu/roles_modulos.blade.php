@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : ''; ?></li>
            <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : ''; ?></li>
        </ol>

        <h1>Permisos por pantallas</h1>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Roles:</label>
                    <select class="form-control" id="rol_id" name="rol_id" style="width: 100%;">
                        <option value="">Selecionar ...</option>
                        @if (!empty($roles))
                            @foreach ($roles as $rol)
                                <option value="{{ $rol->id }}"> {{ $rol->rol }}</option>
                            @endforeach
                        @endif
                    </select>
                    <div id="rol_id_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="seccion">Modulos</label>
                    <select class="form-control dinamic-seccion-vista" name="seccion_modulo_vista_id" id="seccion_modulo_vista_id">
                    </select>
                    <div id="seccion_modulo_vista_id_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="seccion">Secciones</label>
                    <select class="form-control dinamic-seccion-vista" name="seccion_vista_id" id="seccion_vista_id">
                    </select>
                    <div id="seccion_vista_id_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="seccion">Submenus</label>
                    <select class="form-control dinamic-submenu-vista" name="submenu_vista_id" id="submenu_vista_id">
                    </select>
                    <div id="submenu_vista_id_error" class="invalid-feedback"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="d-none" id="check_all">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="check_todos">
                        <label class="form-check-label">Check todos</label>
                    </div>
                </div>
                <br>
                <div class="" id="checks_vistas_container"></div>
            </div>
            <div class="col-md-6">
                @if (!empty($modulos))
                    @foreach ($modulos as $modulo)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="{{ $modulo->id }}"
                                id="check_{{ $modulo->id }}">
                            <label class="form-check-label" for="check">
                                {{ $modulo->nombre }}
                            </label>
                        </div>
                    @endforeach
                @endif

                <button id="btn-guardar-modulo" class="btn btn-primary mt-4" type="button">
                    Guardar modulos
                </button>
            </div>
            <div class="col-md-6">
                <button id="btn-guardar-vista" class="btn btn-primary" type="button">
                    Guardar vista
                </button>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#rol_id").on('change', function() {
            // $('#checks_vistas_container').html('');
            let rol_id = $("#rol_id").val();
            document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);
            ajax.get('api/roles/modulos-by-rol?rol_id=' + rol_id, {}, function(response, headers) {
                $('#seccion_modulo_vista_id').html('');
                $('#seccion_modulo_vista_id').append('<option value="">Seleccionar ...</option>');
                if (response.length > 0) {
                    response.map(item => {
                        document.getElementById("check_" + item.modulo_id).checked = true;
                        $('#seccion_modulo_vista_id').append('<option value="' + item.id + '">' + item.modulo + '</option>');
                    })
                } else {
                    toastr.warning("No modulos asignados....");
                }
            })
        });

        $("#btn-guardar-modulo").on('click', function() {
            let rol_id = $("#rol_id").val();
            let modulo_array = [];
            $("#modulos_checkbox input[type=checkbox]:checked").each(function() {
                let modulo_id = $(this).val();
                modulo_array.push(modulo_id);
            });

            if (modulo_array.length == 0) {
                toastr.error("Seleccionar modulos....");
                return false;
            }

            ajax.post('api/roles/modulos', {
                "rol_id": rol_id,
                "modulo_id": modulo_array
            }, function(response, headers) {
                if (headers.status == 201) {
                    toastr.success("Permisos creados....");
                }
            })
        });

        //change modulo
        $("#seccion_modulo_vista_id").on('change', function() {
            let modulo_id = $("#seccion_modulo_vista_id").val();
            let rol_id = $("#rol_id").val();
            if(!modulo_id){
                $("#titulo_seccion_actual").text('');
                toastr.error("Sin modulo seleccionado");
                $('#checks_permisos_container').html('');
                return false;
            }
            $("#titulo_seccion_actual").text('Asignar Secciones');
            $("#step_actual").val(1);
            $('#checks_permisos_container').html('');
            $('#seccion_vista_id').html('');
            $('#seccion_vista_id').append('<option value="">Seleccionar ...</option>');
            ajax.get('api/menu-secciones?modulo_id=' + modulo_id, {}, function(response_secciones, headers) {
                // document.getElementById("check_all").classList.remove("d-none");
                // document.getElementById("check_todos").checked = false;
                if(response_secciones && response_secciones.length >0){
                    // response_secciones.map(item => {
                    //     $('#seccion_vista_id').append('<option value="' + item.id + '">' + item.nombre + '</option>');
                    // })
                    
                    response_secciones.map(item => {
                        $('#checks_permisos_container').append(
                            '<div class="form-check">' +
                            '<input class="form-check-input" type="checkbox" data-item_id="' + item.id + '" id="secciones_' + item.id + '" >' +
                            '<label class="form-check-label">' + item.nombre + '</label>' +
                            '</div>'
                        );
                    })
                }else{
                    toastr.warning("Sin secciones asignadas....");
                }
            });

            ajax.get('api/menu-secciones/byrol?rol_id=' + rol_id, {}, function(response_secciones_relaciones, headers) {
                if (response_secciones_relaciones.length > 0) {
                    let array_items = [];
                    array_items = [...array_items, response_secciones_relaciones.map(item => item.seccion_id) ];
                        $("#checks_permisos_container input[type=checkbox]").each(function() {
                            let seccion_id = $(this).data('item_id');
                            if (array_items[0].includes(seccion_id)) {
                                document.getElementById("secciones_" + seccion_id).checked = true;
                            }
                        });
                    response_secciones_relaciones.map(item => {
                        $('#seccion_vista_id').append('<option value="' + item.seccion_id + '">' + item.seccion_nombre + '</option>');
                    })
                } else {
                    toastr.warning("Sin secciones asignadas....");
                }
            });
            

        });

        // $("#check_todos").on('change', function() {
        //     $("input[type=checkbox]").each(function() {
        //         let vista_id = $(this).data('item_id');
        //         if(vista_id){
        //             document.getElementById("vista_" + vista_id).checked = document.getElementById("check_todos").checked;
        //         }
        //     });
        // });


        // $("#seccion_vista_id").on('change', function() {
        //     let seccion_id = $("#seccion_vista_id").val();
        //     ajax.get('api/menu-submenu?seccion_id=' + seccion_id, {}, function(response, headers) {
        //         $('#submenu_vista_id').html('');
        //         $('#checks_vistas_container').html('');
        //         document.getElementById("check_all").classList.add("d-none");
        //         $('#submenu_vista_id').append('<option value=""> Seleccionar ...</option>');
        //         response.map(item => {
        //             $('#submenu_vista_id').append('<option value="' + item.id + '">' + item.nombre_submenu + '</option>');
        //         })
        //     });
        // });

        // $("#submenu_vista_id").on('change', function() {
        //     let submenu_id = $("#submenu_vista_id").val();
        //     ajax.get('api/menu-vistas?submenu_id=' + submenu_id, {}, function(response, headers) {

        //         if (response.length > 0) {
        //             $('#checks_vistas_container').html('');
        //             document.getElementById("check_all").classList.remove("d-none");
        //             document.getElementById("check_todos").checked = false;
        //             response.map(item => {
        //                 $('#checks_vistas_container').append(
        //                     '<div class="form-check">' +
        //                     '<input class="form-check-input" type="checkbox" id="vista_' + item.vista_id + '" data-item_id="' + item.vista_id + '">' +
        //                     '<label class="form-check-label">' + item.nombre_vista + '</label>' +
        //                     '</div>'
        //                 );
        //             })

        //             ajax.get('api/menu-vistas/get-by-usuario?usuario_id=' + $("#usuario_id").val(), {}, function(response_selected, headers) {
        //                 let array_items = [];
        //                 array_items = [...array_items,
        //                     response_selected.map(item => item.vista_id)
        //                 ]

        //                 $("input[type=checkbox]").each(function() {
        //                     let vista_id = $(this).data('item_id');
        //                     if (array_items[0].includes(vista_id)) {
        //                         document.getElementById("vista_" + vista_id).checked = true;
        //                     }
        //                 });
        //             });
        //         } else {

        //             document.getElementById("check_all").classList.add("d-none");
        //             toastr.warning("No hay pantallas asignadas....");
        //             $('#checks_vistas_container').html('');
        //         }
        //     });
        // });

        $("#btn-guardar-vista").on('click', function() {
            if ($("#seccion_modulo_vista_id").val() == '') {
                toastr.error("Seleccionar modulo de menu...");
                return false;
            }

            if ($("#usuario_id").val() == '') {
                toastr.error("Seleccionar usuario...");
                return false;
            }


            if ($("#seccion_vista_id").val() == '') {
                toastr.error("Seleccionar seccion de menu...");
                return false;
            }

            if ($("#submenu_vista_id").val() == '') {
                toastr.error("Seleccionar submenu ...");
                return false;
            }

            let vista_array = [];
            $("input[type=checkbox]:checked").each(function() {
                let vista_id = $(this).data('item_id');
                if (vista_id) {
                    vista_array.push(vista_id);
                }
            });

            if (vista_array.length == 0) {
                toastr.error("Seleccionar vistas....");
                return false;
            }

            //guardar
            ajax.post('api/menu-vistas/store-by-usuarios', {
                "usuario_id": $("#usuario_id").val(),
                "seccion_id": $("#seccion_vista_id").val(),
                "submenu_id": $("#submenu_vista_id").val(),
                "vista_id": vista_array
            }, function(response, headers) {
                if (headers.status == 201) {
                    toastr.success("Permisos creados....");
                }
            })
        });

        //fin vistas
    </script>
@endsection
