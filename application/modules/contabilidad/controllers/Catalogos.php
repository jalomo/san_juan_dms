<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Catalogos extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');
    $this->load->helper('general');
  }

  /*
  * funcion que inicia el controlador.
  * desarrollo: jalomo
  * esta funcion estara para  listar los catalogos
  */
  public function index()
  {
    $cuentas = $this->curl->curlGet('api/catalogo-cuentas');
    $data['cuentas'] = procesarResponseApiJsonToArray($cuentas);
    
   
  	$data['titulo'] = "Contabilidad/Catalogos";
    $this->blade->render('catalogos/cont_lista_catalogo',$data);
  }

  

  /*
  * funcion que inicia el controlador.
  * desarrollo: jalomo
  * esta funcion estara para  listar los catalogos
  */
  public function editar()
  {
  	$data['titulo'] = "Contabilidad/Catalogos/editar";
    $this->blade->render('catalogos/cont_editar_catalogo',$data);
  }

  /*
  * funcion que inicia el controlador.
  * desarrollo: jalomo
  * 
  */
  public function inicio()
  {
  	$data['titulo'] = "Contabilidad/Catalogos/editar";
    $this->blade->render('catalogos/inicio',$data);
  }

  public function estado_financiero()
  {
    $data['titulo'] = "Contabilidad/Catalogos/Estado Financiero";
    $this->blade->render('catalogos/cont_estado_financiero',$data);
  }
}