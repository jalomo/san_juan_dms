@layout('tema_luna/layout')
@section('contenido')

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-12">

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary">PDF</button>
            </div>
          </div>

            <div class="row">
              <div class="col-md-2">
                
                <div class="form-group">
                    <label for="producto_id_sat">Mes</label>
                   
                    <select class="form-control input-sm">
                        <option value="">Enero</option>
                        <option value="">Febrero</option>
                        <option value="">Marzo</option>
                        <option value="">Abril</option>
                        <option value="">Mayo</option>
                        <option value="">Junio</option>
                        <option value="">Julio</option>
                        <option value="">Agosto</option>
                        <option value="">Septiembre</option>
                        <option value="">Octubre</option>
                        <option value="">Noviembre</option>
                        <option value="">Diciembre</option>
                    </select>
                    <div id="no_identificacion_error" class="invalid-feedback"></div>
                </div>
                
              </div>
            </div>

              
            
           <div class="row">
              <div class="col-md-12">     
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    
                    <th>Concepto</th>
                    <th>Importe</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>001</td>
                    
                    <td>Reembolso Bancomer</td>
                    <td>$8500.00</td>
                  </tr>

                   <tr>
                    <td>002</td>
                    
                    <td>Reembolso Bancomer</td>
                    <td>$8500.00</td>
                  </tr>

                  
                  
                </tbody>
              </table>
              </div>  
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_contabilidad").addClass("show");
            $("#menu_cont_rep").addClass("show");
            $("#menu_cont_rep").addClass("active");
            $("#menu_cont_re").addClass("show");
            $("#menu_cont_re").addClass("active");
            $("#menu_cont_rep_balanza").addClass("active");
            $("#M05").addClass("active");
        });
</script>
@endsection