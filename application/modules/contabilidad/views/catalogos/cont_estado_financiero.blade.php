@layout('tema_luna/layout')
@section('contenido')

<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                    <label for="producto_id_sat">Mes</label>
                   
                    <select class="form-control input-sm">
                        <option value="">Enero</option>
                        <option value="">Febrero</option>
                        <option value="">Marzo</option>
                        <option value="">Abril</option>
                        <option value="">Mayo</option>
                        <option value="">Junio</option>
                        <option value="">Julio</option>
                        <option value="">Agosto</option>
                        <option value="">Septiembre</option>
                        <option value="">Octubre</option>
                        <option value="">Noviembre</option>
                        <option value="">Diciembre</option>
                    </select>
                    <div id="no_identificacion_error" class="invalid-feedback"></div>
                </div>
            <!--form>
                <div class="form-group">
                    <label for="producto_id_sat">No. de Cuenta</label>
                    <input type="text" class="form-control input-sm" value=""  id="no_identificacion" name="no_identificacion" placeholder="No. de Cuenta">
                    <div id="no_identificacion_error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="producto_id_sat">Nombre Cta</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_unidad" name="clave_unidad" placeholder="Nombre Cta">
                    <div id="clave_unidad_error" class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="producto_id_sat">Cve. Auxiliar</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Cve. Auxiliar">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div> 

                <div class="form-group">
                    <label for="producto_id_sat">Tipo cuenta</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Tipo cuenta">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div> 

                 <div class="form-group">
                    <label for="producto_id_sat">Còdigo agrup.</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Còdigo">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div> 

                <div class="form-group">
                    <label for="producto_id_sat">Naturaleza</label>
                    <input type="text" class="form-control input-sm" value="" id="clave_prod_serv" name="clave_prod_serv" placeholder="Naturaleza">
                    <div id="clave_prod_serv_error" class="invalid-feedback"></div>
                </div> 
                
                

               
                
                <br>
                <button type="button" id="guardar_almacén" class="btn btn-primary">Editar</button>
                
                <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
            </form-->
        </div>
    </div>
</div>



@endsection
@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_contabilidad").addClass("show");
            $("#menu_cont_cat").addClass("show");
            $("#menu_cont_cat").addClass("active");
            $("#menu_cat_conta").addClass("show");
            $("#menu_cat_conta").addClass("active");
            $("#menu_cat_estado").addClass("active");
            $("#M05").addClass("active");
        });
</script>
@endsection