@layout('tema_luna/layout')
@section('contenido')

<script type="text/javascript">
$(document).ready(function(){
    $("#menu_contabilidad").addClass("show");
    $("#menu_cont_mov").addClass("active");
    $("#menu_menu_conta").addClass("active");
    $("#menu_cat_movi").addClass("show");
    $("#menu_conta_mov_cap_poliza").addClass("active");
});
</script>
<div class="container-fluid panel-body">

    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-2">
            
            <form id="registro_catalogo">
               
                    <?php //renderInputText("text", "FECHA", "No. de Cuenta","");?>
                
                    <?php renderInputText("text", "poliza", "Poliza",""); ?>
                
                    <?php renderInputText("text", "concepto", "Concepto",""); ?>
                
                    <?php renderInputText("text", "formulo", "Formulo",""); ?>

                    <?php
                    renderSelectArray("cuenta","cuentas",$cuentas,"id","no_cuenta");
                    ?>

                    <?php
                    renderSelectArray("subcuenta","subcuenta",$subcuentas,"id","no_cuenta");
                    ?>
                    
            
                <br>
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                
                <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
            </form>
        </div>
    </div>
</div>


<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    

    <div class="row">
        <div class="col-md-12">
            
           <div class="row">
              <div class="col-md-12">     
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>poliza</th>
                    <th>concepto</th>
                    <th>formulo</th>
                    <th>cuenta</th>
                    <th>subcuenta</th>
                   
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($polizas as $row)
                  <tr>
                    <td>{{$row->poliza}}</td>
                    <td>{{$row->concepto}}</td>
                    <td>{{$row->formulo}}</td>
                    <td>{{$row->cuenta}}</td>
                    <td>{{$row->subcuenta}}</td>
               
                    <td>
                        <a href="<?php echo base_url()?>contabilidad/movimientos/agregar_conceptos/{{$row->id}}" class="btn btn-primary" type="button" >Agregar conceptos</a>

                            
                            

                    </td>
                  </tr>
                  @endforeach

                  
                  
                </tbody>
              </table>
              </div>  
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $("#guardar").on('click', function() {

        $(".invalid-feedback").html("");
        console.log(procesarForm());
       
        ajax.post(`api/polizas-contabilidad`, procesarForm(), function(response, headers) {
            if (headers.status == 400) {
                console.log(headers);
                return ajax.showValidations(headers);
            }

            utils.displayWarningDialog(headers.message, "success", function(data) {
                return window.location.href = base_url + `contabilidad/movimientos/index`;
            })
        })

    });

    let procesarForm = function() {
        let form = $('#registro_catalogo').serializeArray();
        let newArray = {
            poliza: document.getElementById("poliza").value,
            concepto: document.getElementById("concepto").value,
            formulo: document.getElementById("formulo").value,
            cuenta: document.getElementById("cuenta").value,
            subcuenta: document.getElementById("subcuenta").value,
            
        };
        return newArray;
    }


    
</script>
@endsection