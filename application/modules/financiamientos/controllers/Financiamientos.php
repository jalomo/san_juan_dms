<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Financiamientos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado_bitacora_inicial()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/financiamientos/get-all-info', []));
        $data['titulo'] = "Listado Bitácora inicial";
        $data['bitacora'] = "inicial";
        $this->blade->render('listado', $data);
    }
    public function listado_bitacora_compras()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/financiamientos/get-all-info', []));
        $data['titulo'] = "Listado Bitácora compras";
        $data['bitacora'] = "compras";
        $this->blade->render('listado', $data);
    }
    public function agregar($id = 0,$bitacora='inicial')
    {
        if ($id == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/' . $id));
        }
        $asesores_web = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 13
        ]));
        $disabled = '';
        if($bitacora=='compras'){
            $disabled = 'disabled';
        }
        $data['drop_asesores'] = form_dropdown('id_asesor_ventas', array_combos($asesores_web, 'id', 'nombre', TRUE), set_value('id_asesor_ventas', exist_obj($info, 'id_asesor_ventas')), 'class="form-control busqueda" id="id_asesor_ventas" '.$disabled.'');
        $clientes = procesarResponseApiJsonToArray($this->curl->curlGet('api/clientes'));
        $array_clientes = [];
        foreach ($clientes as $c => $cliente) {
            $info_user = new stdClass();
            $info_user->id = $cliente->id;
            $info_user->nombre = $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;

            $array_clientes[$c] = $info_user;
        }
        $data['drop_clientes'] = form_dropdown('id_cliente', array_combos($array_clientes, 'id', 'nombre', TRUE), set_value('id_cliente', exist_obj($info, 'id_cliente')), 'class="form-control busqueda" id="id_cliente" '.$disabled.' ');
        $data['fecha_buro'] = form_input('fecha_buro', set_value('fecha_buro', exist_obj($info, 'fecha_buro')), 'class="form-control" id="fecha_buro" '.$disabled.'', 'date');
        $data['se_ingresa_si'] = form_radio('se_ingresa', 1, true, 'class="" id="se_ingresa_si" '.$disabled.'');
        $data['se_ingresa_no'] = form_radio('se_ingresa', 0, false, 'class="" id="se_ingresa_no" '.$disabled.'');
        $propuesta = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/propuestas'));
        $data['drop_propuesta'] = form_dropdown('id_propuesta', array_combos($propuesta, 'id', 'propuesta', TRUE), set_value('id_propuesta', exist_obj($info, 'id_propuesta')), 'class="form-control busqueda" id="id_propuesta" '.$disabled.'');
        $data['fecha_ingreso'] = form_input('fecha_ingreso', set_value('fecha_ingreso', exist_obj($info, 'fecha_ingreso')), 'class="form-control" id="fecha_ingreso" '.$disabled.'', 'date');
        $data['hay_unidad_si'] = form_radio('hay_unidad', 1, true, 'class="" id="hay_unidad_si" '.$disabled.'');
        $data['hay_unidad_no'] = form_radio('hay_unidad', 0, false, 'class="" id="hay_unidad_no" '.$disabled.'');
        $financiera1 = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/financieras'));
        $data['drop_financiera1'] = form_dropdown('id_financiera1', array_combos($financiera1, 'id', 'financiera', TRUE), set_value('id_financiera1', exist_obj($info, 'id_financiera1')), 'class="form-control busqueda" id="id_financiera1"'.$disabled.'');
        $financiera2 = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/financieras'));
        $data['drop_financiera2'] = form_dropdown('id_financiera2', array_combos($financiera2, 'id', 'financiera', TRUE), set_value('id_financiera2', exist_obj($info, 'id_financiera2')), 'class="form-control busqueda" id="id_financiera2"'.$disabled.'');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-autos'));
        $data['drop_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'nombre', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda js_unidad" id="id_unidad"'.$disabled.'');
        $data['drop_catalogo'] = form_dropdown('id_catalogo', array_combos($unidades, 'id', 'clave', TRUE), set_value('id_catalogo', exist_obj($info, 'id_catalogo')), 'class="form-control busqueda js_unidad" id="id_catalogo"'.$disabled.'');
        $data['enganche'] = form_input('enganche', set_value('enganche', exist_obj($info, 'enganche')), 'class="form-control numeric" id="enganche" '.$disabled.'');
        $data['mf'] = form_input('mf', set_value('mf', exist_obj($info, 'mf')), 'class="form-control" id="mf" '.$disabled.'');
        $data['plan'] = form_input('plan', set_value('plan', exist_obj($info, 'plan')), 'class="form-control" id="plan" '.$disabled.'');
        $data['comision_financiera'] = form_input('comision_financiera', set_value('comision_financiera', exist_obj($info, 'comision_financiera')), 'class="form-control numeric" id="comision_financiera" '.$disabled.'');
        $perfiles = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/perfiles'));
        $data['drop_perfil'] = form_dropdown('id_perfil', array_combos($perfiles, 'id', 'perfil', TRUE), set_value('id_perfil', exist_obj($info, 'id_perfil')), 'class="form-control busqueda" id="id_perfil" '.$disabled.'');

        $data['score'] = form_input('score', set_value('score', exist_obj($info, 'score')), 'class="form-control numeric" id="score" '.$disabled.'');

        $data['cuenta_mas_alta'] = form_input('cuenta_mas_alta', set_value('cuenta_mas_alta', exist_obj($info, 'cuenta_mas_alta')), 'class="form-control numeric" id="cuenta_mas_alta" '.$disabled.'');
        $data['tiempo_cuenta'] = form_input('tiempo_cuenta', set_value('tiempo_cuenta', exist_obj($info, 'tiempo_cuenta')), 'class="form-control" id="tiempo_cuenta" '.$disabled.'');
        $data['mensualidad'] = form_input('mensualidad', set_value('mensualidad', exist_obj($info, 'mensualidad')), 'class="form-control numeric" id="mensualidad" '.$disabled.'');
        $data['egreso_bruto'] = form_input('egreso_bruto', set_value('egreso_bruto', exist_obj($info, 'egreso_bruto')), 'class="form-control numeric" id="egreso_bruto" '.$disabled.'');
        $data['total'] = form_input('total', set_value('total', exist_obj($info, 'total')), 'class="form-control numeric" id="total" '.$disabled.'');
        $data['total2'] = form_input('total2', set_value('total2', exist_obj($info, 'total2')), 'class="form-control numeric" id="total2"'.$disabled.'');
        $data['ingreso_indep'] = form_input('ingreso_indep', set_value('ingreso_indep', exist_obj($info, 'ingreso_indep')), 'class="form-control numeric" id="ingreso_indep"'.$disabled.'');
        $data['ingreso_nomina'] = form_input('ingreso_nomina', set_value('ingreso_nomina', exist_obj($info, 'ingreso_nomina')), 'class="form-control numeric" id="ingreso_nomina"'.$disabled.'');
        $data['nivel_endeudamiento'] = form_input('nivel_endeudamiento', set_value('nivel_endeudamiento', exist_obj($info, 'nivel_endeudamiento')), 'class="form-control numeric" id="nivel_endeudamiento"'.$disabled.'');
        $data['arraigo_casa'] = form_input('arraigo_casa', set_value('arraigo_casa', exist_obj($info, 'arraigo_casa')), 'class="form-control" id="arraigo_casa"'.$disabled.'');
        $data['arraigo_empleo'] = form_input('arraigo_empleo', set_value('arraigo_empleo', exist_obj($info, 'arraigo_empleo')), 'class="form-control" id="arraigo_empleo"'.$disabled.'');
        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/estatus'));
        $data['drop_estatus'] = form_dropdown('id_estatus', array_combos($estatus, 'id', 'estatus', TRUE),set_value('id_estatus', exist_obj($info, 'id_estatus')), 'class="form-control busqueda" id="id_estatus" disabled');
        $data['comentario'] = form_textarea('comentario', set_value('comentario', exist_obj($info, 'comentario')), 'class="form-control" id="comentario" disabled');

        //Adm utilidad
        $data['bono'] = form_input('bono', set_value('bono', exist_obj($info, 'bono')), 'class="form-control" id="bono" '.$disabled.'');
        $data['plazo'] = form_input('plazo', set_value('plazo', exist_obj($info, 'plazo')), 'class="form-control" id="plazo" '.$disabled.'');
        $data['precio_unidad'] = form_input('precio_unidad', set_value('precio_unidad', exist_obj($info, 'precio_unidad')), 'class="form-control numeric" id="precio_unidad" '.$disabled.'');
        $data['monto_financiar'] = form_input('monto_financiar', set_value('monto_financiar', exist_obj($info, 'monto_financiar')), 'class="form-control numeric" id="monto_financiar" '.$disabled.'');
        $comisiones = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/comisiones'));
        $data['comisiones'] = form_dropdown('id_comision', array_combos($comisiones, 'id', 'valor', TRUE), set_value('id_comision', exist_obj($info, 'id_comision')), 'class="form-control busqueda" id="id_comision" '.$disabled.'');
        $data['comision_agencia'] = form_input('comision_agencia', set_value('comision_agencia', exist_obj($info, 'comision_agencia')), 'class="form-control numeric" id="comision_agencia" '.$disabled.'');
        $data['comision_asesor'] = form_input('comision_asesor', set_value('comision_asesor', exist_obj($info, 'comision_asesor')), 'class="form-control numeric" id="comision_asesor" '.$disabled.'');
        $seguros = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/tipo-seguros'));
        $data['seguros'] = form_dropdown('id_tipo_seguro', array_combos($seguros, 'id', 'seguro', TRUE), set_value('id_tipo_seguro', exist_obj($info, 'id_tipo_seguro')), 'class="form-control busqueda" id="id_tipo_seguro" '.$disabled.'');
        $companias = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/companias-seguros'));
        $data['companias'] = form_dropdown('id_compania', array_combos($companias, 'id', 'compania', TRUE), set_value('id_compania', exist_obj($info, 'id_compania')), 'class="form-control busqueda" id="id_compania" '.$disabled.'');
        //utilidad
        $data['no_poliza'] = form_input('no_poliza', set_value('no_poliza', exist_obj($info, 'no_poliza')), 'class="form-control numeric" id="no_poliza"'.$disabled.'');
        $data['inicio_vigencia'] = form_input('inicio_vigencia', set_value('inicio_vigencia', exist_obj($info, 'inicio_vigencia')), 'class="form-control" id="inicio_vigencia"'.$disabled.'', 'date');
        $data['vencimiento_anual'] = form_input('vencimiento_anual', set_value('vencimiento_anual', exist_obj($info, 'vencimiento_anual')), 'class="form-control" id="vencimiento_anual"'.$disabled.'', 'date');
        $data['prima_total'] = form_input('prima_total', set_value('prima_total', exist_obj($info, 'prima_total')), 'class="form-control numeric" id="prima_total"');
        $data['iva'] = form_input('iva', set_value('iva', exist_obj($info, 'iva')), 'class="form-control numeric" id="iva"'.$disabled.'');
        $data['emision_poliza'] = form_input('emision_poliza', set_value('emision_poliza', exist_obj($info, 'emision_poliza')), 'class="form-control numeric" id="emision_poliza"'.$disabled.'');
        $data['prima_neta'] = form_input('prima_neta', set_value('prima_neta', exist_obj($info, 'prima_neta')), 'class="form-control numeric" id="prima_neta"'.$disabled.'');
        $data['udi_agencia'] = form_input('udi_agencia', set_value('udi_agencia', exist_obj($info, 'udi_agencia')), 'class="form-control numeric" id="udi_agencia" '.$disabled.'');
        $data['comision_asesor_prima'] = form_input('comision_asesor_prima', set_value('comision_asesor_prima', exist_obj($info, 'comision_asesor_prima')), 'class="form-control numeric" id="comision_asesor_prima" '.$disabled.'');
        $udis = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/udi'));
        $data['udis'] = form_dropdown('id_udi', array_combos($udis, 'id', 'udi', TRUE), set_value('id_udi', exist_obj($info, 'id_udi')), 'class="form-control busqueda" id="id_udi" '.$disabled.'');
        //fc
        $data['seguros_fc'] = form_dropdown('id_tipo_seguro_fc', array_combos($seguros, 'id', 'seguro', TRUE), set_value('id_tipo_seguro_fc', exist_obj($info, 'id_tipo_seguro_fc')), 'class="form-control busqueda" id="id_tipo_seguro_fc" '.$disabled.'');
        $data['companias_fc'] = form_dropdown('id_compania_fc', array_combos($companias, 'id', 'compania', TRUE), set_value('id_compania_fc', exist_obj($info, 'id_compania_fc')), 'class="form-control busqueda" id="id_compania_fc" '.$disabled.'');
        $data['no_poliza_fc'] = form_input('no_poliza_fc', set_value('no_poliza_fc', exist_obj($info, 'no_poliza_fc')), 'class="form-control numeric" id="no_poliza_fc" '.$disabled.'');
        $data['inicio_vigencia_fc'] = form_input('inicio_vigencia_fc', set_value('inicio_vigencia_fc', exist_obj($info, 'inicio_vigencia_fc')), 'class="form-control" id="inicio_vigencia_fc" '.$disabled.'', 'date');
        $data['vencimiento_anual_fc'] = form_input('vencimiento_anual_fc', set_value('vencimiento_anual_fc', exist_obj($info, 'vencimiento_anual_fc')), 'class="form-control" id="vencimiento_anual_fc" '.$disabled.'', 'date');
        $data['prima_total_fc'] = form_input('prima_total_fc', set_value('prima_total_fc', exist_obj($info, 'prima_total_fc')), 'class="form-control numeric" id="prima_total_fc" '.$disabled.'');

        $data['iva_fc'] = form_input('iva_fc', set_value('iva_fc', exist_obj($info, 'iva_fc')), 'class="form-control numeric" id="iva_fc" '.$disabled.'');
        $data['emision_poliza_fc'] = form_input('emision_poliza_fc', set_value('emision_poliza_fc', exist_obj($info, 'emision_poliza_fc')), 'class="form-control numeric" id="emision_poliza_fc" '.$disabled.'');
        $data['prima_neta_fc'] = form_input('prima_neta_fc', set_value('prima_neta_fc', exist_obj($info, 'prima_neta_fc')), 'class="form-control numeric" id="prima_neta_fc" '.$disabled.'');
        $data['udi_agencia_fc'] = form_input('udi_agencia_fc', set_value('udi_agencia_fc', exist_obj($info, 'udi_agencia_fc')), 'class="form-control numeric" id="udi_agencia_fc" '.$disabled.'');
        $data['comision_asesor_prima_fc'] = form_input('comision_asesor_prima_fc', set_value('comision_asesor_prima_fc', exist_obj($info, 'comision_asesor_prima_fc')), 'class="form-control numeric" id="comision_asesor_prima_fc" '.$disabled.'');

        //medición de tiempo
        $data['fecha_condicionamiento'] = form_input('fecha_condicionamiento', set_value('fecha_condicionamiento', exist_obj($info, 'fecha_condicionamiento')), 'class="form-control" id="fecha_condicionamiento"'.$disabled.'', 'date');
        $data['hora_condicionamiento'] = form_input('hora_condicionamiento', set_value('hora_condicionamiento', exist_obj($info, 'hora_condicionamiento')), 'class="form-control" id="hora_condicionamiento"'.$disabled.'', 'time');
        $data['fecha_cumplimiento'] = form_input('fecha_cumplimiento', set_value('fecha_cumplimiento', exist_obj($info, 'fecha_cumplimiento')), 'class="form-control" id="fecha_cumplimiento"'.$disabled.'', 'date');
        $data['hora_cumplimiento'] = form_input('hora_cumplimiento', set_value('hora_cumplimiento', exist_obj($info, 'hora_cumplimiento')), 'class="form-control" id="hora_cumplimiento"'.$disabled.'', 'time');
        $data['fecha_aprobacion'] = form_input('fecha_aprobacion', set_value('fecha_aprobacion', exist_obj($info, 'fecha_aprobacion')), 'class="form-control" id="fecha_aprobacion"'.$disabled.'', 'date');
        $data['hora_aprobacion'] = form_input('hora_aprobacion', set_value('hora_aprobacion', exist_obj($info, 'hora_aprobacion')), 'class="form-control" id="hora_aprobacion"'.$disabled.'', 'time');
        $data['fecha_contrato'] = form_input('fecha_contrato', set_value('fecha_contrato', exist_obj($info, 'fecha_contrato')), 'class="form-control" id="fecha_contrato"'.$disabled.'', 'date');
        $data['hora_contrato'] = form_input('hora_contrato', set_value('hora_contrato', exist_obj($info, 'hora_contrato')), 'class="form-control" id="hora_contrato"'.$disabled.'', 'time');
        $data['fecha_compra'] = form_input('fecha_compra', set_value('fecha_compra', exist_obj($info, 'fecha_compra')), 'class="form-control" id="fecha_compra"'.$disabled.'', 'date');
        $data['hora_compra'] = form_input('hora_compra', set_value('hora_compra', exist_obj($info, 'hora_compra')), 'class="form-control" id="hora_compra"'.$disabled.'', 'time');
        $estatus_piso = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/estatus-piso'));
        $data['id_estatus_piso'] = form_dropdown('id_estatus_piso', array_combos($estatus_piso, 'id', 'estatus', TRUE), set_value('id_estatus_piso', exist_obj($info, 'id_estatus_piso')), 'class="form-control busqueda" id="id_estatus_piso" disabled');
        $data['input_comentarios_compra'] = form_textarea('comentarios_compra', set_value('comentarios_compra', exist_obj($info, 'comentarios_compra')), 'class="form-control" id="comentarios_compra" rows="2" disabled');
        $data['udis_fc'] = form_dropdown('id_udi_fc', array_combos($udis, 'id', 'udi', TRUE), set_value('id_udi_fc', exist_obj($info, 'id_udi_fc')), 'class="form-control busqueda" id="id_udi_fc" '.$disabled.'');

        //Bitácora compras
        $data['fecha_envio'] = form_input('fecha_envio', set_value('fecha_envio', exist_obj($info, 'fecha_envio')), 'class="form-control" id="fecha_envio"', 'date');
        $data['anio'] = form_input('anio', set_value('anio', exist_obj($info, 'anio')), 'class="form-control numeric" id="anio" maxlength="4"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $data['accesorios'] = form_textarea('accesorios', set_value('accesorios', exist_obj($info, 'accesorios')), 'class="form-control" id="accesorios" rows="2"');
        $data['extension_garantia'] = form_input('extension_garantia', set_value('extension_garantia', exist_obj($info, 'extension_garantia')), 'class="form-control" id="extension_garantia"');
        $data['observaciones'] = form_textarea('observaciones', set_value('observaciones', exist_obj($info, 'observaciones')), 'class="form-control" id="observaciones" rows="2"');
        $uso_seguros = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/uso-seguros'));
        $data['uso_seguro'] = form_dropdown('id_uso_seguro', array_combos($uso_seguros, 'id', 'uso_seguro', TRUE), set_value('id_uso_seguro', exist_obj($info, 'id_uso_seguro')), 'class="form-control busqueda" id="id_uso_seguro"');
        $data['id'] = $id;
        $data['bitacora'] = $bitacora;
        $data['titulo'] = 'Financiamientos y Seguros';
        $this->blade->render('agregar', $data);
    }
    public function cambiar_estatus()
    {
        $data['id_financiamiento'] = $this->input->get('id_financiamiento');
        $estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/estatus'));
        $data['drop_estatus'] = form_dropdown('id_estatus', array_combos($estatus, 'id', 'estatus', TRUE),$this->input->get('id_estatus'), 'class="form-control busqueda" id="id_estatus"');
        $estatus_piso = procesarResponseApiJsonToArray($this->curl->curlGet('api/financiamientos/catalogos/estatus-piso'));
        $data['estatus_piso'] = form_dropdown('id_estatus_piso', array_combos($estatus_piso, 'id', 'estatus', TRUE),$this->input->get('id_estatus_piso'), 'class="form-control busqueda" id="id_estatus_piso"');
        $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');
        $this->blade->render('comentarios', $data);
    }
    public function historial_comentarios()
    {
        $data['comentarios'] =  procesarResponseApiJsonToArray($this->curl->curlPost('api/financiamientos/historial-estatus/get-comentarios-financiamientos', [
            'id_financiamiento' => $_POST['id_financiamiento']
        ]));
        $this->blade->render('historial_comentarios', $data);
    }
}
