<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tipoCatalogo'] = array(
    '1' => array(
        'id'=>1,'Nombre'=>'Cálculo'
    ),
    '2' => array(
        'id'=>2,'Nombre'=>'Valoraciones'
    ),
    '3' => array(
        'id'=>3,'Nombre'=>'Salario'
    ),
    '4' => array(
        'id'=>4,'Nombre'=>'Faltas'
    )
);

$config['labelTablaSistema'] = array(
    '1' => array( #CALCULO
        'Valor_1'=>'Límite superior',
        'Valor_2'=>'Cuota fija',
        'Valor_3'=>'Procentaje'
    ),
    '2' => array( #Valoraciones
        'Valor_1'=>'Antigüedad',
        'Valor_2'=>'Días disfrute',
        'Valor_3'=>'Días prima v.'
    ),
    '3' => array( #Salario
        'Valor_1'=>'Antigüedad',
        'Valor_2'=>'Días prima v.',
        'Valor_2'=>'Días aguinaldo'
    ),
    '4' => array( #Faltas
        'Valor_1'=>'Días laborados por semana',
        'Valor_2'=>'Días ausentismo mes',
        'Valor_3'=>'Días a descontar'
    )
);

$config['genero'] = array(
    array('id'=>'m','Nombre'=>'Masculino'),
    array('id'=>'f','Nombre'=>'Femenino')
);

