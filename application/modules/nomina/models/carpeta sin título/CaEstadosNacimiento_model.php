<?php defined('BASEPATH') or exit('No direct script access allowed');

class CaEstadosNacimiento_model  extends CI_Model  {

    public $db_nomina;
	function __construct()
    {
        parent::__construct();
        $this->db_nomina = $this->load->database('nomina', TRUE);
    }

    public function getList(){
        $this->db_nomina
            ->select('id,Nombre')
            ->from('ca_entidadesnacimiento');
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->result_array() : false;
        $query->free_result();
        return $listado;
    }
}