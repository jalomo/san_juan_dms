<?php defined('BASEPATH') or exit('No direct script access allowed');

class CaClasificaciones_model  extends CI_Model  {

    public $db_nomina;
	function __construct()
    {
        parent::__construct();
        $this->db_nomina = $this->load->database('nomina', TRUE);
    }

    public function getList(){
        $this->db_nomina
            ->select('id,Clave,Descripcion')
            ->from('ca_clasificaciones');
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->result_array() : false;
        $query->free_result();
        return $listado;
    }

    public function getRow($where = array()){
        $this->db_nomina
            ->select('id,Clave,Descripcion')
            ->from('ca_clasificaciones');

        if(is_array($where) && count($where)>0){
            $this->db_nomina->where($where);
        }
        
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->row_array() : false;
        $query->free_result();
        return $listado;
    }

    public function insert($content){

        $this->load->library('uuid');
        $id = $this->uuid->v4();
        
        $this->db_nomina->set('id',$id);
        $this->db_nomina->set('FechaRegistro',date("Y-m-d H:i:s"));
        $response = $this->db_nomina->insert('ca_clasificaciones',$content);
        return $response? $id : false;
    }

    public function update($id,$content){
        $this->db_nomina->where('id',$id);
        $response = $this->db_nomina->update('ca_clasificaciones',$content);
        return $response? true : false;
    }

    public function getClave(){
        $this->db_nomina->select_max('Clave');
        $query = $this->db_nomina->get('ca_clasificaciones');
        $response = ($query->num_rows() > 0)? current($query->row_array()) : 0;
        $query->free_result();

        return ($response == 0)? 1 : ($response + 1);
    }

    public function delete($id){
        $this->db_nomina->where('id', $id);
        return $this->db_nomina->delete('ca_clasificaciones');
    }
}