<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{

   function __construct($opciones = false)
   {
        parent::__construct();

        $titulo = (is_array($opciones) && array_key_exists('titulo',$opciones))? $opciones['titulo'] : '';
        $size = (is_array($opciones) && array_key_exists('size',$opciones))? $opciones['size'] : array(216,356);
        $position = (is_array($opciones) && array_key_exists('position',$opciones))? $opciones['position'] : 'L';

        $CI =& get_instance();

        $pdf = new TCPDF($position, PDF_UNIT, $size, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
       
        // set default header data

        $CI->load->model('General_model');
        $contenido = $CI->General_model->call_api('configuracion/empresa/store_find',array('id'=>1),'get');
        $dataForm = $contenido['data'];

         // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $dataForm['RazonSocial'], $titulo."\n".date("d/m/Y")."\nRFC: ".$dataForm['RFC'], array(0,0,0), array(10,64,128));
        $pdf->SetHeaderData('', 2, $dataForm['RazonSocial'], $titulo."\n".date("d/m/Y")."\nRFC: ".$dataForm['RFC'], array(0,0,0), array(10,64,128));

        $pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(5, PDF_MARGIN_TOP, 5);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->ln(2);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        $CI->tcpdf = $pdf;

   }
}
