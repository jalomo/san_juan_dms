<?php defined('BASEPATH') or exit('No direct script access allowed');

# ca_usuarios
$this->forge->drop_table('ca_trabajadores',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Nombre' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Apellido1' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Apellido2' => array( 'type' => 'VARCHAR', 'constraint' => 250,'null' => TRUE ),
    'id_Genero' => array( 'type' => 'CHAR', 'constraint' => 1 ),
    'FechaNacimiento' => array( 'type' => 'DATE'),
    'id_EntidadNacimiento' => array( 'type' => 'INT', 'constraint' => 2 ),
    'NumeroIMSS' => array( 'type' => 'CHAR', 'constraint' => 11 ),
    'RFC' => array( 'type' => 'CHAR', 'constraint' => 15 ),
    'CURP' => array( 'type' => 'CHAR', 'constraint' => 18 ),
    'id_Clasificacion' => array( 'type' => 'CHAR', 'constraint' => 36 ),
    'id_Departamento' => array( 'type' => 'CHAR', 'constraint' => 36 ),
    'id_Puesto' => array( 'type' => 'CHAR', 'constraint' => 36 ),
    'FechaAlta' => array( 'type' => 'DATE'),

    'id_UsuarioRegistro' => array( 'type' => 'INT','constraint' => 11,'null'=>true),
    'FechaRegistro' => array( 'type' => 'TIMESTAMP','null'=>true),
    'id_UsuarioActualizacion' => array( 'type' => 'INT','constraint' => 11,'null'=>true),
    'FechaActualizacion' => array( 'type' => 'TIMESTAMP','null'=>true),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_trabajadores',true);

# ca_entidadesnacimiento
$this->forge->drop_table('ca_entidadesnacimiento',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'INT', 'constraint' => 11, 'unique' => TRUE ),
    'Nombre' => array( 'type' => 'VARCHAR', 'constraint' => 250 )
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_entidadesnacimiento',true);

$data = array(
    array('id' => 1,'Nombre' => 'Aguascalientes'),
    array('id' => 2,'Nombre' => 'Baja California'),
    array('id' => 3,'Nombre' => 'Baja California Sur'),
    array('id' => 4,'Nombre' => 'Campeche'),
    array('id' => 5,'Nombre' => 'Coahuila de Zaragoza'),
    array('id' => 6,'Nombre' => 'Colima'),
    array('id' => 7,'Nombre' => 'Chiapas'),
    array('id' => 8,'Nombre' => 'Chihuahua'),
    array('id' => 9,'Nombre' => 'Distrito Federal'),
    array('id' => 10,'Nombre' => 'Durango'),
    array('id' => 11,'Nombre' => 'Guanajuato'),
    array('id' => 12,'Nombre' => 'Guerrero'),
    array('id' => 13,'Nombre' => 'Hidalgo'),
    array('id' => 14,'Nombre' => 'Jalisco'),
    array('id' => 15,'Nombre' => 'México'),
    array('id' => 16,'Nombre' => 'Michoacán de Ocampo'),
    array('id' => 17,'Nombre' => 'Morelos'),
    array('id' => 18,'Nombre' => 'Nayarit'),
    array('id' => 19,'Nombre' => 'Nuevo León'),
    array('id' => 20,'Nombre' => 'Oaxaca'),
    array('id' => 21,'Nombre' => 'Puebla'),
    array('id' => 22,'Nombre' => 'Querétaro'),
    array('id' => 23,'Nombre' => 'Quintana Roo'),
    array('id' => 24,'Nombre' => 'San Luis Potosí'),
    array('id' => 25,'Nombre' => 'Sinaloa'),
    array('id' => 26,'Nombre' => 'Sonora'),
    array('id' => 27,'Nombre' => 'Tabasco'),
    array('id' => 28,'Nombre' => 'Tamaulipas'),
    array('id' => 29,'Nombre' => 'Tlaxcala'),
    array('id' => 30,'Nombre' => 'Veracruz de Ignacio de la Llave'),
    array('id' => 31,'Nombre' => 'Yucatán'),
    array('id' => 32,'Nombre' => 'Zacatecas'),
);
$this->db_nomina->insert_batch('ca_entidadesnacimiento', $data);

# ca_usuarios
$this->forge->add_foreign_key('ca_trabajadores','id_EntidadNacimiento','ca_entidadesnacimiento(id)');
$this->forge->add_foreign_key('ca_trabajadores','id_Clasificacion','ca_clasificaciones(id)');
$this->forge->add_foreign_key('ca_trabajadores','id_Departamento','ca_departamentos(id)');
$this->forge->add_foreign_key('ca_trabajadores','id_Puesto','ca_puestos(id)');

# ca_clasificaciones
$data = array(
    array('id' => utils::guid(), 'Clave' => 1,'Descripcion' => 'Clasificación #1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 2,'Descripcion' => 'Clasificación #2','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 3,'Descripcion' => 'Clasificación #3','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 4,'Descripcion' => 'Clasificación #4','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 5,'Descripcion' => 'Clasificación #5','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 6,'Descripcion' => 'Clasificación #6','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 7,'Descripcion' => 'Clasificación #7','FechaRegistro' => date("Y-m-d H:i:s"))
);
$this->db_nomina->insert_batch('ca_clasificaciones', $data);


# ca_departamentos
$data = array(
    array('id' => utils::guid(), 'Clave' => 1,'Descripcion' => 'Departamento #1','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 2,'Descripcion' => 'Departamento #2','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 3,'Descripcion' => 'Departamento #3','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 4,'Descripcion' => 'Departamento #4','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 5,'Descripcion' => 'Departamento #5','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 6,'Descripcion' => 'Departamento #6','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 7,'Descripcion' => 'Departamento #7','CuentaCOI'=> 0,'DepartamentoCOI'=>0,'FechaRegistro' => date("Y-m-d H:i:s")),
    
);
$this->db_nomina->insert_batch('ca_departamentos', $data);

# ca_departamentos
$data = array(
    array('id' => utils::guid(), 'Clave' => 1,'Descripcion' => 'Puesto #1','SalarioDiario'=>'1000.00','SalarioMaximo'=>'1010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 2,'Descripcion' => 'Puesto #2','SalarioDiario'=>'2000.00','SalarioMaximo'=>'2010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 3,'Descripcion' => 'Puesto #3','SalarioDiario'=>'3000.00','SalarioMaximo'=>'3010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 4,'Descripcion' => 'Puesto #4','SalarioDiario'=>'4000.00','SalarioMaximo'=>'4010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 5,'Descripcion' => 'Puesto #5','SalarioDiario'=>'5000.00','SalarioMaximo'=>'5010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 6,'Descripcion' => 'Puesto #6','SalarioDiario'=>'6000.00','SalarioMaximo'=>'6010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s")),
    array('id' => utils::guid(), 'Clave' => 7,'Descripcion' => 'Puesto #7','SalarioDiario'=>'7000.00','SalarioMaximo'=>'7010.00','id_RiesgoPuesto'=>'1','FechaRegistro' => date("Y-m-d H:i:s"))
);
$this->db_nomina->insert_batch('ca_puestos', $data);
