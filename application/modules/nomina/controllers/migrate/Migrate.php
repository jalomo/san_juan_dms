<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migrate extends MX_Controller
{
    public $forge;
    public $db_nomina;
    public function __construct()
    {
        parent::__construct();
        $this->db_nomina = $this->load->database('nomina', TRUE);
        $this->forge = $this->load->dbforge($this->db_nomina, TRUE);
    }

    public function index()
    {
        $this->create();
        
    }

    public function create(){
        include('2020_07_01.php');
        include('2020_07_15.php');
    }
    public function upgrade(){
        include('2020_07_15.php');
    }



    
}