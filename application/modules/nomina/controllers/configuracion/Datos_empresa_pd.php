<?php defined('BASEPATH') or exit('No direct script access allowed');

class Datos_empresa_pd extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/configuracion/datos_empresa_pd/');
        $this->breadcrumb = array(
            'Configuración',
            'Parámetros del sistema',
            // array('name'=>'Datos percepcion','url'=>site_url('nomina/configuracion/datos_empresa_pd/'))
        );
    }

    public function obtener_tabs($id){
        $contentForm = array(
            'id' => $id,
            'tab_1' => '',
            'tab_2' => '',
            
            'url_1' => site_url('nomina/configuracion/datos_empresa_pd/index?id=1'),
            'url_2' => site_url('nomina/configuracion/datos_empresa/datos_patron?id='.$id),
            // 'url_3' => site_url('nomina/configuracion/datos_empresa/domicilio_fiscal?id='.$id),

            'content_form' => ''
        );
        return $contentForm;
    }


    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }        
                            }
                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                        $form[$key][$key2]['value'] = $value2['input']['value'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;
    }

    public function index($parametros = 1){
        $this->breadcrumb[] = array('name'=>'Percepciones y deducciones','url'=>site_url('nomina/configuracion/datos_empresa_pd/'));

        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $id = 1;
        $contenido_form = $this->General_model->call_api('configuracion/parametros_pyd/store_find',array('id'=>$id),'get');
        if($contenido_form['data'] == false){
            $id = 0;
        }
        unset($contenido_form);
        
        $contenido_form = $this->General_model->form('configuracion/parametros_pyd/store_form',array('id'=>$id),'get');
        $contenido_form = $this->form($contenido_form);

        $obtener_tabs = $this->obtener_tabs(0);
        //$obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_1'] = 'active';
        $obtener_tabs['id'] = $id;

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa_pd/index',$contenido_form,true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa_pd/index_tabs',$obtener_tabs,true);

        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        $id = $this->input->post('id');

        $this->load->model('General_model');
        if($id == 1){
            $response = $this->General_model->call_api('configuracion/parametros_pyd/store',$parametros,'put');
        }else{
            $response = $this->General_model->call_api('configuracion/parametros_pyd/store',$parametros,'post');
        }
        
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }


}