<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tablero_total_recibos extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/tablero_total_recibos/');
        $this->breadcrumb = array(
            'Nomina',
            'Tablero de nomina',
            array('name'=>'Recibos Electrónicos','url'=>site_url('omina/inicio/tablero_total_recibos'))
        );
    }

    public function index($id_tipo_perdiodo = 1 , $id_periodo = false){

        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/tablero_total_recibos',array('id_periodo' => $id_periodo),'get');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');

        $dataContent = array(
            'contenido' => $dataForm['data'],
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
        );
        // utils::pre($dataContent);

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_recibos/index', $dataContent,true);
        $this->output( $html_content );

        
    }
}