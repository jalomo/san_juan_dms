<?php defined('BASEPATH') or exit('No direct script access allowed');

class Consultanomina extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/trabajadores/consultanomina/');
        $this->breadcrumb = array(
            'Nomina',
            'Trabajadores',
            array('name'=>'Consulta de nómina','url'=>site_url('nomina/inicio/consultanomina'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->title = 'Nómina del trabajador';

        $this->load->model('General_model');
        $periodo = $this->General_model->call_api('procesos/transacciones/periodo_actual',array(),'get');

        $dataContent = array(
            'periodo' => $periodo['data']
        );

        $html = $this->load->view('/inicio/consultanomina/index',$dataContent,true);
        $this->output($html);
    }

    public function obtener_listado(){
        
        $periodo = $this->input->post('periodo');

        $this->load->model('General_model');
        $formContent = $this->General_model->call_api('procesos/transacciones/precalculo_total_nomina_general',array('periodo'=>$periodo),'get');
        $this->response($formContent);
    }

}