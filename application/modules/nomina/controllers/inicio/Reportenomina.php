<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reportenomina extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/trabajadores/reportenomina/');
        $this->breadcrumb = array(
            'Nomina',
            'Trabajadores',
            array('name'=>'Reporte de la nómina','url'=>site_url('nomina/inicio/reportenomina'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->title = 'Reporte de la nómina';

        $this->load->model('General_model');
        $this->General_model->call_api('procesos/transacciones/proceso_temporal',array(),'get');


        $contenido = $this->General_model->call_api('reportes/nomina/reporte_general',array(),'get');
        $dataForm = $contenido['data'];

        $periodo = $this->General_model->call_api('procesos/transacciones/periodo_actual',array(),'get');
        $dataForm['periodo'] = $periodo['data'];

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/reportenomina/index',$dataForm,true);

        $this->output($html);
    }

    public function index_get()
    {
        
        $data = array(
            'id_departamento' => $this->input->post('id_departamento'),
            'id_formapago' => $this->input->post('id_formapago'),
            'id_clasificacion' => $this->input->post('id_clasificacion'),
            'jornada' => $this->input->post('jornada'),
            'id_pyd' => $this->input->post('id_pyd'),
            'periodo' => $this->input->post('periodo'),
            
        );

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('reportes/nomina/reporte_general_datos',$data,'post');

        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_pdf(){

        // utils::pre($_POST);
        $id_clasificacion = $this->input->post('id_clasificacion');
        $id_pyd = $this->input->post('id_pyd');
        $tipo_pyd = $this->input->post('tipo_pyd');
        $id_formapago = $this->input->post('id_formapago');
        $id_departamento = $this->input->post('id_departamento');
        $periodo = $this->input->post('periodo');

      
        $this->load->model('General_model');
        $clasificacion = $this->General_model->call_api('catalogos/clasificaciones/store_find',array('id'=>$id_clasificacion),'get');
        $id_clasificacion_desc = (is_array($clasificacion['data']))? $clasificacion['data']['Descripcion'] : 'Todos';

        $departamentos = $this->General_model->call_api('catalogos/departamentos/store',array(),'get');
        $departamentos_list = (is_array($departamentos['data']))? $departamentos['data'] : array();

        $departamentos_desc = array();
        $departamentos_id = array();
        if(is_array($id_departamento) && count($id_departamento)>0){

            foreach ($departamentos_list as $key => $value) {
                $departamentos_id[$value['id']] = $value['Descripcion'];
            }

            foreach ($id_departamento as $key => $value) {
                if(array_key_exists($value,$departamentos_id)){
                    $departamentos_desc[] =($departamentos_id[$value]);
                }
            }
        }
        $departamentos_desc = (count($departamentos_desc)>0)? implode(', ',$departamentos_desc) : '';

        $formapago = $this->General_model->call_api('catalogos/formapago/store',array(),'get');
        $formapago_list = (is_array($formapago['data']))? $formapago['data'] : array();

        $formapago_desc = array();
        $formapago_id = array();
        if(is_array($id_formapago) && count($id_formapago)>0){

            foreach ($formapago_list as $key => $value) {
                $formapago_id[$value['id']] = $value['Descripcion'];
            }

            foreach ($id_formapago as $key => $value) {
                if(array_key_exists($value,$formapago_id)){
                    $formapago_desc[] =($formapago_id[$value]);
                }
            }
        }
        $formapago_desc = (count($formapago_desc)>0)? implode(', ',$formapago_desc) : '';

        $periodo = $this->General_model->call_api('procesos/transacciones/periodo_actual',array(),'get');
        $periodo_desc = (is_array($periodo['data']))? utils::aFecha($periodo['data']['FechaInicio']).' al '.utils::aFecha($periodo['data']['FechaFin']) : '';

        $parametros = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store_find',array('id'=>1),'get');
        $parametros_desc = (is_array($parametros['data']))? $parametros['data']['Descripcion_Periodicidad'] : '';

        $this->load->library('Pdf',array(
            'titulo' => 'Reporte de la nómina'
        ));

        $filterData = array(
            'clasificacion' => $id_clasificacion_desc,
            'departamentos' => $departamentos_desc,
            'periodo_desc' => $periodo_desc,
            'parametros_desc' => $parametros_desc,
            'formapago_desc' => $formapago_desc
        );

        $this->load->library('parser');
        $view = $this->parser->parse('/inicio/reportenomina/index_pdf',$filterData,true);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);

        


        $this->load->library('table');
        $template = array(    
            'table_open'  => '<table border="0" cellpadding="4" cellspacing="" class="mytable" style="background-color: #f3f4f5;border-color:#c3c3c3;">',
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th style="font-weight:bold; text-align:left;background-color: #c3c3c3;" >',
            'heading_cell_end'      => '</th>',
            'row_start'             => '<tr style="border-bottom: 1px solid #ddd;background-color: #ffffff;" >',
            'row_end'               => '</tr>',
            'cell_start'            => '<td style="padding: 5px;" >',
            'cell_end'              => '</td>',
        );        
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Clave', 'Nombre del trabajador', 'Días trabajados','Sueldo','Otras percep.','Total percep.','Neto pagado',
            'Total en especie','Salario por hora','Total IMSS',
            'Horas por día','Total ISR','Subs Empleo','Otras deduc.','Total deduc.',
            'T. Efectivo','N.S.S.','RFC','CURP',
            'Sal. Diario','S.D.I.','D. Jornada','F. Alta','Tipo Salario','U.T. Laborales'
        ));

        $data_ = array(
            'id_departamento' => $this->input->post('id_departamento'),
            'id_formapago' => $this->input->post('id_formapago'),
            'id_clasificacion' => $this->input->post('id_clasificacion'),
            'jornada' => $this->input->post('jornada'),
            'id_pyd' => $this->input->post('id_pyd'),
            'periodo' => $this->input->post('periodo'),
        );

        $reporte = $this->General_model->call_api('reportes/nomina/reporte_general_datos',$data_,'post');
        // utils::pre($reporte);

        setlocale(LC_ALL, 'en_US');
        foreach ($reporte['data'] as $key => $value) {

            $jornada = ($value['Jornada'] == 'si')? 'Completa' : 'No Completa';
            $this->table->add_row(
                $value['Clave'], trim($value['Nombre'].' '.$value['Apellido_1'].''.$value['Apellido_2']), $value['DiasPeriodo'], money_format('%n',$value['Salario']), money_format('%n',$value['OtrasPercepciones']), money_format('%n',$value['TotalPercepciones']),money_format('%n',$value['NetoPagado']),
                money_format('%n',$value['TotalEspecie']),$value['SalarioHora'],money_format('%n',$value['TotalImss']),
                $value['HorasDia'],money_format('%n',$value['TotalIsr']),money_format('%n',$value['SubsidioEmpleo']),money_format('%n',$value['OtrasDeducciones']),money_format('%n',$value['TotalDeducciones']),
                money_format('%n',$value['TotalEfectivo']),$value['nss'],$value['rfc'],$value['curp'],
                $value['Salario'],$value['SDICapturado'],$jornada,utils::aFecha($value['FechaAlta'],true),$value['TipoPago'],$value['UTLaboradas']
            );

        }

        $table = array(
            'table' => $this->table->generate()
        );
        $view = $this->parser->parse('/inicio/reportenomina/index_pdf_table',$table,true);
        $this->tcpdf->SetFont('', '', 7);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);



        $this->tcpdf->Output('Reporte de la nomina.pdf', 'D');  

        
    }


    

}