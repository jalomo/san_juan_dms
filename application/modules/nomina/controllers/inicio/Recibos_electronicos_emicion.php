<?php defined('BASEPATH') or exit('No direct script access allowed');

class Recibos_electronicos_emicion extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/recibos_electronicos_emicion/');
        $this->breadcrumb = array(
            'Nomina',
            'e Recibos',
            array('name'=>'Emisión de recibos','url'=>site_url('nomina/inicio/recibos_electronicos_emicion'))
        );
    }

    public function index($id_tipo_perdiodo = 1 , $id_periodo = false,$id_departamento = false,$id_puesto = false){

        $this->scripts[] = script_tag($this->pathScript.'index.js');
        
        $this->load->model('General_model');
        
        $departamentos_api = $this->General_model->call_api('catalogos/departamentos/store',array(),'get');
        $departamentos = (is_array($departamentos_api['data']))? $departamentos_api['data'] : array();

        $puestos_api = $this->General_model->call_api('catalogos/puestos/store',array(),'get');
        $puestos = (is_array($puestos_api['data']))? $puestos_api['data'] : array();

        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        
        $dataContent = array(
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'id_departamento' => $id_departamento,
            'departamentos' => $departamentos,
            'id_puesto' => $id_puesto,
            'puestos' => $puestos
        );
    
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/recibos_electronicos_emicion/index', $dataContent ,true);

        $this->output($html);
    }

    public function index_get(){

        $id_periodo = $this->input->get_post('id_periodo');
        $id_departamento = $this->input->get('id_departamento');
        $id_puesto = $this->input->get('id_puesto');

        $this->load->model('General_model');
        if($id_puesto == false){
            $dataForm = $this->General_model->call_api('catalogos/periodo_pago/recibo_electronico',array('id_periodo'=>$id_periodo,'id_departamento'=>$id_departamento,'esta_timbrado'=>0),'get');
        }else{
            $dataForm = $this->General_model->call_api('catalogos/periodo_pago/recibo_electronico',array('id_periodo'=>$id_periodo,'id_departamento'=>$id_departamento,'id_puesto'=>$id_puesto,'esta_timbrado'=>0),'get');
        }
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    
    }
}