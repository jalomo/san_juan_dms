<?php defined('BASEPATH') or exit('No direct script access allowed');

class Recibos_electronicos_historico extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/recibos_electronicos_historico/');
        $this->breadcrumb = array(
            'Nomina',
            'e Recibos',
            array('name'=>'Histórico de Recibos Electrónicos','url'=>site_url('nomina/inicio/recibos_electronicos_historico'))
        );
    }

    public function index($id_departamento = false,$id_puesto = false){

        $this->scripts[] = script_tag($this->pathScript.'index.js');
        
        $this->load->model('General_model');
        
        $departamentos_api = $this->General_model->call_api('catalogos/departamentos/store',array(),'get');
        $departamentos = (is_array($departamentos_api['data']))? $departamentos_api['data'] : array();

        $puestos_api = $this->General_model->call_api('catalogos/puestos/store',array(),'get');
        $puestos = (is_array($puestos_api['data']))? $puestos_api['data'] : array();

        $dataContent = array(
            'id_departamento' => $id_departamento,
            'departamentos' => $departamentos,
            'id_puesto' => $id_puesto,
            'puestos' => $puestos
        );
    
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/recibos_electronicos_historico/index', $dataContent ,true);

        $this->output($html);
    }

    public function index_get(){
        
        $id_departamento = $this->input->get('id_departamento');
        $id_puesto = $this->input->get('id_puesto');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/periodo_pago/recibo_electronico_historico',array('id_departamento'=>$id_departamento,'id_puesto'=>$id_puesto),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    
    }
}