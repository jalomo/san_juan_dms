<?php defined('BASEPATH') or exit('No direct script access allowed');

class Percepcionesdeducciones extends MY_Controller
{
    public $title = '';
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/percepcionesdeducciones/');
        $this->breadcrumb = array(
            'Inicio',
            'P/D',
            array('name'=>'Percepción o Deducción','url'=>site_url('nomina/inicio/percepcionesdeducciones'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->library('parser');
        $html = $this->load->view('/inicio/percepcioneseducciones/index',false,true);

        $this->output($html);
    }

    public function index_get(){

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('nomina/percepcionesdeducciones/store',array(),'get');

        $this->response($dataContent);
    }

    public function obtener_tabs($id){
        $contentForm = array(
            'identity' => $id,
            'tab_1' => '',
            'tab_2' => '',
            
            'url_1' => site_url('nomina/inicio/nominatrabajador/detalle?id='.$id),
            'url_2' => site_url('nomina/inicio/nominatrabajador/detalle_movimientos?id='.$id),

            'detalle_form' => '',
            'content_form' => ''
        );
        return $contentForm;
    }


    public function obtener_alta_tabs($id = 0){
        $contentForm = array(
            'identity' => $id,
            'tab_1' => '',
            'tab_2' => '',
            
            'url_1' => site_url('nomina/inicio/nominatrabajador/detalle?id='.$id),
            'url_2' => site_url('nomina/inicio/nominatrabajador/detalle_movimientos?id='.$id),

            'detalle_form' => '',
            'content_form' => ''
        );
        return $contentForm;
    }

    public function form($form){
        $this->load->helper('form');

        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['group'] == 'grupo17'){
                        
                        $template = '{item}<div class="custom-control custom-checkbox">{input} <label class="custom-control-label" for="grupo9_{id}" >{label}</label></div>{/item}';
                        $opciones = array();
                        foreach ($value2['input']['catalog'] as $key3 => $value3) {
                            $values = json_decode($value2['input']['value']);
                            $checked = (is_array($values) && count($values)>0 && in_array($value3['id'],$values))? true : false;

                            $form_checkbox = array(
                                'id'          => 'grupo9_'.$key3,
                                'name'          => $value2['input']['name'],
                                'value'         => $value3['id'],
                                'class' => 'custom-control-input',
                                'checked'       => $checked
                            );

                            $opciones[$key3]['input'] = form_checkbox($form_checkbox);
                            $opciones[$key3]['label'] = $value3['Descripcion'];
                            $opciones[$key3]['id'] = $key3;
                        }
                        
                        $this->load->library('parser');
                        $form[$key][$key2]['input'] = $this->parser->parse_string($template, array('item'=>$opciones),true);

                    }else if($value2['group'] == 'grupo9'){
                        
                        $template = '{item}<div class="custom-control custom-checkbox">{input} <label class="custom-control-label" for="grupo9_{id}" >{label}</label></div>{/item}';
                        $opciones = array();
                        foreach ($value2['input']['catalog'] as $key3 => $value3) {

                            $form_checkbox = array(
                                'id'          => 'grupo9_'.$key3,
                                'name'          => $value2['input']['name'],
                                'value'         => $value3['id'],
                                'class' => 'custom-control-input'
                                // 'checked'       => TRUE,
                            );

                            $opciones[$key3]['input'] = form_checkbox($form_checkbox);
                            $opciones[$key3]['label'] = $value3['Descripcion'];
                            $opciones[$key3]['id'] = $key3;
                        }
                        
                        $this->load->library('parser');
                        $form[$key][$key2]['input'] = $this->parser->parse_string($template, array('item'=>$opciones),true);

                    }else if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                    }else{
                        $form[$key][$key2]['input'] = form_input($value2['input']);
                    }
                    $form[$key][$key2]['key'] = $value2['input']['id'];
                    //$form[$key]['label'] = $value2['label'];
                    // unset($form[$key][$key2]);
                    
                }
            }
        }
        return $form;
    }
 
    public function alta(){

        $this->scripts[] = script_tag($this->pathScript.'alta.js');

        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/percepcionesdeducciones/store_form',array(),'get');
        $form = $this->form($form);

        $form['identity'] = 0;

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/percepcioneseducciones/form_alta_datosgenerales',$form,true);
        $this->output($html);
    }

    public function cambios(){
        $id = base64_decode($this->input->get('id'));

        $this->scripts[] = script_tag($this->pathScript.'cambios.js');
        $this->breadcrumb[] = 'Cambios';

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/percepcionesdeducciones/store_form',array('id'=>$id),'get');
        $form = $this->form($form);
        
        $form['identity'] = $id;
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/percepcioneseducciones/form_cambios_datosgenerales',$form,true);
        $this->output($html);

    }

    public function guardar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/percepcionesdeducciones/store',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function actualizar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/percepcionesdeducciones/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }

    public function basefiscales($id = 0){
        $this->scripts[] = script_tag($this->pathScript.'basefiscales.js');
        $this->breadcrumb[] = 'Bases fiscales';

        $dataForm = array(
            'identity' => $id
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/percepcioneseducciones/basefiscales',$dataForm,true);
        $this->output($html);
    }

    public function basefiscales_list(){
        $id = $this->input->get('id');
        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('nomina/pydbasesfiscales/list',array('id'=>$id),'get');
        $this->response($dataContent);
    }

    public function basefiscales_modificar($id_pyd,$id_Base_Fiscal){

        $this->scripts[] = script_tag($this->pathScript.'basefiscales_modificar.js');
        $this->breadcrumb[] = array('name'=>'Bases fiscales','url'=>site_url('nomina/inicio/percepcionesdeducciones/basefiscales/'.$id_pyd));
        $this->breadcrumb[] = 'Modificar';


        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/pydbasesfiscales/store_form',array('id'=>$id_Base_Fiscal),'get');
        $form = $this->form($form);

        $form['id_pyd'] = $id_pyd;
        $form['id_Base_Fiscal'] = $id_Base_Fiscal;
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/percepcioneseducciones/basefiscales_modificar',$form,true);
        $this->output($html);

    }

    public function basefiscales_modificar_actualizar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/pydbasesfiscales/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }


}