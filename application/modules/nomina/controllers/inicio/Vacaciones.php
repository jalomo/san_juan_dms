<?php defined('BASEPATH') or exit('No direct script access allowed');

class Vacaciones extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/vacaciones/');
        $this->breadcrumb = array(
            'Nomina',
            'Movimientos',
            array('name'=>'Vacaciones','url'=>site_url('nomina/inicio/vacaciones'))
        );
    }

    // public function fechas_semana($year,$month,$day){
        
    //     # Obtenemos el numero de la semana
    //     $semana=date("W",mktime(0,0,0,$month,$day,$year));
        
    //     # Obtenemos el día de la semana de la fecha dada
    //     $diaSemana=date("w",mktime(0,0,0,$month,$day,$year));
        
    //     # el 0 equivale al domingo...
    //     if($diaSemana==0)
    //         $diaSemana=7;
        
    //     # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
    //     $primerDia=date("Y-m-d",mktime(0,0,0,$month,$day-$diaSemana+1,$year));
    //     $primerDia_desc=date("d/m/Y",mktime(0,0,0,$month,$day-$diaSemana+1,$year));
        
    //     # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
    //     $ultimoDia=date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diaSemana),$year));
    //     $ultimoDia_desc=date("d/m/Y",mktime(0,0,0,$month,$day+(7-$diaSemana),$year));

    //     return array(
    //         'diaSemana' => $diaSemana,
    //         'numSemana' => $semana,
    //         'primerDia' => $primerDia,
    //         'ultimoDia' => $ultimoDia,
    //         'primerDia_desc' => $primerDia_desc,
    //         'ultimoDia_desc' => $ultimoDia_desc
    //     );
    // }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');

        

        $dataContent = array(
            'trabajadores' => $dataForm['data'],
            'id_trabajador' => $id_trabajador
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/vacaciones/index', $dataContent,true);
        
        $this->output($html);
    }

    // public function form($form){
    //     $this->load->helper('form');
    //     if(is_array($form)){
    //         foreach ($form as $key => $value) {
    //             foreach ($value as $key2 => $value2) {
    //                 if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
    //                     if($value2['input']['catalog'] != false){
    //                         $opciones = array();
    //                         foreach ($value2['input']['catalog'] as $value3) {
    //                             if(array_key_exists('Clave',$value3)){
    //                                 $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
    //                             }else{
    //                                 $opciones[$value3['id']] = $value3['Descripcion'];
    //                             }
                                
    //                         }

    //                         $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
    //                     }else{
    //                         $form[$key][$key2]['input'] = form_input($value2['input']);
    //                     }
    //                     $form[$key][$key2]['key'] = $value2['input']['id'];
    //                 }else{
    //                     unset($form[$key][$key2]);
    //                 }
    //             }
    //         }
    //     }
    //     return $form;
    // }

    public function index_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/vacaciones/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function alta($trabajador,$semana = 1){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $dataForm_trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$trabajador),'get');

        // $periodo = $this->General_model->call_api('procesos/transacciones/periodo_actual',array(),'get');

        $dataForm = array(
            'id_trabajador' => $trabajador,
            'trabajador' => $dataForm_trabajador['data'],
            // 'periodo' => $periodo['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/vacaciones/alta', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_guardar(){

        $parametros = $this->input->post();
        $id_Trabajador = $this->input->post('id_Trabajador');
        $Fecha = $this->input->post('FechaDisfrute');
        
        $this->load->model('General_model');
        $datos = $this->General_model->call_api('nomina/vacaciones/store_find',array('id_Trabajador'=>$id_Trabajador,'FechaDisfrute'=>$Fecha),'get');
        
        if($datos['data'] === false){
            $response = $this->General_model->call_api('nomina/vacaciones/store',$parametros,'post');
            $code = (array_key_exists('code',$response))? $response['code'] : 200;
        }else{
            $code = 406;
            $response['status'] = 'error';
            $response['message']['FechaDisfrute'] = 'Ya se encuentra registrado';
        }
        $this->response($response,$code);
    }


    public function editar($id,$id_trabajador){
        
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Editar';

        $this->load->model('General_model');
        $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
        $registro = $this->General_model->call_api('nomina/vacaciones/store_find',array('id'=>$id),'get');

        $dataForm = array(
            'id' => $id,
            'id_trabajador' => $id_trabajador,
            'trabajador' => $trabajador['data'],
            'registro' => $registro['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/vacaciones/editar', $dataForm,true);
        
        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        $id_Trabajador = $this->input->post('id_Trabajador');
        $Fecha = $this->input->post('Fecha');
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/vacaciones/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }

    public function delete(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/vacaciones/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }
}