<?php defined('BASEPATH') or exit('No direct script access allowed');

class Parametrosnomina extends MY_Controller
{
    public $title = '';
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = 'js/nomina/inicio/parametrosnomina/';
        $this->breadcrumb = array(
            'Inicio',
            'Param. Nómina',
            'Parámetros de la nómina'
        );
    }

    public function index()
    {
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->breadcrumb[] = 'Generales';

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/parametrosnominagen/store_form',array('id'=>1));

        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['input']['catalog'] != false){
                        $opciones = array();
                        foreach ($value2['input']['catalog'] as $value3) {
                            $opciones[$value3['id']] = $value3['Descripcion'];
                        }

                        $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                    }else{
                        $form[$key][$key2]['input'] = form_input($value2['input']);
                    }
                    $form[$key][$key2]['key'] = $value2['input']['id'];
                }
            }
        }

        $this->load->library('parser');
        $contentForm = array(
            'identity' => 1,
            'tab_1' => 'active',
            'tab_2' => '',
            'tab_3' => '',
            'url_1' => site_url('nomina/inicio/parametrosnomina'),
            'url_2' => site_url('nomina/inicio/parametrosnomina/imss'),
            'url_3' => site_url('nomina/inicio/parametrosnomina/retencionImss'),
            'content_form' => $this->parser->parse('/inicio/parametrosnomina/index/generales', $form,true)
        );

        $html = $this->parser->parse('/inicio/parametrosnomina/index/form', $contentForm,true);
        $this->output($html);
    }

    public function guardar_general(){
        $parametros = $this->input->post();

        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }


    public function imss()
    {
        $this->scripts[] = script_tag($this->pathScript.'imss.js');
        $this->breadcrumb[] = 'Cuotas del IMSS';

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/parametrosnominaimss/store_form',array('id'=>1));
        
        $this->load->helper('form');
        $compiled = array();
        if(is_array($form)){
            foreach ($form as $key => $value) {
                $label = '';
                $input1 = '';
                $input2 = '';
                $input3 = '';
                $key_1 = '';
                $key_2 = '';
                $key_3 = '';

                foreach ($value as $key2 => $value2) {
                    $label = $value2['label'];
                    $temp_input = '';
                    $temp_key = '';

                    if($value2['input']['catalog'] != false){
                        $opciones = array();
                        foreach ($value2['input']['catalog'] as $value3) {
                            $opciones[$value3['id']] = $value3['Descripcion'];
                        }
                        

                        $temp_input = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                    }else{
                        $temp_input = form_input($value2['input']);
                    }
                    
                    switch ($key2) {
                        case 0:
                            $input1 = $temp_input;
                            $key_1 = $value2['input']['id'];
                            break;
                        case 1:
                            $input2 = $temp_input;
                            $key_2 = $value2['input']['id'];
                            break;
                        case 2:
                            $input3 = $temp_input;
                            $key_3 = $value2['input']['id'];
                            break;
                    }
                }
                $compiled[$key][0] = array(
                    'label' => $label,
                    'input1' => $input1,
                    'input2' => $input2,
                    'input3' => $input3,
                    'key1' => $key_1,
                    'key2' => $key_2,
                    'key3' => $key_3
                );
            }
        }

        $this->load->library('parser');
        $contentForm = array(
            'identity' => 1,
            'tab_1' => '',
            'tab_2' => 'active',
            'tab_3' => '',
            'url_1' => site_url('nomina/inicio/parametrosnomina'),
            'url_2' => site_url('nomina/inicio/parametrosnomina/imss'),
            'url_3' => site_url('nomina/inicio/parametrosnomina/retencionImss'),
            'content_form' => $this->parser->parse('/inicio/parametrosnomina/index/imss', $compiled,true)
        );

        $html = $this->parser->parse('/inicio/parametrosnomina/index/form', $contentForm,true);
        $this->output($html);
    }

    public function guardar_imss(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/trabajador/parametrosnominaimss/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }

    public function retencionImss()
    {
        $this->scripts[] = script_tag($this->pathScript.'retencionImss.js');
        $this->breadcrumb[] = 'Retención IMSS';

        $this->load->model('General_model');
        $form = $this->General_model->form('nomina/trabajador/parametrosnominaretencion/store_form',array('id'=>1));
        
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['input']['catalog'] != false){
                        $opciones = array();
                        foreach ($value2['input']['catalog'] as $value3) {
                            $opciones[$value3['id']] = $value3['Descripcion'];
                        }

                        $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                    }else{
                        $form[$key][$key2]['input'] = form_input($value2['input']);
                    }
                    $form[$key][$key2]['key'] = $value2['input']['id'];
                }
            }
        }

        $this->load->library('parser');
        $contentForm = array(
            'identity' => 1,
            'tab_1' => '',
            'tab_2' => '',
            'tab_3' => 'active',
            'url_1' => site_url('nomina/inicio/parametrosnomina'),
            'url_2' => site_url('nomina/inicio/parametrosnomina/imss'),
            'url_3' => site_url('nomina/inicio/parametrosnomina/retencionImss'),
            'content_form' => $this->parser->parse('/inicio/parametrosnomina/index/retencionImss', $form,true)
        );

        $html = $this->parser->parse('/inicio/parametrosnomina/index/form', $contentForm,true);
        $this->output($html);
    }

    public function guardar_retencionImss(){
        $parametros = $this->input->post();
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/trabajador/parametrosnominaretencion/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }
}
