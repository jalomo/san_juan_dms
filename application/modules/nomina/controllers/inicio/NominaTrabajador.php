<?php defined('BASEPATH') or exit('No direct script access allowed');

class Nominatrabajador extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/trabajadores/nominatrabajador/');
        $this->breadcrumb = array(
            'Nomina',
            'Trabajadores',// array('name'=>'Trabajadores','url'=>site_url('nomina/inicio/trabajadoresMenu')),
            array('name'=>'Nómina del trabajador','url'=>site_url('nomina/inicio/nominatrabajador'))
        );
    }

    public function obtener_detalle($id){
        $this->load->model('General_model');
        $formContent = $this->General_model->call_api('nomina/trabajador/datosgenerales/detalle',array('id'=>$id),'get');

        $this->load->library('parser');
        return $this->parser->parse('/inicio/nominatrabajador/obtener_detalle', $formContent['data'],true);
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->title = 'Nómina del trabajador';
        $html = $this->load->view('/inicio/nominatrabajador/index',false,true);
        $this->output($html);
    }

    public function obtener_tabs($id,$periodo=''){

        $contentForm = array(
            'identity' => $id,
            'tab_1' => '',
            'tab_2' => '',
            'tab_3' => '',
            'tab_4' => '',
            'tab_5' => '',
            'tab_6' => '',
            'tab_7' => '',
            
            'url_1' => site_url('nomina/inicio/nominaTrabajador/detalle/'.$id.'/'.$periodo),
            'url_2' => site_url('nomina/inicio/nominaTrabajador/detalle_movimientos/'.$id.'/'.$periodo),
            'url_3' => site_url('nomina/inicio/nominaTrabajador/detalle_faltas/'.$id.'/'.$periodo),
            'url_4' => site_url('nomina/inicio/nominaTrabajador/detalle_vacaciones/'.$id.'/'.$periodo),
            'url_5' => site_url('nomina/inicio/nominaTrabajador/detalle_horasExtra/'.$id.'/'.$periodo),
            'url_6' => site_url('nomina/inicio/nominaTrabajador/detalle_acumulados/'.$id.'/'.$periodo),
            'url_7' => site_url('nomina/inicio/nominaTrabajador/detalle_reciboElectronico/'.$id.'/'.$periodo),

            'detalle_form' => '',
            'content_form' => '',
            'url' => '',
            'periodos_trabajador' => array()
        );
        return $contentForm;
    }

    public function detalle($idTrabajador,$periodo=''){
        $this->scripts[] = script_tag($this->pathScript.'detalle.js');
        $this->title = 'Nómina del trabajador';
        $this->breadcrumb[] = 'Datos';

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('procesos/trabajador/pago/store_find',array('id_Periodo'=>$periodo),'get');
        $data_content = array(
            'montos' => $dataContent['data']
        );
        

        $obtener_tabs = $this->obtener_tabs($idTrabajador,$periodo);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['periodo'] = $periodo;
        $obtener_tabs['detalle_form'] = $this->obtener_detalle($idTrabajador);
        $obtener_tabs['tab_1'] = 'active';
        $obtener_tabs['url'] = site_url('nomina/inicio/nominaTrabajador/detalle/'.$idTrabajador);
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_recibo',$data_content,true);


        $this->load->model('General_model');
        $periodos_trabajador = $this->General_model->call_api('procesos/trabajador/periodos_concluidos',array('id_trabajador'=>$idTrabajador),'get');
        $obtener_tabs['periodos_trabajador'] = $periodos_trabajador['data'];

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }

    public function detalle_listado_percepciones(){
        $id_trabajador = $this->input->get('id_trabajador');
        $id_periodo = $this->input->get('id_periodo');

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('procesos/trabajador/conceptos_pago/store_findAll',array('id_Trabajador'=>$id_trabajador,'id_Periodo'=>$id_periodo,'ClavePD'=>'P'),'get');
        $this->response($dataContent);
    }

    public function detalle_listado_deducciones(){
        $id_trabajador = $this->input->get('id_trabajador');
        $id_periodo = $this->input->get('id_periodo');

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('procesos/trabajador/conceptos_pago/store_findAll',array('id_Trabajador'=>$id_trabajador,'id_Periodo'=>$id_periodo,'ClavePD'=>'D'),'get');
        $this->response($dataContent);
    }
    
    public function detalle_movimientos($idTrabajador,$periodo=false){
        $this->scripts[] = script_tag($this->pathScript.'detalle_movimientos.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';

        $detalle = $this->obtener_detalle($idTrabajador);

        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['periodo'] = $periodo;
        $obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_2'] = 'active';
        $obtener_tabs['url'] = site_url('nomina/inicio/nominaTrabajador/detalle/'.$idTrabajador);
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_movimientos',false,true);

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }

    public function detalle_faltas($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'detalle_faltas.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';


        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['detalle_form'] = $this->obtener_detalle($idTrabajador);
        $obtener_tabs['tab_3'] = 'active';
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_faltas',false,true);

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }

    public function detalle_vacaciones($idTrabajador,$periodo = ''){
        $this->scripts[] = script_tag($this->pathScript.'detalle_vacaciones.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';

        $this->load->model('General_model');
        $dataContent2 = $this->General_model->call_api('procesos/trabajador/pago/store_find',array('id_Periodo'=>$periodo),'get');
        $dataContent = $this->General_model->call_api('procesos/trabajador/conceptos_pago/store_find',array('id_PyD'=>9,'id_Periodo'=>$periodo,'id_Trabajador'=>$idTrabajador),'get');

        $data_content = array(
            'vacaciones' => $dataContent['data'],
            'periodo' => $dataContent2['data'],
            'id'=>$idTrabajador
        );

        $detalle = $this->obtener_detalle($idTrabajador);

        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['periodo'] = $periodo;
        $obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_4'] = 'active';
        $obtener_tabs['url'] = site_url('nomina/inicio/nominaTrabajador/detalle_vacaciones/'.$idTrabajador);
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_vacaciones',$data_content,true);

        $this->load->model('General_model');
        $periodos_trabajador = $this->General_model->call_api('periodos/periodos_concluidos',array('id_trabajador'=>$idTrabajador),'get');
        $obtener_tabs['periodos_trabajador'] = $periodos_trabajador['data'];
        

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }

    public function vacaciones_get(){
        
        $trabajador = $this->input->get_post('trabajador');
        $periodo = $this->input->get_post('periodo');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/vacaciones/store_findAll',array('id_Trabajador'=>$trabajador,'id_PeriodoPago' => $periodo),'get');
        $this->response($dataForm);
    }

    public function detalle_horasExtra($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'detalle_horasExtra.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';

        $detalle = $this->obtener_detalle($idTrabajador);

        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_5'] = 'active';
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_horasExtra',array('id'=>$idTrabajador),true);

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }

    public function detalle_acumulados($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'detalle_acumulados.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';

        $detalle = $this->obtener_detalle($idTrabajador);

        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_6'] = 'active';
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_acumulados',false,true);

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }
    
    public function detalle_reciboElectronico($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'detalle_reciboElectronico.js');
        $this->title = 'Nómina del trabajador';

        $this->breadcrumb[] = 'Datos';

        $detalle = $this->obtener_detalle($idTrabajador);

        $obtener_tabs = $this->obtener_tabs($idTrabajador);
        $obtener_tabs['identity'] = $idTrabajador;
        $obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_7'] = 'active';
        $obtener_tabs['content_form'] = $this->load->view('/inicio/nominatrabajador/detalle_reciboElectronico',false,true);

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/nominatrabajador/detalle',$obtener_tabs,true);
        $this->output($html);
    }
}