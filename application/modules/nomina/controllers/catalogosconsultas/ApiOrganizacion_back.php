<?php defined('BASEPATH') or exit('No direct script access allowed');

class ApiOrganizacion extends CI_Controller
{
    public $results = array('estatus' => 'ok', 'data' => false, 'info' => false);
    public $httpStatus = 200;
    public $rulesValidationPuestos = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->rulesValidationPuestos = array(
            array(
                'field' => 'Clave',
                'label' => 'Clave',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'Descripcion',
                'label' => 'Descripcion',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'SalarioDiario',
                'label' => 'SalarioDiario',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'SalarioMaximo',
                'label' => 'SalarioMaximo',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'id_RiesgoPuesto',
                'label' => 'Riesgo puesto',
                'rules' => 'trim|required'
            )
        );
    }

    /******************************************************************************* */
    /** PUESTOS */
    /******************************************************************************* */
    public function listPuestos(){
        $this->load->model('CaPuestos_model');
        $this->results['data'] = $this->CaPuestos_model->getList();
        $this->response($this->results,$this->httpStatus);
    }

    public function addPuesto()
    {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules($this->rulesValidationPuestos);

		if ($this->form_validation->run() == true) {

            $this->load->model('CaPuestos_model');
            $claveActual = $this->CaPuestos_model->getClave();
            $clave = $this->input->post('clave');
            $updateClave = false;
            if($clave == $claveActual){
                $updateClave = true;
            }

            $dataForm = array(
                'Clave' => $claveActual,
                'Descripcion' => $this->input->post('Descripcion'),
                'SalarioDiario' => $this->input->post('SalarioDiario'),
                'SalarioMaximo' => $this->input->post('SalarioMaximo'),
                'id_RiesgoPuesto' => $this->input->post('id_RiesgoPuesto')
            );
            
            $this->load->model('CaPuestos_model');
            $response = $this->CaPuestos_model->insert($dataForm);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
                $this->results['data'] = array('id'=>$response);
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);

    }

    public function updatePuesto()
    {
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules($this->rulesValidationPuestos);

		if ($this->form_validation->run() == true) {
            $id = $this->input->get('identity');

            $dataForm = array(
                'Descripcion' => $this->input->post('Descripcion'),
                'SalarioDiario' => $this->input->post('SalarioDiario'),
                'SalarioMaximo' => $this->input->post('SalarioMaximo'),
                'id_RiesgoPuesto' => $this->input->post('id_RiesgoPuesto')
            );
            
            $this->load->model('CaPuestos_model');
            $response = $this->CaPuestos_model->update($id,$dataForm);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
                $this->results['data'] = array('id'=>$id);
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);

    }

    public function deletePuesto()
    {
        $id = $this->input->post('identity');

        $this->load->model('CaPuestos_model');
        $response = $this->CaPuestos_model->delete($id);

        if($response){
            $this->httpStatus = 200;
            $this->results['estatus'] = 'ok';
            $this->results['info'] = 'El proceso se ha realizado correctamente';
            $this->results['data'] = array('id'=>$id);
        }else{
            $this->httpStatus = 400;
            $this->results['estatus'] = 'error';
            $this->results['info'] = 'Favor de intentar nuevamente';
        }

        $this->response($this->results,$this->httpStatus);
    }


    
    

    /******************************************************************************* */
    /** CLASIFICACIONES */
    /******************************************************************************* */
    public function listClasificaciones(){
        $this->load->model('CaClasificaciones_model');
        $this->results['data'] = $this->CaClasificaciones_model->getList();
        $this->response($this->results,$this->httpStatus);
    }

    public function addClasificacion()
    {
        
        $this->load->library('form_validation');
        // $this->form_validation->set_message('is_unique', 'El campo %s ya se encuentra registrado.');
		$this->form_validation->set_rules('clave', 'Clave', 'trim|required|numeric');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');

		if ($this->form_validation->run() == true) {

            $this->load->model('CaClasificaciones_model');
            $claveActual = $this->CaClasificaciones_model->getClave();
            $clave = $this->input->post('clave');
            $updateClave = false;
            if($clave == $claveActual){
                $updateClave = true;
            }

            $dataForm = array(
                'Clave' => $claveActual,
                'Descripcion' => $this->input->post('descripcion'),
            );

            $this->load->model('CaClasificaciones_model');
            $response = $this->CaClasificaciones_model->insert($dataForm);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
                $this->results['data'] = array('id'=>$response);
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function updateClasificacion()
    {
        
        $this->load->library('form_validation');
		$this->form_validation->set_rules('clave', 'Clave', 'trim|required|numeric');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');

		if ($this->form_validation->run() == true) {
            $id = $this->input->get('identity');
            $dataForm = array(
                'Descripcion' => $this->input->post('descripcion'),
            );

            $this->load->model('CaClasificaciones_model');
            $response = $this->CaClasificaciones_model->update($id,$dataForm);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
                $this->results['data'] = array('id'=>$id);
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function deleteClasificacion()
    {
        $id = $this->input->post('identity');

        $this->load->model('CaClasificaciones_model');
        $response = $this->CaClasificaciones_model->delete($id);

        if($response){
            $this->httpStatus = 200;
            $this->results['estatus'] = 'ok';
            $this->results['info'] = 'El proceso se ha realizado correctamente';
            $this->results['data'] = array('id'=>$id);
        }else{
            $this->httpStatus = 400;
            $this->results['estatus'] = 'error';
            $this->results['info'] = 'Favor de intentar nuevamente';
        }

        $this->response($this->results,$this->httpStatus);
    }


    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }
}