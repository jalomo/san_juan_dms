<?php defined('BASEPATH') or exit('No direct script access allowed');

class Historico_salarios extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/catalogosconsultas/historico_salarios/');
        $this->breadcrumb = array(
            'Catalogos y consultas',
            'Consulta de movimientos',
            array('name'=>'Hístorico de salarios','url'=>site_url('nomina/catalogosconsultas/historico_salarios'))
        );
    }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');


        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');

        $dataContent = array(
            'trabajadores' => $dataForm['data'],
            'id_trabajador' => $id_trabajador,
            // 'listado_fechas' => $listado_fechas,
            // 'numSemana' => $fecha_inicio['numSemana'],
            // 'fecha_inicio' => $fecha_inicio
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/historico_salarios/index', $dataContent,true);
        
        $this->output($html);
    }

    public function index_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/hist_trab_salario/store_findAll',array('id_Trabajador'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function alta($trabajador){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $dataForm_trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$trabajador),'get');
        $dataForm_trabajador2 = $this->General_model->call_api('nomina/trabajador/salario/store_find',array('id_Trabajador'=>$trabajador),'get');

        $dataForm = array(
            'id' => $dataForm_trabajador2['data']['id'],
            'id_trabajador' => $trabajador,
            'trabajador' => $dataForm_trabajador['data'],
            'salario' => $dataForm_trabajador2['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/historico_salarios/alta', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_guardar(){

        $parametros = $this->input->post();
        $id_Trabajador = $this->input->post('id_Trabajador');
        $Fecha = $this->input->post('FechaAplicacion');
        
        $this->load->model('General_model');
        $datos = $this->General_model->call_api('nomina/trabajador/salario/store_find',array('id_Trabajador'=>$id_Trabajador,'FechaAplicacion'=>$Fecha),'get');

        if($datos['data'] === false){
            $response = $this->General_model->call_api('nomina/trabajador/salario/store',$parametros,'put');
            $code = (array_key_exists('code',$response))? $response['code'] : 200;
        }else{
            $code = 406;
            $response['status'] = 'error';
            $response['message']['Fecha'] = 'Ya se encuentra registrado un movimiento';
        }
        $this->response($response,$code);
    }


    public function editar($id,$id_trabajador){
        
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Editar';

        $this->load->model('General_model');
        $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
        $registro = $this->General_model->call_api('nomina/horas_extras/store_find',array('id'=>$id),'get');

        $dataForm = array(
            'id' => $id,
            'id_trabajador' => $id_trabajador,
            'trabajador' => $trabajador['data'],
            'registro' => $registro['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/horas_extras/editar', $dataForm,true);
        
        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        $id_Trabajador = $this->input->post('id_Trabajador');
        $Fecha = $this->input->post('Fecha');
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/horas_extras/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }

    public function delete(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/horas_extras/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }
}