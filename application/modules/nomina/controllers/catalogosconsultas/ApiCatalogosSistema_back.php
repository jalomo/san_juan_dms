<?php defined('BASEPATH') or exit('No direct script access allowed');

class ApiCatalogosSistema extends CI_Controller
{
    public $results = array('estatus' => 'ok', 'data' => false, 'info' => false);
    public $httpStatus = 200;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /******************************************************************************* */
    /** TABLAS DEL SISTEMA */
    /******************************************************************************* */
    public function listTablasSistema(){
        $this->load->model('CaTablasSistema_model');
        $this->results['data'] = $this->CaTablasSistema_model->getList();
        $this->response($this->results,$this->httpStatus);
    }

    public function updateTablasSistema()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('Clave', 'Clave', 'trim|required|numeric');
		$this->form_validation->set_rules('Descripcion', 'Descripcion', 'trim|required');
		$this->form_validation->set_rules('Contenido', 'Contenido', 'callback_tablaSistemaContenido');

		if ($this->form_validation->run() == true) {
            $id_usuario = $this->session->userdata('id');
            $id_tabla = $this->input->get('identity');
       
            $dataForm = array(
                'Descripcion' => $this->input->post('Descripcion')
            );

            $this->load->model('CaTablasSistema_model');
            $response = $this->CaTablasSistema_model->update($id_tabla,$dataForm);
            if($response){
                $this->load->model('CaValoresTablaSistema_model');
                $contenido = $this->CaValoresTablaSistema_model->getListDataTemp($id_tabla,$id_usuario);
                $band = false;
                if(is_array($contenido)){
                    $this->CaValoresTablaSistema_model->delete($id_tabla);

                    foreach ($contenido as $key => $value) {

                        $dataContent = array(
                            'id' => $value['id_Origen'],
                            'id_TablaSistema' => $id_tabla,
                            'Valor_1' => $value['Valor_1'],
                            'Valor_2' => $value['Valor_2'],
                            'Valor_3' => $value['Valor_3'],
                        );

                        $response = $this->CaValoresTablaSistema_model->insert($dataContent);
                        if($response == false){
                            $band = true;
                            break;
                        }
                    }

                    if($band){
                        $this->httpStatus = 400;
                        $this->results['estatus'] = 'error';
                        $this->results['info'] = 'Favor de intentar nuevamente';
                    }else{
                        $this->httpStatus = 200;
                        $this->results['estatus'] = 'ok';
                        $this->results['data'] = array('id'=>$id_tabla);
                        $this->results['info'] = 'El proceso se ha realizado correctamente';
                    }
                }
                
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }
        
		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function tablaSistemaContenido($str)
        {
            
            $id_usuario = $this->session->userdata('id');
            $id_tabla = $this->input->get('identity');
            $this->load->model('CaValoresTablaSistemaTemp_model');
            $count = $this->CaValoresTablaSistemaTemp_model->countDataTemp($id_tabla,$id_usuario);

            if ($count == 0)
            {
                    $this->form_validation->set_message('tablaSistemaContenido', 'Campo obligatorio');
                    return FALSE;
            }
            else
            {
                    return TRUE;
            }
        }

    /******************************************************************************* */
    /** TABLAS DEL VALORES DEL SISTEMA */
    /******************************************************************************* */
    public function listTablasSistemaValoresTemp(){

        $identity = $this->input->get('identity');
        $id_Usuario = $this->session->userdata('id');

        $this->load->model('CaValoresTablaSistema_model');
        $data = $this->CaValoresTablaSistema_model->getListDataTemp($identity,$id_Usuario);
        $this->results['data'] = $data;
        $this->response($this->results,$this->httpStatus);
    }

    public function rowTablasSistemaValoresTemp(){

        $identity = $this->input->get('identity');
        $id_Usuario = $this->session->userdata('id');

        $this->load->model('CaValoresTablaSistema_model');
        $data = $this->CaValoresTablaSistema_model->getRowDataTemp($identity,$id_Usuario);
        $this->results['data'] = $data;
        $this->response($this->results,$this->httpStatus);
    }

    public function addContentTable()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('Valor_1', $this->input->post('Label_Valor_1'), 'trim|required|numeric');
		$this->form_validation->set_rules('Valor_2', $this->input->post('Label_Valor_2'), 'trim|required');
		$this->form_validation->set_rules('Valor_3', $this->input->post('Label_Valor_3'), 'trim|required');

		if ($this->form_validation->run() == true) {
            $identity = $this->input->get('identity');
            $idRegistro = false;
       
            $this->load->model('CaValoresTablaSistema_model');
            $data = false;//$this->CaValoresTablaSistema_model->getRowDataTemp($identity);
            
            if(is_array($data)){
                exit();
            }else{
                $dataContent = array(
                    'id_TablaSistema' => $identity,
                    'Valor_1' => $this->input->post('Valor_1'),
                    'Valor_2' => $this->input->post('Valor_2'),
                    'Valor_3' => $this->input->post('Valor_3')
                );
                $this->load->model('CaValoresTablaSistema_model');
                $idRegistro = $this->CaValoresTablaSistema_model->setRowDataTemp($dataContent);
            }

            $this->httpStatus = 200;
            $this->results['estatus'] = 'ok';
            $this->results['data'] = array('id'=>$idRegistro);
            $this->results['info'] = 'El proceso se ha realizado correctamente';
		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function deleteValorTablasSistemaTemp()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('identity','IDENTIFICADOR', 'trim|required');
		
		if ($this->form_validation->run() == true) {
            $identity = $this->input->post('identity');
            
            $this->load->model('CaValoresTablaSistemaTemp_model');
            $response = $this->CaValoresTablaSistemaTemp_model->delete($identity);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
            } else {
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }
            
		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function editContentTable()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('Valor_1', $this->input->post('Label_Valor_1'), 'trim|required|numeric');
		$this->form_validation->set_rules('Valor_2', $this->input->post('Label_Valor_2'), 'trim|required');
		$this->form_validation->set_rules('Valor_3', $this->input->post('Label_Valor_3'), 'trim|required');

		if ($this->form_validation->run() == true) {
            $identity = $this->input->get('identity');
            $idRegistro = false;
       
            $this->load->model('CaValoresTablaSistema_model');
            $data = $this->CaValoresTablaSistema_model->getRowDataTemp($identity);

            if(is_array($data)){
                $dataContent = array(
                    'Valor_1' => $this->input->post('Valor_1'),
                    'Valor_2' => $this->input->post('Valor_2'),
                    'Valor_3' => $this->input->post('Valor_3')
                );
                $this->load->model('CaValoresTablaSistema_model');
                $idRegistro = $this->CaValoresTablaSistema_model->updateRowDataTemp($identity,$dataContent);

                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

            $this->httpStatus = 200;
            $this->results['estatus'] = 'ok';
            $this->results['data'] = array('id'=>$idRegistro);
            $this->results['info'] = 'El proceso se ha realizado correctamente';
		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }










    // public function updateDepartamentos()
    // {
        
    //     $this->load->library('form_validation');
	// 	$this->form_validation->set_rules('clave', 'Clave', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
	// 	$this->form_validation->set_rules('cuenta', 'Cuenta', 'trim|required');
	// 	$this->form_validation->set_rules('departamento', 'Departamento', 'trim|required');

	// 	if ($this->form_validation->run() == true) {
    //         $id = $this->input->get('identity');
    //         $dataForm = array(
    //             'Descripcion' => $this->input->post('descripcion'),
    //             'CuentaCOI' => $this->input->post('cuenta'),
    //             'DepartamentoCOI' => $this->input->post('departamento')
    //         );

    //         $this->load->model('CaDepartamentos_model');
    //         $response = $this->CaDepartamentos_model->update($id,$dataForm);
    //         if($response){
    //             $this->httpStatus = 200;
    //             $this->results['estatus'] = 'ok';
    //             $this->results['info'] = 'El proceso se ha realizado correctamente';
    //             $this->results['data'] = array('id'=>$id);
    //         }else{
    //             $this->httpStatus = 400;
    //             $this->results['estatus'] = 'error';
    //             $this->results['info'] = 'Favor de intentar nuevamente';
    //         }

	// 	} else {
	// 		$this->httpStatus = 406;
	// 		$this->results['estatus'] = 'error';
	// 		$this->results['info'] = $this->form_validation->error_array();
	// 	}
    //     $this->response($this->results,$this->httpStatus);
    // }

    // public function deleteDepartamento()
    // {
    //     $id = $this->input->post('identity');

    //     $this->load->model('CaDepartamentos_model');
    //     $response = $this->CaDepartamentos_model->delete($id);

    //     if($response){
    //         $this->httpStatus = 200;
    //         $this->results['estatus'] = 'ok';
    //         $this->results['info'] = 'El proceso se ha realizado correctamente';
    //         $this->results['data'] = array('id'=>$id);
    //     }else{
    //         $this->httpStatus = 400;
    //         $this->results['estatus'] = 'error';
    //         $this->results['info'] = 'Favor de intentar nuevamente';
    //     }

    //     $this->response($this->results,$this->httpStatus);
    // }

    // /******************************************************************************* */
    // /** CLASIFICACIONES */
    // /******************************************************************************* */
    // public function listClasificaciones(){
    //     $this->load->model('CaClasificaciones_model');
    //     $this->results['data'] = $this->CaClasificaciones_model->getList();
    //     $this->response($this->results,$this->httpStatus);
    // }

    // public function addClasificacion()
    // {
        
    //     $this->load->library('form_validation');
    //     // $this->form_validation->set_message('is_unique', 'El campo %s ya se encuentra registrado.');
	// 	$this->form_validation->set_rules('clave', 'Clave', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');

	// 	if ($this->form_validation->run() == true) {

    //         $this->load->model('CaClasificaciones_model');
    //         $claveActual = $this->CaClasificaciones_model->getClave();
    //         $clave = $this->input->post('clave');
    //         $updateClave = false;
    //         if($clave == $claveActual){
    //             $updateClave = true;
    //         }

    //         $dataForm = array(
    //             'Clave' => $claveActual,
    //             'Descripcion' => $this->input->post('descripcion'),
    //         );

    //         $this->load->model('CaClasificaciones_model');
    //         $response = $this->CaClasificaciones_model->insert($dataForm);
    //         if($response){
    //             $this->httpStatus = 200;
    //             $this->results['estatus'] = 'ok';
    //             $this->results['info'] = 'El proceso se ha realizado correctamente';
    //             $this->results['data'] = array('id'=>$response);
    //         }else{
    //             $this->httpStatus = 400;
    //             $this->results['estatus'] = 'error';
    //             $this->results['info'] = 'Favor de intentar nuevamente';
    //         }

	// 	} else {
	// 		$this->httpStatus = 406;
	// 		$this->results['estatus'] = 'error';
	// 		$this->results['info'] = $this->form_validation->error_array();
	// 	}
    //     $this->response($this->results,$this->httpStatus);
    // }

    // public function updateClasificacion()
    // {
        
    //     $this->load->library('form_validation');
	// 	$this->form_validation->set_rules('clave', 'Clave', 'trim|required|numeric');
	// 	$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');

	// 	if ($this->form_validation->run() == true) {
    //         $id = $this->input->get('identity');
    //         $dataForm = array(
    //             'Descripcion' => $this->input->post('descripcion'),
    //         );

    //         $this->load->model('CaClasificaciones_model');
    //         $response = $this->CaClasificaciones_model->update($id,$dataForm);
    //         if($response){
    //             $this->httpStatus = 200;
    //             $this->results['estatus'] = 'ok';
    //             $this->results['info'] = 'El proceso se ha realizado correctamente';
    //             $this->results['data'] = array('id'=>$id);
    //         }else{
    //             $this->httpStatus = 400;
    //             $this->results['estatus'] = 'error';
    //             $this->results['info'] = 'Favor de intentar nuevamente';
    //         }

	// 	} else {
	// 		$this->httpStatus = 406;
	// 		$this->results['estatus'] = 'error';
	// 		$this->results['info'] = $this->form_validation->error_array();
	// 	}
    //     $this->response($this->results,$this->httpStatus);
    // }

    // public function deleteClasificacion()
    // {
    //     $id = $this->input->post('identity');

    //     $this->load->model('CaClasificaciones_model');
    //     $response = $this->CaClasificaciones_model->delete($id);

    //     if($response){
    //         $this->httpStatus = 200;
    //         $this->results['estatus'] = 'ok';
    //         $this->results['info'] = 'El proceso se ha realizado correctamente';
    //         $this->results['data'] = array('id'=>$id);
    //     }else{
    //         $this->httpStatus = 400;
    //         $this->results['estatus'] = 'error';
    //         $this->results['info'] = 'Favor de intentar nuevamente';
    //     }

    //     $this->response($this->results,$this->httpStatus);
    // }


    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }
}