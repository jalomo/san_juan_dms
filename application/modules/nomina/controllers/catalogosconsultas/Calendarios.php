<?php defined('BASEPATH') or exit('No direct script access allowed');

class Calendarios extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->breadcrumb = array(
            'Catalogos y consultas',
            'Catalogos del sistema',
            array('name'=>'Calendarios','url'=>site_url('nomina/catalogosconsultas/calendarios')),
        );
        $this->pathScript = 'js/nomina/catalogosconsultas/calendarios/';
    }

    public function index()
    {
        $this->scripts[] = script_tag($this->pathScript.'index.js');


        $html = $this->load->view('/catalogosconsultas/calendarios/index',false,true);
        $this->output($html);
    }

    public function index_get(){

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('catalogos/calendarios/store',array(),'get');
        $this->response($dataContent);
    }

    public function cambios($identity){
        $this->breadcrumb[] = 'Cambios de calendarios';

        $this->scripts[] = script_tag($this->pathScript.'cambios.js');

        $dataForm = array(
            'identity' => $identity
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/calendarios/cambios',$dataForm,true);

        $this->output($html);

    }

    public function cambios_get(){

        $id = $this->input->get('id');

        $this->load->model('General_model');
        $dataContent = $this->General_model->call_api('catalogos/detalle_calendario/store_findAll',array('id_Calendario'=>$id),'get');

        $this->response($dataContent);
    }

    public function cambios_actualizar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/detalle_calendario/store',$parametros,'put');

        $Periodo = $this->input->post('Periodo');
        $id_Calendario = $this->input->post('id_Calendario');

        if($Periodo >= 2){
            $response_inicio = $this->General_model->call_api('catalogos/detalle_calendario/store_find',array('Periodo'=> ($Periodo-1),'id_Calendario'=>$id_Calendario ),'get');
            if(is_array($response_inicio['data']) && count($response_inicio['data'])>0){
                
                $id = $response_inicio['data']['id'];
                $inicio = $response_inicio['data']['Inicio'];
                $fin = $this->input->post('inicio_anterior');

                $datetime1 = new DateTime($inicio);
                $datetime2 = new DateTime($fin);
                $interval = $datetime1->diff($datetime2);

                $dataInicio = array(
                    'id' => $id,
                    'Fin' => $fin,
                    'Dias' => $interval->format('%a')
                );
                $this->General_model->call_api('catalogos/detalle_calendario/store',$dataInicio,'put');   
            }
        }

        $response_fin = $this->General_model->call_api('catalogos/detalle_calendario/store_find',array('Periodo'=> ($Periodo+1),'id_Calendario'=>$id_Calendario ),'get');
        if(is_array($response_fin['data']) && count($response_fin['data'])>0){
            
            $id = $response_fin['data']['id'];
            $inicio = $this->input->post('fin_siguiente');
            $fin = $response_fin['data']['Fin'];

            $datetime1 = new DateTime($inicio);
            $datetime2 = new DateTime($fin);
            $interval = $datetime1->diff($datetime2);

            $dataFin = array(
                'id' => $id,
                'Inicio' => $inicio,
                'Dias' => $interval->format('%a')
            );
            $this->General_model->call_api('catalogos/detalle_calendario/store',$dataFin,'put');   
        }

        $code = (array_key_exists('code',$response))? $response['code'] : 200; 
        $this->response($response,$code);
    }
    
}



