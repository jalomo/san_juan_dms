<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tipos_faltas extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/catalogosconsultas/tipos_faltas/');
        $this->breadcrumb = array(
            'Nomina',
            'Movimientos',
            array('name'=>'Tipos de faltas','url'=>site_url('nomina/catalogosconsultas/tipos_faltas'))
        );
    }

    public function index($id_trabajador = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/tipos_faltas/index', array(),true);
        
        $this->output($html);
    }

    public function index_get()
    {
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/tipos_faltas/store',array(),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    
                }
            }
        }
        return $form;
    }

    

    public function alta(){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $formContent = $this->General_model->form('catalogos/tipos_faltas/store_form',array(),'get');
        $formContent = $this->form($formContent);
        $clave = $this->General_model->call_api('catalogos/tipos_faltas/store_clave',array(),'get');
        $formContent['clave'] = $clave['data']['Clave'];
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/tipos_faltas/alta', $formContent,true);
        
        $this->output($html);
    }

    public function alta_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/tipos_faltas/store',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }


    public function editar($id){
        
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'editar';

        $this->load->model('General_model');
        $formContent = $this->General_model->form('catalogos/tipos_faltas/store_form',array('id'=>$id),'get');
        $formContent = $this->form($formContent);
        
        $clave = $this->General_model->call_api('catalogos/tipos_faltas/store_find',array('id'=>$id),'get');
        $formContent['id'] = $id;
        $formContent['clave'] = $clave['data']['Clave'];
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/tipos_faltas/editar', $formContent,true);
        
        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/tipos_faltas/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }

    public function delete(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/tipos_faltas/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }
}