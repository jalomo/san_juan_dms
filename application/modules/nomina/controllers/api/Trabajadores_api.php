<?php defined('BASEPATH') or exit('No direct script access allowed');

class Trabajadores_api extends CI_Controller
{
    public $results = array('estatus' => 'ok', 'data' => false, 'info' => false);
    public $httpStatus = 200;
    
    public function __construct()
    {
        parent::__construct();
    }

    /******************************************************************************* */
    /** ALTA TRABAJADOR */
    /******************************************************************************* */
    public function trabajador_guardar()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('Clave', 'Clave', 'trim|required|numeric');
        $this->form_validation->set_rules('Nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('Apellido1', 'Apellido paterno', 'trim|required');
        $this->form_validation->set_rules('Apellido2', 'Apellido materno', 'trim');
        $this->form_validation->set_rules('FechaNacimiento', 'Fecha nacimiento', 'trim|required');
        $this->form_validation->set_rules('id_EntidadNacimiento', 'Lugar de nacimiento', 'trim|required');
        $this->form_validation->set_rules('NumeroIMSS', 'IMSS', 'trim|required');
        $this->form_validation->set_rules('id_Genero', 'Sexo', 'trim|required');
        $this->form_validation->set_rules('RFC', 'RFC', 'trim|required');
        $this->form_validation->set_rules('CURP', 'CURP', 'trim|required');
        $this->form_validation->set_rules('id_Clasificacion', 'Clasificación', 'trim|required');
        $this->form_validation->set_rules('id_Departamento', 'Departamento', 'trim|required');
        $this->form_validation->set_rules('id_Puesto', 'Puesto', 'trim|required');
        $this->form_validation->set_rules('FechaAlta', 'Fecha alta', 'trim|required');

		if ($this->form_validation->run() == true) {

            $this->load->model('CaTrabajadores_model');
            $claveActual = $this->CaTrabajadores_model->getClave();
            $clave = $this->input->post('clave');
            $updateClave = false;
            if($clave == $claveActual){
                $updateClave = true;
            }

            $dataForm = array(
                'Clave' => $claveActual,
                'Nombre' => $this->input->post('Nombre'),
                'Apellido1' => $this->input->post('Apellido1'),
                'Apellido2' => $this->input->post('Apellido2'),
                'FechaNacimiento' => $this->input->post('FechaNacimiento'),
                'id_EntidadNacimiento' => $this->input->post('id_EntidadNacimiento'),
                'NumeroIMSS' => $this->input->post('NumeroIMSS'),
                'id_Genero' => $this->input->post('id_Genero'),
                'RFC' => $this->input->post('RFC'),
                'CURP' => $this->input->post('CURP'),
                'id_Clasificacion' => $this->input->post('id_Clasificacion'),
                'id_Departamento' => $this->input->post('id_Departamento'),
                'id_Puesto' => $this->input->post('id_Puesto'),
                'FechaAlta' => $this->input->post('FechaAlta'), 
            );
            
            $this->load->model('CaTrabajadores_model');
            $response = $this->CaTrabajadores_model->insert($dataForm);
            if($response){
                $this->httpStatus = 200;
                $this->results['estatus'] = 'ok';
                $this->results['info'] = 'El proceso se ha realizado correctamente';
                $this->results['data'] = array('id'=>$response);
            }else{
                $this->httpStatus = 400;
                $this->results['estatus'] = 'error';
                $this->results['info'] = 'Favor de intentar nuevamente';
            }

		} else {
			$this->httpStatus = 406;
			$this->results['estatus'] = 'error';
			$this->results['info'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);

    }

    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }
}