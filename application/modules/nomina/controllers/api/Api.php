<?php defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MX_Controller
{
    public $api_path = '';
    public $results = array('status' => 'success', 'data' => false, 'message' => false);
    public $httpStatus = 200;

    public function __construct()
    {
        parent::__construct();
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $this->api_path = 'http://localhost/sistema_nomina/';
        }else{
            $this->api_path = 'https://www.sohex.net/nomina_dms/';
        }
    }

    public function store($tipo,$metodo){
        $departamentos = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'catalogos/departamentos/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'catalogos/departamentos/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'catalogos/departamentos/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'catalogos/departamentos/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'catalogos/departamentos/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'catalogos/departamentos/store')
        );

        $clasificaciones = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'catalogos/clasificaciones/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'catalogos/clasificaciones/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'catalogos/clasificaciones/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'catalogos/clasificaciones/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'catalogos/clasificaciones/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'catalogos/clasificaciones/store')
        );

        $puestos = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'catalogos/puestos/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'catalogos/puestos/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'catalogos/puestos/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'catalogos/puestos/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'catalogos/puestos/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'catalogos/puestos/store')
        );

        $tablassistema = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'catalogos/tablassistema/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'catalogos/tablassistema/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'catalogos/tablassistema/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'catalogos/tablassistema/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'catalogos/tablassistema/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'catalogos/tablassistema/store'),

            'datos_alta' => array('tipo' => 'get','url' => $this->api_path.'catalogos/tablassistema/get'),
            'add' => array('tipo' => 'post','url' => $this->api_path.'catalogos/tablassistema/add'),
            'edit' => array('tipo' => 'post','url' => $this->api_path.'catalogos/tablassistema/edit'),
            'listTemp' => array('tipo' => 'get','url' => $this->api_path.'catalogos/tablassistema/listTemp'),
        );

        $detablassistematemp = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'catalogos/detablassistematemp/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'catalogos/detablassistematemp/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'catalogos/detablassistematemp/store_find'),
            'findAll' => array('tipo' => 'get','url' => $this->api_path.'catalogos/detablassistematemp/store_findAll'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'catalogos/detablassistematemp/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'catalogos/detablassistematemp/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'catalogos/detablassistematemp/store')
        );

        $trabajador_datosgenerales = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosgenerales/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosgenerales/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosgenerales/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosgenerales/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/datosgenerales/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/datosgenerales/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosgenerales/validate'),
        );

        $trabajador_salario = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/salario/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/salario/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/salario/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/salario/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/salario/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/salario/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/salario/validate'),
        );

        $trabajador_datospersonales = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datospersonales/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datospersonales/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datospersonales/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datospersonales/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/datospersonales/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/datospersonales/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datospersonales/validate'),
        );

        $trabajador_datosimss = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosimss/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosimss/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosimss/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosimss/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/datosimss/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/datosimss/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosimss/validate'),
        );

        $trabajador_datossalud = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datossalud/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datossalud/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datossalud/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datossalud/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/datossalud/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/datossalud/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datossalud/validate'),
        );

        $trabajador_datosfiscales = array(
            'get' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosfiscales/store'),
            'clave' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosfiscales/store_clave'),
            'find' => array('tipo' => 'get','url' => $this->api_path.'nomina/trabajador/datosfiscales/store_find'),
            'post' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosfiscales/store'),
            'put' => array('tipo' => 'put','url' => $this->api_path.'nomina/trabajador/datosfiscales/store'),
            'delete' => array('tipo' => 'delete','url' => $this->api_path.'nomina/trabajador/datosfiscales/store'),
            'validate' => array('tipo' => 'post','url' => $this->api_path.'nomina/trabajador/datosfiscales/validate'),
        );

        $config = false;
        switch ($tipo) {
            case 'departamentos':
                $config = (array_key_exists($metodo,$departamentos))? $departamentos[$metodo] : false;
                break;

            case 'clasificaciones':
                $config = (array_key_exists($metodo,$clasificaciones))? $clasificaciones[$metodo] : false;
                break;
            case 'puestos':
                $config = (array_key_exists($metodo,$puestos))? $puestos[$metodo] : false;
                break;
            case 'tablassistema':
                $config = (array_key_exists($metodo,$tablassistema))? $tablassistema[$metodo] : false;
                break;
            case 'detablassistematemp':
                $config = (array_key_exists($metodo,$detablassistematemp))? $detablassistematemp[$metodo] : false;
                break;

            case 'trabajador_datosgenerales':
                $config = (array_key_exists($metodo,$trabajador_datosgenerales))? $trabajador_datosgenerales[$metodo] : false;
                break;
            case 'trabajador_salario':
                $config = (array_key_exists($metodo,$trabajador_salario))? $trabajador_salario[$metodo] : false;
                break;
            case 'trabajador_datospersonales':
                $config = (array_key_exists($metodo,$trabajador_datospersonales))? $trabajador_datospersonales[$metodo] : false;
                break;
            case 'trabajador_datosimss':
                $config = (array_key_exists($metodo,$trabajador_datosimss))? $trabajador_datosimss[$metodo] : false;
                break;
            case 'trabajador_datossalud':
                $config = (array_key_exists($metodo,$trabajador_datossalud))? $trabajador_datossalud[$metodo] : false;
                break;
            case 'trabajador_datosfiscales':
                $config = (array_key_exists($metodo,$trabajador_datosfiscales))? $trabajador_datosfiscales[$metodo] : false;
                break;
        }
        return $config;
    }

    public function runner()
    {

        $tipo = $this->uri->segment(5);
        $metodo = $this->uri->segment(6);
        $config = $this->store($tipo,$metodo);

        $response = array();
        $http_estatus = 200;

        if(is_array($config)){

            $this->load->library('Restclient');
            switch ($config['tipo']) {
                case 'get':
                    $parameters = (array)$this->input->post();
                    $response = $this->restclient->get($config['url'],$parameters);
                    break;

                case 'post':
                    $parameters = (array)$this->input->post();
                    $response = $this->restclient->post($config['url'],$parameters);
                    break;

                case 'put':
                    $parameters = (array)$this->input->post();
                    if(is_array($parameters) && !array_key_exists('id',$parameters) ){
                        $parameters['id'] = $this->input->get('id');
                    }
                    $response = $this->restclient->put($config['url'],$parameters);
                    break;
                case 'delete':
                    $parameters = (array)$this->input->post();
                    $response = $this->restclient->delete($config['url'],$parameters);
                    break;

            }
            $http_estatus = $this->restclient->http_code();

        }
        $this->response($response,$http_estatus);
    }

    public function guardar_trabajador(){

        $this->load->library('Restclient');

        $general = $this->input->post('general');
        $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosgenerales/validate',$general);
        $http_estatus = $this->restclient->http_code();

        if($http_estatus == 200){

            $salario = $this->input->post('salario');
            $response = $this->restclient->post($this->api_path.'nomina/trabajador/salario/validate',$salario);
            $http_estatus = $this->restclient->http_code();
            if($http_estatus == 200){

                $personales = $this->input->post('personales');
                $response = $this->restclient->post($this->api_path.'nomina/trabajador/datospersonales/validate',$personales);
                $http_estatus = $this->restclient->http_code();
                if($http_estatus == 200){

                    $imss = $this->input->post('imss');
                    $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosimss/validate',$imss);
                    $http_estatus = $this->restclient->http_code();
                    if($http_estatus == 200){

                        $salud = $this->input->post('salud');
                        $response = $this->restclient->post($this->api_path.'nomina/trabajador/datossalud/validate',$salud);
                        $http_estatus = $this->restclient->http_code();
                        if($http_estatus == 200){

                            $fiscales = $this->input->post('fiscales');
                            $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosfiscales/validate',$fiscales);
                            $http_estatus = $this->restclient->http_code();
                            if($http_estatus == 200){

                                    $this->results['tab'] = 1;
                                    $this->results['status'] = 'success';
                                    $this->results['message'] = '';                             

                            } else {
                                $this->results['tab'] = 6;
                                $this->results['status'] = 'error';
                                $this->results['message'] = $response['message'];
                            }


                        } else {
                            $this->results['tab'] = 5;
                            $this->results['status'] = 'error';
                            $this->results['message'] = $response['message'];
                        }


                    } else {
                        $this->results['tab'] = 4;
                        $this->results['status'] = 'error';
                        $this->results['message'] = $response['message'];
                    }


                } else {
                    $this->results['tab'] = 3;
                    $this->results['status'] = 'error';
                    $this->results['message'] = $response['message'];
                }

            } else {
                $this->results['tab'] = 2;
                $this->results['status'] = 'error';
                $this->results['message'] = $response['message'];
            }

        } else {
            $this->results['tab'] = 1;
            $this->results['status'] = 'error';
            $this->results['message'] = $response['message'];
        }

        $this->response($this->results,200);

    }

    public function guardar_trabajador2(){

        $this->load->library('Restclient');

        $general = $this->input->post('general');
        $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosgenerales/validate',$general);
        $http_estatus = $this->restclient->http_code();

        if($http_estatus == 200){

            $salario = $this->input->post('salario');
            $response = $this->restclient->post($this->api_path.'nomina/trabajador/salario/validate',$salario);
            $http_estatus = $this->restclient->http_code();
            if($http_estatus == 200){

                $personales = $this->input->post('personales');
                $response = $this->restclient->post($this->api_path.'nomina/trabajador/datospersonales/validate',$personales);
                $http_estatus = $this->restclient->http_code();
                if($http_estatus == 200){

                    $imss = $this->input->post('imss');
                    $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosimss/validate',$imss);
                    $http_estatus = $this->restclient->http_code();
                    if($http_estatus == 200){

                        $salud = $this->input->post('salud');
                        $response = $this->restclient->post($this->api_path.'nomina/trabajador/datossalud/validate',$salud);
                        $http_estatus = $this->restclient->http_code();
                        if($http_estatus == 200){

                            $fiscales = $this->input->post('fiscales');
                            $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosfiscales/validate',$fiscales);
                            $http_estatus = $this->restclient->http_code();
                            if($http_estatus == 200){


                                $response_tab1 = $this->restclient->post($this->api_path.'nomina/trabajador/datosgenerales/store',$general);
                                if($http_estatus == 200){
                                    $id_Trabajador = current($response_tab1['data']);
                                    $this->results['data']['general'] = $id_Trabajador;

                                    $contrato = $this->input->post('contrato');
                                    if(is_array($contrato)){
                                        $contrato['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $contrato = array('id_Trabajador' => $contrato);
                                    }
                                    $response_tab0 = $this->restclient->post($this->api_path.'nomina/trabajador/trabajadorcontrato/store',$contrato);
                                    $this->results['data']['contrato'] = ($this->restclient->http_code() == 200)? current($response_tab0['data']) : false;

                                    if(is_array($salario)){
                                        $salario['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $salario = array('id_Trabajador' => $id_Trabajador);
                                    }
                                    $response_tab2 = $this->restclient->post($this->api_path.'nomina/trabajador/salario/store',$salario);
                                    $this->results['data']['salario'] = ($this->restclient->http_code() == 200)? current($response_tab2['data']) : false;

                                    if(is_array($personales)){
                                        $personales['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $personales = array('id_Trabajador' => $id_Trabajador);
                                    }
                                    $response_tab3 = $this->restclient->post($this->api_path.'nomina/trabajador/datospersonales/store',$personales);
                                    $this->results['data']['personales'] = ($this->restclient->http_code() == 200)? current($response_tab3['data']) : false;

                                    if(is_array($imss)){
                                        $imss['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $imss = array('id_Trabajador' => $id_Trabajador);
                                    }
                                    $response_tab4 = $this->restclient->post($this->api_path.'nomina/trabajador/datosimss/store',$imss);
                                    $this->results['data']['imss'] = ($this->restclient->http_code() == 200)? current($response_tab4['data']) : false;

                                    if(is_array($salud)){
                                        $salud['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $salud = array('id_Trabajador' => $id_Trabajador);
                                    }
                                    $response_tab5 = $this->restclient->post($this->api_path.'nomina/trabajador/datossalud/store',$salud);
                                    $this->results['data']['salud'] = ($this->restclient->http_code() == 200)? current($response_tab5['data']) : false;

                                    if(is_array($fiscales)){
                                        $fiscales['id_Trabajador'] = $id_Trabajador;
                                    } else {
                                        $fiscales = array('id_Trabajador' => $id_Trabajador);
                                    }
                                    $response_tab6 = $this->restclient->post($this->api_path.'nomina/trabajador/datosfiscales/store',$fiscales);
                                    $this->results['data']['fiscales'] = ($this->restclient->http_code() == 200)? current($response_tab6['data']) : false;

                                    $this->results['tab'] = 1;
                                    $this->results['status'] = 'success';
                                    $this->results['message'] = $response_tab1['message'];

                                } else {
                                    $this->results['tab'] = 1;
                                    $this->results['status'] = 'error';
                                    $this->results['message'] = $response_tab1['message'];
                                }

                            } else {
                                $this->results['tab'] = 6;
                                $this->results['status'] = 'error';
                                $this->results['message'] = $response['message'];
                            }


                        } else {
                            $this->results['tab'] = 5;
                            $this->results['status'] = 'error';
                            $this->results['message'] = $response['message'];
                        }


                    } else {
                        $this->results['tab'] = 4;
                        $this->results['status'] = 'error';
                        $this->results['message'] = $response['message'];
                    }


                } else {
                    $this->results['tab'] = 3;
                    $this->results['status'] = 'error';
                    $this->results['message'] = $response['message'];
                }

            } else {
                $this->results['tab'] = 2;
                $this->results['status'] = 'error';
                $this->results['message'] = $response['message'];
            }

        } else {
            $this->results['tab'] = 1;
            $this->results['status'] = 'error';
            $this->results['message'] = $response['message'];
        }

        $this->response($this->results,200);

    }

    // public function actualiza_trabajador(){

    //     $this->load->library('Restclient');

    //     $general = $this->input->post('general');
    //     $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosgenerales/validate',$general);
    //     $http_estatus = $this->restclient->http_code();

    //     if($http_estatus == 200){

    //         $salario = $this->input->post('salario');
    //         $response = $this->restclient->post($this->api_path.'nomina/trabajador/salario/validate',$salario);
    //         $http_estatus = $this->restclient->http_code();
    //         if($http_estatus == 200){

    //             $personales = $this->input->post('personales');
    //             $response = $this->restclient->post($this->api_path.'nomina/trabajador/datospersonales/validate',$personales);
    //             $http_estatus = $this->restclient->http_code();
    //             if($http_estatus == 200){

    //                 $imss = $this->input->post('imss');
    //                 $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosimss/validate',$imss);
    //                 $http_estatus = $this->restclient->http_code();
    //                 if($http_estatus == 200){

    //                     $salud = $this->input->post('salud');
    //                     $response = $this->restclient->post($this->api_path.'nomina/trabajador/datossalud/validate',$salud);
    //                     $http_estatus = $this->restclient->http_code();
    //                     if($http_estatus == 200){

    //                         $fiscales = $this->input->post('fiscales');
    //                         $response = $this->restclient->post($this->api_path.'nomina/trabajador/datosfiscales/validate',$fiscales);
    //                         $http_estatus = $this->restclient->http_code();
    //                         if($http_estatus == 200){

    //                             $tab1 = $this->restclient->put($this->api_path.'nomina/trabajador/datosfiscales/store',$fiscales);
    //                             $this->results['data']['general'] = ($tab1['status'] == 'success')? true : false ;

    //                             $tab2 = $this->restclient->put($this->api_path.'nomina/trabajador/salario/store',$salario);
    //                             $this->results['data']['salario'] = ($tab2['status'] == 'success')? true : false ;

    //                             // $this->results['data']['personales'] = $this->restclient->put($this->api_path.'nomina/trabajador/datospersonales/store',$personales);
    //                             // $this->results['data']['imss'] = $this->restclient->put($this->api_path.'nomina/trabajador/datosimss/store',$imss);
    //                             // $this->results['data']['salud'] = $this->restclient->put($this->api_path.'nomina/trabajador/datossalud/store',$salud);
    //                             // $this->results['data']['fiscales'] = $this->restclient->put($this->api_path.'nomina/trabajador/datosfiscales/store',$fiscales);

    //                             $this->results['status'] = 'success';

    //                         } else {
    //                             $this->results['tab'] = 6;
    //                             $this->results['status'] = 'error';
    //                             $this->results['message'] = $response['message'];
    //                         }


    //                     } else {
    //                         $this->results['tab'] = 5;
    //                         $this->results['status'] = 'error';
    //                         $this->results['message'] = $response['message'];
    //                     }


    //                 } else {
    //                     $this->results['tab'] = 4;
    //                     $this->results['status'] = 'error';
    //                     $this->results['message'] = $response['message'];
    //                 }


    //             } else {
    //                 $this->results['tab'] = 3;
    //                 $this->results['status'] = 'error';
    //                 $this->results['message'] = $response['message'];
    //             }

    //         } else {
    //             $this->results['tab'] = 2;
    //             $this->results['status'] = 'error';
    //             $this->results['message'] = $response['message'];
    //         }

    //     } else {
    //         $this->results['tab'] = 1;
    //         $this->results['status'] = 'error';
    //         $this->results['message'] = $response['message'];
    //     }

    //     $this->response($this->results,200);

    // }

    public function upload_file(){
        $file_name_with_full_path = $_FILES['file'];
        $this->load->model('General_model');
        $response = $this->General_model->upload_file($file_name_with_full_path);
        $this->response($response);
    }


    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }
}