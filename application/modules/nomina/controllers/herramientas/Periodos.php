<?php defined('BASEPATH') or exit('No direct script access allowed');

class Periodos extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/herramientas/periodos/');
        $this->breadcrumb = array(
            'Nomina',
            'Herramientas',
            array('name'=>'Periodos de la nomina','url'=>site_url('nomina/herramientas/periodos'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');


        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('configuracion/periodicidades/store',array(),'get');

        $dataContent = array(
            'periodicidad' => $dataForm['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/herramientas/periodos/index', $dataContent,true);
        
        $this->output($html);
    }

    public function index_get()
    {
        $id_Periodicidad = $this->input->post();

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('procesos/transacciones/store_findAll',array('id_Periodicidad'=>$id_Periodicidad),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_recalcular_get()
    {
        $periodo = $this->input->post('periodo');
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('procesos/transacciones/precalculo_periodo_actual',array('periodo'=>$periodo),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_guardar()
    {
        
        $data_content = array(
            'id' => 1,
            'Periodicidad' => $this->input->post('periodo')
        );
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store',$data_content,'put');
        $this->General_model->call_api('procesos/transacciones/calculo_periodo_actual',$data_content,'get');

        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    

    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;

    }

    public function alta($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $dataForm = $this->General_model->form('nomina/movimientos/store_form',array(),'get');
        $dataForm = $this->form($dataForm);
        $dataForm['id_trabajador'] = $idTrabajador;
        $dataForm['id_Movimiento'] = false;
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/movimientos/form', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_guardar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/movimientos/store',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function editar($idTrabajador,$idMovimiento){
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Editar';

        $this->load->model('General_model');
        $dataForm = $this->General_model->form('nomina/movimientos/store_form',array('id'=>$idMovimiento),'get');
        $dataForm = $this->form($dataForm);
        $dataForm['id_trabajador'] = $idTrabajador;
        $dataForm['id_movimiento'] = $idMovimiento;
        

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/movimientos/form', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_actualizar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/movimientos/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }
    

}