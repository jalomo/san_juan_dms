<?php defined('BASEPATH') or exit('No direct script access allowed');

class Movimientos_trabajador extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/reportes/movimientos_trabajador/');
        $this->breadcrumb = array(
            'Reportes',
            'Movimientos de nómina',
            array('name'=>'Movimientos por trabajador','url'=>site_url('nomina/reportes/movimientos_trabajador'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');
        $this->title = 'Reporte de la nómina';

        $this->load->model('General_model');
        $departamentos = $this->General_model->call_api('catalogos/departamentos/store',array(),'get');
        $departamentos_list = (is_array($departamentos['data']))? $departamentos['data'] : array();

        $puestos = $this->General_model->call_api('catalogos/puestos/store',array(),'get');
        $puestos_list = (is_array($puestos['data']))? $puestos['data'] : array();

        $trabajadores = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',array(),'get');
        $trabajadores_list = (is_array($trabajadores['data']))? $trabajadores['data'] : array();

        $pyd = $this->General_model->call_api('nomina/percepcionesdeducciones/store',array(),'get');
        $pyd_list = (is_array($pyd['data']))? $pyd['data'] : array();

        $dataForm = array(
            'departamentos' => $departamentos_list,
            'puestos' => $puestos_list,
            'trabajadores' => $trabajadores_list,
            'pyd' => $pyd_list,
        );


        $this->load->library('parser');
        $html = $this->parser->parse('/reportes/movimientos_trabajador/index',$dataForm,true);

        $this->output($html);
    }

    public function index_get()
    {
        
        $data = $this->input->post();
            
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('reportes/trabajador/reporte_movimientos_trabajador',$data,'post');

        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_pdf(){

        $dataContent = $this->input->post();
      
        $this->load->library('Pdf',array(
            'titulo' => 'Movimientos por trabajador',
            'position' => 'L',
            'size' => 'A4'
        ));

        $filterData = array(
            'departamentos' => $this->input->post('departamento_desc'),
            'puesto' => $this->input->post('puesto_desc'),
            'jornada' => $this->input->post('jornada_desc')
        );

        $this->load->library('parser');
        $view = $this->parser->parse('/reportes/movimientos_trabajador/index_pdf',$filterData,true);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);


        $this->load->library('table');
        $template = array(    
            'table_open'  => '<table border="0" cellpadding="4" cellspacing="" class="mytable" style="background-color: #f3f4f5;border-color:#c3c3c3;">',
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th style="font-weight:bold; text-align:left;background-color: #c3c3c3;" >',
            'heading_cell_end'      => '</th>',
            'row_start'             => '<tr style="border-bottom: 1px solid #ddd;background-color: #ffffff;" >',
            'row_end'               => '</tr>',
            'cell_start'            => '<td style="padding: 5px;" >',
            'cell_end'              => '</td>',
        );        
        $this->table->set_template($template);
        $this->table->set_heading(array(
            'Clave', 
            'Nombre del trabajador',
            'Mov. Deducc. o Percep.',
            'J. Completa',
            'F. Inicio',
            'F. Termino',
            'Monto Mov.',
            'Monto Acum.',
            'Monto Limite',
            'Saldo'
        ));

        

        $this->load->model('General_model');
        $reporte = $this->General_model->call_api('reportes/trabajador/reporte_movimientos_trabajador',$dataContent,'post');
        
        setlocale(LC_ALL, 'en_US');

        foreach ($reporte['data'] as $key => $value) {
            $this->table->add_row(
                $value['Clave_trabajador'], 
                trim($value['Nombre'].' '.$value['Apellido_1'].' '.$value['Apellido_2']), 
                trim($value['Clave_pyd'].' '.$value['Descipcion_pyd']), 
                $value['JornadaCompleta'],
                ((strlen(trim($value['FechaInicio'])) > 0 && ( trim($value['FechaInicio']) != '0000-00-00'))? utils::aFecha($value['FechaInicio'],true) : ''), 
                ((strlen(trim($value['FechaTermino'])) > 0 && ( trim($value['FechaTermino']) != '0000-00-00'))? utils::aFecha($value['FechaTermino'],true) : ''),
                money_format('%n',$value['MontoMov']),
                money_format('%n',$value['MontoAcum']),
                money_format('%n',$value['MontoLimite']),
                money_format('%n',$value['Saldo'])
            );
        }

        $table = array(
            'table' => $this->table->generate()
        );
        $view = $this->parser->parse('/reportes/movimientos_trabajador/index_pdf_table',$table,true);
        $this->tcpdf->SetFont('', '', 9);
        $this->tcpdf->writeHTMLCell(0, 0, '', '', $view, 0, 1, 0, true, '', true);

        $this->tcpdf->Output('Reporte movimientos trabajadores.pdf', 'D');
    }

}