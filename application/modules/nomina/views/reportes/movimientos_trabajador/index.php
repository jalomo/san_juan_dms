<?php
	echo script_tag('js/nomina/jquery.fileDownload.js');
?>

<script id="tmpl_trabajadores" type="text/template">
    {{#data}}
    <div class="custom-control custom-checkbox">
        <input checked type="checkbox" name="id_trabajador[]" class="custom-control-input" value="{{id}}"
            id="id_trabajador_{{id}}">
        <label class="custom-control-label" for="id_trabajador_{{id}}">{{Clave}} -
            {{Nombre}} {{Apellido_1}} {{Apellido_2}}</label>
    </div>
    {{/data}}
</script>

<div class="row justify-content-md-center">

	<div class="col-sm-12 mb-2">
		<h3 class="card-title text-dark">Horas Extras</h3>
	</div>
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">

				<form id="form_content">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="id_departamento">Departamento</label>
								<select class="custom-select" name="id_departamento">
									<option value="">Todas</option>
									{departamentos}
									<option value="{id}">{Clave} - {Descripcion}</option>
									{/departamentos}
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="id_puestos">Puesto</label>
								<select class="custom-select" name="id_puesto">
									<option value="">Todas</option>
									{puestos}
									<option value="{id}">{Clave} - {Descripcion}</option>
									{/puestos}
								</select>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="jornada">Jornada</label>
								<select class="custom-select" name="jornada">
									<option value="">Todas</option>
									<option value="si">Completa</option>
									<option value="no">No Completa</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="">Trabajadores</label>
								<div id="listado_trabajadores">
									{trabajadores}
									<div class="custom-control custom-checkbox">
										<input checked type="checkbox" name="id_trabajador[]"
											class="custom-control-input" value="{id}" id="id_trabajador_{id}">
										<label class="custom-control-label" for="id_trabajador_{id}">{Clave} -
											{Nombre} {Apellido_1} {Apellido_2}</label>
									</div>
									{/trabajadores}
								</div>
							</div>
						</div>

						<div class="col-sm-8">
							<div class="form-group">
								<label for="id_puestos">Tipos de movimiento</label>
								<div class="row">
									<div class="col-sm-6">
										{pyd}
										<div class="custom-control custom-checkbox">
											<input name="id_pyd[]" clave="{cve_pd}" type="checkbox"
												class="custom-control-input id_pyd" value="{id}" id="id_pyd_{id}">
											<label class="custom-control-label" for="id_pyd_{id}">{Clave}
												{Descripcion}</label>
										</div>
										{/pyd}
									</div>
									<div class="col-sm-6">

										<div class="row">
											<div class="col-sm-12">
												<div class="custom-control custom-radio">
													<input checked value="1"
														onchange="Apps.check_percepcion_deduccion()" type="radio"
														name="tipo_pyd" class="custom-control-input" id="pyd_1">
													<label class="custom-control-label" for="pyd_1">Todos</label>
												</div>
											</div>

											<div class="col-sm-12">
												<div class="custom-control custom-radio">
													<input value="2" onchange="Apps.check_percepcion_deduccion()"
														type="radio" name="tipo_pyd" class="custom-control-input"
														id="pyd_2">
													<label class="custom-control-label" for="pyd_2">Percepciones</label>
												</div>
											</div>

											<div class="col-sm-12">
												<div class="custom-control custom-radio">
													<input value="3" onchange="Apps.check_percepcion_deduccion()"
														type="radio" name="tipo_pyd" class="custom-control-input"
														id="pyd_3">
													<label class="custom-control-label" for="pyd_3">Deducciones</label>
												</div>
											</div>

											<!-- <div class="col-sm-12">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="customCheck__4">
												<label class="custom-control-label" for="customCheck__4">Per/Ded cálculo</label>
											</div>-->
										</div>

										</div>
									</div>

								</div>
							</div>

						</div>


						<div class="row mt-4">
							<div class="col-sm-8">&nbsp;</div>
							<div class="col-sm-4">
								<button class="btn btn-secondary" onclick="location.reload();"
									type="button">Restablecer</button>
								<button class="btn btn-success" onclick="Apps.cargarDatos();"
									type="button">Buscar</button>
							</div>
						</div>

				</form>


			</div>

		</div>
	</div>
</div>

<div class="row justify-content-md-center mt-4">

	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">
				<div class="row mb-1">
					<div class="col-md-8"></div>
					<div class="col-md-4 mb-4" align="right">
						<!-- <a class="btn btn-primary"
							href="<?php echo site_url('nomina/inicio/trabajador/alta') ?>">Exportar a excel</a> -->
						<button class="btn btn-primary" type="button" onclick="Apps.descargar();">Exportar</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered dataTable" id="listado" width="100%" cellspacing="0">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
