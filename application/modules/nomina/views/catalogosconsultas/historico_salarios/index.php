<script>
    var  id_trabajador = "{id_trabajador}";
</script>

<div class="row">
    <div class="col-sm-12">
        <h3 class="mt-2 mb-3">Hístorico de salarios</h3>
    </div>
</div>

<div class="card">
	<div class="card-body">

		<form id="general_form">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Trabajador</label>
					<div class="col-sm-10">
						<select class="custom-select" onchange="Apps.cargarDatos();" id="trabajador" name="trabajador" >
							{trabajadores}
							<option value="{id}" >{Clave} {Nombre} {Apellido_1} {Apellido_2}</option>
							{/trabajadores}
						</select>

					</div>
				</div>
			</div>
			</div>

		</form>

	</div>
</div>


<div class="card mt-4">
	<div class="card-body ">
		<div class="row mb-4">
			<div class="col-md-11">&nbsp;</div>
			<div class="col-md-1">
				<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Agregar</button>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div>
