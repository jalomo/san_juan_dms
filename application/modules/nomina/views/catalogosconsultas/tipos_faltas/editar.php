<script>
var id = "{id}";
</script>

<div class="row">

	<div class="col-sm-12">
		<div class="mt-1">
			<div class="row">
				<div class="col-sm-12 mb-2">
					<h3>Edición de tipo de falta</h3>
				</div>
			</div>
			<div class="card">
				<div class="card-body">

					<form id="general_form">
                        <div class="form-group row">
							<label for="staticEmail" class="col-sm-3 col-form-label">Clave</label>
							<div class="col-sm-4">
                                <input type="" readonly class="form-control-plaintext" id="staticEmail" value="{clave}" />    
							</div>
						</div>
                        {grupo1}
						<div class="form-group row">
							<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
							<div class="col-sm-4">
								{input}
                                <small id="msg_{key}" class="text-danger"></small>
							</div>
						</div>
                        {/grupo1}
					</form>

				</div>
			</div>

		</div>

	</div>
</div>

<div class="col-sm-12">
	<div class="form-group row mt-3">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<!-- <button onclick="Apps.regresar(this);" type="button" class="btn btn-danger col-md-4">Cancelar</button> -->
			<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</div>
