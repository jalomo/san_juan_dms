
<script>
	const identity = "{identity}";
</script>
<script id="template" type="x-tmpl-mustache">
	<div class="form-group row">
		<label for="Clave" class="col-sm-3 col-form-label">Clave:</label>
		<div class="col-sm-9">
			<input type="number" class="form-control-plaintext" readonly="true" id="Clave" name="Clave" value="{{Clave}}">
            <small id="msg_Clave" class="form-text text-danger"></small>
		</div>
	</div>
	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Descripción:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="Descripcion" name="Descripcion" value="{{DataForm.Descripcion}}">
            <small id="msg_Descripcion" class="form-text text-danger"></small>
		</div>
	</div>
	<div class="form-group row">
		<label for="SalarioDiario" class="col-sm-3 col-form-label">Salario diario:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="SalarioDiario" name="SalarioDiario" value="{{DataForm.SalarioDiario}}">
            <small id="msg_SalarioDiario" class="form-text text-danger"></small>
		</div>
	</div>
	<div class="form-group row">
		<label for="SalarioMaximo" class="col-sm-3 col-form-label">Salario máximo:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="SalarioMaximo" name="SalarioMaximo" value="{{DataForm.SalarioMaximo}}">
            <small id="msg_SalarioMaximo" class="form-text text-danger"></small>
		</div>
	</div>

	<h6 class="text-muted mt-5">Recibos electrónicos</h6>
	<hr/>
	<div class="form-group row">
		<label for="id_RiesgoPuesto" class="col-sm-3 col-form-label">Riesgo puesto:</label>
		<div class="col-sm-9">
			<select class="form-control" id="id_RiesgoPuesto" name="id_RiesgoPuesto" selected="{{DataForm.id_RiesgoPuesto}}">
				{{#RiesgoPuesto}}
				<option value="{{id}}" {{selected}} >{{Numero}} - {{Descripcion}}</option>
				{{/RiesgoPuesto}}
			</select>
            <small id="msg_id_RiesgoPuesto" class="form-text text-danger"></small>
		</div>
	</div>
</script>
<form id="formContent">
	<div id="contenedor"></div>

	<div class="form-group row">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<button onclick="Apps.guardar();" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</form>
