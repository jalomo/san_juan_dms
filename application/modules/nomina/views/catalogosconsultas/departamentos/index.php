<div class="row mb-3">
	<div class="col-md-8"></div>
	<div class="col-md-4" align="right">
		<a class="btn btn-primary" href="<?php echo site_url('nomina/catalogosconsultas/departamentos/alta') ?>">Registrar</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="listado" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Clave</th>
						<th>Descripción</th>
						<th>Cuenta</th>
						<th>-</th>
						<th>-</th>
					</tr>
				</thead>
				<tbody>	
				</tbody>
				<tfoot>
					<tr>
						<th>Clave</th>
						<th>Descripción</th>
						<th>Cuenta</th>
						<th>-</th>
						<th>-</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
