<br/>
<div class="row">
    <div class="col-sm-12">
		<h3 class="mt-2 mb-3">Total de percepciones: $<?php echo utils::formatMoney($total) ?></h3>
    </div>
</div>
<br/>

<script>
	var id_tipo_perdiodo = "{id_tipo_perdiodo}";
	var id_perdiodo = "{id_perdiodo}";
</script>

<div class="card">
	<div class="card-body">
		<form class="">
			<div class="row">
				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_tipo_periodo">Tipo de periodo:</label>
						<select onchange="Apps.buscar_periodos();" class="custom-select" id="id_tipo_periodo">
							<?php foreach ($tipo_periodos as $key => $value) { ?>
							<option <?php echo($id_tipo_perdiodo == $value['id'])? 'selected' : ''; ?>
								value="<?php echo $value['id']; ?>"><?php echo $value['Descripcion']; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">Periodo:</label>
						<select class="custom-select" id="id_periodo">
							<?php foreach ($perdiodo as $key => $value) { ?>
                                <option <?php echo($id_perdiodo == $value['id'])? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo utils::aFecha($value['FechaInicio'],true); ?> al <?php echo utils::aFecha($value['FechaFin'],true); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>


				<div class="col-sm-4 mb-2">
					<div class="form-group">
						<label for="id_periodo">&nbsp;</label>
						<div class="col-sm-3">
						<button type="button" class="btn btn-primary mb-2" onclick="var opcion=$('select#periodo_trabajador option:selected').val(); Apps.cargarDatos(opcion);">Buscar</button>
					</div>
					</div>
				</div>

			</div>
		</form>
	</div>
</div>



<nav class="nav nav-pills nav-justified mt-5">
  <a class="nav-item nav-link {tab1}" href="<?php echo site_url('nomina/inicio/tablero_total_percepciones/tablero_pyd/'.$id_tipo_perdiodo.'/'.$id_perdiodo); ?>">Percepciones y deducciones</a>
  <a class="nav-item nav-link {tab5}" href="<?php echo site_url('nomina/inicio/tablero_total_percepciones/tablero_antiguedad/'.$id_tipo_perdiodo.'/'.$id_perdiodo); ?>">Antigüedad</a>
  <a class="nav-item nav-link {tab2}" href="<?php echo site_url('nomina/inicio/tablero_total_percepciones/tablero_departamentos/'.$id_tipo_perdiodo.'/'.$id_perdiodo); ?>">Departamentos</a>
  <a class="nav-item nav-link {tab3}" href="<?php echo site_url('nomina/inicio/tablero_total_percepciones/tablero_puestos/'.$id_tipo_perdiodo.'/'.$id_perdiodo); ?>">Puestos</a>
  <a class="nav-item nav-link {tab4}" href="<?php echo site_url('nomina/inicio/tablero_total_percepciones/tablero_clasificaciones/'.$id_tipo_perdiodo.'/'.$id_perdiodo); ?>">Clasificaciones</a>
</nav>
<br/>
<div class="tab-content" id="myTabContent">
    {contenido}
</div>