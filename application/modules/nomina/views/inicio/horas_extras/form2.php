<script>
    var  id_trabajador = "{id_trabajador}";
    var  id_movimiento = "{id_movimiento}";

    var  semana = "{semana}";

    var  dia_1 = "{dia_1}";
    var  dia_2 = "{dia_2}";
    var  dia_3 = "{dia_3}";
    var  dia_4 = "{dia_4}";
    var  dia_5 = "{dia_5}";
    var  dia_6 = "{dia_6}";
    var  dia_7 = "{dia_7}";
</script>

<div class="card mt-4">
	<div class="card-body ">


		<form id="general_form">


        
            <div class="form-group row">
                <label for="Semana" class="col-sm-2 col-form-label">Semana</label>
                <div class="col-sm-7">
                <input type="text" readonly class="form-control-plaintext" id="Semana" value="{semana_desc}">
                </div>
            </div>

            {grupo1}
            <div class="form-group row">
                <label for="{key}" class="col-sm-2 col-form-label">{label}</label>
                <div class="col-sm-7">
                    {input}
                    <small id="msg_{key}" class="form-text text-danger"></small>
                </div>
            </div>
            {/grupo1}
        <div style="display:none;">
            {grupo2}
            <div class="form-group row">
                <label for="{key}" class="col-sm-2 col-form-label">{label}</label>
                <div class="col-sm-10">
                    {input}
                    <small id="msg_{key}" class="form-text text-danger"></small>
                </div>
            </div>
            {/grupo2}

            {grupo0}
            <div class="form-group row">
                <label for="{key}" class="col-sm-2 col-form-label">{label}</label>
                <div class="col-sm-10">
                    {input}
                    <small id="msg_{key}" class="form-text text-danger"></small>
                </div>
            </div>
            {/grupo0}
            </div>

            <div class="col-sm-12">
                <div class="form-group row mt-3">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-9">
                        <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                    </div>
                </div>
            </div>

		</form>

	</div>
</div>
