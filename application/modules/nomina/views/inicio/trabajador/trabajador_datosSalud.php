<input type="hidden" class="form-control" name="id" value="{{id}}">

<div class="row">
	
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">

				<h6 class="text-dark">Restricciones de salud:</h5>
				<hr class="style-six" />

				<div class="form-group row">
					<div class="col-sm-12">
						<textarea class="form-control id="RestriccionesSalud" name="RestriccionesSalud" placeholder="" rows="5" >{{RestriccionesSalud}}</textarea>
						<small id="msg_RestriccionesSalud" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="col-sm-12 mt-3">
		<div class="card">
			<div class="card-body">

				<h6 class="text-dark">En caso de accidente avisar a:</h6>
				<hr class="style-six" />
			
				<div class="form-group row">
					<label for="EmergenciaNombre" class="col-sm-2 col-form-label">Nombre:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="EmergenciaNombre" name="EmergenciaNombre" value="{{EmergenciaNombre}}">
						<small id="msg_EmergenciaNombre" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="EmergenciaDireccion" class="col-sm-2 col-form-label">Dirección:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="EmergenciaDireccion" name="EmergenciaDireccion" value="{{EmergenciaDireccion}}">
						<small id="msg_EmergenciaDireccion" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="EmergenciaTelefono" class="col-sm-2 col-form-label">Teléfono:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="EmergenciaTelefono" name="EmergenciaTelefono" value="{{EmergenciaTelefono}}">
						<small id="msg_EmergenciaTelefono" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>