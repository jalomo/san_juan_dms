<input type="hidden" class="form-control" name="id" value="{{id}}">

<div class="row">
	<div class="col-sm-8">
		<div class="card">
			<div class="card-body">
			
				<div class="form-group row">
					<label for="id_TipoEmpleado" class="col-sm-4 col-form-label">Tipo empleado:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TipoEmpleado" name="id_TipoEmpleado" attr-id="{{id_TipoEmpleado}}"> 
                            {{#TipoEmpleado}}<option value="{{id}}" >{{Nombre}}</option> {{/TipoEmpleado}}
                        </select>
						<small id="msg_id_TipoEmpleado" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_TipoJornadaImss" class="col-sm-4 col-form-label">Tipo de jornada:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TipoJornadaImss" name="id_TipoJornadaImss" attr-id="{{id_TipoJornadaImss}}"> 
                            {{#TipoJornadaImss}}<option value="{{id}}" >{{Nombre}}</option> {{/TipoJornadaImss}}
                        </select>
						<small id="msg_id_TipoJornadaImss" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_TurnoImss" class="col-sm-4 col-form-label">Turno:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TurnoImss" name="id_TurnoImss" attr-id="{{id_TurnoImss}}" > 
                            {{#TurnosImss}}<option value="{{id}}" >{{Nombre}}</option> {{/TurnosImss}}
                        </select>
						<small id="msg_id_TurnoImss" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="UMF" class="col-sm-4 col-form-label">Unidad Médica Familiar:</label>
					<div class="col-sm-8">
						<input type="number" class="form-control" id="UMF" name="UMF" value="{{UMF}}">
						<small id="msg_UMF" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="DescPensionAlimenticia" class="col-sm-4 col-form-label">Descuenta pensión alimenticia:</label>
					<div class="col-sm-8">
						<select class="form-control" id="DescPensionAlimenticia" name="DescPensionAlimenticia" attr-id="{{DescPensionAlimenticia}}"> 
							<option value="no" >No</option>
                            <option value="si" >Si</option>
                        </select>
						<small id="msg_DescPensionAlimenticia" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- <div class="col-sm-4">
		<div class="card">
			<div class="card-body">

				<h6 class="text-dark">Contrato</h6>
				<hr class="style-six" />
			
				<div class="form-group row">
					<label for="id_TipoContrato" class="col-sm-5 col-form-label">Tipo de contrato:</label>
					<div class="col-sm-7">
						<input readonly class="form-control-plaintext" id="id_TipoContrato" name="id_TipoContrato" value="Permanente">
						<small id="msg_id_TipoContrato" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="FechaInicio" class="col-sm-5 col-form-label">Fecha de inicio:</label>
					<div class="col-sm-7">
						<input readonly class="form-control-plaintext" id="FechaInicio" name="FechaInicio" value="">
						<small id="msg_FechaInicio" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>

	</div> -->
</div>