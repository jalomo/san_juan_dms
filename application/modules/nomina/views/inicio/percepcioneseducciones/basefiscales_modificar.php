<script>
	var id_pyd = "{id_pyd}";
	var id_Base_Fiscal = "{id_Base_Fiscal}";
</script>

<div class="row">

	<div class="col-sm-12">
		<div class="mt-1">
			<div class="row">
				<div class="col-sm-12 mb-2">
					<h3>Datos Fiscales</h3>
				</div>
			</div>
			<div class="card">
				<div class="card-body">

					<form id="general_form">
                        {grupo1}
						<div class="form-group row">
							<label for="{key}" class="col-sm-2 col-form-label">{label}</label>
							<div class="col-sm-10">
								{input}
                                <small id="msg_{key}" class="text-danger"></small>
							</div>
						</div>
                        {/grupo1}
					</form>

				</div>
			</div>

		</div>

	</div>
</div>

<div class="col-sm-12">
	<div class="form-group row mt-3">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<button onclick="Apps.regresar(this);" type="button" class="btn btn-danger col-md-4">Cancelar</button>
			<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</div>
