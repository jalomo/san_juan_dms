<script>
    var  identity = "{identity}";
</script>

<div class="row">
    <div class="col-sm-12">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link {tab_1}" href="{url_1}">Datos Generales</a>
            <a class="nav-item nav-link {tab_2}" href="{url_2}">Bases Fiscales</a>
        </nav>
    </div>

    <div class="col-sm-12">
        <div class="mt-5" >
            {content_form}
        </div>
    </div>
</div>