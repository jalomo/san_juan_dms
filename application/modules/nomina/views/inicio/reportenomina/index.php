<?php
	echo script_tag('js/nomina/jquery.fileDownload.js');
?>

<div class="row justify-content-md-center">

	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">
				<h3 class="card-title text-dark">Datos generales</h3>



				<form id="form_content">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group row">
								<label for="id_departamento" class="col-sm-4 col-form-label">Departamento</label>
								<div class="col-sm-8">

									{departamentos}
									<div class="custom-control custom-checkbox">
										<input checked type="checkbox" name="id_departamento[]" class="custom-control-input" value="{id}"
											id="id_departamento_{id}">
										<label class="custom-control-label" for="id_departamento_{id}">{Clave} -
											{Descripcion}</label>
									</div>
									{/departamentos}

								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group row">
								<label for="id_formapago" class="col-sm-4 col-form-label">Forma de pago</label>
								<div class="col-sm-8">

									{formapago}
									<div class="custom-control custom-checkbox">
										<input type="checkbox" name="id_formapago[]" value="{id}"  class="custom-control-input"
											id="id_formapago_{id}">
										<label class="custom-control-label"
											for="id_formapago_{id}">{Descripcion}</label>
									</div>
									{/formapago}

								</div>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-sm-6">

							<div class="row">
								<!-- <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="staticEmail" class="col-sm-4 col-form-label">Registro Patronal</label>
                                        <div class="col-sm-8">
                                            <select class="custom-select">
                                                <option value="{id}">Todos</option>
                                            </select>          
                                        </div>
                                    </div>
                                </div> -->
								<div class="col-sm-12">
									<div class="form-group row">
										<label for="staticEmail" class="col-sm-4 col-form-label">Clasificacion</label>
										<div class="col-sm-8">
											<select name="id_clasificacion" class="custom-select">
												<option value="">Todas</option>
												{clasificaciones}
												<option value="{id}">{Clave} - {Descripcion}</option>
												{/clasificaciones}
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group row">
										<label for="staticEmail" name="jornada" class="col-sm-4 col-form-label">Jornada</label>
										<div class="col-sm-8">
											<select class="custom-select">
												<option value="">Todas</option>
												<option value="si">Completa</option>
												<option value="no">No completa</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="row">
								<!-- <div class="col-sm-12">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="customCheck1">
										<label class="custom-control-label" for="customCheck1">Detalle de percepciones
											y/o deducciones</label>
									</div>
								</div> -->

								<!-- <div class="col-sm-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Exportar directamente a excel</label>
                                    </div>
                                </div> -->

							</div>
						</div>

					</div>


					<div class="row">
						<div class="col-sm-6">
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-4 col-form-label">Tipos de movimiento</label>
								<div class="col-sm-8">
									{pyd}
									<div class="custom-control custom-checkbox">
										<input name="id_pyd[]" clave="{cve_pd}" type="checkbox"
											class="custom-control-input id_pyd" value="{id}" id="id_pyd_{id}">
										<label class="custom-control-label" for="id_pyd_{id}">{Clave}
											{Descripcion}</label>
									</div>
									{/pyd}

								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-12">
									<div class="custom-control custom-radio">
										<input checked value="1" onchange="Apps.check_percepcion_deduccion()"
											type="radio" name="tipo_pyd" class="custom-control-input" id="pyd_1">
										<label class="custom-control-label" for="pyd_1">Todos</label>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="custom-control custom-radio">
										<input value="2" onchange="Apps.check_percepcion_deduccion()" type="radio"
											name="tipo_pyd" class="custom-control-input" id="pyd_2">
										<label class="custom-control-label" for="pyd_2">Percepciones</label>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="custom-control custom-radio">
										<input value="3" onchange="Apps.check_percepcion_deduccion()" type="radio"
											name="tipo_pyd" class="custom-control-input" id="pyd_3">
										<label class="custom-control-label" for="pyd_3">Deducciones</label>
									</div>
								</div>

								<!-- <div class="col-sm-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck__4">
                                        <label class="custom-control-label" for="customCheck__4">Per/Ded cálculo</label>
                                    </div>
                                </div> -->

							</div>
						</div>

					</div>
					<!-- <div class="row">
						<div class="col-sm-12">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Exportar a exel</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="" placeholder=""
										value="Reporte_de_la_nomina">
								</div>
							</div>
						</div>
					</div> -->

					<div class="row mt-4">
						<div class="col-sm-6">&nbsp;</div>
						<div class="col-sm-6">
							<button class="btn btn-success" onclick="Apps.buscar();" type="button">Aceptar</button>
						</div>
					</div>

				</form>


			</div>

		</div>
	</div>
</div>

<?php if(isset($periodo)){ ?>
<script>
var periodo = "<?php echo $periodo['id']; ?>";
</script>

<div class="row mt-3 mb-3">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12">
						<h4>Periodo: <?php echo utils::aFecha($periodo['FechaInicio'],true); ?> al <?php echo utils::aFecha($periodo['FechaFin'],true); ?></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="row justify-content-md-center mt-4">

	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">
				<div class="row mb-1">
					<div class="col-md-8"></div>
					<div class="col-md-4 mb-4" align="right">
						<!-- <a class="btn btn-primary"
							href="<?php echo site_url('nomina/inicio/trabajador/alta') ?>">Exportar a excel</a> -->
                            <button class="btn btn-primary" type="button" onclick="Apps.descargar();" >Exportar</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered dataTable" id="listado" width="100%" cellspacing="0">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
