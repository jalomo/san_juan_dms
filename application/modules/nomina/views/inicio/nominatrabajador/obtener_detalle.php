<div class="row">
	<div class="col-sm-12 mb-2">
		<h4>Datos del trabajador</h4>
	</div>

	<?php if(isset($Nombre) && strlen(trim($Nombre))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Nombre:</label>
			<div>
				<input type="" readonly class="form-control-plaintext" value="{Nombre} {Apellido_1} {Apellido_2}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($CURP) && strlen(trim($CURP))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">CURP:</label>
			<div>
				<input type="" readonly class="form-control-plaintext" value="{CURP}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($RFC) && strlen(trim($RFC))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">RFC:</label>
			<div>
				<input type="" readonly class="form-control-plaintext" value="{RFC}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($NumeroImss) && strlen(trim($NumeroImss))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">R. IMSS:</label>
			<div>
				<input type="" readonly class="form-control-plaintext" value="{NumeroImss}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($Regimen) && strlen(trim($Regimen))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Régimen:</label>
			<div>
				<input type="" readonly class="form-control-plaintext" value="{Regimen}">
			</div>
		</div>
	</div>
	<?php } ?>




	<?php if(isset($Registro_Patronal) && strlen(trim($Registro_Patronal))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Registro patronal:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{Registro_Patronal}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($Departamento_Clave) && strlen(trim($Departamento_Clave))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Departamento:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{Departamento_Clave} {Departamento}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($Puesto_Clave) && strlen(trim($Puesto_Clave))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Puesto:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{Puesto_Clave}  {Puesto}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($Clasificacion_Clave) && strlen(trim($Clasificacion_Clave))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Clasificación:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{Clasificacion_Clave} {Clasificacion}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($FechaAlta) && strlen(trim($FechaAlta))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">F. Alta:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="<?php echo utils::aFecha($FechaAlta); ?>">
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if(isset($SalarioDia) && strlen(trim($SalarioDia))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">S. diario:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext"
					value="$<?php echo utils::formatMoney($SalarioDia); ?>">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($NumeroNomina) && strlen(trim($NumeroNomina))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">No. Nómina:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{NumeroNomina}">
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if(isset($EstatusTrabajador) && strlen(trim($EstatusTrabajador))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Estatus del trabajador:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{EstatusTrabajador}">
			</div>
		</div>
	</div>
	<?php } ?>

	<?php if(isset($DiasTrabajados) && strlen(trim($DiasTrabajados))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Días trabajados:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{DiasTrabajados}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($Faltas) && strlen(trim($Faltas))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Faltas:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{Faltas}">
			</div>
		</div>
	</div>
	<?php } ?>


	<?php if(isset($ReciboElectronico) && strlen(trim($ReciboElectronico))>0 ){ ?>
	<div class="col-sm-4">
		<div class="form-group">
			<label class="small">Recibo electrónico:</label>
			<div class="">
				<input type="" readonly class="form-control-plaintext" value="{ReciboElectronico}">
			</div>
		</div>
	</div>
	<?php } ?>



</div>

</form>
