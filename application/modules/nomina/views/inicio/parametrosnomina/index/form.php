<script>
    var  identity = "{identity}";
</script>

<div class="row">
    <div class="col-sm-12">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link {tab_1}" href="{url_1}">Generales</a>
            <a class="nav-item nav-link {tab_2}" href="{url_2}">Cuotas del IMSS</a>
            <a class="nav-item nav-link {tab_3}" href="{url_3}">Retención IMSS</a>
        </nav>
    </div>
    <div class="col-sm-12">
        <div class="mt-5" >
            {content_form}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group row mt-3">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-9">
                <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
            </div>
        </div>
    </div>
</div>