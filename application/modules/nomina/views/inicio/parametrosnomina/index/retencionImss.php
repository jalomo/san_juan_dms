<?php echo form_open('nomina/inicio/parametrosnomina/index', 'class="" id="general_form"'); ?>
<div class="row justify-content-md-center">
    
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title text-dark">Tablas del sistema</h3>
                        {grupo1}
                            <div class="form-group">
                                <label>{label}</label>
                                {input}
                                <small id="msg_{key}" class="form-text text-danger"></small>
                            </div>
                        {/grupo1}
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<?php echo form_close(); ?>