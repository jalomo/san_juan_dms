<?php echo form_open('nomina/inicio/parametrosnomina/index', 'class="" id="general_form"'); ?>

<div class="row justify-content-md-center">
	<div class="col-9">

		<div class="card">
			<div class="card-body">

				<table id="table-1">

					<tbody>
						<tr>
							<td width="250px">
								<h4 class="card-title text-dark">Descripción del seguro</h4>
							</td>
							<td>
								<h4 class="card-title text-dark">Tope (SF)</h4>
							</td>
							<td>
								<h4 class="card-title text-dark">% Patrón</h4>
							</td>
							<td>
								<h4 class="card-title text-dark">% Obrero</h4>
							</td>
						</tr>
						{grupo1}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo1}
						{grupo2}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo2}
						{grupo3}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo3}
						{grupo4}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo4}
						{grupo5}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo5}
						{grupo6}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo6}
						{grupo7}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo7}
						{grupo8}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo8}
						{grupo9}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo9}
						{grupo10}
						<tr>
							<td>{label}</td>
							<td>{input1}</td>
							<td>{input2}</td>
							<td>{input3}</td>
						</tr>
						{/grupo10}

					</tbody>
				</table>


			</div>
		</div>

	</div>

	<style>
		table#table-1>tbody>th>td {
			background-color: #ffffff;
			padding: 10px;
		}

		table#table-1>tbody>tr>td {
			background-color: #ffffff;
			padding: 10px;
		}

	</style>

	<?php echo form_close(); ?>
