<script>
    var contenido2 = [ ['Depatamentos', 'Total'], <?php foreach ($contenido as $key => $value) { echo '["'.$key.'",'.$value.'.],';  } ?>];
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    

    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawStuff2);

    function drawStuff2() {
    var data = new google.visualization.arrayToDataTable(contenido2);


    var options2 = {
        title: 'Trabajadores Activos por departamentos',
        width: 900,
        legend: { position: 'none' },
        chart: { title: 'Trabajadores Activos por departamentos',
                subtitle: '' },
        bars: 'vertical', // Required for Material Bar Charts.
        axes: {
        y: {
            0: { side: 'buttom', label: 'Total de trabajadores'} // Top x-axis.
        }
        },
        bar: { groupWidth: "90%" }
    };

    var chart2 = new google.charts.Bar(document.getElementById('top_x_div_depatamentos'));
    chart2.draw(data, options2);
    };
</script>


<div class="card">
  <div class="card-body">
    <div id="top_x_div_depatamentos" style="width: 900px; height: 500px;"></div>
  </div>
</div>