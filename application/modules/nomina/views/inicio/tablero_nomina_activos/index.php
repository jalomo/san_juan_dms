<br/>
<div class="row">
    <div class="col-sm-12">
        <h3 class="mt-2 mb-3">Trabajadores Activos</h3>
    </div>
</div>
<br/>

<nav class="nav nav-pills nav-justified">
  <a class="nav-item nav-link {Estatus}" href="<?php echo site_url('nomina/inicio/tablero_nomina_activos/tablero_estatus'); ?>">Activos</a>
  <a class="nav-item nav-link {tab2}" href="<?php echo site_url('nomina/inicio/tablero_nomina_activos/tablero_departamentos'); ?>">Departamentos</a>
  <a class="nav-item nav-link {tab3}" href="<?php echo site_url('nomina/inicio/tablero_nomina_activos/tablero_puestos'); ?>">Puestos</a>
  <a class="nav-item nav-link {tab4}" href="<?php echo site_url('nomina/inicio/tablero_nomina_activos/tablero_clasificaciones'); ?>">Clasificaciones</a>
  
</nav>
<br/>
<div class="tab-content" id="myTabContent">
    {contenido}
</div>