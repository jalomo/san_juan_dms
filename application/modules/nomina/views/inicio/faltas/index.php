<script>
    var  id_trabajador = "{id_trabajador}";
</script>

<div class="row mt-4 mb-3">
	<div class="col-sm-12">
		<h3>Faltas</h3>
	</div>
</div>

<div class="card">
	<div class="card-body">

		<form id="general_form">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">Trabajador</label>
					<div class="col-sm-10">
						<select class="custom-select" onchange="Apps.cargarDatos();" id="trabajador" name="trabajador" >
							{trabajadores}
							<option value="{id}" >{Clave} {Nombre} {Apellido_1} {Apellido_2}</option>
							{/trabajadores}
						</select>

					</div>
				</div>
			</div>
			</div>

		</form>

	</div>
</div>


<div class="card mt-4">
	<div class="card-body ">
		<div class="row mb-4">
			<div class="col-md-11">&nbsp;</div>
			<div class="col-md-1">
				<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Agregar</button>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered table-striped" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div>


<div class="mx-auto col-sm-4 mt-4">
	<div class="card">
		<div class="card-body">
		<h4 class="card-subtitle mb-2 text-muted">Ayuda</h4>
			<ul class="list-unstyled mt-3">
				<li class="media">
					<i class="mr-3 fas fa-pencil-alt" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;" >Editar</h5>
					</div>
				</li>
				<li class="media mt-2">
					<i class="mr-3 fas fa-trash" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;">Eliminar</h5>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>