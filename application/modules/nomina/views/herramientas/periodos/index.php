<!-- <div class="card mt-4">
	<div class="card-body ">
		<div class="row mb-4">
			<div class="col-md-10">&nbsp;</div>
			<div class="col-md-2">
				<button onclick="Apps.addContenido();" type="button" class="btn btn-primary">Editar último periodo</button>
			</div>
		</div>
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div> -->


<div class="row">
	<div class="col-sm-12">
		<h3 class="mt-2 mb-3">Periodos de la nomina</h3>
	</div>
</div>

<div class="card">
	<div class="card-body">

		<form id="general_form">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-2 col-form-label">Periodicidad</label>
						<div class="col-sm-10">
							<select class="custom-select" onchange="Apps.cargarDatos();" id="Periodicidad" name="Periodicidad">
								{periodicidad}
								<option value="{id}">[{Clave}] {Descripcion}</option>
								{/periodicidad}
							</select>
						</div>
					</div>

					<div class="row mb-4">
						<div class="col-md-9">&nbsp;</div>
						<div class="col-md-3">
              <button onclick="location.reload();" type="button" class="btn btn-default">Limpiar</button>
							<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Buscar</button>
						</div>
					</div>

				</div>
			</div>

		</form>

	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">
		<div class="row">
			<div class="table-responsive">
				<table class="table table-bordered" id="listado" width="100%" cellspacing="0"></table>
			</div>
		</div>

	</div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">

			<div class="modal-body">
				<h3 class="mb-3">Editar último periodo</h3>
				<form>

					<div class="form-group">
						<label for="exampleFormControlSelect1">Periodicidad</label>
						<select onchange="Apps.recalcular();" class="form-control" id="periodicidad" name="periodicidad">
							{periodicidad}
							<option value="{id}">{Clave} {Descripcion}</option>
							{/periodicidad}
						</select>
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">Fecha Inicio</label>
						<input type="" class="form-control-plaintext" readonly id="fecha_inicio" placeholder="">
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">Fecha Fin</label>
						<input type="" class="form-control-plaintext" readonly id="fecha_fin" placeholder="">
					</div>
				</form>

				<div class="form-group row mt-3">
					<div class="col-sm-3">&nbsp;</div>
					<div class="col-sm-9">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
					</div>
				</div>


			</div>

		</div>
	</div>
</div>
