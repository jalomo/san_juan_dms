@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }

    </style>
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" class="table table-bordered" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No cuenta</th>
                                <th>VIN</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>Tipo cliente</th>
                                <th>Tel oficina</th>
                                <th>Tel casa</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Línea</th>
                                <th>Tipo vehículo</th>
                                <th>Año</th>
                                <th>Vendedor anterior</th>
                                <th>Asignado</th>
                                <th>Email</th>
                                <th>Fecha inicio contrato</th>
                                <th>Fecha fin contrato</th>
                                <th>Intentos</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $clase_erronea = ''; ?>
                            @foreach ($info as $v => $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->no_cuenta }}</td>
                                    <td>{{ $data->vin }}</td>
                                    <td>{{ $data->apellido }}</td>
                                    <td>{{ $data->nombre }}</td>
                                    <td>{{ $data->tipo_cliente }}</td>
                                    <td>{{ str_replace('-','',$data->telefono_oficina) }}</td>
                                    <td>{{ str_replace('-','',$data->telefono_casa) }}</td>
                                    <td>{{ $data->marca }}</td>
                                    <td>{{ $data->modelo }}</td>
                                    <td>{{ $data->linea }}</td>
                                    <td>{{ $data->tipo_vehiculo }}</td>
                                    <td>{{ $data->anio }}</td>
                                    <td>{{ $data->vendedor_asignado }}</td>
                                    <td>{{ $data->nombre_usuario.' '.$data->ap_usuario.' '.$data->am_usuario }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->fecha_inicio_contrato }}</td>
                                    <td>{{ $data->fecha_termino }}</td>
                                    <td>
                                        <input type="hidden" id="intentos_{{ $data->id }}"
                                            value="{{ $data->intentos }}">
                                        <span id="lbl_intentos_{{ $data->id }}">{{ $data->intentos }}</span>
                                    </td>
                                    <td>{{ $data->estatus }}</td>
                                    <!--<td>{{ $data->nombre_cliente . ' ' . $data->ap_cliente . ' ' . $data->am_cliente }}</td>-->

                                    <td>
                                        <a class="js_add_historial" data-id="{{ $data->id }}"
                                            data-estatus_id="{{ $data->estatus_id }}" title="Agregar">
                                            <i class="pe pe-7s-plus"></i>
                                        </a>
                                        <a class="js_historial" data-id="{{ $data->id }}" title="historial">
                                            <i class="pe pe-7s-info"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';
        $("body").on("click", '.js_add_historial', function(e) {
            e.preventDefault();
            aPos = $(this);
            var url = site_url + "/proactivo/agregar_historial/0";
            customModal(url, {
                    "id": $(this).data('id'),
                    "estatus_id": $(this).data('estatus_id'),
                }, "GET", "lg", saveHistorial, "", "Guardar", "Cancelar", "Ingresar comentario",
                "modalComentario");
        });

        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            var url = site_url + "/proactivo/historial_comentarios/";
            customModal(url, {
                "id": id
            }, "POST", "lg", "", "", "", "Cerrar", "Historial de comentarios", "modalHistorialComentarios");
        });

        function saveHistorial() {
            const data = {
                observaciones: $("#observaciones").val(),
                estatus_id: $("#estatus_id").val(),
                fecha: $("#fecha").val(),
                hora: $("#hora").val(),
                user_id: "{{ $this->session->userdata('id') }}",
                cp_id: $("#cp_id").val(),
            }
            if(data.fecha==''||data.hora==''){
                return toastr.error(
                        "Es necesario ingresar todos los datos");
            }
            var url = site_url + "/proactivo/validarHoras";
            ajaxJson(url, {
                fecha: $("#fecha").val(),
                hora: $("#hora").val(),
            }, "POST", "", function(result) {
                if (result == -1) {
                    return toastr.error(
                        "Solamente se pueden programar notificaciones en un horario de 09:00 a 19:00 hrs");
                } else if (result == -2) {
                    return toastr.error("No se pueden programar notificaciones en fechas anteriores");
                } else {
                    ajax.post('api/historial-contacto-proactivo', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            let newIntentos = parseInt($("#intentos_" + data.cp_id).val()) + 1;
                            $("#lbl_intentos_" + data.cp_id).text(newIntentos);
                            $("#intentos_" + data.cp_id).val(newIntentos)
                            var titulo = (headers.status != 200) ? headers.message :
                                "Comentario guardado con éxito";
                            utils.displayWarningDialog("Comentario guardado con éxito", "success", function(
                                result) {
                                $(".modalComentario").modal('hide')
                            })
                        })
                }

            });

        }
        $("body").on('click', '#buscar', function() {
            buscarInformacion();
        });

        function buscarInformacion() {
            var url = site_url + "/financiamientos/lista_ventas/";
            ajaxLoad(url, {
                "fecha": $("#fecha").val(),
            }, "tabla-items", "POST", function() {
                inicializar_tabla_local();
            });
        }

        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }
    </script>
@endsection
