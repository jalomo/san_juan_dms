<table id="tbl" width="100%" class="table table-bordered" cellspacing="0">
    <thead>
        <tr>
            <th>Fecha creación</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Serie</th>
            <th>Cliente</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Línea</th>
            <th>Tipo vehículo</th>
            <th>Año</th>
            <th>Comentario</th>
            <th width="30%">Estatus</th>
            <th>Usuario</th>
        </tr>
    </thead>
    <tbody>
        @if (count($historial) > 0)
            @foreach ($historial as $c => $value)
                <tr>
                    <td>{{ $value->created_at }}</td>
                    <td>{{ $value->fecha }}</td>
                    <td>{{ $value->hora }}</td>
                    <td>{{ $value->vin }}</td>
                    <td>{{ $value->nombre.' '.$value->apellido }}</td>
                    <td>{{ $value->marca }}</td>
                    <td>{{ $value->modelo }}</td>
                    <td>{{ $value->linea }}</td>
                    <td>{{ $value->tipo_vehiculo }}</td>
                    <td>{{ $value->anio }}</td>
                    <td>{{ $value->observaciones }}</td>
                    <td>{{ $value->estatus }}</td>
                    <td>{{ $value->nombre_usuario . ' ' . $value->ap_usuario . ' ', $value->am_usuario }}
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="text-center">
                <td colspan="10">Aún no se han registrado comentarios... </td>
            </tr>
        @endif
    </tbody>
</table>
