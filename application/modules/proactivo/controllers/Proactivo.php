<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Proactivo extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function listado()
    {
        if($this->session->userdata('rol_id')==1){
            $array_user = [];
        }else{
            $array_user = [
                'asignado' => $this->session->userdata('id')
            ];
        }
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/contacto-proactivo-ventas/master/get-all',$array_user));
        $data['titulo'] = "Listado Contacto Proactivo Ventas";
        $this->blade->render('listado', $data);
    }
    public function buscar_proactivo(){
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/contacto-proactivo-ventas/master/get-all', [

        ]));
        $this->blade->render('v_buscar_proactivos', $data);
    }
    public function historial(){
        $data['historial'] =  procesarResponseApiJsonToArray($this->curl->curlPost('api/historial-proactivo-ventas/master/get-historial',[]));
        $data['vendedores'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 14
        ]));
        $data['id_usuario'] = form_dropdown('id_usuario', array_combos($data['vendedores'], 'id', 'nombre', TRUE),'', 'class="form-control busqueda" id="id_usuario"');
        $data['fecha_inicio'] = form_input('fecha_inicio','', 'class="form-control" id="fecha_inicio"', 'date');
        $data['fecha_fin'] = form_input('fecha_fin','', 'class="form-control" id="fecha_fin"', 'date');
        $data['catalogos_estatus'] = procesarResponseApiJsonToArray($this->curl->curlGet('api/estatus-contacto-proactivo'));
        $data['estatus'] = form_dropdown('estatus_id', array_combos($data['catalogos_estatus'], 'id', 'estatus', TRUE),'', 'class="form-control" id="estatus_id"');
        $data['titulo'] = "Historial Contacto Proactivo Ventas";
        $this->blade->render('historial_proactivo', $data);
    }
    public function buscar_historial(){
        $data['historial'] =  procesarResponseApiJsonToArray($this->curl->curlPost('api/historial-proactivo-ventas/master/get-historial',[
            "user_id" => $_POST['id_usuario'],
            "estatus_id" => $_POST['estatus_id'],
            "fecha_inicio" => $_POST['fecha_inicio'],
            "fecha_fin" => $_POST['fecha_fin']
        ]));
        $this->blade->render('v_buscar_historial', $data);
    }
    public function agregar_historial(){
        $data['cp_id'] = $_GET['id'];
        $catalogos_estatus = procesarResponseApiJsonToArray($this->curl->curlGet('api/estatus-contacto-proactivo'));
        $data['estatus'] = form_dropdown('estatus_id', array_combos($catalogos_estatus, 'id', 'estatus', TRUE),$_GET['estatus_id'], 'class="form-control" id="estatus_id"');
        $data['observaciones'] = form_textarea('observaciones','', 'class="form-control" id="observaciones"');
        $data['fecha'] = form_input('fecha','', 'class="form-control" id="fecha"', 'date');
        $data['hora'] = form_input('hora','', 'class="form-control" id="hora"', 'time');

        $this->blade->render('agregar_historial', $data);
    }
    public function historial_comentarios()
    {
        $data['comentarios'] =  procesarResponseApiJsonToArray($this->curl->curlPost('api/historial-proactivo-ventas/master/get-historial',[
            'cp_id' => $_POST['id']
        ]));
        $this->blade->render('historial_comentarios', $data);
    }
    public function validarHoras(){
        $fecha_agendar = $_POST['fecha'].' '.$_POST['hora'];
        if($_POST['hora']<'09:00' || $_POST['hora'] > '19:00'){
            echo -1;
            exit();
        }
        if($fecha_agendar<=date('Y-m-d H:i')){
            echo -2;
            exit();
        }


    }
    
}
