<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gerencia extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }

    public function index()
    {
        $data['titulo'] = "Concentrado ventas";

        $this->blade->render('iframe_concentrado_ventas', $data);
    }
}
