@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : "" }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"></li>
    </ol>
    <div class="row">
        <div class="col-md-12 table-responsive">
            <iframe class="embed-responsive-item" src="<?php echo SERVICIOS_PLAN_MYLSA; ?>" frameborder="0" id="panel_ventanilla" style="height:95vh;width:80vw;"></iframe>
        </div>
    </div>
    @endsection

    @section('scripts')

    @endsection