<form id="frm">
    <input type="hidden" name="ticket" id="ticket" value="{{ $ticket }}">
    <div class="row">
        <div class="col-sm-4">
            <label for="">Evidencia</label>
            <input type="file" class="form-control-file" id="evidencia" name="evidencia">
            <div id="evidencia_error" class="invalid-feedback"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <label for="">Comentario</label>
        <div class="col-sm-12">
            <textarea name="comentario" id="comentario" cols="20" rows="10" class="form-control"></textarea>
        </div>
    </div>
</form>