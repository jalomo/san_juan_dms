@layout('tema_luna/layout')
@section('contenido')
    <style type="text/css">
        .my_alert-danger {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            font-weight: bold;
            padding: 5px !important;
        }

        td a i {
            font-size: 25px !important;
        }

        .visualizar {
            visibility: visible;
        }

        .ocultar {
            display: none;
        }

        label {
            font-weight: bold;
            font-size: 20px;
            width: 100%
        }

        .evidencia {
            width: 200px;
            height: 200px;
        }

    </style>
    <div class="container-fluid panel-body">
        <input type="hidden" value="{{ $ticket_id }}" id="ticket_id" name="ticket_id">
        <input type="hidden" value="{{ $info[0]->estatus_id }}" id="estatus_anterior_id" name="estatus_anterior_id">
        <input type="hidden" value="{{ $info[0]->user_id }}" id="user_id" name="user_id">
        <input type="hidden" value="{{ $info[0]->folio }}" id="folio" name="folio">
        <input type="hidden" value="{{ $info[0]->rol_id }}" id="rol_id" name="rol_id">
        <input type="hidden" value="{{ $info[0]->estatus_ticket }}" id="estatus_ticket" name="estatus_ticket">
        <input type="hidden" value="{{ $usuario_actual }}" id="usuario_actual" name="usuario_actual">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Folio</label>
                {{ $info[0]->folio }}
            </div>
            <div class="col-sm-4">
                <label for="">Fecha</label>
                {{ formatDate($info[0]->created_at) }}
            </div>
            <div class="col-sm-4">
                <label for="">Asunto</label>
                {{ $info[0]->asunto }}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Estatus actual ticket</label>
                {{ $info[0]->estatus_ticket }}
            </div>
            <div class="col-sm-4">
                <label for="">Prioridad</label>
                {{ $info[0]->prioridad }}
            </div>
            <div class="col-sm-4">
                <label for="">Evidencia</label>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Usuario</label>
                {{ $info[0]->nombre_usuario . ' ' . $info[0]->ap_usuario . ' ' . $info[0]->am_usuario }}
            </div>
            <div class="col-sm-4">
                <label for="">Teléfono usuario</label>
                {{ $info[0]->telefono_usuario }}
            </div>
            <div class="col-sm-4">
                <label for="">Correo usuario</label>
                {{ $info[0]->email_usuario }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Rol</label>
                {{ $info[0]->rol }}
            </div>
        </div>
        <hr>
        <h2>Historial</h2>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                        <th>Fecha</th>
                        <th>Comentario</th>
                        <th>Usuario</th>
                        <th>Estatus anterior</th>
                        <th>Estatus nuevo</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Minutos transcurridos</th>
                        <th>Evidencia</th>
                    </thead>
                    <tbody>
                        @foreach ($historial as $h => $data)
                            <tr>
                                <td>{{ formatDate($data->created_at) }}</td>
                                <td>{{ $data->comentario }}</td>
                                <td>{{ $data->usuario }}</td>
                                <td>{{ $data->estatus_anterior }}</td>
                                <td>{{ $data->estatus_nuevo }}</td>
                                <td>{{ $data->inicio_estatus }}</td>
                                <td>{{ $data->fin_estatus }}</td>
                                <td>{{ dateDiffMinutes($data->inicio_estatus, $data->fin_estatus) }}</td>
                                @if ($data->evidencia != '')
                                    <a target="_blank" href="{{ $data->evidencia }}">Ver evidencia</a>
                                @else
                                    -
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @if ($info[0]->rol_id != 3)
            <div class="row pull-right">
                <div class="col-sm-12">
                    <button id="js_evidencia" class="btn btn-info">+ Agregar evidencia</button>
                </div>
            </div>
        @endif
        <br>
        <hr>
        <h2>Evidencia</h2>
        <div class="row">
            <div class="col-sm-4">
                <?php
                $evidencia1 = procesarResponseApiJsonToArray($this->curl->curlGet('api/getimg?url=' . $info[0]->evidencia));
                ?>
                <img class="evidencia" src="data:image/png;base64,{{ $evidencia1 }}" alt="Evidencia" />
            </div>
            @foreach ($evidencia as $h => $e)
                <div class="col-sm-4">
                    <img class="evidencia"
                        src="data:image/png;base64,{{ procesarResponseApiJsonToArray($this->curl->curlGet('api/getimg?url=' . $e->evidencia)) }}"
                        alt="Evidencia" />
                </div>
            @endforeach
        </div>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
@endsection

@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        const saveEvidencia = () => {
            let evidencia = $('#evidencia')[0].files[0];
            let paqueteDeDatos = new FormData();
            paqueteDeDatos.append('ticket_id', $("#ticket").val());
            paqueteDeDatos.append('evidencia', evidencia);
            paqueteDeDatos.append('folio', $("#folio").val());
            paqueteDeDatos.append('usuario', "{{ $this->session->userdata('usuario') }}");
            paqueteDeDatos.append('rol_id', $("#rol_id").val());
            paqueteDeDatos.append('comentario', $("#comentario").val());
            if (!utils.isDefined(evidencia) && !evidencia) {
                return toastr.error("Adjuntar evidencia");
            }
            // $("#guardar").prop('disabled', true);
            ajax.postFile(`api/evidencia-tickets/master/upload`, paqueteDeDatos, function(response, header) {
                if (header.status == 200 || header.status == 204) {
                    utils.displayWarningDialog(
                        "Evidencia guardada correctamente",
                        "success",
                        function(
                            data) {
                            return window.location.reload();

                        })
                }
                if (header.status == 500) {
                    let titulo = "Error subiendo evidencia!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })
            // $("#guardar").prop('disabled', true);
        }
        $("#js_evidencia").on('click', function() {
            var url = site_url + "/soporte/agregar_evidencia";
            customModal(url, {
                    "ticket": $("#ticket_id").val(),
                    "comentario": $("#comentario_id").val(),
                    "folio": $("#folio").val(),
                    "rol_id": $("#rol_id").val(),
                    'usuario': "{{ $this->session->userdata('usuario') }}",
                    'user_id': "{{ $this->session->userdata('id') }}"
                }, "POST", "lg", saveEvidencia, "", "Guardar", "Cancelar", "Guardar evidencia",
                "modalEvidencia");
        })
    </script>
@endsection
