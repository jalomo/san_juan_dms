@layout('tema_luna/layout')
<style>
    .bgwhite {
        background-color: #fff !important
    }

</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <hr>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label>Fecha recepción</label>
                    {{ $fecha_recepcion }}
                </div>
                <div class="col-sm-4">
                    <label>Serie</label>
                    {{ $serie }}
                </div>
                <div class="col-sm-4">
                    <label>Unidad</label>
                    {{ $unidad_descripcion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Modelo</label>
                    {{ $modelo }}
                </div>
                <div class="col-sm-4">
                    <label>Color exterior</label>
                    {{ $color_exterior }}
                </div>
                <div class="col-sm-4">
                    <label>Color interior</label>
                    {{ $color_interior }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label># Económico</label>
                    {{ $economico }}
                </div>
                <div class="col-sm-4">
                    <label>Ubicacion</label>
                    {{ $ubicacion_id }}
                </div>
                <div class="col-sm-4">
                    <label>Ubicacion llaves</label>
                    {{ $ubicacion_llaves_id }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <label>Comentario</label>
                    {{ $comentario }}
                </div>
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary pull-right">Guardar</button>
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script type="text/javascript">
        $('.numeric').numeric();
        $('.positive').numeric();
        $(".busqueda").select2();

        const url_api = "{{ API_URL_DEV }}";
        const parameter_serie = "{{ $parameter_serie }}";
        const id = "{{ $id }}";
        const ultimo_servicio = "{{ $ultimo_servicio }}";
        const buscarRemisionBySerie = (serie) => {
            let url = url_api + 'api/unidades/get-all?serie=' + serie;
            ajaxJson(url, {}, "GET", "", function(result) {
                result = result.data;
                $(".busqueda").select2('destroy');
                if (result.length > 0) {
                    result = result[0];
                    $("#id").val(result.id);
                    $("#unidad_descripcion").val(result.unidad_descripcion);
                    $("#fecha_recepcion").val(result.fecha_recepcion);
                    $("#color_exterior").val(result.color_exterior);
                    $("#color_interior").val(result.color_interior);
                    $("#modelo").val(result.modelo);
                    $("#economico").val(result.economico);
                    $("#ubicacion_id").val(result.ubicacion_id);
                    $("#ubicacion_llaves_id").val(result.ubicacion_llaves_id);
                    $("#comentario").val(result.comentario);
                } else {
                    $("#unidad_descripcion").val('');
                    $("#fecha_recepcion").val('');
                    $("#color_exterior").val('');
                    $("#color_interior").val('');
                    $("#modelo").val('');
                    $("#economico").val('');
                    $("#ubicacion_id").val('');
                    $("#ubicacion_llaves_id").val('');
                    $("#comentario").val('');
                }
                $(".busqueda").select2();
            });
        }
        $("#guardar").on('click', async function() {
            const data = {
                ubicacion_id: $("#ubicacion_id").val(),
                estatus_id: 1,
                ubicacion_llaves_id: $("#ubicacion_llaves_id").val(),
                fecha_recepcion: $("#fecha_recepcion").val(),
                comentario: $("#comentario").val(),
            }
            if(ultimo_servicio!=''){
                data.ultimo_servicio = ultimo_servicio;
            }
            console.log('data',data)
            ajax.put('api/unidades/update-remision/' + $("#id").val(), data,
                function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    var titulo = (headers.status != 200) ? headers.message :
                        "Información actualizada con éxito";
                    utils.displayWarningDialog("Información actualizada con éxito", "success",
                        function(
                            data) {
                            return window.location.href = base_url +
                                'oasis/listado_unidades_recibidas';
                        })
                })
        })
        $("#serie").on('change', () => buscarRemisionBySerie($("#serie").val()))

        // if(id==0){
        //     $("#serie").val(parameter_serie);
        //     buscarRemisionBySerie(parameter_serie);
        // }
    </script>
@endsection
