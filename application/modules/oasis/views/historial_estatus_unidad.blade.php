@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <div class="row">
            <div class="col-sm-12">
                <div id="tabla-items">
                    <table id="tbl" class="table table-bordered" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Fecha</th>
                                <th>Serie</th>
                                <th>Serie corta</th>
                                <th>Estatus anterior</th>
                                <th>Estatus nuevo</th>
                                <th>Comentario</th>
                                <th>Ubicación</th>
                                <th>Ubicación llaves</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $clase_erronea = ''; ?>
                            @foreach ($info as $v => $data)
                                <tr>
                                    <td>{{$data->imagen}}</td>
                                    <td>{{$data->created_at}}</td>
                                    <td>{{$data->serie}}</td>
                                    <td>{{$data->serie_corta}}</td>
                                    <td>{{$data->estatus_anterior}}</td>
                                    <td>{{$data->estatus_nuevo}}</td>
                                    <td>{{$data->comentario}}</td>
                                    <td>{{$data->ubicacion}}</td>
                                    <td>{{$data->ubicacion_llaves}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ base_url('js/custom/bootbox.min.js') }}"></script>
    <script src="{{ base_url('js/custom/general.js') }}"></script>
    <script>
        inicializar_tabla_local();
        var site_url = "{{ site_url() }}";
        var cliente = '';
        var aPos = '';
       
        function inicializar_tabla_local() {
            $('#tbl').DataTable({
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar:"
                },
                "scrollX": true
            });
        }

    </script>
@endsection
