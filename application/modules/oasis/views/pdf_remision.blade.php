<html lang="es">

<head>
    <title>Nota de Venta</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="estilos.css" />
    <link rel="shortcut icon" href="/favicon.ico" />

    <style>
        @page {
            sheet-size: A4;
            size: portrait;
            /* <length>{1,2} | auto | portrait | landscape */
            /* 'em' 'ex' and % are not allowed; length values are width height */
            margin: 5mm;
            /* <any of the usual CSS values for margins> */
            /*(% of page-box width for LR, of height for TB) */
            margin-header: 5mm;
            /* <any of the usual CSS values for margins> */
            margin-footer: 5mm;
            /* <any of the usual CSS values for margins> */
        }

    </style>

    <style>
        .contenedor {
            width: 100%;
            font-size: 9px;
        }

        body {
            font-family: dejavusans;
            font-size: 10pt;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th {
            font-size: 12px;
        }

        td {
            font-size: 10px;
            padding: 2px 0px 0px 0px;
        }

        .td-nota {
            /*font-size: 10px;*/
            text-transform: uppercase;
        }

        .td-espaciado {
            font-size: 11px;
            text-transform: uppercase;
            text-align: center;
            height: 80px;
        }

        .td-espaciado2 {
            font-size: 11px;
            text-transform: uppercase;
            text-align: center;
            height: 40px;
        }

        .contorno {
            border: 1px solid black;
        }

        .contorno2 {
            /*border-top: 1px solid black; */
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .td-punteado {
            border-bottom: 0.5px solid black;
            border-style: dashed;
        }

        .td-titulo {
            font-weight: bold;
        }

        .td-encabezado {
            /*font-weight: bold;*/
            font-size: 9px;
            text-decoration: underline;
        }

        .page_break {
            page-break-before: always;
        }

        .titulo-tabla {
            background-color: #d0d3d4;
            font-size: 12px;
            height: 30px;
            text-align: center;
            border: 1px solid black;
        }

        .titulo-tabla2 {
            background-color: #d0d3d4;
            font-size: 9px;
            font-weight: bold;
            height: 30px;
            text-align: center;
            border: 1px solid black;
        }

        .paginado {
            text-align: right;
            position: fixed;
            bottom: 0;
            right: 0;
            width: 100px;
            font-size: 12px;
            font-weight: bold;
        }

        .contenedor-imprenta {
            margin-top: 5px;
            font-size: 10px;
            font-family: courier;
            padding-left: 8px;
            line-height: 2.5em;
            position: relative;
            width: 100%;
        }

        .col.noborders-topleft {
            border-top: 1px solid white !important;
            border-left: 1px solid white !important;
        }

        .data {
            font-weight: bold;
            font-size: 14px;
        }

    </style>
</head>

<body>
    <div class="contenedor">
        <table>
            <tr>
                <td rowspan="3" style="width: 50%;">
                    <img src="<?= base_url() ?>img/logo.png" style="width:100px;margin-bottom: 5px;">
                </td>
                <td style="width: 20%;" class="td-titulo">
                    FOLIO FISCAL DIGITAL
                </td>
                <td>
                    <?= isset($folio_fiscal) ? $folio_fiscal : 'FEFOM 926020' ?>
                </td>
            </tr>
            <tr>
                <td class="td-titulo">
                    NÚMERO DE DOCUMENTO
                </td>
                <td>
                    <?= isset($num_documento) ? $num_documento : '719282' ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>
                </td>
            </tr>
            <tr>
                <td class="td-titulo">
                    <?= isset($sucursal_emisora) ? $sucursal_emisora : 'FORD MOTOR COMPANY S.A. DE C.V.' ?>
                </td>
                <td class="td-titulo" colspan="2">
                    <?= isset($lugar_fecha_emision) ? $lugar_fecha_emision : '01210 A 2021-08-03T21:09:36' ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    R.F.C. <?= isset($rfc_emisor) ? $rfc_emisor : 'FMO8304236C5' ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Registro Cámara Industrial Transformación No. 509
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    Régimen Fiscal: 601-General de Ley Personas Morales
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td class="td-titulo" style="width: 70%;font-size: 12px;height: 25px;">
                    VENDIDO A:
                </td>
                <td class="td-titulo" style="width: 10%;font-size: 12px;">
                    FECHA
                </td>
                <td class="contorno" align="right">
                    <span class="data">{{ date_eng2esp_1($unidad[0]->fecha_remision) }}</span>
                </td>
            </tr>
        </table>

        <table class="contorno">
            <tr>
                <td style="width: 79%;padding-left: 10px;">
                    <?= isset($sucursal_receptora) ? $sucursal_receptora : 'Mylsa Queretaro, S.A de C.V.' ?>
                </td>
                <td>
                    BID:&nbsp;&nbsp; <?= isset($bid_receptor) ? $bid_receptor : 'M2137' ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 10px;">
                    R.F.C.:&nbsp;&nbsp; <?= isset($rfc_receptor) ? $rfc_receptor : 'MYB020125CM3' ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 10px;">
                    Uso cfdi:&nbsp;&nbsp; <?= isset($cfdi) ? $cfdi : 'G01-Adquisición de mercancias' ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br><br><br><br>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="width: 50%;" class="titulo-tabla">
                    DESCRIPCION DE LA UNIDAD
                </td>
                <td style="width: 30%;" class="titulo-tabla">
                    NUMERO DE SERIE
                </td>
                <td style="width: 20%;" class="titulo-tabla">
                    MODELO
                </td>
            </tr>
            <tr>
                <td class="contorno td-espaciado">
                    <span class="data">{{ $unidad[0]->unidad_descripcion }}</span>
                </td>
                <td class="contorno td-espaciado">
                    <span class="data">{{ $unidad[0]->serie }}</span>
                </td>
                <td class="contorno td-espaciado">
                    <span class="data">{{ $unidad[0]->modelo }}</span>
                </td>
            </tr>
        </table>

        <table style="margin-top: 4px;">
            <tr>
                <td style="width: 8%;" class="titulo-tabla2">
                    CLAVE <br> PROD SERV
                </td>
                <td style="width: 8%;" class="titulo-tabla2">
                    CANTIDAD
                </td>
                <td style="width: 40%;" class="titulo-tabla2">
                    DESCRIPCIÓN
                </td>
                <td style="width: 8%;" class="titulo-tabla2">
                    CLAVE <br> UNIDAD
                </td>
                <td style="width: 8%;" class="titulo-tabla2">
                    UNIDAD DE <br> MEDIDA
                </td>
                <td style="width: 9%;" class="titulo-tabla2">
                    PRECIO <br> UNITARIO
                </td>
                <td style="width: 8%;" class="titulo-tabla2">
                    DESCUENTO
                </td>
                <td style="width: 10%;" class="titulo-tabla2">
                    IMPORTE
                </td>
            </tr>
            <tr>
                <td align="center" class="contorno2">

                    {{ $unidad[0]->clave_vehicular . ' - ' . $tipos_auto[$unidad[0]->tipo_auto] }}
                </td>
                <td align="center" class="contorno2">
                    <?= isset($cantidad) ? number_format($cantidad, 2) : '1.00' ?>
                </td>
                <td align="center" class="contorno2">
                    <span class="data">{{ $unidad[0]->unidad_descripcion }}</span>
                </td>
                <td align="center">
                    <?= isset($clave_unidad) ? $clave_unidad : 'EA-Elemento' ?>
                </td>
                <td align="center">
                    <?= isset($unidad_articulo) ? $unidad_articulo : 'PIEZA' ?>
                </td>
                <td align="center" class="contorno2">
                    $&nbsp;<?= isset($precio_unitario) ? number_format($precio_unitario, 2) : '573,925.00' ?>
                </td>
                <td align="center" class="contorno2">
                    <?= isset($descuento) ? number_format($descuento, 2) : '' ?>
                </td>
                <td align="center" class="contorno2">
                    $&nbsp;<?= isset($importe) ? number_format($importe, 2) : '573,925.00' ?>
                </td>
            </tr>
            <tr>
                <td class="td-punteado contorno2">
                    <br>
                </td>
                <td class="td-punteado contorno2">
                    <br>
                </td>
                <td style="padding-left: 65px;" class="td-punteado contorno2" colspan="5">
                    <b>Base:</b> <?= isset($precio_base) ? number_format($precio_base, 2) : '573,925.00' ?>,&nbsp;&nbsp;
                    <b>Tipo factor:</b> <?= isset($tipo_factor) ? $tipo_factor : 'Tasa' ?>,&nbsp;&nbsp;
                    <b>Tasa o cuota:</b> <?= isset($cuota) ? $cuota : '16.00%' ?>,&nbsp;&nbsp;
                    <b>Impuesto:</b> <?= isset($impuesto) ? $impuesto : '002-IVA' ?>
                </td>
                <td style="border-right: 1px solid black; " align="center" class="td-punteado contorno2">
                    $&nbsp;<?= isset($monto_impuesto) ? number_format($monto_impuesto, 2) : '91,828.00' ?>
                </td>
            </tr>
            <tr>
                <td class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td class="contorno2">
                    <b>Clave vehicular:</b> <span class="data">{{ $unidad[0]->clave_vehicular }}</span>
                </td>
                <td align="center">
                    <br>
                </td>
                <td align="center">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
            </tr>
            <tr>
                <td class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td class="contorno2">
                    <b>Niv:</b> <?= isset($niv) ? $niv : 'AFAHR6MB4MP119034' ?>
                </td>
                <td align="center">
                    <br>
                </td>
                <td align="center">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
                <td align="center" class="contorno2">
                    <br>
                </td>
            </tr>
            <?php if (isset($articulos)): ?>
            <?php for ($i = 0; $i < count($articulos); $i++): ?>
            <tr>
                <td style="font-size: 9px;" align="center" class="contorno2">
                    <?= $articulos[$i]['clave_prov'] ?>
                </td>
                <td style="padding-right: 2px;" align="right" class="contorno2">
                    <?= $articulos[$i]['cantidad'] ?>
                </td>
                <td style="padding-left: 3px;" align="left" class="contorno2">
                    <?= $articulos[$i]['descripcion'] ?>
                </td>
                <td align="left">
                    <?= $articulos[$i]['clave_unidad'] ?>
                </td>
                <td align="center">
                    <?= $articulos[$i]['unidad_medida'] ?>
                </td>
                <td align="right" class="contorno2">
                    <?= $articulos[$i]['precio_unitario'] != '' ? "$&nbsp;" . number_format($articulos[$i]['precio_unitario'], 2) : '' ?>
                </td>
                <td align="right" class="contorno2">
                    <?= $articulos[$i]['descuento'] != '' ? number_format($articulos[$i]['descuento'], 2) : '' ?>
                </td>
                <td align="right" class="contorno2">
                    <?= $articulos[$i]['importe'] != '' ? "$&nbsp;" . number_format($articulos[$i]['importe'], 2) : '' ?>
                </td>
            </tr>
            <?php endfor; ?>
            <?php endif; ?>
            <tr>
                <td class="contorno2">
                    <br>
                </td>
                <td class="contorno2">
                    <br>
                </td>
                <td style="padding-left: 65px;" class="contorno2" colspan="5">
                    <b>Base:</b>
                    <?= isset($precio_base_art) ? number_format($precio_base_art, 2) : '3,950.00' ?>,&nbsp;&nbsp;
                    <b>Tipo factor:</b> <?= isset($tipo_factor) ? $tipo_factor : 'Tasa' ?>,&nbsp;&nbsp;
                    <b>Tasa o cuota:</b> <?= isset($cuota) ? $cuota : '16.00%' ?>,&nbsp;&nbsp;
                    <b>Impuesto:</b> <?= isset($impuesto) ? $impuesto : '002-IVA' ?>
                </td>
                <td style="border-right: 1px solid black; " align="right" class="contorno2">
                    $&nbsp;<?= isset($monto_impuesto_art) ? number_format($monto_impuesto_art, 2) : '632.00' ?>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black;" align="center" class="contorno2">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;" align="center" class="contorno2">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;height: 200px;" class="contorno2">
                    <?= isset($nota_de_venta) ? $nota_de_venta : 'S/D' ?>
                </td>
                <td style="border-bottom: 1px solid black;" align="center">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;" align="center">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;" align="center" class="contorno2">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;" align="center" class="contorno2">
                    <br>
                </td>
                <td style="border-bottom: 1px solid black;" align="center" class="contorno2">
                    <br>
                </td>
            </tr>
        </table>
    </div>

    <div class="paginado">
        1/3
    </div>

    <!-- Hoja 2 -->
    <div class="page_break">
        <div class="contenedor">
            <table>
                <tr>
                    <td rowspan="3" style="width: 50%;">
                        <img src="<?= base_url() ?>img/logo.png" style="width:100px;margin-bottom: 5px;">
                    </td>
                    <td style="width: 20%;" class="td-titulo">
                        FOLIO FISCAL DIGITAL
                    </td>
                    <td>
                        <?= isset($folio_fiscal) ? $folio_fiscal : 'FEFOM 926020' ?>
                    </td>
                </tr>
                <tr>
                    <td class="td-titulo">
                        NÚMERO DE DOCUMENTO
                    </td>
                    <td>
                        <?= isset($num_documento) ? $num_documento : '719282' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="td-titulo">
                        <?= isset($sucursal_emisora) ? $sucursal_emisora : 'FORD MOTOR COMPANY S.A. DE C.V.' ?>
                    </td>
                    <td class="td-titulo" colspan="2">
                        <span class="data">{{ date_eng2esp_1($unidad[0]->created_at) }}</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        R.F.C. <?= isset($rfc_emisor) ? $rfc_emisor : 'FMO8304236C5' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Registro Cámara Industrial Transformación No. 509
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Régimen Fiscal: 601-General de Ley Personas Morales
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="td-titulo" style="width: 70%;font-size: 12px;height: 25px;">
                        VENDIDO A:
                    </td>
                    <td class="td-titulo" style="width: 10%;font-size: 12px;">
                        FECHA
                    </td>
                    <td class="contorno" align="right">
                        <span class="data">{{ date_eng2esp_1($unidad[0]->fecha_remision) }}</span>
                    </td>
                </tr>
            </table>

            <table class="contorno">
                <tr>
                    <td style="width: 79%;padding-left: 10px;">
                        <?= isset($sucursal_receptora) ? $sucursal_receptora : 'Mylsa Queretaro, S.A de C.V.' ?>
                    </td>
                    <td>
                        BID:&nbsp;&nbsp; <?= isset($bid_receptor) ? $bid_receptor : 'M2137' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 10px;">
                        R.F.C.:&nbsp;&nbsp; <?= isset($rfc_receptor) ? $rfc_receptor : 'MYB020125CM3' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 10px;">
                        Uso cfdi:&nbsp;&nbsp; <?= isset($cfdi) ? $cfdi : 'G01-Adquisición de mercancias' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br><br><br><br>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td style="width: 50%;" class="titulo-tabla">
                        DESCRIPCION DE LA UNIDAD
                    </td>
                    <td style="width: 30%;" class="titulo-tabla">
                        NUMERO DE SERIE
                    </td>
                    <td style="width: 20%;" class="titulo-tabla">
                        MODELO
                    </td>
                </tr>
                <tr>
                    <td class="contorno td-espaciado">
                        <span class="data">{{ $unidad[0]->unidad_descripcion }}</span>
                    </td>
                    <td class="contorno td-espaciado">
                        <?= isset($vin) ? $vin : 'AFAHR6MB4MP119034' ?>
                    </td>
                    <td class="contorno td-espaciado">
                        <span class="data">{{ $unidad[0]->modelo }}</span>
                    </td>
                </tr>
            </table>

            <table style="margin-top: 4px;">
                <tr>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CLAVE <br> PROD SERV
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CANTIDAD
                    </td>
                    <td style="width: 40%;" class="titulo-tabla2">
                        DESCRIPCIÓN
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CLAVE <br> UNIDAD
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        UNIDAD DE <br> MEDIDA
                    </td>
                    <td style="width: 9%;" class="titulo-tabla2">
                        PRECIO <br> UNITARIO
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        DESCUENTO
                    </td>
                    <td style="width: 10%;" class="titulo-tabla2">
                        IMPORTE
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid black;" colspan="5">
                        <?= isset($clave_prov_servicio) ? $clave_prov_servicio : '99-Por definir' ?>
                    </td>
                    <td style="padding-right: 3px;" align="right" colspan="2">
                        SUBTOTAL
                    </td>
                    <td align="center" class="contorno">
                        $&nbsp;<?= isset($subtotal) ? number_format($subtotal, 2) : '582,519.00' ?>
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid black;" colspan="5">
                        <br>
                    </td>
                    <td style="padding-right: 3px;" align="right" colspan="2">
                        <?= isset($impuesto) ? $impuesto . ' ' . $cuota : '002-IVA 16.00 %' ?>
                    </td>
                    <td align="center" class="contorno">
                        $&nbsp;<?= isset($impuesto_subtotal) ? number_format($impuesto_subtotal, 2) : '92,460.00' ?>
                    </td>
                </tr>
                <tr>
                    <td style="border-left: 1px solid black;border-bottom: 1px solid black; font-size: 11px;"
                        colspan="5">
                        <b>Importe con letra:</b>
                        <?= isset($importe_letra) ? $importe_letra : 'SEISCIENTOS SETENTA Y CUATRO MIL NOVECIENTOS SETENTA Y NUEVE PESOS 00/100  MXN' ?>
                    </td>
                    <td style="border-bottom: 1px solid black;" align="right" colspan="2">
                        <br>
                    </td>
                    <td align="center" class="contorno">
                        <br>
                    </td>
                </tr>
            </table>

            <div class="contenedor-imprenta">
                CLAVE VEHICULAR: &nbsp;&nbsp;&nbsp;&nbsp;
                <span>{{ $unidad[0]->clave_vehicular }}</span><br>
                COLOR EXTERIOR: &nbsp;&nbsp;&nbsp;&nbsp;
                <span>{{ $unidad[0]->color_exterior }}</span><br>
                COLOR INTERIOR: &nbsp;&nbsp;&nbsp;&nbsp;
                <span>{{ $unidad[0]->color_interior }}</span><br>
                FINANCIERA: &nbsp;&nbsp;&nbsp;&nbsp;
                FORD CREDIT {{ $unidad[0]->fp}}<br>

                EL VENDEDOR DE LA UNIDAD FUE: &nbsp;&nbsp;&nbsp;&nbsp;
                <span>{{ $unidad[0]->vendedor }}</span><br>

                <br>
                DONATIVO POR CUENTA DEL C.C.P. Y A.M.D.F.: &nbsp;&nbsp;
                <?= isset($donativo_cuenta) ? number_format($donativo_cuenta, 2) : '630.00' ?><br>
                CUOTA PUB./ HOLDBACK POR CUENTA DE LA A.M.D.F.: &nbsp;&nbsp;
                <?= isset($cuota_publica) ? number_format($cuota_publica, 2) : '4,014.00' ?><br>
                SERV ADMON LOGISTICA POR CUENTA SERVICIOS LOGISTICOS AMD :&nbsp;&nbsp;
                <?= isset($serv_logistica) ? number_format($serv_logistica, 2) : ': 3,950.00' ?>
            </div>
        </div>
    </div>

    <div class="paginado">
        2/3
    </div>

    <!-- Hoja 3 -->
    <div class="page_break">
        <div class="contenedor">
            <table>
                <tr>
                    <td rowspan="3" style="width: 50%;">
                        <img src="<?= base_url() ?>img/logo.png" style="width:100px;margin-bottom: 5px;">
                    </td>
                    <td style="width: 20%;" class="td-titulo">
                        FOLIO FISCAL DIGITAL
                    </td>
                    <td>
                        <?= isset($folio_fiscal) ? $folio_fiscal : 'FEFOM 926020' ?>
                    </td>
                </tr>
                <tr>
                    <td class="td-titulo">
                        NÚMERO DE DOCUMENTO
                    </td>
                    <td>
                        <?= isset($num_documento) ? $num_documento : '719282' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="td-titulo">
                        <?= isset($sucursal_emisora) ? $sucursal_emisora : 'FORD MOTOR COMPANY S.A. DE C.V.' ?>
                    </td>
                    <td class="td-titulo" colspan="2">
                        <?= isset($lugar_fecha_emision) ? $lugar_fecha_emision : '01210 A 2021-08-03T21:09:36' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        R.F.C. <?= isset($rfc_emisor) ? $rfc_emisor : 'FMO8304236C5' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Registro Cámara Industrial Transformación No. 509
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Régimen Fiscal: 601-General de Ley Personas Morales
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td class="td-titulo" style="width: 70%;font-size: 12px;height: 25px;">
                        VENDIDO A:
                    </td>
                    <td class="td-titulo" style="width: 10%;font-size: 12px;">
                        FECHA
                    </td>
                    <td class="contorno" align="right">
                        <span class="data">{{ date_eng2esp_1($unidad[0]->fecha_remision) }}</span>
                    </td>
                </tr>
            </table>

            <table class="contorno">
                <tr>
                    <td style="width: 79%;padding-left: 10px;">
                        <?= isset($sucursal_receptora) ? $sucursal_receptora : 'Mylsa Queretaro, S.A de C.V.' ?>
                    </td>
                    <td>
                        BID:&nbsp;&nbsp; <?= isset($bid_receptor) ? $bid_receptor : 'M2137' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 10px;">
                        R.F.C.:&nbsp;&nbsp; <?= isset($rfc_receptor) ? $rfc_receptor : 'MYB020125CM3' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 10px;">
                        Uso cfdi:&nbsp;&nbsp; <?= isset($cfdi) ? $cfdi : 'G01-Adquisición de mercancias' ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br><br><br><br>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td style="width: 50%;" class="titulo-tabla">
                        DESCRIPCION DE LA UNIDAD
                    </td>
                    <td style="width: 30%;" class="titulo-tabla">
                        NUMERO DE SERIE
                    </td>
                    <td style="width: 20%;" class="titulo-tabla">
                        MODELO
                    </td>
                </tr>
                <tr>
                    <td class="contorno td-espaciado">
                        <span class="data">{{ $unidad[0]->unidad_descripcion }}</span>
                    </td>
                    <td class="contorno td-espaciado">
                        <span class="data">{{$unidad[0]->serie }}</span>
                    </td>
                    <td class="contorno td-espaciado">
                        <span class="data">{{ $unidad[0]->modelo }}</span>
                    </td>
                </tr>
            </table>

            <table style="margin-top: 4px;">
                <tr>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CLAVE <br> PROD SERV
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CANTIDAD
                    </td>
                    <td style="width: 40%;" class="titulo-tabla2">
                        DESCRIPCIÓN
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        CLAVE <br> UNIDAD
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        UNIDAD DE <br> MEDIDA
                    </td>
                    <td style="width: 9%;" class="titulo-tabla2">
                        PRECIO <br> UNITARIO
                    </td>
                    <td style="width: 8%;" class="titulo-tabla2">
                        DESCUENTO
                    </td>
                    <td style="width: 10%;" class="titulo-tabla2">
                        IMPORTE
                    </td>
                </tr>
                <tr>
                    <td style="height: 400px;" colspan="8">
                        <br>
                    </td>
                </tr>
            </table>

            <table style="margin-top: 4px;">
                <tr>
                    <td>
                        <br>
                    </td>
                    <td class="contorno td-espaciado2">
                        <span class="data">{{$unidad[0]->serie }}</span>
                    </td>
                    <td class="contorno td-espaciado2">
                        <span class="data">{{$unidad[0]->linea }}</span>
                    </td>
                    <td class="contorno td-espaciado2">
                        <?= isset($n_pedido) ? $n_pedido : 'F014' ?>
                    </td>
                    <td class="contorno td-espaciado2">
                        <span class="data">{{$unidad[0]->fp }}</span>
                    </td>
                    <td class="contorno td-espaciado2">
                        $&nbsp;<?= isset($importe_total) ? number_format($importe_total, 2) : '674,979.00' ?>
                    </td>
                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width: 8%;">
                        <br>
                    </td>
                    <td style="width: 22%;" class="titulo-tabla2">
                        NUMERO DE SERIE
                    </td>
                    <td style="width: 10%;" class="titulo-tabla2">
                        LINEA
                    </td>
                    <td style="width: 10%;" class="titulo-tabla2">
                        PEDIDO
                    </td>
                    <td style="width: 10%;" class="titulo-tabla2">
                        COND. PAGO
                    </td>
                    <td style="width: 22%;" class="titulo-tabla2">
                        IMPORTE TOTAL
                    </td>
                    <td style="width: 8%;">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br>
                    </td>
                    <td class="td-titulo" colspan="5" align="center">
                        Este documento es una representación de un CFDI
                    </td>
                    <td>
                        <br>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="paginado">
        3/3
    </div>
</body>

</html>
