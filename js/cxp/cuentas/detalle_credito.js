function inicializaTabla() {
	$('#tbl_abonos').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		order: 2,
		"ajax": {
			url: PATH_API + "api/abonos-por-pagar/listado-abonos-by-orden-entrada?orden_entrada_id=" + cuenta_por_pagar_id,
			type: 'GET',
		},
		columns: [
			{
				title: "Num pago",
				render: function (data, type, row) {
					if(row.tipo_abono_id == 2) {
						let pagonum = count++;
						return pagonum + '/' + cantidad_mes;
					} else {
						return '-';
					}
				}
			},
			{
				title: "Tipo abono",
				data:"tipo_abono"
			},	
			{
				title: "Fecha vencimiento",
				render: function (data, type, row) {
					return obtenerFechaMostrar(row.fecha_vencimiento);
				}
			},
			{
				title: "Monto a abonar",
				render: function (data, type, row) {
					return parseFloat(row.total_abono).toFixed(2).toLocaleString();
				}
			},
			{
				title: "Fecha pago",
				render: function (data, type, row) {
					return utils.isDefined(row.fecha_pago) ? obtenerFechaMostrar(row.fecha_pago) : null;
				}
			},	
			{
				title: "Monto pagado",
				render: function (data, type, row) {
					return utils.isDefined(row.total_pago) ? parseFloat(row.total_pago).toFixed(2).toLocaleString() : null;
				}
			},
			{
				title: "Estatus abono",
				data:"estatus_abono"
			}
		],
		"createdRow": function( row, data, dataIndex ) {
            switch (data['estatus_abono_id']) {
                case 2:
                    $(row).find('td:eq(6)').css('background-color', '#f6ffa4');
                    break;
                case 3:
                    $(row).find('td:eq(6)').css('background-color', '#8cdd8c');
                    break;
                default:
                    break;
            }  
        }
	});
}

function obtenerFechaMostrar(fecha) {
	const dia = 2,
		mes = 1,
		anio = 0;
	fecha = fecha.split('T');
	fecha = fecha[0].split('-');
	return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}


this.inicializaTabla();
$("#proveedor_id").select2();
