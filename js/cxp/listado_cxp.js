function inicializaTabla() {
    let total_pagar = 0;
    let tot = 0;
    var params = $.param({
        'tipo_forma_pago_id': 2,
        'estatus_cuenta_id': 1,
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val()
    });
    $('#tbl_cxp').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        order: 0,
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            total_monto_abonar = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    let value_a = intVal(a) ? intVal(a) : 0;
                    let value_b = intVal(b) ? intVal(b) : 0;
                    let suma = value_a + value_b;
                    return suma;
                }, 0);
            $("#total_monto_abonar").html('$' + total_monto_abonar.toFixed(2));
            total_monto_pagado = api
                .column(8)
                .data()
                .reduce(function(a, b) {
                    let value_a = intVal(a) ? intVal(a) : 0;
                    let value_b = intVal(b) ? intVal(b) : 0;
                    let suma = value_a + value_b;
                    return suma;
                }, 0);
            $("#total_monto_pagado").html('$' + total_monto_pagado.toFixed(2));
        },
        "ajax": {
            url: PATH_API + "api/cuentas-por-pagar/getkardexpagos?" + params,
            type: 'GET',
        },
        columns: [{
                title: "order",
                visible: false,
                render: function(data, type, row) {
                    if (row.fecha_vencimiento) {
                        return obtenerFechaMostrarUSA(row.fecha_vencimiento)
                    } else {
                        return '--';
                    }
                }
            },
            {
                title: "Folio",
                data: 'folio',
            },
            {
                title: "Concepto",
                data: 'concepto',
            },
            {
                title: "Proveedor",
                render: function(data, type, row) {
                    return row.numero_proveedor + ' - ' + row.proveedor;
                }
            },
            {
                title: "Tipo pago",
                //data: 'tipo_pago',
                render: function(data, type, row) {
                    if (row.tipo_pago) {
                        return row.tipo_pago;
                    } else {
                        return '--';
                    }
                }
            },
            {
                title: "Fecha vencimiento",
                render: function(data, type, row) {
                    if (row.fecha_vencimiento) {
                        return obtenerFechaMostrar(row.fecha_vencimiento)
                    } else {
                        return '--';
                    }
                }
            },

            {
                title: "Monto abono",
                data: 'total_abono',
            },
            {
                title: "Fecha pago",
                render: function(data, type, row) {
                    if (row.fecha_pago) {
                        return obtenerFechaMostrar(row.fecha_pago)
                    } else {
                        return '--';
                    }
                }
            },
            {
                title: "Monto pago",
                data: 'total_pago',
            },
            {
                title: "Estatus actual",
                data: 'estatus_abono',
            },
        ],
        "createdRow": function( row, data, dataIndex ) {
            switch (data['estatus_abono_id']) {
                case 2:
                    $(row).find('td:eq(8)').css('background-color', '#f6ffa4');
                    break;
                case 3:
                    $(row).find('td:eq(8)').css('background-color', '#8cdd8c');
                    break;
                default:
                    break;
            }  
        },
    });
}


function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function obtenerFechaMostrarUSA(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[anio] + '/' + fecha[mes] + '/' + fecha[dia];
}

function filtrar() {
    var params = $.param({
        'tipo_forma_pago_id': 2, //forma de pago credito
        'estatus_cuenta_id': 1,
        'folio': $("#folio").val(),
        'proveedor_id': $("#proveedor_id option:selected").val(),
        'estatus_abono_id': $("#estatus_abono_id option:selected").val(),
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val()
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar/getkardexpagos?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#fecha_inicio").val('');
    $("#fecha_fin").val('');
    $("#proveedor_id").val('');
    $("#proveedor_id").trigger('change');
    $("#estatus_abono_id").val('');
    $("#estatus_abono_id").trigger('change');
    var params = $.param({
        'tipo_forma_pago_id': 2, //forma de pago credito
        'folio': '',
        'proveedor_id': '',
        'estatus_abono_id': '',
        'estatus_cuenta_id': 1,
        'fecha_inicio': '',
        'fecha_fin': ''
    });

    $('#tbl_cxp').DataTable().ajax.url(PATH_API + 'api/cuentas-por-pagar/getkardexpagos?' + params).load()
}

this.inicializaTabla();
$("#proveedor_id").select2();