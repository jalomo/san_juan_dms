var _ajax = function() {
    this.responseDefault = {
            header: {
                url: null,
                status: 200,
                message: null,
                contentType: null,
                successfull: false
            },
            data: null
        },
        this.headerSend = function(params) {
            var config = {
                method: null,
                headers: utils.header()
            };
            return utils.objCombine(config, params);
        },
        this.statusSuccess = [200, 201, 204],
        this.fetch = function(_url, header, _callback, _showLoader, _showMessages) {
            var $this = this;
            var resp = this.responseDefault;
            fetch(API_URL + _url, header)
                .then(function(response) {
                    var contentType = response.headers.get("content-type");
                    resp.header.contentType = contentType;
                    resp.header.status = response.status;
                    var messageTemp = utf8.decode(response.headers.get("X-Message"));
                    try {
                        if (utils.string.isJson(messageTemp)) {
                            resp.header.message = (messageTemp.length) ? messageTemp : '';

                        } else {
                            resp.header.message = (messageTemp.length) ? $.trim(messageTemp.split('"').join('')) : '';
                        }
                    } catch (error) {
                        resp.header.message = (messageTemp.length) ? $.trim(messageTemp.split('"').join('')) : '';
                    }
                    resp.header.url = _url;
                    return (contentType && contentType.includes("application/json")) ? response.json() : {};
                }).then(function(data) {
                    resp.data = data;
                }).catch(function(error) {
                    resp.header.status = '500';
                    resp.header.message = error.toString();

                }).finally(function() {
                    $this.responsefn(resp, _callback, _showLoader, _showMessages);
                });
        },
        this.responsefn = function(response, _callback, _showLoader, _showMessages) {
            if (_showLoader != false) {
                utils.Loader.closed();
            }

            response.header.successfull = (this.statusSuccess.indexOf(response.header.status) != '-1');
            if (this.statusSuccess.indexOf(response.header.status) == '-1') {
                if (_showMessages != false) {
                    this.showValidations(response.header);
                }
                response.data = null;
            } else {
                if (_showMessages != false) {
                    console.log(response.header.message);

                }
            }

            utils.executeFn(_callback, response.data, response.header);
        },

        this.buildFetch = function(_url, headerfn, _callback, _showMessages, _showLoader) {
            _callback = (typeof _callback !== 'undefined') ? _callback : null;
            _showMessages = (typeof _showMessages !== 'undefined') ? _showMessages : true;
            _showLoader = (typeof _showLoader !== 'undefined') ? _showLoader : true;

            if (_showLoader != false) {
                utils.Loader.open();
            }
            this.fetch(_url, headerfn, _callback, _showLoader, _showMessages);
        },

        this.get = function(_url, _params, _callback, _showMessages, _showLoader) {
            _params = (typeof _params !== 'undefined' && utils.objSize(_params) > 0) ? '?' + $.param(_params) : '';
            var headerfn = this.headerSend({
                method: 'GET'
            });
            this.buildFetch(_url + _params, headerfn, _callback, _showMessages, _showLoader)
        },
        this.post = function(_url, _params, _callback, _showMessages, _showLoader) {
            _params = (typeof _params !== 'undefined') ? _params : null;
            var headerfn = this.headerSend({
                method: 'POST',
                body: JSON.stringify(_params)
            });
            this.fetch(_url, headerfn, _callback, _showMessages, _showLoader);
        },

        this.postFile = function(_url, _params, _callback, _showMessages, _showLoader) {
            console.log(_params)
            _params = (typeof _params !== 'undefined') ? _params : null;
            var headerfn = this.headerSend({
                headers: {},
                method: 'POST',
                body: _params
            });
            this.fetch(_url, headerfn, _callback, _showMessages, _showLoader);
        },

        this.delete = function(_url, _params, _callback, _showMessages, _showLoader) {
            _params = (typeof _params !== 'undefined' && utils.objSize(_params) > 0) ? '?' + $.param(_params) : '';
            var headerfn = this.headerSend({
                method: 'DELETE',
            });
            this.fetch(_url + _params, headerfn, _callback, _showMessages, _showLoader);
        },

        this.put = function(_url, _params, _callback, _showMessages, _showLoader) {
            _params = (typeof _params !== 'undefined') ? _params : null;
            var headerfn = this.headerSend({
                method: 'PUT',
                body: JSON.stringify(_params)
            });
            this.fetch(_url, headerfn, _callback, _showMessages, _showLoader);
        },
        this.showError = function(key, value) {
            if ($('small#' + key + '_Help').length == 0) {
                $('[name=' + key + ']').after('<small id="' + key + '_Help" class="text-danger ajaxError"></small>');
            }
            if ($('small#' + key + '_Help').is(':visible') == false) {
                $('small#' + key + '_Help').show();
            }
            $('small#' + key + '_Help').html(value);
            $('[name=' + key + ']').parent().addClass('warning-input');
        },
        this.showValidations_ = function(header) {
            if (utils.string.isJson(header.message)) {
                console.log(header.message);
                $('.warning-input').each(function(index) {
                    $(this).children('.validation_error').remove();
                    $(this).removeClass('warning-input');
                });

                var header_mensages = (typeof header.message != 'string') ? utils.string.toOBJ(header.message) : header.message;
                if (typeof header_mensages != 'string' && typeof header.message != 'string') {
                    return this.buildValidations(header_mensages);
                }
            }
        }
    this.showValidations = function(header) {
        $('.warning-input').each(function(index) {
            $(this).children('.validation_error').remove();
            $(this).removeClass('warning-input');
            $(".text-danger").html("");
            $(".select2-selection").removeAttr('style');
        });
        var header_mensages = JSON.parse(header.message);
        return this.buildValidations(header_mensages);
    }

    this.buildValidations = function(error) {
        toastr.clear()
        $.each(error, function(indexInArray, valueOfElement) {
            var tagname = $('[name="' + indexInArray + '"]').prop("tagName");
            if (tagname == "SELECT") {
                $('[name="' + indexInArray + '"]').parent().eq(0).addClass('warning-input');
            } else {
                $('[name="' + indexInArray + '"]').parent().eq(0).addClass('warning-input');
            }
            var elemento = '';
            if (valueOfElement.length > 0) {
                if (tagname == "SELECT") {
                    $('[name="' + indexInArray + '"]').after("<div class='validation_error'><small class='form-text text-danger'>" + valueOfElement + "</small></div>");
                } else {
                    $('[name="' + indexInArray + '"]').parent().after("<div class='validation_error'><small class='form-text text-danger'>" + valueOfElement + "</small></div>");
                }
            }
            toastr["error"](valueOfElement);

        });

    }

    this.destruir = function() {
        $('.warning-input').each(function(index) {
            $(this).children('.validation_error').remove();
            $(this).removeClass('warning-input');
            $(".text-danger").html("");
            $(".select2-selection").removeAttr('style');
        });
    }
};

var _utils = function() {
    this.objSize = function(obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        }, this.logout = function() {
            localStorage.clear();
            window.location.href = API_URL + '/logout';
        },
        this.ObjToArray = function(obj) {
            return Object.keys(obj).map(function(key) {
                return [Number(key), obj[key]];
            });
        }
    this.isFunction = function(_callback) {
            return (typeof _callback == 'function');
        }
        //verificar formulas
    this.calcularPrecio = function(descuento, precio_producto, cantidad) {
        let des = descuento / 100;
        let precio_total = (100 * des) + parseInt(precio_producto);
        return precio_total * parseInt(cantidad);
    }

    this.showMessageStringToJson = function(header_response) {
        var error = JSON.parse(header_response.message);
        $.each(error, function(input_id, mensaje) {
            return message.error(mensaje[0]);
        });
    }
    this.showMessageStringToJsonArray = function(header_response) {
        var error = JSON.parse(header_response.message);
        $.each(error, function(indexInArray, valueOfElement) {
            var tagname = $('[name="' + indexInArray + '"]').prop("tagName");

            if (tagname == "SELECT") {
                $('[name="' + indexInArray + '"]').parent().eq(0).addClass('warning-input');
                $('[name="' + indexInArray + '"]').next().find('.select2-selection').css('border-color', '#d9534f');
            } else {
                $('[name="' + indexInArray + '"]').parent().eq(0).addClass('warning-input');
            }
            var elemento = '';
            if (valueOfElement.length > 0) {
                $.each(valueOfElement, function(index, msg) {
                    elemento += "<small class='form-text text-danger'>" + msg + "</small>";
                });
                if (tagname == "SELECT") {
                    $('[name="' + indexInArray + '"]').parent().after("<div class='validation_error'>" + elemento + "</div>");
                } else {
                    $('[name="' + indexInArray + '"]').after("<div class='validation_error'>" + elemento + "</div>");
                }
            } else {
                if (tagname == "SELECT") {
                    $('[name="' + indexInArray + '"]').after("<div class='validation_error'><small class='form-text text-danger'>" + valueOfElement + "</small></div>");
                } else {
                    $('[name="' + indexInArray + '"]').parent().after("<div class='validation_error'><small class='form-text text-danger'>" + valueOfElement + "</small></div>");

                }
            }
        });
    }
    this.executeFn = function(_callback, param1, param2, param3) {
        param1 = (typeof param1 !== 'undefined') ? param1 : '';
        param2 = (typeof param2 !== 'undefined') ? param2 : '';
        param3 = (typeof param3 !== 'undefined') ? param3 : '';

        if (this.isFunction(_callback)) {
            _callback(param1, param2, param3);
            return true;
        }
        return false;
    }
    this.objCombine = function(obj, src) {
            for (var key in src) {
                if (src.hasOwnProperty(key)) obj[key] = src[key];
            }
            return obj;
        },
        this.getId = function() {
            var number = Math.random();
            number.toString(36);
            return number.toString(36).substr(2, 9);
        },
        this.string = {
            empty: function(value) {
                return (!value || value == undefined || value == "" || value.length == 0) ? '' : value;
            },
            firstUpperCase: function(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            },
            replace: function(replaceString, find, replace) {
                replaceString = $.trim(replaceString);
                for (var i = 0; i < find.length; i++) {
                    replaceString = replaceString.replace(find[i], replace[i]);
                }
                return replaceString;
            },
            toOBJ: function(cadena) {
                var obj = {};
                try {
                    obj = JSON.parse(cadena);
                } catch (error) {
                    obj = $.parseJSON(cadena);
                }
                return obj;
            },
            isJson: function(element) {
                element = typeof element !== "string" ? JSON.stringify(element) : element;
                try {
                    element = JSON.parse(element);
                } catch (e) {
                    return false;
                }
                if (typeof element === "object" && element !== null) {
                    return true;
                }
                return false;
            }
        },
        this.dateToLetras = function(input_fecha = null) {
            let input_date = new Date(input_fecha);
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            return input_date.toLocaleDateString("es-ES", options);
        },
        this.isDefined = function(value) {
            var undefined = void(0);
            return value !== undefined && value != null;
        },
        this.header = function() {
            var userData = JSON.parse(localStorage.getItem('userData'));
            var _return;
            try {
                _return = {
                    'Accept': 'application/json',
                    // 'Authorization': 'Bearer ' + userData.access_token,
                    'Content-Type': 'application/json'
                };
            } catch (error) {
                _return = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                };
            }
            return _return;
        },
        this.makeId = function() {
            var text = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        },
        this.validaCurp = function(curp) {
            if (curp != '') {
                var str = curp;
                var regex = /^([a-zA-ZñÑ])([aeiouAEIOU])([a-zA-Z]{2})(\d{2})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])([hmHM])([aA][sS]|([bB]([cC]|[sS]))|([cC]([cc]|[sS]|[hH]|[lL]|[mM]))|([dD]([fF][gG]))|([gG]([tT]|[rR]))|[hH][gG]|[jJ][cC]|([mM]([cC]|[nN]|[sS]))|([nN]([tT]|[lL]))|[oO][cC]|[pP][lL]|([qQ]([tT]|[rR]))|([sS]([pP]|[lL]|[rR]))|([tT]([cC]|[sS]|[lL]))|[vV][zZ]|[yY][nN]|[zZ][sS]|[nN][eE])([a-zA-Z]{3})([a-zA-Z]\d|\d{2})$/
                return str.match(regex);
            }
            return false;
        },
        this.validaRfc = function(rfc) {
            if (rfc != '') {
                var str = rfc;
                var regex = /^[A-Za-z]{4}[ |\-]{0,1}[0-9]{6}[ |\-]{0,1}[0-9A-Za-z]{3}$/
                return str.match(regex);
            }
            return false;
        },
        this.obtenerEdad = function(fecha) {
            var hoy = new Date();
            var cumple_anos = new Date(fecha);
            var edad = hoy.getFullYear() - cumple_anos.getFullYear();
            var mes = hoy.getMonth() - cumple_anos.getMonth();
            if (mes < 0 || (mes === 0 && hoy.getDate() < cumple_anos.getDate())) {
                edad--;
            }
            return edad;
        }, this.serializeForm = function(form_id) {
            return $('#' + form_id).serialize(form);
        },
        this.Loader = {
            open: function(_msg) {
                _msg = (typeof _msg !== 'undefined') ? _msg : '';
                $('#page-loader h1').html(_msg);
                $('#page-loader').show();
            },
            closed: function() {
                $('#page-loader').hide();
            }
        },
        this.detectIE = function() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }
            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }
            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }
            return false;
        },
        this.displayWarningDialog = function(titulo, tipo, callback, showCancelButton) {
            Swal.fire({
                title: utils.isDefined(titulo) ? titulo : '¿Estás seguro?',
                icon: utils.isDefined(tipo) ? tipo : 'warning',
                showCloseButton: true,
                showCancelButton: utils.isDefined(showCancelButton) ? showCancelButton : false,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cerrar',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-outline-secondary'
            }).then(callback);
        },
        this.button = function($btn, loading) {
            if (utils.isDefined($btn) && utils.isDefined(loading)) {
                var $this = $($btn);
                var original_text = $($this).html();
                if (loading) {
                    var loadingText = '<i class="fa fa-spinner fa-spin fa-fw"></i>' + loading;
                    if ($($this).html() !== loadingText) {
                        $this.html(loadingText);
                        $this.attr('disabled', true);
                    }
                    setTimeout(function() {
                        $this.html(original_text);
                        $this.attr('disabled', false);
                    }, 2000);
                }
            }
        }
};

var utf8 = new function() {
    this.encode = function(strData) {
            var tmpArr = []
            var i = 0
            var c1 = 0
            var seqlen = 0
            strData += ''
            while (i < strData.length) {
                c1 = strData.charCodeAt(i) & 0xFF
                seqlen = 0

                if (c1 <= 0xBF) {
                    c1 = (c1 & 0x7F);
                    seqlen = 1;
                } else if (c1 <= 0xDF) {
                    c1 = (c1 & 0x1F);
                    seqlen = 2;
                } else if (c1 <= 0xEF) {
                    c1 = (c1 & 0x0F);
                    seqlen = 3;
                } else {
                    c1 = (c1 & 0x07);
                    seqlen = 4;
                }

                for (var ai = 1; ai < seqlen; ++ai) {
                    c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
                }

                if (seqlen === 4) {
                    c1 -= 0x10000;
                    tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)));
                    tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)));
                } else {
                    tmpArr.push(String.fromCharCode(c1));
                }
                i += seqlen;
            }
            return tmpArr.join('');
        },
        this.decode = function(str_data) {
            var tmp_arr = [],
                i = 0,
                ac = 0,
                c1 = 0,
                c2 = 0,
                c3 = 0,
                c4 = 0;

            str_data += '';
            while (i < str_data.length) {
                c1 = str_data.charCodeAt(i);
                if (c1 <= 191) {
                    tmp_arr[ac++] = String.fromCharCode(c1);
                    i++;
                } else if (c1 <= 223) {
                    c2 = str_data.charCodeAt(i + 1);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
                    i += 2;
                } else if (c1 <= 239) {
                    c2 = str_data.charCodeAt(i + 1);
                    c3 = str_data.charCodeAt(i + 2);
                    tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                } else {
                    c2 = str_data.charCodeAt(i + 1);
                    c3 = str_data.charCodeAt(i + 2);
                    c4 = str_data.charCodeAt(i + 3);
                    c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
                    c1 -= 0x10000;
                    tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
                    tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
                    i += 4;
                }
            }
            return tmp_arr.join('');
        }
};

let tot = 0;
var utils = new _utils();
var ajax = new _ajax();

$(document).ready(function() {

    // $('.money_format').mask('000,000,000,000,000.00', { reverse: true });

    const obtenerNotificaciones = () => {
            ajax.get(`api/notificaciones`, {
                telefono: telefono,
            }, function(response, headers) {
                if (headers.status == 200) {
                    if (response && response) {
                        let datos = response.shift();
                        let tot_anterior = sessionStorage.total_notificaciones;
                        tot = response.length + 1;
                        if (tot > tot_anterior) {
                            toastr.info("Usted tiene " + tot + " nuevas notificaciones.");
                            toastr.info(datos.usuario + '<br/>' + datos.mensaje);
                            doBounce($("#total_notify"), 10, '10px', 600);
                        }
                        $("#total_notify").html(tot);
                    } else {
                        tot = 0;
                        $("#total_notify").html(tot);
                    }
                    sessionStorage.setItem('total_notificaciones', tot)
                }
            });
        }
        // typeof telefono !== 'undefined' && setInterval(obtenerNotificaciones, 5000);

});



function doBounce(element, times, distance, speed) {
    for (var i = 0; i < times; i++) {
        element.animate({ marginTop: '-=' + distance }, speed)
            .animate({ marginTop: '+=' + distance }, speed);
    }
}