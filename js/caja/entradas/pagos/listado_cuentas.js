function inicializaTabla() {
    var tbl_cxc = $('#tbl_cxc').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        footerCallback: function(row, data, start, end, display) {
            var api = this.api(),
                data;
            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            // Total over all pages
            total_saldo_neto = api
                .column(6)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            $("#total_saldo_neto_tabla").html('$ <span class="money_format">' + total_saldo_neto.toFixed(2) + '</span>');
            total_pago = api
                .column(7)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_pagar_tabla").html('$ <span class="money_format">' + total_pago.toFixed(2) + '</span>');

            total_abonado = api
                .column(8)
                .data()
                .reduce(function(a, b) {
                    console.log(a);
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_abonado_tabla").html('$ <span class="money_format">' + total_abonado.toFixed(2) + '</span>');
        },
        "ajax": {
            url: PATH_API + "api/cuentas-por-cobrar",
            type: 'GET',
        },
        columns: [{
                title: "Folio",
                data: 'folio',
            },
            {
                title: "Concepto",
                data: 'concepto',
            },
            {
                title: "Cliente",
                render: function(data, type, row) {
                    return row.numero_cliente + ' - ' + row.nombre_cliente;
                }
            },
            {
                title: "Plazo credito",
                data: 'plazo_credito',
            },
            {
                title: "Estatus",
                data: 'estatus_cuenta',
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha)

                }
            },
            {
                title: "Saldo neto",
                data: 'importe',
                render: function(data, type, row) {
                    return data >= 1 ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : '';
                }
            },
            {
                title: "Monto a pagar",
                data: 'total',
                render: function(data, type, row) {
                    return data >= 1 ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : '';
                }
            },
            {
                title: "Monto pagado",
                data: 'saldo_acomulado',
                render: function(data, type, row) {
                    return data >= 1 ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : ' ';
                }
            },
            {
                title: '-',
                render: function(data, type, row) {
                    let btn_tipo = '';
                    let btn_detalle = '';
                    if (row.tipo_proceso_id != 10) {
                        if (row.estatus_cuenta_id == 1) {
                            btn_tipo = '<button title="A credito" onclick="tipoPago(this)" data-folio_id="' + row.folio_id + '" class="btn btn-primary"><i class="fas fa-list"></i></button>';
                        }
                    }
                    btn_detalle = '<button title="Detalle venta" onclick="detallePago(this)" data-id="' + row.id + '" class="btn btn-default"><i class="fas fa-dollar-sign "></i></button>';
                    return btn_tipo + ' ' + btn_detalle;

                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            switch (data['estatus_cuenta_id']) {
                case 2:
                    $(row).find('td:eq(4)').css('background-color', '#8cdd8c');
                    break;
                case 3:
                    $(row).find('td:eq(4)').css('background-color', '#f6ffa4');
                    break;
                case 4:
                    $(row).find('td:eq(4)').css('background-color', '#e37f7f');
                    break;
                default:
                    break;
            }
        }
    });
}

function detallePago(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/detalle_pago/' + cuenta_por_cobrar;
    }, 200);
}

function tipoPago(_this) {
    setTimeout(() => {
        var folio_id = $(_this).data('folio_id');
        window.location.href = PATH + '/caja/entradas/tipoPago/' + folio_id;
    }, 200);
}

function asientos_cuenta(_this) {
    setTimeout(() => {
        var cuenta_por_cobrar = $(_this).data('id');
        window.location.href = PATH + '/caja/entradas/asientos_cuenta/' + cuenta_por_cobrar;
    }, 200);
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'folio': $("#folio").val(),
        'cliente_id': $("#cliente_id option:selected").val(),
        'estatus_cuenta_id': $("#estatus_cuenta_id option:selected").val(),
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function limpiarfiltro() {
    $("#folio").val('');
    $("#cliente_id").val('');
    $("#estatus_cuenta_id").val('');
    $("#cliente_id").trigger('change');
    var params = $.param({
        'folio': '',
        'cliente_id': '',
        'estatus_cuenta_id': '',
    });

    $('#tbl_cxc').DataTable().ajax.url(PATH_API + 'api/cuentas-por-cobrar?' + params).load()
}

function openModal() {
    $('#modal-cuenta').modal('show');
    $('.select2Modal').select2({
        dropdownParent: $('#modal-cuenta')
    });
}

let form_cuenta = function() {
    return {
        tipo_proceso: 10,
        cliente_id: document.getElementById("cliente_modal_id").value,
        estatus_cuenta_id: 1, //EN PROCESO
        tipo_forma_pago_id: 1, //Contado
        tipo_pago_id: 1, //Efectivo
        plazo_credito_id: 1, //1 MES DEFAULT
        concepto: document.getElementById("concepto").value,
        fecha: document.getElementById("fecha").value,
        venta_total: document.getElementById("venta_total").value,
    };
}

$("#btn-agregar_cuenta").on('click', function() {
    $("#tipo_pago_id").val();
    $("#concepto").val();
    $("#venta_total").val();

    $(".invalid-feedback").html("");
    toastr.info("Procesando petición ..");
    $.isLoading({
        text: "Realizando petición ...."
    });
    ajax.post(`api/cuentas-por-cobrar/set-complemento-cxc`, form_cuenta(), function(response, headers) {
        if (headers.status == 400) {
            $.isLoading("hide");
            return ajax.showValidations(headers);
        } else {
            $.isLoading("hide");
            utils.displayWarningDialog('Cuenta registrada correctamente', "success", function(data) {
                $('#modal-cuenta').modal('hide')
                filtrar();
            })
        }
    })
})

this.inicializaTabla();
$(".select2").select2();