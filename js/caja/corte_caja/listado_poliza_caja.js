function inicializaTabla() {

    $('#tabla_caja').DataTable({
        language: {
            url: PATH_LANGUAGE,

        },
        footerCallback: function(row, data, start, end, display) {
            let total_cantidad, total_reportado, total_sobrantes = 0;
            let total_faltantes = 0;
            var api = this.api(),
                data;
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };
            total_cantidad = api
                .column(3).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $("#total_cantidad").html('$ ' + total_cantidad.toFixed(2));

            total_reportado = api
                .column(4).data().reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            $("#total_reportado").html('$ ' + total_reportado.toFixed(2));

            $('#tabla_caja').DataTable().rows().data().each(function(el, index) {
                total_faltantes += parseFloat(el.faltantes);
                total_sobrantes += parseFloat(el.sobrantes);
            });

            $("#total_faltantes").html('$ <span class="money_format"> ' + total_faltantes.toFixed(2) + '</span>');
            $("#total_sobrantes").html('$ <span class="money_format">' + total_sobrantes.toFixed(2) + '</span>');
        },
        "ajax": {
            url: PATH_API + "api/corte-caja/getPolizas",
            type: 'GET',
            dataSrc: "",

        },
        columns: [{
                title: "Caja",
                data: 'CajaNombre',
            },
            {
                title: "Usuario corte",
                render: function(data, type, row) {
                    return row.nombre + ' ' + row.apellido_paterno + ' ' + row.apellido_materno;
                }
            },
            {
                title: "Tipo pago",
                data: 'TipoPago',
            },
            {
                title: "Cantidad en caja",
                data: 'total_cantidad_caja',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Cantidad reportado",
                data: 'total_cantidad_reportada',
                render: function(data, type, row) {
                    return utils.isDefined(data) ? '$ <span class="money_format">' + parseFloat(data).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Faltantes",
                render: function(data, type, row) {
                    return utils.isDefined(row.faltantes) ? '$ <span class="money_format">' + parseFloat(row.faltantes).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Sobrantes",
                data: 'sobrantes',
                render: function(data, type, row) {
                    return utils.isDefined(row.sobrantes) ? '$ <span class="money_format">' + parseFloat(row.sobrantes).toFixed(2) + '</span>' : null;
                }
            },
            {
                title: "Fecha",
                render: function(data, type, row) {
                    return obtenerFechaMostrar(row.fecha_transaccion)
                }
            }
        ],
        "createdRow": function(row, data, dataIndex) {
            if (data['faltantes'] >= 0.01) {
                $(row).find('td:eq(5)').css('background-color', '#e37f7f');
            }
            if (data['sobrantes'] >= 0.01) {
                $(row).find('td:eq(6)').css('background-color', '#8cdd8c');
            }
        }
    });
}


function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function filtrar() {
    var params = $.param({
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val(),
        'caja_id': $("#caja_id option:selected").val(),
    });

    $('#tabla_caja').DataTable().ajax.url(PATH_API + 'api/corte-caja/getPolizas?' + params).load()
}

function limpiarfiltro() {
    $("#fecha_inicio").val('');
    $("#fecha_fin").val('');
    $("#caja_id").val('');
    $("#caja_id").trigger('change');
    var params = $.param({
        'caja_id': '',
        'fecha_inicio': '',
        'fecha_fin': ''
    });

    $('#tabla_caja').DataTable().ajax.url(PATH_API + 'api/corte-caja/getPolizas?' + params).load()
}

this.inicializaTabla();
$("#caja_id").select2();