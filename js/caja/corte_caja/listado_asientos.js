function crearTabla() {
    $('#tabla_asientos').dataTable(this.configuracionTabla());
}

function configuracionTabla() {
    return {
        language: {
            url: PATH_LANGUAGE
        },
        order: [
            [0, 'asc']
        ],
        columns: [

            {
                title: "Concepto",
                data: 'referencia',
            },
            {
                title: "Cuenta",
                render: function(data, type, row) {
                    return row.no_cuenta + ' - ' + row.nombre_cuenta;
                }
            },
            {
                title: "Abono",
                data: 'tipo_asiento_id',
                render: function(data, type, row) {
                    return data == 1 ? '<span class="">' + parseFloat(row.total_pago).toFixed(2) + '</span>' : '  '
                }
            },
            {
                title: "Cargo",
                data: 'tipo_asiento_id',
                render: function(data, type, row) {
                    return data == 2 ? '<span class="">' + parseFloat(row.total_pago).toFixed(2) + '</span>    ' : '  '
                }
            },
            {
                title: "Fecha pago",
                render: function(data, type, row) {
                    if (row.fecha_pago) {
                        return obtenerFechaMostrar(row.fecha_pago)
                    } else {
                        return '--';
                    }
                }
            },
            {
                title: "Estatus",
                data: 'estatus',
            },
            {
                title: "Acciones",
                render: function(data, type, row) {
                    let btn_autorizar = '';
                    let btn_cancelar = '';
                    if (row.estatus_id == 1) {
                        btn_cancelar = '<button title="Cancelar" onclick="cambiarEstatus(this)" data-asiento_id="' + row.id + '" data-estatus_id="3" type="button" class="btn btn-danger"><i class="fa fa-times fa-1x"></i></button>';
                        btn_autorizar = '<button title="Autorizar" onclick="cambiarEstatus(this)" data-asiento_id="' + row.id + '" data-estatus_id="2" type="button" class="btn btn-success"><i class="fa fa-check fa-1x"></i></button>';
                    }
                    return btn_autorizar + ' ' + btn_cancelar;
                }
            },
        ],

    }
}

function filtrar() {
    var params = {
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val(),
    };
    this.getBusqueda(params).then(x => {
        this.calcularTotales();
    });
}

function imprimir_reporte() {
    // var params = {
    //     'fecha_inicio': $("#fecha_inicio").val(),
    //     'fecha_fin': $("#fecha_fin").val(),
    // };
    // this.getBusqueda(params).then(x => {
    //     this.calcularTotales();
    // });
    window.location.href = PATH + '/caja/reportes/imprimir_reporte?fecha_inicio=' + $("#fecha_inicio").val() + '&fecha_fin=' + $("#fecha_fin").val()
}

function cambiarEstatus(_this) {
    let tipo = $(_this).data('estatus_id');
    let msj = tipo == 2 ? ' autorizar ' : ' cancelar ';
    Swal.fire({
        icon: 'warning',
        title: '¿Esta seguro de' + msj + 'el registro?',
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No"
    }).then((result) => {
        if (result.value) {
            ajax.put('api/detalle-asientos-cuentas/updateEstatus/' + $(_this).data('asiento_id'), { estatus_id: tipo }, function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                } else {
                    utils.displayWarningDialog(headers.message, "success", function(data) {
                        this.filtrar();
                    })
                }
            })
        }
    });
}

function autorizacion_multiple() {
    $.isLoading({ text: "Cargando..." });
    toastr.info("Procesando información.....");

    var params = {
        'fecha_inicio': $("#fecha_inicio").val(),
        'fecha_fin': $("#fecha_fin").val(),
        'estatus_id': 1
    };
    ajax.get("api/detalle-asientos-cuentas/busqueda-filtros", params, function(response, header) {
        if (header.status == 200) {
            let count = 0;
            if (response && response.length > 0) {
                $.each(response, function(index, value) {
                    toastr.clear();
                    ajax.put('api/detalle-asientos-cuentas/updateEstatus/' + value.id, { estatus_id: 2 }, function(respuesta, headers) {
                        if (headers.status == 400) {
                            $.isLoading("hide");
                            return ajax.showValidations(headers);
                        } else {
                            count++;
                            toastr.success("Asiento autorizado correctamente " + value.id);
                            if (response.length == count) {
                                $.isLoading("hide");
                                utils.displayWarningDialog("Proceso de autorización concluido correctamente.!", "success", function(data) {
                                    this.filtrar();
                                })
                            }
                        }
                    })
                });
            } else {
                $.isLoading("hide");
                utils.displayWarningDialog("Sin cuentas pendientes por autorizar", "info", function(data) {
                    return false;
                })
            }
        }
    });
}

function getBusqueda(params) {
    return new Promise(function(resolve, reject) {

        ajax.get("api/detalle-asientos-cuentas/busqueda-filtros", params, function(response, header) {
            var listado = $('#tabla_asientos').DataTable();
            resolve(response);
            if (header.status == 200) {
                if ($('small.form-text.text-danger').length > 0) {
                    $('small.form-text.text-danger').each(function() {
                        $(this).empty();
                    });
                }
                listado.clear().draw();
                if (response && response.length > 0) {
                    response.forEach(listado.row.add);
                    listado.draw();
                } else {
                    toastr.info("Sin resultados..");
                }
            }
        });
    });
}

function calcularTotales() {
    let total_abonos = 0;
    let total_cargos = 0;
    $('table#tabla_asientos tbody tr').each(function() {
        if ($.isNumeric($(this).find("td").eq(2).text())) {
            total_abonos += parseFloat($(this).find("td").eq(2).text());
        }
        if ($.isNumeric($(this).find("td").eq(3).text())) {
            total_cargos += parseFloat($(this).find("td").eq(3).text());
        }
        $(this).find("td").eq(2).addClass('money_format');
        $(this).find("td").eq(3).addClass('money_format');

    });
    $("#total_abonos").html(parseFloat(total_abonos).toFixed(2));
    $("#total_cargos").html(parseFloat(total_cargos).toFixed(2));

    $("#total_abonos").addClass('money_format');
    $("#total_cargos").addClass('money_format');
    $('.money_format').mask("#,##0.00", { reverse: true });
}

function obtenerFechaMostrar(fecha) {
    const dia = 2,
        mes = 1,
        anio = 0;
    fecha = fecha.split('T');
    fecha = fecha[0].split('-');
    return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}

function limpiarfiltro() {
    $("#fecha_inicio").val('');
    $("#fecha_fin").val('');
}
this.crearTabla();
this.filtrar();
$('.money_format').mask("#,##0.00", { reverse: true });