function inicializaTabla() {
    $('#listadoPolizaProveedor').DataTable({
        language: {
            url: PATH_LANGUAGE
        },
        "ajax": {
            url: PATH_API + "api/polizas/devolucion_proveedor/listado",
            type: 'POST',
            data: {
                fecha_inicio: $("#fecha_inicio").val(),
                fecha_fin: $("#fecha_fin").val()
            }
        },
        columns: [{
                title: "folio",
                data: 'folio',
            },
            {
                title: "Clave<br>producto",
                data: 'no_identificacion',
            },
            {
                title: "Descrición",
                data: 'descripcion',
            },
            {
                title: "Cantidad",
                data: 'cantidad'
            },
            {
                title: "V/Unitario",
                data: 'precio',
            },
            {
                title: "Observaciones",
                data: 'observaciones',
            },
            {
                title: "Total devolución",
                data: 'total_compra',
            },
            {
                title: "Estatus",
                data: 'estatusCompra',
            },
        ]
    });
}

function getTotalesVentas() {
    ajax.post(`api/polizas/devolucion_proveedor/totales`, {
        fecha_inicio: $("#fecha_inicio").val(),
        fecha_fin: $("#fecha_fin").val()
    }, function(response, headers) {
        if (headers.status == 200) {
            $("#totalCompras").html(utils.isDefined(response.sum_compra_total) ? response.sum_compra_total : '');
            $("#totalCantidad").html(utils.isDefined(response.total_cantidad) ? response.total_cantidad : '');
            $("#sumaVUnitario").html(utils.isDefined(response.sum_valor_unitario) ? response.sum_valor_unitario : '');
        }
    });
}

this.inicializaTabla();
this.getTotalesVentas();