$(document).ready(function () {
    $("#subir_archivo").on('click', function (e) {
        let archivo = $('#archivo_universo_partes')[0].files[0];
        let paqueteDeDatos = new FormData();
        paqueteDeDatos.append('archivo_universo_partes', archivo);
        if (!utils.isDefined(archivo) && !archivo) {
            return toastr.error("Adjuntar archivo");
        }

        var sweet_loader = '<div class="sweet_loader"><i class="fas fa-circle-notch fa-spin fa-7x"></i></div>';
        swal.fire({
            showConfirmButton: false,
            allowOutsideClick: false,
            onRender: function () {
                // there will only ever be one sweet alert open.
                $('.swal2-content').prepend(sweet_loader);
            }
        });

        ajax.postFile(`api/productos/actualiza-precios`, paqueteDeDatos, function (response, header) {
            if (header.status == 200 || header.status == 204) {
                let texto = `Actualizados: ${response.actualizados ? response.no_actualizados : 0},  No actualizados: ${response.no_actualizados ? response.no_actualizados : 0}`
                Swal.fire({
                    title: '¡Productos actualizados!',
                    text: texto,
                    icon: 'success',
                    allowOutsideClick: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar!'
                }).then(okbutton => {
                    if (okbutton.value) {
                        return window.location.href = base_url + "refacciones/productos/listado";
                    }

                })
            }

            if (header.status == 500) {
                let titulo = "Error subiendo arhivo!"
                utils.displayWarningDialog(titulo, 'warning', function (result) { });
            }
        })

    })

});