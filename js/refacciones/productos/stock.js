
var tabla_stock = $('#tabla_stock').DataTable({
	language: {
		url: PATH_LANGUAGE
	},
	"ajax": {
		url: PATH_API + "api/productos/listadoStock",
		type: 'GET',
	},
	columns: [{
		title: "#",
		data: 'id',
	},
	{
		title: "Prefijo",
		data: 'prefijo',
	},
	{
		title: "Sufijo",
		data: 'sufijo',
	},
	{
		title: "Basico",
		data: 'basico',
	},
	{
		title: "No. identificación",
		data: 'no_identificacion',
	},
	{
		title: "Descripcion",
		data: 'descripcion',
	},
	{
		title: "Precio unitario",
		data: 'valor_unitario',
	},
	{
		title: "Unidad",
		data: 'unidad',
	},
	{
		title: "Cantidad actual",
		data: 'cantidad_actual',
	},
	{
		title: "Almacen primario",
		data: 'cantidad_almacen_primario'
	},
	{
		title: "Almacen secundario",
		data: 'cantidad_almacen_secundario'
	}

	]
});



const buscarproducto = () => {

	let object = {};
	if (document.getElementById('descripcion').value !== '') {
		object['descripcion'] = document.getElementById('descripcion').value;
	}else if (document.getElementById('no_identificacion').value !== '') {
		object['no_identificacion'] = document.getElementById('no_identificacion').value;
	}

	let parametros = $.param(object);
	
	$('#tabla_stock').DataTable().ajax.url(PATH_API + 'api/productos/listadoStock?' + parametros).load()
}