var _appsFunction = function () {
    this.init = function () {

		var change_input = {
			"NumeroExterior": {type: "number",'min':'0'},
			"NumeroInterior": {type: "number",'min':'0'},
            "CP": {type: "number",'max':'99999','maxlength':5}
		};
		update_inputs(change_input);
		update_select2('form#general_form select');
    },
    
    this.guardar = function(){

        var data_send =  $('form#general_form').serializeArray();
        data_send.push({ 'name': 'id_Empresa','value':id_Empresa });

       $.ajax({
           dataType: "json",
           cache: false,
           type: 'POST',
           url: PATH + '/nomina/configuracion/datos_empresa/domicilio_fiscal_actualizar',
           data: data_send,
           success: function (response, status, xhr) {

                if (response.status == 'success') {
                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: response.message,
                        confirmButtonText: "Aceptar"
                    });
                }

           }

       });
   }
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});