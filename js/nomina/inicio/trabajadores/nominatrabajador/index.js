var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/get',
				type: "POST"
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre',
					'data': function(data){
						var row = [
							data.Nombre,
							data.Apellido_1,
							data.Apellido_2
						];
						return row.join(' ');
					}
				},
				{
					title: 'Departamento',
					'data': 'Descripcion_Departamento'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-primary' href='" + PATH + '/nomina/inicio/nominaTrabajador/detalle/' + data.id + "' style='font-size: 1.2em;'; > <i class='far fa-list-alt'></i> </a>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
