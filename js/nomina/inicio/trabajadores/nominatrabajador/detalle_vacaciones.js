var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {

		if(periodo == ''){
			var opcion=$('select#periodo_trabajador option:selected').val(); 
			window.location.href = PATH+'/nomina/inicio/nominaTrabajador/detalle_vacaciones/'+identity+'/'+opcion;
			return;
		}

		$('table#table_content').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/nominaTrabajador/vacaciones_get',
				type: "POST",
				data: function(){
					return {
						trabajador: identity,
						periodo: $('select#periodo_trabajador option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'Días de disfrute',
					data: 'DiasDisfrute'
				},
				{
					title: 'Fecha de disfrute',
					data: 'FechaDisfrute',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
                },
                // {
				// 	title: 'Estatus',
				// 	data: 'Descripcion_EstatusTrab'
                // },
                // {
				// 	title: 'Prima vacacional',
				// 	defaultContent: '-'
                // },
				{
					title: 'Fecha de pago',
					data: 'FechaPagoPV',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				// {
				// 	title: 'Pago',
				// 	defaultContent: '-'
				// }
			],
			initComplete: function(settings, data) {
				$('table#table_content').append(
					$('<tfoot/>').append( $("table#table_content thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
