var _appsFunction = function () {
	this.init = function () {},
		// this.delete = function ($this) {
		// 	Swal.fire({
		// 		icon: 'warning',
		// 		title: '¿Esta seguro de eliminar el registro?',
		// 		//text: response.info,
		// 		showCancelButton: true,
		// 		confirmButtonText: "Si",
		// 		cancelButtonText: "No"
		// 	}).then((result) => {
		// 		if (result.value) {

		// 			$.ajax({
		// 				dataType: "json",
		// 				type: 'delete',
		// 				url: NOMINA_API + 'catalogos/clasificaciones/store',
		// 				data: {
		// 					'identity': $($this).attr('data-id')
		// 				},
		// 				success: function (response, status, xhr) {
		// 					Swal.fire({
		// 						icon: 'success',
		// 						title: '',
		// 						text: response.info,
		// 						confirmButtonText: "Aceptar"
		// 					}).then((result) => {
		// 						var table = $('table#listado').DataTable();
		// 						table.ajax.reload(null, false);
		// 					});
		// 				}

		// 			});
		// 		}
		// 	});
		// },
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[1, 'asc']],
			ajax: {
				url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/get',
				type: "POST"
			},
			columns: [
				{
					title: 'Fotografia',
					"orderable": "false",
					data: function ( data, type, row, meta ) {
						var image = '<img src="'+NOMINA_API_IMAGEN+'nomina/trabajador/imagen?idx='+data.id+'"  style="max-height: 120px;max-width:120px;" onerror="this.src='+"'http://localhost/dms-sohex/js/nomina/blanco.png'"+'" class="img-fluid" >';
						return image;
					}
					

					
				},
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre',
					'data': function(data){
						var row = [
							data.Nombre,
							data.Apellido_1,
							data.Apellido_2
						];
						return row.join(' ');
					}
				},
				{
					title: 'Departamento',
					'data': 'Descripcion_Departamento'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-success btn-sm' href='" + PATH + '/nomina/inicio/trabajador/datos_generales?id=' + data.id + "' title='Datos Generales' > <i class='fa fa-user' aria-hidden='true'></i> </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/trabajador/historico_trabajador?id=' + data.id + "' title='Histórico' > <i class='fas fa-user-clock'></i> </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/trabajador/familia_beneficiarios?id=' + data.id + "' title='Familia y beneficiarios' > <i class='fas fa-users' ></i> </a>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			},
			createdRow: function( row, data, dataIndex){  
                $('td', row).css('background-color', '#fff');
            }
		});

		var table = $('table#listado').DataTable();
		$('table#listado').on( 'page.dt', function () {
			
			var info = table.page.info();
			//$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
		} );
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
