var _appsFunction = function () {
    this.fecha_hoy,
    this.init = function () {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        this.fecha_hoy =  yyyy + '-' + mm + '-' + dd;
    },

    this.guardar = function () {

        if($('small.form-text.text-danger').length > 0){
            $('small.form-text.text-danger').each(function() {
                $( this ).empty();
            });
        }
        
        var dataSend = $('form#formContent_General').serializeArray();
        dataSend.push({name: 'id', value: identity});
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_salario();
            },error: function (xhr, status, errors) {
                
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {
                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }

                    $('a[href="#generales"]').tab('show')
                    $('a[href="#generales"]').click();
                }else{
                    Apps.guardar_salario();
                }
                
            }

        });
    },

    this.guardar_salario = function () {

        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });

        var dataSend = $('form#formContent_Salario').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        dataSend.push({name: 'DiasDescanso', value: (salarioDias.length > 0)? JSON.stringify(salarioDias) : '' });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_salario/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_personales();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#salario"]').tab('show')
                    $('a[href="#salario"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_personales();
                }
                
            }

        });
    },

    this.guardar_personales = function () {

        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        
        var dataSend = $('form#formContent_Personales').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        dataSend.push({name: 'DiasDescanso', value: (salarioDias.length > 0)? JSON.stringify(salarioDias) : '' });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datospersonales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_imss();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#personales"]').tab('show')
                    $('a[href="#personales"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_imss();
                }
                
            }

        });
    },

    this.guardar_imss = function () {

        var dataSend = $('form#formContent_imss').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosimss/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_salud();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#imss"]').tab('show')
                    $('a[href="#imss"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_salud();
                }
                
            }

        });
    },

    this.guardar_salud = function () {

        var dataSend = $('form#formContent_salud').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datossalud/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.guardar_fiscales();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#salud"]').tab('show')
                    $('a[href="#salud"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.guardar_fiscales();
                }
                
            }

        });
    },

    this.guardar_fiscales = function () {

        var dataSend = $('form#formContent_fiscales').serializeArray();
        dataSend.push({name: 'id_Trabajador', value: identity});
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosfiscales/put',
            data: dataSend,
            success: function (response, status, xhr) {
                Apps.finalizar_guardado();
            },error: function (xhr, status, errors) {
                 
                var response = JSON.parse(xhr.responseText);
                if (response.validation == true) {

                    $('a[href="#fiscales"]').tab('show')
                    $('a[href="#fiscales"]').click();

                    if($.type(response.message) == 'object'){
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }else if($.type(response.message) == 'string'){
                        toastr["error"](response.message);
                    }
                    
                }else{
                    Apps.finalizar_guardado();
                }
                
            }

        });
    },

    this.finalizar_guardado = function(){
        Swal.fire({
            icon: 'success',
            title: '',
            text: 'El registro se ha actualizado correctamente',
            confirmButtonText: "Aceptar"
        }).then((result) => {
            window.location.href = PATH + '/nomina/inicio/trabajador/index';
        });
    },

    this.guardar_back = function () {
        
        var general = $('form#formContent_General').serializeArray();
        var dataObj_general = {id:identity};
        $(general).each(function(i, field){
            dataObj_general[field.name] = $.trim(field.value);
        });

        
        var salarioDias = [];
        $("form#formContent_Salario input:checkbox:checked").each(function(){
            salarioDias.push($(this).val());
        });
        var salario = $('form#formContent_Salario').serializeArray();
        var dataObj_salario = { id_Trabajador:identity ,'DiasDescanso' : JSON.stringify(salarioDias) };
        $(salario).each(function(i, field){
            dataObj_salario[field.name] = $.trim(field.value);
        });


        var personales = $('form#formContent_Personales').serializeArray();
        var dataObj_personales = { id_Trabajador:identity };
        $(personales).each(function(i, field){
            dataObj_personales[field.name] = $.trim(field.value);
        });

        var imss = $('form#formContent_imss').serializeArray();
        var dataObj_imss = {id_Trabajador:identity};
        $(imss).each(function(i, field){
            dataObj_imss[field.name] = $.trim(field.value);
        });

        var salud = $('form#formContent_salud').serializeArray();
        var dataObj_salud = {id_Trabajador:identity};
        $(salud).each(function(i, field){
            dataObj_salud[field.name] = $.trim(field.value);
        });

        var fiscales = $('form#formContent_fiscales').serializeArray();
        var dataObj_fiscales = {id_Trabajador:identity};
        $(fiscales).each(function(i, field){
            dataObj_fiscales[field.name] = $.trim(field.value);
        });
        
        var dataSend = {
            general: dataObj_general,
            salario: dataObj_salario,
            personales: dataObj_personales,
            imss: dataObj_imss,
            salud: dataObj_salud,
            fiscales: dataObj_fiscales
        };

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/actualiza_trabajador',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
                    Swal.fire({
                    	icon: 'success',
                    	title: '',
                    	text: response.message,
                    	confirmButtonText: "Aceptar"
                    }).then((result) => {
                    	window.location.href = PATH + '/nomina/inicio/trabajador/index?id=' + response.data.general;
                    });
                } else {
                    switch (response.tab) {
                        case 1:
                            $('a[href="#generales"]').tab('show')
                            $('a[href="#generales"]').click();
                            break;
                        case 2:
                            $('a[href="#salario"]').tab('show')
                            $('a[href="#salario"]').click();
                            break;
                        case 3:
                            $('a[href="#personales"]').tab('show')
                            $('a[href="#personales"]').click();
                            break;
                        case 4:
                            $('a[href="#imss"]').tab('show')
                            $('a[href="#imss"]').click();
                            break;
                        case 5:
                            $('a[href="#salud"]').tab('show')
                            $('a[href="#salud"]').click();
                            break;
                        case 6:
                            $('a[href="#fiscales"]').tab('show')
                            $('a[href="#fiscales"]').click();
                            break;
                    }

                    if($.type(response.message) == 'object'){
                        
                        $.each(response.message, function(index, value) {
                            if ($('small#msg_' + index).length) {
                                $('small#msg_' + index).html(value);
                            }else{
                                toastr["error"](value);
                            }
                        });
                        toastr["error"]("Es necesario verificar la información");
                    }

                }
            }

        });
    },
    
    this.LoadDatosGenerales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosgenerales/find',
            data: {id: identity},
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    
                    $('input[name=Clave]').val(response.extends.Clave);

                    var datosGenerales_tpl = $('script#datosGenerales').html()
                    var storangeGenerales = {
                        Nombre: response.data.Nombre,
                        Apellido1: response.data.Apellido_1,
                        Apellido2: response.data.Apellido_2,
                        FechaNacimiento: moment(response.data.FechaNacimiento).format('YYYY-MM-DD'),
                        id_LugarNacimiento: response.data.id_LugarNacimiento,
                        EntidadesNacimiento: response.extends.EntidadesNacimiento,
                        NumeroImss: response.data.NumeroImss,
                        id_Genero: response.data.id_Genero,
                        Genero: response.extends.Genero,
                        RFC: response.data.RFC,
                        CURP: response.data.CURP,
                        id_Clasificacion: response.data.id_Clasificacion,
                        Clasificaciones: response.extends.Clasificaciones,
                        id_Departamento: response.data.id_Departamento,
                        Departamentos: response.extends.Departamentos,
                        id_Puesto: response.data.id_Puesto,
                        Puestos: response.extends.Puestos,
                        FechaAlta: moment(response.data.FechaAlta).format('YYYY-MM-DD')
                    };
                    var renderedContent = Mustache.render(datosGenerales_tpl,storangeGenerales);
                    $('form#formContent_General').html(renderedContent);

                    
                    $.ajax({
                        dataType: "json",
                        type: 'post',
                        url: PATH + '/nomina/inicio/trabajador/editar_get_imagen',
                        data: {id: identity},
                        success: function (response, status, xhr) {
                            if(response.data != null){
                                try {
                                    $('input#fotografia_nombre').val(response.data.Nombre);
                                    $('label[for=imagen_logo]').html(response.data.Nombre);
                                    $('textarea#Fotografia').val(response.data.Contenido);
                                    $('img#preview_imagen').attr('src',NOMINA_API_IMAGEN+'nomina/trabajador/imagen?idx='+identity);
                                    
                                } catch (error) {
                                    
                                }
                                
                            }
                        }
            
                    });

                    $('form#formContent_General select').each(function( index ) {
                        var id = $(this).attr('attr-id');
                        if($.trim(id).length > 0){
                            $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            });
                        }else{
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            }).val('').change();
                        }                        
                    });
                }
            }

        });
    },

    this.SelectedFile = function() {
        //Toma el archivo elegido por el input 
        $('div#preview_imagen_div').hide();
        var uploadFile = document.getElementById("imagen_logo").files[0];

        if (!window.FileReader) {
            $('small#msg_Fotografia').html('El navegador no soporta la lectura de archivos');
            return;
        }
    
        if (!(/\.(jpg|png|jpeg|gif)$/i).test(uploadFile.name)) {
            $('small#msg_Fotografia').html('El archivo a adjuntar no es una imagen');
        }
        else {
            var img = new Image();
            img.onload = function () {
                var $_this = this;
                // if (this.width.toFixed(0) != 200 && this.height.toFixed(0) != 200) {
                //     alert('Las medidas deben ser: 200 * 200');
                // }
                // else 
                if (uploadFile.size > 5000000)
                {
                    $('small#msg_Fotografia').html('El peso de la imagen no puede exceder los 5mb')
                } else {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {

                        var canvas = document.createElement('canvas');
                        var ctx = canvas.getContext('2d');

                        // We set the dimensions at the wanted size.
                        canvas.width = 200;
                        canvas.height = 200;

                        // We resize the image with the canvas method drawImage();
                        ctx.drawImage($_this, 0, 0, 200, 200);

                        var dataURI = canvas.toDataURL();

                        var NombreDeTuVariableAEnviar = e.target.result;
                        $('textarea#Fotografia').val(dataURI);
                        $('img#preview_imagen').attr('src',dataURI);
                        $('div#preview_imagen_div').show();

                        var filename = $('input#imagen_logo').val().split('\\').pop();
                        $('label[for=imagen_logo]').html(filename);
                        $('input[name=fotografia_nombre]').val(filename);
                    }
                    reader.readAsDataURL(uploadFile);
                }
            };
            img.src = URL.createObjectURL(uploadFile);
        }    
    },

    this.factor_integracion = function () {

        if($.trim($('select[name=id_TablaSistema] option:selected').val()).length > 0 && $.trim($('input[name=FechaAlta]').val()).length > 0 && $.trim($('input[name=SalarioDia]').val()).length > 0){

            if($('small.form-text.text-danger').length > 0){
                $('small.form-text.text-danger').each(function() {
                    $( this ).empty();
                });
            }
            
            var dataSend = [];
            dataSend.push({name: 'catalogo', value: $('select[name=id_TablaSistema] option:selected').val() });
            dataSend.push({name: 'fecha_alta', value: $('input[name=FechaAlta]').val() });
            dataSend.push({name: 'monto', value: $('input[name=SalarioDia]').val() });

            $.ajax({
                dataType: "json",
                type: 'POST',
                url: PATH + '/nomina/api/calculos/factor_integracion',
                data: dataSend,
                success: function (response, status, xhr) {
                    // Apps.guardar_salario();
                    $('input[name=Calculado]').val(response.data.sdi_round);
                    $('input[name=SDICapturado]').val(response.data.sdi_round);

                },
            });
        }
    },

    this.CalcularDia = function(){
        var salarioDia = $('input[name=SalarioDia]').val();
        var horas = $('input[name=HorasDia]').val();
        var salarioHora = parseFloat(salarioDia/horas).toFixed(2);
        $('input[name=SalarioHora]').val(salarioHora);
        Apps.factor_integracion();
    },

    this.CalcularHoras = function(){
        var salarioDia = $('input[name=SalarioDia]').val();
        var horas = $('input[name=HorasDia]').val();
        var salarioHora = parseFloat(salarioDia/horas).toFixed(2);
        $('input[name=SalarioHora]').val(salarioHora);
        Apps.factor_integracion();
    },

    this.calcularDiasSemana = function(){
        $('form#formContent_Salario input[type=checkbox]').attr('checked',false);
        var max = $('input[name=DiasSemana]').val();
        $('form#formContent_Salario input[type=checkbox]').each(function( index ) {
            var $this = this;
            if(index < max){
                $($this).attr('checked',true); 
            }
        });

    },

    this.jornada_completa = function(){
        var opcion = $('select[name=JornadaCompleta] option:selected').val();
        if(opcion == 'si'){
            var change_input = {
                "HorasDia": {type:'','class':'form-control-plaintext',readonly:true},
                "DiasSemana": {type:'','class':'form-control-plaintext',readonly:true}
            };
        }else{
            var change_input = {
                "HorasDia": {type:'number','class':'form-control',readonly:false},
                "DiasSemana": {type:'number','class':'form-control',readonly:false}
            };

        }
        
        update_inputs(change_input);
    },

    this.LoadSalario = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_salario/find',
            data: {id_Trabajador: identity},
            success: function (response, status, xhr) {
                // try {
                        
                    if(response.status == 'success'){
                        var datosSalario_tpl = $('script#datosSalario').html()
                        var storangeSalario = {
                            id: response.data.id,
                            JornadaCompleta: response.data.JornadaCompleta,
                            SalarioDia: response.data.SalarioDia,
                            SalarioHora: response.data.SalarioHora,
                            HorasDia: response.data.HorasDia,
                            DiasSemana: response.data.DiasSemana,
                            DiasPeriodo: response.data.DiasPeriodo,
                            FechaAplicacion: moment(response.data.FechaAplicacion).format('YYYY-MM-DD'),
                            id_FormaPago: response.data.FormaPago,
                            FormaPago: response.extends.FormaPago,

                            id_BaseCotizacion: response.data.id_BaseCotizacion,
                            BaseCotizacion: response.extends.BaseCotizacion,
                            id_TablaSistema: response.data.id_TablaSistema,
                            TablaSDI: response.extends.TablasSistema,
                            Calculado: response.data.Calculado,
                            SDICapturado: response.data.SDICapturado,

                            BancoOperador: response.extends.BancoOperador,
                            id_BancoOperador: response.data.id_BancoOperador,
                            LocalidadSucursal: response.data.LocalidadSucursal,
                            CuentaDeposito: response.data.CuentaDeposito,
                            CuentaBanco: response.data.CuentaBanco,

                            NoFonacot: response.data.NoFonacot,
                        };
                        renderedContent = Mustache.render(datosSalario_tpl,storangeSalario);
                        $('form#formContent_Salario').html(renderedContent);
                    
                        try {
                            var obj = JSON.parse(response.data.DiasDescanso);
                            $.each(obj, function( index, value ) {
                                $('[name*=diasDescanso][value='+value+']').prop('checked', true);
                            });
                        } catch (error) {
                            Apps.calcularDiasSemana();    
                        }

                        $('input[name=DiasPeriodo]').removeClass('form-control');
                        var max_salario = $('input[name=SalarioDia]').val();
                        var fecha_alta = $('input[name=FechaAlta]').val();
    
                        $('select[name=JornadaCompleta]').attr('onchange','Apps.jornada_completa();');
                        $('select[name=id_TablaSistema]').attr('onchange','Apps.factor_integracion();');
                        
                        var change_input = {
                            "SalarioHora": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                            "SalarioDia": {type:'number',step:'0.01',min:'1',onblur:'Apps.CalcularDia(this);',min:max_salario},
                            "HorasDia": {type:'',class:'form-control-plaintext',step:'0.01',min:'1',onblur:'Apps.CalcularHoras(this);',max:24},
                            "DiasSemana": {type:'',class:'form-control-plaintext',step:'1',min:'1',max:'7',onblur:'Apps.calcularDiasSemana();'},
                            "DiasPeriodo": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                            "FechaAplicacion": {type:'date',value:fecha_alta,min:fecha_alta},
    
                            "SDICapturado": {type:'','class':'form-control-plaintext',readonly:'readonly'},
                        };
                        update_inputs(change_input);
                        
                        
                        $('form#formContent_Salario select').each(function( index ) {
                            var id = $(this).attr('attr-id');
                            if($.trim(id).length > 0){
                                $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                                $(this).select2({    
                                    language: {
                                    noResults: function() { return "No hay resultados"; },
                                    searching: function() { return "Buscando.."; }
                                    }
                                });
                            }else{
                                $(this).select2({    
                                    language: {
                                    noResults: function() { return "No hay resultados"; },
                                    searching: function() { return "Buscando.."; }
                                    }
                                }).val('').change();
                            }                        
                        });
                    }
                // } catch (error) {
                    
                // }
            }
        });
    },


    this.LoadDatosPersonales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datospersonales/find',
            data: {id_Trabajador: identity},
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosPersonales_tpl = $('script#datosPersonales').html()
                    var storangePersonales = {
                        id: response.data.id,
                        CalleNumero: response.data.CalleNumero,
                        Colonia: response.data.Colonia,
                        Poblacion: response.data.Poblacion,
                        Poblacion: response.data.Poblacion,
                        id_EntidadFederativa: response.data.id_EntidadFederativa,
                        EntidadesFederativa: response.extends.EntidadesFederativa,
                        Pais: response.data.Pais,
                        CorreoElectronico_1: response.data.CorreoElectronico_1,
                        CorreoElectronico_2: response.data.CorreoElectronico_2,
                        CodigoPostal: response.data.CodigoPostal,
                        Telefono_1: response.data.Telefono_1,
                        Telefono_2: response.data.Telefono_2,

                        id_NivelEstudios: response.data.id_NivelEstudios,
                        NivelesEstudio: response.extends.NivelesEstudio,
                        Profesion: response.data.Profesion,
                        EstadoCivil: response.extends.EstadoCivil,
                        id_EstadoCivil: response.data.id_EstadoCivil,
                        TipoSangre: response.extends.TipoSangre,
                        id_TipoSangre: response.data.id_TipoSangre
                    };
                    renderedContent = Mustache.render(datosPersonales_tpl,storangePersonales);
                    $('form#formContent_Personales').html(renderedContent);
                    $('form#formContent_Personales select').each(function( index ) {
                        var id = $(this).attr('attr-id');
                        if($.trim(id).length > 0){
                            $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            });
                        }else{
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            }).val('').change();
                        }                        
                    });

                }
            }
        });
    },

    this.LoadIMSS = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosimss/find',
            data: {id_Trabajador: identity},
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosIMSS_tpl = $('script#datosIMSS').html()
                    var storangeIMSS = {
                        id: response.data.id,
                        TipoEmpleado: response.extends.TipoEmpleado,
                        id_TipoEmpleado: response.data.id_TipoEmpleado,
                        TipoJornadaImss: response.extends.TipoJornadaImss,
                        id_TipoJornadaImss: response.data.id_TipoJornadaImss,
                        TurnosImss: response.extends.TurnosImss,
                        id_TurnoImss: response.data.id_TurnoImss,
                        UMF: response.data.UMF,
                        DescPensionAlimenticia: response.data.DescPensionAlimenticia
                    };
                    var renderedContent = Mustache.render(datosIMSS_tpl,storangeIMSS);
                    $('form#formContent_imss').html(renderedContent);
                    $('form#formContent_imss select').each(function( index ) {
                        var id = $(this).attr('attr-id');
                        if($.trim(id).length > 0){
                            $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            });
                        }else{
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            }).val('').change();
                        }                        
                    });                    
                }
            }
        });
    },

    this.LoaSalud = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datossalud/find',
            data: {id_Trabajador: identity},
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosSalud_tpl = $('script#datosSalud').html()
                    var storangeSalud = {
                        id: response.data.id,
                        RestriccionesSalud: response.data.RestriccionesSalud,
                        EmergenciaTelefono: response.data.EmergenciaTelefono,
                        EmergenciaNombre: response.data.EmergenciaNombre,
                        EmergenciaDireccion: response.data.EmergenciaDireccion,
                    }
                    var renderedContent = Mustache.render(datosSalud_tpl,storangeSalud);
                    $('form#formContent_salud').html(renderedContent);
                    
                }
            }
        });
    },

    this.LoadDatosFiscales = function(){
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/api/api/runner/trabajador_datosfiscales/find',
            data: {id_Trabajador: identity},
            success: function (response, status, xhr) {
                if(response.status == 'success'){
                    var datosFiscales_tpl = $('script#datosFiscales').html()
                    var storangeFiscales = {
                        id: response.data.id,
                        id_RegimenContrato: response.data.id_RegimenContrato,
                        RegimenContrato: response.extends.RegimenContrato,
                        id_EntidadFederativa: response.data.id_EntidadFederativa,
                        EntidadesFederativa: response.extends.EntidadesFederativa,
                        id_TipoContrato: response.data.id_TipoContrato,
                        TipoContrato: response.extends.TipoContrato,
                        id_TipoJornada: response.data.id_TipoJornada,
                        TipoJornada: response.extends.TipoJornada
                    };

                    var renderedContent = Mustache.render(datosFiscales_tpl,storangeFiscales);
                    $('form#formContent_fiscales').html(renderedContent);
                    $('form#formContent_fiscales select').each(function( index ) {
                        var id = $(this).attr('attr-id');
                        if($.trim(id).length > 0){
                            $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            });
                        }else{
                            $(this).select2({    
                                language: {
                                noResults: function() { return "No hay resultados"; },
                                searching: function() { return "Buscando.."; }
                                }
                            }).val('').change();
                        }                        
                    });        
                }
            }
        });
    },

    this.showTab = function($this){

        if($($this).attr('loader_select') != 'true'){
            $($this).attr('loader_select','true');

            setTimeout(() => {
                var ids = $($this).attr('href');
                $('div.tab-pane'+ids+' form').attr('loader','1');
                $('div.tab-pane'+ids+' form select')
                    .select2({    
                        language: {
                        noResults: function() { return "No hay resultados"; },
                        searching: function() { return "Buscando.."; }
                        }
                    });
            }, 200);
        }

    },

    this.render = function(){
        this.LoadDatosGenerales();
        this.LoadSalario();
        this.LoadDatosPersonales();
        this.LoadIMSS();
        this.LoaSalud();
        this.LoadDatosFiscales();
    }

}

var Apps;
$(function () {
    Apps = new _appsFunction();
    Apps.init();
    Apps.render();
});
