var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			columns: [
				{
					title: 'Tipo',
				},
				{
					title: 'Fecha de inicio',
				},
				{
					title: 'Número de dias'
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
