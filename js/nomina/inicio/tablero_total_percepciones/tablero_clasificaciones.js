var _appsFunction = function () {
	this.init = function () {},
	
	this.buscar_periodos = function(){
		
		var param1 = $('select#id_tipo_periodo option:selected').val();
		
		$.ajax({
			dataType: "json",
			type: 'GET',
			url: PATH + '/nomina/inicio/tablero_total_percepciones/periodos_cerrados_anio_transacciones',
			data: {id_tipo_periodo: param1},
			success: function (response, status, xhr) {

				if (response.status == 'success') {
					$('select#id_periodo option').remove()
					if(response.data.length > 0){
						response.data.forEach(element => {
							$('select#id_periodo').append('<option value="'+element.id+'">'+moment(element.FechaInicio).format("DD/MM/YYYY")+' al '+moment(element.FechaFin).format("DD/MM/YYYY")+'</option>');
						});						
					}else{
						toastr.warning('No hay periodos pagados para el tipo de periodo seleccionado');
					}
				}
			}

		});
    },
    this.cargarDatos = function(opcion){
        var tipo_periodo = $('select#id_tipo_periodo option:selected').val();
        var periodo = $('select#id_periodo option:selected').val();
        window.location.href = PATH + "/nomina/inicio/tablero_total_percepciones/tablero_clasificaciones/"+tipo_periodo+"/"+periodo;
    }
    
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	if(id_perdiodo == false){
		Apps.cargarDatos();
	}
});
