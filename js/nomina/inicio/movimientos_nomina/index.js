var _appsFunction = function () {
	this.init = function () {},
	this.addContenido = function(){
		var id = $('select[name=trabajador] option:selected').val();
		window.location.href = PATH + '/nomina/inicio/movientos_nomina/alta/'+id;
	},
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},
	this.get = function () {

		if(id_trabajador != false){
			$('select[name="trabajador"]').find('option[value="'+id_trabajador+'"]').attr("selected",true);
		}

		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/inicio/movientos_nomina/index_get',
				type: "POST",
				data: function(){
					return {id: $('select[name=trabajador] option:selected').val() };
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave_Trabajador'
				},
				{
					title: 'Nombre completo',
					'data': function(data){
						var row = [
							data.Nombre_Trabajador,
							data.Apellido_1_Trabajador,
							data.Apellido_2_Trabajador
						];
						return row.join(' ');
					}
				},
				{
					title: 'P/D',
					data: 'Clave_PyD'
				},
				{
					title: 'Descripción',
					data: 'Descripcion_PyD'
				},
				{
					title: 'Tipo',
					data: 'Descripcion_MovAplicacion'
				},
				{
					title: 'Fórmula',
					data: 'FormulaMonto'
				},
				{
					title: '-',
					render: function ( data, type, row, meta ) {

						if(id_trabajador == false){
							id_trabajador = $('select[name=trabajador] option:selected').val();
						}

						if(row.Procesado == 0){
							return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/movientos_nomina/editar/'+id_trabajador+ '/' + row.id + "' title='' > <i class='fas fa-pencil-alt'></i> </a>";
						}
						return '';

					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			},
			createdRow: function( row, data, dataIndex){  
                $('td', row).css('background-color', '#fff');
            }
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
