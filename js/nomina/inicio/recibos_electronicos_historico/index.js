var _appsFunction = function () {
    this.init = function () {},
    this.trabajadores = [],

    this.agrega_trabajador = function (rfc,valor) {
        
        var arr = this.trabajadores;
        var band = true;

        $.each( arr, function( key, item ) {
            if(item.rfc == rfc){
                band = false;
            }
        });

        if(band == true){
            Apps.trabajadores.push({rfc:rfc,value:valor});
        }
    }

	this.cargarDatos = function(){
        var id1 = $('select#id_departamento option:selected').val();
        var id2 = $('select#id_puesto option:selected').val();
		window.location.href = PATH + '/nomina/inicio/recibos_electronicos_historico/index/'+id1+'/'+id2;
    },
    
    this.buscar_periodos = function(){
		
		var param1 = $('select#id_tipo_periodo option:selected').val();
		
		$.ajax({
			dataType: "json",
			type: 'GET',
			// url: PATH + '/nomina/inicio/recibos_electronicos/get_periodos',
			url: PATH + 'nomina/inicio/tablero_total_percepciones/periodos_cerrados_anio_transacciones',
			data: {id_tipo_periodo: param1},
			success: function (response, status, xhr) {

				if (response.status == 'success') {
					$('select#id_periodo option').remove()
					if(response.data.length > 0){
						response.data.forEach(element => {
							$('select#id_periodo').append('<option value="'+element.id+'">'+moment(element.FechaInicio).format("DD/MM/YYYY")+' al '+moment(element.FechaFin).format("DD/MM/YYYY")+'</option>');
						});
                    }else{
						toastr.warning('No hay periodos pagados para el tipo de periodo seleccionado');
					}
				}
			}

		});
	},

	this.mostrarRecibo = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/recibos_electronicos/detalle/'+id+'/'+id2+'/'+id3;
	},
	this.descargarRecibo = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/recibos_electronicos/recibo/'+id+'/'+id2+'/'+id3;
	},
	this.descargarXML = function ($this) {
		var id = $($this).attr('data-id_trabajadorpago');
		var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');

		window.location.href = PATH + '/nomina/inicio/recibos_electronicos/xml/'+id+'/'+id2+'/'+id3;
	},

	this.timbrado = function($this){
        var id2 = $($this).attr('data-id_trabajdor');
		var id3 = $($this).attr('data-id_periodo');
        var id4 = $($this).attr('data-rfc');

        Swal.fire({
            title: 'Timbrar nomina <br/>RFC: '+id4,
            // showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Si`,
            cancelButtonText: `No`,
        }).then((result) => {
            if (result.value == true) {
                Apps.timbrar(id2,id3,id4);
            }
        })
        
        // this.timbrar(id2,id3,id4);
        //toastr.info('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 15000,progressBar: true,})
    },
	this.timbrar = function(id_trabajdor,id_periodo,rfc){

        toastr.info('Timbrando Nomina', 'RFC: '+rfc, {timeOut: 15000,progressBar: true,})
        setTimeout(() => {
            $.ajax({
                dataType: "json",
                type: 'GET',
                url: PATH + '/nomina/inicio/recibos_electronicos/timbrado',
                data: {
                    id_Trabajador: id_trabajdor,
                    id_Periodo: id_periodo
                },
                success: function (response, status, xhr) {
                    toastr.clear();
                    try {
                        if (response.status == 'success') {
                            var table = $('table#listado').DataTable();
                            table.ajax.reload();

                            setTimeout(() => {
                                toastr.success('El timbrado ha finalizado correctamente');
                            }, 1000);
                        }
                    } catch (error) {
                        Swal.insertQueueStep({
                            icon: 'error',
                            title: 'Lo sentimos, no hemos podido procesar tu solicitud en este momento.'
                        });
                    }
                }

            });
        }, 1000);
	},

	this.reload_table = function(){
		var tables = $('table#listado').DataTable();
		tables.ajax.reload( null, false );
	},

	this.get = function () {
	
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 8, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/recibos_electronicos_historico/index_get',
				type: "GET",
				data: function(){
					return {
						id_departamento: id_departamento,
						id_puesto: id_puesto
					};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre',
					'data': function(data){
						var row = [
							data.Nombre,
							data.Apellido_1,
							data.Apellido_2
						];
						return row.join(' ');
					}
				},
				{
					title: 'RFC',
					data: 'rfc'
				},
				{
					title: 'Puesto',
					data: 'Puesto'
                },
                {
					title: 'T. Percepciones',
					data: 'total_percepciones',
					render: function ( data, type, row, meta ) {
						return '$'+ (new Intl.NumberFormat('en-US').format(data));
					}
				},
                {
					title: 'T. Deducción',
					data: 'TotalDeduccion',
					render: function ( data, type, row, meta ) {
						return '$'+ (new Intl.NumberFormat('en-US').format(data));
					}
				},
                {
					title: 'F. Emisión',
					data: 'fecha_calculo',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'F. Timbrado',
					data: 'fecha_timbrado',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'F. Nómina',
					data: 'FechaPeriodo',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
                {
					title: 'Estado',
					data: 'id_comprobantenomina',
					render: function ( data, type, row, meta ) {
						if(data != null){
							return 'Timbrado';
						}else{
							return 'No generado';
						}
					}
				}
            ],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
    Apps = new _appsFunction();
    if(id_departamento == false){
       Apps.cargarDatos(); 
    } else {
        Apps.get();
    }

});
