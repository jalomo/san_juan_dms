var _appsFunction = function () {
	this.init = function () {
        $('select[name=id_departamento]').attr('onchange','Apps.buscar_trabajadores();');
        $('select[name=id_puesto]').attr('onchange','Apps.buscar_trabajadores();');
        $('select[name=jornada]').attr('onchange','Apps.buscar_trabajadores();');

        this.ini_listado();
    },
	
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
    },
    
    this.buscar_trabajadores = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/reportes/horas_extras/index_trabajadores',
            data: {
                'id_departamento': $('select[name=id_departamento] option:selected').val(),
                'id_puesto': $('select[name=id_puesto] option:selected').val(),
                // 'jornada': $('select[name=jornada] option:selected').val()
            },
            success: function (response, status, xhr) {
                if(response.data == false){
                    $('div#listado_trabajadores').html('');
                    toastr.warning('No se encontraron trabajadores');
                }else{
                    var template = document.getElementById('tmpl_trabajadores').innerHTML;
                    var rendered = Mustache.render(template, response );
                    $('div#listado_trabajadores').html(rendered);
                }
            }
        });
    },

    this.descargar = function(){
		var data_save = $('form#form_content').serializeArray();
		data_save.push({ name: "departamento_desc", value: $('select[name=id_departamento] option:selected').text() });
		data_save.push({ name: "puesto_desc", value: $('select[name=id_puesto] option:selected').text() });
		data_save.push({ name: "jornada_desc", value: $('select[name=jornada] option:selected').text() });
		data_save.push({ name: "fecha_inicio_desc", value: $('input[name=fecha_inicio]').val() });
		data_save.push({ name: "fecha_fin_desc", value: $('input[name=fecha_fin]').val() });

		$.fileDownload(PATH+'nomina/reportes/horas_extras/index_pdf',{
			httpMethod: "POST",
			data: data_save,
		}).done(function () {  }).fail(function () {  });
 
    	return false; //this is critical to stop the click event which will trigger a normal file download
	},

    this.ini_listado = function(){

		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + 'nomina/reportes/horas_extras/index_busqueda',
				type: "POST",
				data: function(){
					return $('form#form_content').serializeArray();
				}
			},
			columns: [
                {
					title: 'Clave',
                    data: 'Clave',
                },
				{
					title: 'Nombre',
					render: function ( data, type, row, meta ) {
                        var nombre = [ row.Nombre,row.Apellido_1,row.Apellido_2 ];
                        return nombre.join(' ');
					}
                },
                {
					title: 'J. Completa',
                    data: 'JornadaCompleta',
                },
                {
					title: 'Fecha',
                    data: 'Fecha',
                    render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
                },
                {
					title: 'Horas',
                    data: 'Horas',
                }
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
