var _appsFunction = function () {
    this.init = function () {},
    this.tipo_periodicidad = 0,
    this.fecha_inicio = null,
    this.fecha_fin = null,

    this.addContenido = function(){
        var table = $('table#listado').DataTable();
        table.ajax.reload(null, false);
        return;
        $('div#exampleModal').modal('show');
    },

    this.recalcular = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/index_recalcular_get',
            data: {
                'periodo': $('select#periodicidad option:selected').val()
            },
            success: function (response, status, xhr) {

                var fecha = moment(response.data.FechaInicio);
                if(fecha.isValid()){
                    $('input#fecha_inicio').val(fecha.format('DD-MM-YYYY'));
                }

                var fecha = moment(response.data.FechaFin);
                if(fecha.isValid()){
                    $('input#fecha_fin').val(fecha.format('DD-MM-YYYY'));
                }

            }
        });
    },  

    this.guardar = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/index_guardar',
            data: {
                'periodo': $('select#periodicidad option:selected').val()
            },
            success: function (response, status, xhr) {
                $('div#exampleModal').modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: response.message,
                    confirmButtonText: "Aceptar"
                }).then((result) => {
                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                });

            }
        });
    },  

	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[0, 'desc']],
			ajax: {
				url: PATH + '/nomina/herramientas/periodos/index_get',
                type: "POST",
                data: function ( d ) {
                    return { 'id_Periodicidad': $('select[name=Periodicidad] option:selected').val() };
                }
			},
			columns: [
				{
                    title: 'Clave',
                    data: 'Clave'
                },
                {
                    title: 'Estado del periodo',
                    data: 'Procesado',
                    render: function ( data, type, row, meta ) {
                        if(row.Procesado == '1'){
                            return 'Concluido'
                        }else{
                            var actual = moment();
                            var fecha = moment(row.FechaInicio);
                            if(fecha.isValid()){
                                if(actual.diff(fecha) > 0){
                                    return 'En curso';
                                }
                            }
                        }
                        return 'Pendiente';
                    }
				},
				{
					title: 'Inicio de inicio',
                    data: 'FechaInicio',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return fecha. format('DD/MM/YYYY');
                        }
                        return data;
                    }
                },
                {
					title: 'Fecha fin',
                    data: 'FechaFin',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return fecha. format('DD/MM/YYYY');
                        }
                        return data;
                    }
                },

                {
					title: 'Fecha de corte',
                    data: 'FechaProceso',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return fecha. format('DD/MM/YYYY');
                        }
                        return data;
                    }
                }
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
            },
            createdRow: function( row, data, dataIndex){
                Apps.tipo_periodicidad = data.id_Periodicidad;
                Apps.fecha_inicio = data.FechaInicio;
                Apps.fecha_fin = data.FechaFin;
                
                var band = false;
                if(data.Procesado == '0'){
                    var actual = moment();
                    var fecha = moment(data.FechaInicio);
                    if(fecha.isValid()){
                        if(actual.diff(fecha) > 0){
                            band = true;
                            $('td', row).css('background-color', '#d4edda');
                        }
                    }
                }
                
                if(band == false){
                    $('td', row).css('background-color', '#fff');
                }
                
            },
            // fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //     $('td', nRow).css('background-color', 'red');
            // }
		});

		var table = $('table#listado').DataTable();
		$('table#listado').on( 'page.dt', function () {
			
			// var info = table.page.info();
			//$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
		} );
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
    Apps.get();
    
    $('div#exampleModal').modal('hide');
        $('div#exampleModal').on('show.bs.modal', function (event) {
            setTimeout(() => {
                $('select#periodicidad option[value='+Apps.tipo_periodicidad+']').prop('selected',true);

                var fecha = moment(Apps.fecha_inicio);
                if(fecha.isValid()){
                    $('input#fecha_inicio').val(fecha.format('DD-MM-YYYY'));
                }

                var fecha = moment(Apps.fecha_fin);
                if(fecha.isValid()){
                    $('input#fecha_fin').val(fecha.format('DD-MM-YYYY'));
                }

            },300);
         })

});
