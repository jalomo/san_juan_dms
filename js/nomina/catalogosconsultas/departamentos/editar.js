var _appsFunction = function () {
	this.init = function () {},

		this.guardar = function () {

			var data_save = $('form#formContent').serializeArray();
			data_save.push({ name: "id", value: identity });
	
			$.ajax({
				dataType: "json",
				type: 'post',
				url: PATH + '/nomina/api/api/runner/departamentos/put',
				data: data_save,
				success: function (response, status, xhr) {
					if (response.status == 'success') {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							window.location.href = PATH + '/nomina/catalogosconsultas/departamentos/index?id=' + response.data.id;
						});
					}
				}

			});
		},
		this.render = function(){
			$.ajax({
				dataType: "json",
				type: 'POST',
				url: PATH + '/nomina/api/api/runner/departamentos/find',
				data: {'id':identity},
				success: function (response, status, xhr) {
					if(response.status == 'success'){
						var template = $('script#template').html()
						var storange = {
							"formContent": [
								{
									"nombreCampo": "Clave",
									"name": "Clave",
									"value": response.data.Clave,
									"type": "number",
									"class": 'form-control-plaintext',
									"options": "readonly"
								},
								{
									"nombreCampo": "Descripción",
									"name": "Descripcion",
									"value": response.data.Descripcion,
									"type": "text",
									"class": 'form-control',
									"options": ""
								},
								{
									"nombreCampo": "Cuenta",
									"name": "Cuenta",
									"value": response.data.Cuenta,
									"type": "text",
									"class": 'form-control',
									"options": ""
								},
								{
									"nombreCampo": "Departamento",
									"name": "Departamento",
									"value": response.data.Departamento,
									"type": "text",
									"class": 'form-control',
									"options": ""
								},
							
							]
						};
						var renderedContent = Mustache.render(template,storange);
						$('form#formContent > div#contenedor').html(renderedContent);
					}
				}

			});
		}

}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.render();
});
