var _appsFunction = function () {
	this.init = function () {},

		this.get = function () {
			$('table#listado').dataTable({
				"ajax": {
					"url": PATH + '/nomina/api/api/runner/tablassistema/get',
					"type": "GET"
				},
				// "order": [[ 0, "desc" ]],
				columns: [
					{'data': 'Clave'},
					{'data': 'Descripcion'},
					{
						'data': function (data) {
							return "<a class='btn btn-success' href='" + PATH + '/nomina/catalogosconsultas/tablassistema/editar?id=' + data.id + "'> Editar </a>";
						}
					}
				]
			})
		}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
