var _appsFunction = function () {
    this.init = function () {},
    this.CambioFechaInicio = function($this){
        var fecha_inicio = moment( $('form#modal_contenido input[name=Inicio]').val() );
        var fecha_fin = moment( $('form#modal_contenido input[name=Fin]').val() );
        if(fecha_fin.isValid() && fecha_inicio.isValid()){
            var dias = moment.duration(fecha_fin.diff(fecha_inicio)).asDays();
            if(dias > 0){
                $('form#modal_contenido input[name=Dias]').val(dias);
            }else{
                $('form#modal_contenido input[name=Fin]').val( fecha_inicio.add(1, 'days').format('YYYY-MM-DD') );
                $('form#modal_contenido input[name=Dias]').val(1);
            }
        }
    },
    this.CambioFechaFin = function($this){
        var fecha_inicio = moment( $('form#modal_contenido input[name=Inicio]').val() );
        var fecha_fin = moment( $('form#modal_contenido input[name=Fin]').val() );
        if(fecha_fin.isValid() && fecha_inicio.isValid()){
            var dias = moment.duration(fecha_fin.diff(fecha_inicio)).asDays();
            if(dias > 0){
                $('form#modal_contenido input[name=Dias]').val(dias);
            }else{
                $('form#modal_contenido input[name=Inicio]').val( fecha_fin.subtract(1, 'days').format('YYYY-MM-DD') );
                $('form#modal_contenido input[name=Dias]').val(1);
            }
        }
    },
    this.modal_abrir = function($this){

        var template = document.getElementById('template_contenido').innerHTML;
        var rendered = Mustache.render(template, { 
            id: $($this).attr('identificador'),
            id_Calendario: $($this).attr('calendario'),
            Periodo: $($this).attr('Periodo'),
            Inicio: $($this).attr('Inicio'),
            Fin: $($this).attr('Fin'),
            Dias: $($this).attr('Dias'),
        });
        document.getElementById('modal_contenido').innerHTML = rendered;

        $('div#exampleModal').modal();
    },
    this.modal_guardar = function($this){

        var dataSend = $('form#modal_contenido').serializeArray();

        var fecha_inicio = moment( $('form#modal_contenido input[name=Inicio]').val() );
        if(fecha_inicio.isValid()){
            dataSend.push({name: 'inicio_anterior',value: fecha_inicio.subtract(1, 'days').format('YYYY-MM-DD') });
        }

        var fecha_fin = moment( $('form#modal_contenido input[name=Fin]').val() );
        if(fecha_fin.isValid()){
            dataSend.push({name: 'fin_siguiente',value: fecha_fin.add(1, 'days').format('YYYY-MM-DD') });
        }

        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/catalogosconsultas/calendarios/cambios_actualizar',
            data: dataSend,
            success: function (response, status, xhr) {
                if (response.status == 'success') {
                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: response.message,
                        confirmButtonText: "Aceptar"
                    }).then((result) => {
                        $('div#exampleModal').modal('hide');
                        var table = $('table#listado').DataTable();
                        table.ajax.reload(null, false);
                    });
                }
            }

        });
        
    },
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
            pageLength: 25,
			ajax: {
				url: PATH + '/nomina/catalogosconsultas/calendarios/cambios_get?id='+identity,
				type: "POST"
			},
			columns: [
				{
					title: 'Periodo',
					data: 'Periodo'
                },
                {
					title: 'Inicio',
                    data: 'Inicio',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return fecha. format('DD-MM-YYYY');
                        }
                        return data;
                    }
                },
                {
					title: 'Fin',
                    data: 'Fin',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return fecha. format('DD-MM-YYYY');
                        }
                        return data;
                    }
                },
                {
					title: 'Días',
					data: 'Dias'
				},
				{
					title: '-',
					'data': function (data) {
						return "<button  calendario='"+data.id_Calendario+"' identificador='"+data.id+"' periodo='"+data.Periodo+"' inicio='"+data.Inicio+"' fin='"+data.Fin+"' dias='"+data.Dias+"' type='button' class='btn btn-info btn-sm' href='#' onclick='Apps.modal_abrir(this)' title='Editar' > <i class='fas fa-pencil-alt'></i> </button>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
