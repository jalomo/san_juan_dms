$('.tb1_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='asistente_arranque_ck']:radio").is(':checked')) {
        var valor = $("input[name='asistente_arranque_ck']:checked").val();
        $("input[name='asistente_arranque']").val(valor);
    }
});

$('.tb1_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='preservacion_carril_ck']:radio").is(':checked')) {
        var valor = $("input[name='preservacion_carril_ck']:checked").val();
        $("input[name='preservacion_carril']").val(valor);
    }
});

$('.tb1_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='sensores_del_lat_ck']:radio").is(':checked')) {
        var valor = $("input[name='sensores_del_lat_ck']:checked").val();
        $("#sensores_del_lat").val(valor);
    }
});

$('.tb1_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='blis_ck']:radio").is(':checked')) {
        var valor = $("input[name='blis_ck']:checked").val();
        $("#blis").val(valor);
    }
});

$('.tb1_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='alert_trafico_cruzado_ck']:radio").is(':checked')) {
        var valor = $("input[name='alert_trafico_cruzado_ck']:checked").val();
        $("#alert_trafico_cruzado").val(valor);
    }
});

$('.tb1_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='asistencia_activa_estaci_ck']:radio").is(':checked')) {
        var valor = $("input[name='asistencia_activa_estaci_ck']:checked").val();
        $("#asistencia_activa_estaci").val(valor);
    }
});

$('.tb1_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='camara_reversa_ck']:radio").is(':checked')) {
        var valor = $("input[name='camara_reversa_ck']:checked").val();
        $("#camara_reversa").val(valor);
    }
});

$('.tb1_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='camara_frontal_ck']:radio").is(':checked')) {
        var valor = $("input[name='camara_frontal_ck']:checked").val();
        $("#camara_frontal").val(valor);
    }
});

$('.tb1_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='camara_360_grados_ck']:radio").is(':checked')) {
        var valor = $("input[name='camara_360_grados_ck']:checked").val();
        $("#camara_360_grados").val(valor);
    }
});

$('.tb1_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb1_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_mon_pll_tms_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_mon_pll_tms_ck']:checked").val();
        $("#sistema_mon_pll_tms").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb2_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_frenado_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_frenado_ck']:checked").val();
        $("#sistema_frenado").val(valor);
    }
});

$('.tb2_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='control_traccion_ck']:radio").is(':checked')) {
        var valor = $("input[name='control_traccion_ck']:checked").val();
        $("#control_traccion").val(valor);
    }
});

$('.tb2_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='esc_ck']:radio").is(':checked')) {
        var valor = $("input[name='esc_ck']:checked").val();
        $("#esc").val(valor);
    }
});

$('.tb2_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='rsc_ck']:radio").is(':checked')) {
        var valor = $("input[name='rsc_ck']:checked").val();
        $("#rsc").val(valor);
    }
});

$('.tb2_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='control_torque_curvas_ck']:radio").is(':checked')) {
        var valor = $("input[name='control_torque_curvas_ck']:checked").val();
        $("#control_torque_curvas").val(valor);
    }
});

$('.tb2_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='control_curvas_ck']:radio").is(':checked')) {
        var valor = $("input[name='control_curvas_ck']:checked").val();
        $("#control_curvas").val(valor);
    }
});

$('.tb2_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='control_balance_remolque_ck']:radio").is(':checked')) {
        var valor = $("input[name='control_balance_remolque_ck']:checked").val();
        $("#control_balance_remolque").val(valor);
    }
});

$('.tb2_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='frenos_electronicos_ck']:radio").is(':checked')) {
        var valor = $("input[name='frenos_electronicos_ck']:checked").val();
        $("#frenos_electronicos").val(valor);
    }
});

$('.tb2_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb2_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='control_electronico_des_ck']:radio").is(':checked')) {
        var valor = $("input[name='control_electronico_des_ck']:checked").val();
        $("#control_electronico_des").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb3_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='cinturon_seguridad_ck']:radio").is(':checked')) {
        var valor = $("input[name='cinturon_seguridad_ck']:checked").val();
        $("#cinturon_seguridad").val(valor);
    }
});

$('.tb3_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='bolsas_aire_ck']:radio").is(':checked')) {
        var valor = $("input[name='bolsas_aire_ck']:checked").val();
        $("#bolsas_aire").val(valor);
    }
});

$('.tb3_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='chasis_carroceria_ck']:radio").is(':checked')) {
        var valor = $("input[name='chasis_carroceria_ck']:checked").val();
        $("#chasis_carroceria").val(valor);
    }
});

$('.tb3_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='carroseria_hidroforma_ck']:radio").is(':checked')) {
        var valor = $("input[name='carroseria_hidroforma_ck']:checked").val();
        $("#carroseria_hidroforma").val(valor);
    }
});

$('.tb3_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='cristales_ck']:radio").is(':checked')) {
        var valor = $("input[name='cristales_ck']:checked").val();
        $("#cristales").val(valor);
    }
});

$('.tb3_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='cabeceras_ck']:radio").is(':checked')) {
        var valor = $("input[name='cabeceras_ck']:checked").val();
        $("#cabeceras").val(valor);
    }
});

$('.tb3_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_alerta_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_alerta_ck']:checked").val();
        $("#sistema_alerta").val(valor);
    }
});

$('.tb3_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='teclado_puerta_ck']:radio").is(':checked')) {
        var valor = $("input[name='teclado_puerta_ck']:checked").val();
        $("#teclado_puerta").val(valor);
    }
});

$('.tb3_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='alarma_volumetrica_ck']:radio").is(':checked')) {
        var valor = $("input[name='alarma_volumetrica_ck']:checked").val();
        $("#alarma_volumetrica").val(valor);
    }
});

$('.tb3_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='alarma_perimetral_ck']:radio").is(':checked')) {
        var valor = $("input[name='alarma_perimetral_ck']:checked").val();
        $("#alarma_perimetral").val(valor);
    }
});

$('.tb3_ck11').change(function(){
    if($(this).is(':checked')){
        $('.tb3_ck11').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='mikey_ck']:radio").is(':checked')) {
        var valor = $("input[name='mikey_ck']:checked").val();
        $("#mikey").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb4_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='accionamiento_manual_ck']:radio").is(':checked')) {
        var valor = $("input[name='accionamiento_manual_ck']:checked").val();
        $("#accionamiento_manual").val(valor);
    }
});

$('.tb4_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='accionamiento_electrico_ck']:radio").is(':checked')) {
        var valor = $("input[name='accionamiento_electrico_ck']:checked").val();
        $("#accionamiento_electrico").val(valor);
    }
});

$('.tb4_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='asiento_delantero_asaje_ck']:radio").is(':checked')) {
        var valor = $("input[name='asiento_delantero_asaje_ck']:checked").val();
        $("#asiento_delantero_asaje").val(valor);
    }
});

$('.tb4_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='asiento_calefaccion_ck']:radio").is(':checked')) {
        var valor = $("input[name='asiento_calefaccion_ck']:checked").val();
        $("#asiento_calefaccion").val(valor);
    }
});

$('.tb4_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='easy_power_fold_ck']:radio").is(':checked')) {
        var valor = $("input[name='easy_power_fold_ck']:checked").val();
        $("#easy_power_fold").val(valor);
    }
});

$('.tb4_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb4_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='memoria_asientos_ck']:radio").is(':checked')) {
        var valor = $("input[name='memoria_asientos_ck']:checked").val();
        $("#memoria_asientos").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb5_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='iluminacion_ambiental_ck']:radio").is(':checked')) {
        var valor = $("input[name='iluminacion_ambiental_ck']:checked").val();
        $("#iluminacion_ambiental").val(valor);
    }
});

$('.tb5_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_iluminacion_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_iluminacion_ck']:checked").val();
        $("#sistema_iluminacion").val(valor);
    }
});

$('.tb5_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='faros_bixenon_ck']:radio").is(':checked')) {
        var valor = $("input[name='faros_bixenon_ck']:checked").val();
        $("#faros_bixenon").val(valor);
    }
});

$('.tb5_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='hid_ck']:radio").is(':checked')) {
        var valor = $("input[name='hid_ck']:checked").val();
        $("#hid").val(valor);
    }
});

$('.tb5_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb5_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='led_ck']:radio").is(':checked')) {
        var valor = $("input[name='led_ck']:checked").val();
        $("#led").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb6_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='peps_ck']:radio").is(':checked')) {
        var valor = $("input[name='peps_ck']:checked").val();
        $("#peps").val(valor);
    }
});

$('.tb6_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='apertura_cajuela_ck']:radio").is(':checked')) {
        var valor = $("input[name='apertura_cajuela_ck']:checked").val();
        $("#apertura_cajuela").val(valor);
    }
});

$('.tb6_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='encendido_remoto_ck']:radio").is(':checked')) {
        var valor = $("input[name='encendido_remoto_ck']:checked").val();
        $("#encendido_remoto").val(valor);
    }
});

$('.tb6_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='techo_panoramico_ck']:radio").is(':checked')) {
        var valor = $("input[name='techo_panoramico_ck']:checked").val();
        $("#techo_panoramico").val(valor);
    }
});

$('.tb6_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='aire_acondicionado_ck']:radio").is(':checked')) {
        var valor = $("input[name='aire_acondicionado_ck']:checked").val();
        $("#aire_acondicionado").val(valor);
    }
});

$('.tb6_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='eatc_ck']:radio").is(':checked')) {
        var valor = $("input[name='eatc_ck']:checked").val();
        $("#eatc").val(valor);
    }
});

$('.tb6_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='consola_enfriamiento_ck']:radio").is(':checked')) {
        var valor = $("input[name='consola_enfriamiento_ck']:checked").val();
        $("#consola_enfriamiento").val(valor);
    }
});

$('.tb6_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='volante_ajuste_altura_ck']:radio").is(':checked')) {
        var valor = $("input[name='volante_ajuste_altura_ck']:checked").val();
        $("#volante_ajuste_altura").val(valor);
    }
});

$('.tb6_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='volante_calefactado_ck']:radio").is(':checked')) {
        var valor = $("input[name='volante_calefactado_ck']:checked").val();
        $("#volante_calefactado").val(valor);
    }
});

$('.tb6_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='estribos_electricos_ck']:radio").is(':checked')) {
        var valor = $("input[name='estribos_electricos_ck']:checked").val();
        $("#estribos_electricos").val(valor);
    }
});

$('.tb6_ck11').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck11').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='apertura_caja_carga_ck']:radio").is(':checked')) {
        var valor = $("input[name='apertura_caja_carga_ck']:checked").val();
        $("#apertura_caja_carga").val(valor);
    }
});

$('.tb6_ck12').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck12').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='limpiaparabrisas_ck']:radio").is(':checked')) {
        var valor = $("input[name='limpiaparabrisas_ck']:checked").val();
        $("#limpiaparabrisas").val(valor);
    }
});

$('.tb6_ck13').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck13').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='espejo_electrico_ck']:radio").is(':checked')) {
        var valor = $("input[name='espejo_electrico_ck']:checked").val();
        $("#espejo_electrico").val(valor);
    }
});

$('.tb6_ck14').change(function(){
    if($(this).is(':checked')){
        $('.tb6_ck14').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='espejo_abatible_ck']:radio").is(':checked')) {
        var valor = $("input[name='espejo_abatible_ck']:checked").val();
        $("#espejo_abatible").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb7_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='conectividad_ck']:radio").is(':checked')) {
        var valor = $("input[name='conectividad_ck']:checked").val();
        $("#conectividad").val(valor);
    }
});

$('.tb7_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sync_ck']:radio").is(':checked')) {
        var valor = $("input[name='sync_ck']:checked").val();
        $("#sync").val(valor);
    }
});

$('.tb7_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='myFord_touch_ck']:radio").is(':checked')) {
        var valor = $("input[name='myFord_touch_ck']:checked").val();
        $("#myFord_touch").val(valor);
    }
});

$('.tb7_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='myFord_touch_navigation_ck']:radio").is(':checked')) {
        var valor = $("input[name='myFord_touch_navigation_ck']:checked").val();
        $("#myFord_touch_navigation").val(valor);
    }
});

$('.tb7_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sync_fordpass_ck']:radio").is(':checked')) {
        var valor = $("input[name='sync_fordpass_ck']:checked").val();
        $("#sync_fordpass").val(valor);
    }
});

$('.tb7_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='apple_carplay_ck']:radio").is(':checked')) {
        var valor = $("input[name='apple_carplay_ck']:checked").val();
        $("#apple_carplay").val(valor);
    }
});

$('.tb7_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='android_auto_ck']:radio").is(':checked')) {
        var valor = $("input[name='android_auto_ck']:checked").val();
        $("#android_auto").val(valor);
    }
});

$('.tb7_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='consola_smartphone_ck']:radio").is(':checked')) {
        var valor = $("input[name='consola_smartphone_ck']:checked").val();
        $("#consola_smartphone").val(valor);
    }
});

$('.tb7_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='audio_shaker_pro_ck']:radio").is(':checked')) {
        var valor = $("input[name='audio_shaker_pro_ck']:checked").val();
        $("#audio_shaker_pro").val(valor);
    }
});

$('.tb7_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='radio_hd_ck']:radio").is(':checked')) {
        var valor = $("input[name='radio_hd_ck']:checked").val();
        $("#radio_hd").val(valor);
    }
});

$('.tb7_ck11').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck11').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='equipo_sony_ck']:radio").is(':checked')) {
        var valor = $("input[name='equipo_sony_ck']:checked").val();
        $("#equipo_sony").val(valor);
    }
});

$('.tb7_ck12').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck12').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='puertos_usb_ck']:radio").is(':checked')) {
        var valor = $("input[name='puertos_usb_ck']:checked").val();
        $("#puertos_usb").val(valor);
    }
});

$('.tb7_ck13').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck13').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='bluetooh_ck']:radio").is(':checked')) {
        var valor = $("input[name='bluetooh_ck']:checked").val();
        $("#bluetooh").val(valor);
    }
});

$('.tb7_ck14').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck14').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='wifi_ck']:radio").is(':checked')) {
        var valor = $("input[name='wifi_ck']:checked").val();
        $("#wifi").val(valor);
    }
});

$('.tb7_ck15').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck15').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='bluetooh_ck']:radio").is(':checked')) {
        var valor = $("input[name='bluetooh_ck']:checked").val();
        $("#bluetooh").val(valor);
    }
});

$('.tb7_ck16').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck16').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='inversor_corriente_ck']:radio").is(':checked')) {
        var valor = $("input[name='inversor_corriente_ck']:checked").val();
        $("#inversor_corriente").val(valor);
    }
});

$('.tb7_ck17').change(function(){
    if($(this).is(':checked')){
        $('.tb7_ck17').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='espejo_electrocromatico_ck']:radio").is(':checked')) {
        var valor = $("input[name='espejo_electrocromatico_ck']:checked").val();
        $("#espejo_electrocromatico").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb8_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='motores_ck']:radio").is(':checked')) {
        var valor = $("input[name='motores_ck']:checked").val();
        $("#motores").val(valor);
    }
});

$('.tb8_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='clutch_ck']:radio").is(':checked')) {
        var valor = $("input[name='clutch_ck']:checked").val();
        $("#clutch").val(valor);
    }
});

$('.tb8_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='caja_cambios_ck']:radio").is(':checked')) {
        var valor = $("input[name='caja_cambios_ck']:checked").val();
        $("#caja_cambios").val(valor);
    }
});

$('.tb8_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='arbol_transmisiones_ck']:radio").is(':checked')) {
        var valor = $("input[name='arbol_transmisiones_ck']:checked").val();
        $("#arbol_transmisiones").val(valor);
    }
});

$('.tb8_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='diferencial_ck']:radio").is(':checked')) {
        var valor = $("input[name='diferencial_ck']:checked").val();
        $("#diferencial").val(valor);
    }
});

$('.tb8_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='diferencial_traccion_ck']:radio").is(':checked')) {
        var valor = $("input[name='diferencial_traccion_ck']:checked").val();
        $("#diferencial_traccion").val(valor);
    }
});

$('.tb8_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='caja_reductora_ck']:radio").is(':checked')) {
        var valor = $("input[name='caja_reductora_ck']:checked").val();
        $("#caja_reductora").val(valor);
    }
});

$('.tb8_ck8').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck8').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='caja_transferencia_ck']:radio").is(':checked')) {
        var valor = $("input[name='caja_transferencia_ck']:checked").val();
        $("#caja_transferencia").val(valor);
    }
});

$('.tb8_ck9').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck9').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_dinamico_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_dinamico_ck']:checked").val();
        $("#sistema_dinamico").val(valor);
    }
});

$('.tb8_ck10').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck10').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='direccion_ck']:radio").is(':checked')) {
        var valor = $("input[name='direccion_ck']:checked").val();
        $("#direccion").val(valor);
    }
});

$('.tb8_ck11').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck11').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='suspension_ck']:radio").is(':checked')) {
        var valor = $("input[name='suspension_ck']:checked").val();
        $("#suspension").val(valor);
    }
});

$('.tb8_ck12').change(function(){
    if($(this).is(':checked')){
        $('.tb8_ck12').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='toma_poder_ck']:radio").is(':checked')) {
        var valor = $("input[name='toma_poder_ck']:checked").val();
        $("#toma_poder").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb9_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb9_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='ivct_ck']:radio").is(':checked')) {
        var valor = $("input[name='ivct_ck']:checked").val();
        $("#ivct").val(valor);
    }
});

$('.tb9_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb9_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='tivct_ck']:radio").is(':checked')) {
        var valor = $("input[name='tivct_ck']:checked").val();
        $("#tivct").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb10_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb10_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='vehiculo_hibrido_ck']:radio").is(':checked')) {
        var valor = $("input[name='vehiculo_hibrido_ck']:checked").val();
        $("#vehiculo_hibrido").val(valor);
    }
});

$('.tb10_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb10_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='motor_gasolina_ck']:radio").is(':checked')) {
        var valor = $("input[name='motor_gasolina_ck']:checked").val();
        $("#motor_gasolina").val(valor);
    }
});

$('.tb10_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb10_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='tips_manejo_ck']:radio").is(':checked')) {
        var valor = $("input[name='tips_manejo_ck']:checked").val();
        $("#tips_manejo").val(valor);
    }
});

$('.tb10_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb10_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='transmision_ecvt_ck']:radio").is(':checked')) {
        var valor = $("input[name='transmision_ecvt_ck']:checked").val();
        $("#transmision_ecvt").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb11_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb11_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='easy_fuel_ck']:radio").is(':checked')) {
        var valor = $("input[name='easy_fuel_ck']:checked").val();
        $("#easy_fuel").val(valor);
    }
});

$('.tb11_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb11_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='track_apps_ck']:radio").is(':checked')) {
        var valor = $("input[name='track_apps_ck']:checked").val();
        $("#track_apps").val(valor);
    }
});

$('.tb11_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb11_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='auto_start_stop_ck']:radio").is(':checked')) {
        var valor = $("input[name='auto_start_stop_ck']:checked").val();
        $("#auto_start_stop").val(valor);
    }
});

$('.tb11_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb11_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='sistema_ctr_terreno_ck']:radio").is(':checked')) {
        var valor = $("input[name='sistema_ctr_terreno_ck']:checked").val();
        $("#sistema_ctr_terreno").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb12_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb12_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='carta_factura_ck']:radio").is(':checked')) {
        var valor = $("input[name='carta_factura_ck']:checked").val();
        $("#carta_factura").val(valor);
    }
});

$('.tb12_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb12_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='poliza_garantia_ck']:radio").is(':checked')) {
        var valor = $("input[name='poliza_garantia_ck']:checked").val();
        $("#poliza_garantia").val(valor);
    }
});

$('.tb12_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb12_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='poliza_seguro_ck']:radio").is(':checked')) {
        var valor = $("input[name='poliza_seguro_ck']:checked").val();
        $("#poliza_seguro").val(valor);
    }
});

$('.tb12_ck4').change(function(){
    if($(this).is(':checked')){
        $('.tb12_ck4').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='tenencia_ck']:radio").is(':checked')) {
        var valor = $("input[name='tenencia_ck']:checked").val();
        $("#tenencia").val(valor);
    }
});

$('.tb12_ck5').change(function(){
    if($(this).is(':checked')){
        $('.tb12_ck5').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='herramientas_ck']:radio").is(':checked')) {
        var valor = $("input[name='herramientas_ck']:checked").val();
        $("#herramientas").val(valor);
    }
});

///////////////////////////////////////////////////////////////////////////////////////////

$('.tb13_ck1').change(function(){
    if($(this).is(':checked')){
        $('.tb13_ck1').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='explico_asistencia_ford_ck']:radio").is(':checked')) {
        var valor = $("input[name='explico_asistencia_ford_ck']:checked").val();
        $("#explico_asistencia_ford").val(valor);
    }
});

$('.tb13_ck2').change(function(){
    if($(this).is(':checked')){
        $('.tb13_ck2').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='explico_ford_protect_ck']:radio").is(':checked')) {
        var valor = $("input[name='explico_ford_protect_ck']:checked").val();
        $("#explico_ford_protect").val(valor);
    }
});

$('.tb13_ck3').change(function(){
    if($(this).is(':checked')){
        $('.tb13_ck3').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='explico_ford_protect_ck']:radio").is(':checked')) {
        var valor = $("input[name='explico_ford_protect_ck']:checked").val();
        $("#explico_ford_protect").val(valor);
    }
});

$('.accesorios_personalizar').change(function(){
    if($(this).is(':checked')){
        $('.accesorios_personalizar').not(this).prop('checked', false);
    }
    
    //Mostramos el valor en un input correspondiente
    if ($("input[name='accesorios_personalizar_ck']:radio").is(':checked')) {
        var valor = $("input[name='accesorios_personalizar_ck']:checked").val();
        $("#accesorios_personalizar").val(valor);
    }
});

$('.tarjeta_sd').change(function(){
    if($(this).is(':checked')){
        $('.tarjeta_sd').not(this).prop('checked', false);
    }
    
    //Mostramos el valor en un input correspondiente
    if ($("input[name='tarjeta_sd_ck']:radio").is(':checked')) {
        var valor = $("input[name='tarjeta_sd_ck']:checked").val();
        $("#tarjeta_sd").val(valor);
    }
});


$('.prueba_manejo').change(function(){
    if($(this).is(':checked')){
        $('.prueba_manejo').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='prueba_manejo_ck']:radio").is(':checked')) {
        var valor = $("input[name='prueba_manejo_ck']:checked").val();
        $("#prueba_manejo").val(valor);
    }
});

$('.tb13_ck6').change(function(){
    if($(this).is(':checked')){
        $('.tb13_ck6').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='informacion_mantenimiento_ck']:radio").is(':checked')) {
        var valor = $("input[name='informacion_mantenimiento_ck']:checked").val();
        $("#informacion_mantenimiento").val(valor);
    }
});

$('.tb13_ck7').change(function(){
    if($(this).is(':checked')){
        $('.tb13_ck7').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input correspondiente
    if ($("input[name='asesor_servicio_ck']:radio").is(':checked')) {
        var valor = $("input[name='asesor_servicio_ck']:checked").val();
        $("#asesor_servicio").val(valor);
    }
});

// $('.tb13_ck1').change(function(){
//     if($(this).is(':checked')){
//         $('.tb13_ck1').not(this).prop('checked', false);
//     }
//     //Mostramos el valor en un input correspondiente
//     if ($("input[name='explico_asistencia_ford_ck']:radio").is(':checked')) {
//         var valor = $("input[name='explico_asistencia_ford_ck']:checked").val();
//         $("#explico_asistencia_ford").val(valor);
//     }
// });