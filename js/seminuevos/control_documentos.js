$('.doc10_fisica').change(function(){
    if($(this).is(':checked')){
        $('.doc10_fisica').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='birlo']:radio").is(':checked')) {
        var valor = $("input[name='birlo']:checked").val();
        $("input[name='doc10_fisica']").val(valor);
    }
});

$('.doc11_fisica').change(function(){
    if($(this).is(':checked')){
        $('.doc11_fisica').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='control']:radio").is(':checked')) {
        var valor = $("input[name='control']:checked").val();
        $("input[name='doc11_fisica']").val(valor);
    }
});

$('.doc12_fisica').change(function(){
    if($(this).is(':checked')){
        $('.doc12_fisica').not(this).prop('checked', false);
    }
    //Mostramos el valor en un input
    if ($("input[name='audifonos']:radio").is(':checked')) {
        var valor = $("input[name='audifonos']:checked").val();
        $("input[name='doc12_fisica']").val(valor);
    }
});